"use strict";

/**
 * Модальное окно в стиле bootstrap. Работает как написано здесь https://getbootstrap.com/docs/4.0/components/modal/
 */
Vue.component('modal', {
    template: `
        <div class="modal show" :id="modal_id" :style="display">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <slot name="body"></slot>
                    </div>
                </div>
            </div>
        </div>
`,
    props: {
        modal_id: {
            type: String,
            default: ''
        },
        show: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        display: function() {
            return 'display:' + (this.show ? 'block;' : 'none;');
        }
    },
    mounted: function() {
        this.modal_id = 'modal_' + Math.random();
    }
});