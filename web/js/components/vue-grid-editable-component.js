"use strict";

/**
 * Компонент таблиц c редактируемыми ячейками
 */
Vue.component('grid-editable', {
    mixins: [stringMixins],
    template: `
    <table :class="[responsive != false ? 'table-responsive' : '', 'table', 'table-sm', 'small', 'border']">
        <thead style="cursor: pointer">
            <tr v-if="typeof border_columns.left !== 'undefined'" class="bg-light text-nowrap">
                <th v-if="checkboxes != false"></th>
                <th v-if="hidden_rows != false"></th>
                <th v-for="(title, index) in heads" :class="getColumnClass(index)">
                    <span v-if="border_columns.left && border_columns.left.hasOwnProperty(index)">{{border_columns.left[index]}}</span>
                </th> 
            </tr>
            <tr class="bg-light text-nowrap">
                <th v-if="checkboxes != false"><input v-on:click="toggleAll" v-model="checked_all" type="checkbox"></th>
                <th v-if="hidden_rows != false"></th>
                <th v-for="(title, index) in heads" v-on:click="sortBy(index)" :class="getColumnClass(index)">
                    {{title}}
                    <i v-if="sortable != false && !!title" :class="sortOrders.get(index) > 0 ? 'fa fa-sort-up' : 'fa fa-sort-down'"></i>
                 </th>
            </tr>
            <tr v-if="editable_rows.length > 0" class="bg-light text-nowrap">
                <th v-if="checkboxes != false"></th>
                <th v-if="hidden_rows != false"></th>
                <th v-for="(title, index) in heads" :class="getColumnClass(index)">
                    
                    <input
                        v-on:keyup.enter="updateCol(index, $event)"
                        v-show="isEditableCell(index)"  
                        style="width: 50px; height: 22px;"/>
                 </th>
            </tr>
            
        </thead>
        <tbody>
            <template v-for="(row, index) in rows">
                <tr v-show="isShown(index) == true" :class="{'bg-light text-nowrap': index % 2 != 0}" :style="getRowStyle(row)" v-on:dblclick="ShowHiddenRow(row, hidden_rows)">
                    <td v-if="checkboxes != false">
                        <input type="checkbox" v-model="row.checked" :disabled="row.disabled == true" :name = "'ch_' + index">
                    </td> 
                    <td v-if="hidden_rows != false">
                         <i :style="hidden_rows != false && row.additional_params.shown_hidden_row ? 'width: 5px;' : 'width: 5px; color: #888888;'" :class="hidden_rows != false && row.additional_params.shown_hidden_row ? 'fa fa-caret-down' : 'fa fa-caret-right'" v-on:click="ShowHiddenRow(row, hidden_rows)"></i>
                    </td>
                    <slot name="rows" :row="row">
                        <td v-for="(cell, key) in row" v-if="showCell(key)" :class="getColumnClass(key)">
                            <span :style="isBoldCell(key) ? 'font-weight: bold;' : ''" v-show="!editable(index, key)" v-on:dblclick="!hasError(index, key) ? cellDblClick(index, key, cell) : ''">
                                {{renderCell(index, key, cell)}}                                
                            </span>
                            <input type="text" data-toggle="tooltip" data-placement="right" data-original-title="" title="" style="min-width: 100px;"
                                v-show="editable(index, key)"
                                v-model="row[key]"
                                v-bind:data-original-title="getError(index,key)"
                                v-bind:class="getError(index,key) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'"    
                                v-on:keyup.enter="updateCell(index, key, cell)">
                        </td>
                    </slot>
                </tr>
                <tr v-if="isShown(index) == true && hidden_rows != false && row.additional_params.shown_hidden_row" :class="{'bg-light text-nowrap': index % 2 != 0}">
                    <td colspan="1"></td>
                    <td colspan="100">
                        <span v-for="(field, index_hidden_row) in row.additional_params.hidden_row" :style="getStyle()">
                            <span class="field_title_hidden_row">{{field.title}}:</span> 
                            <span class="field_value_hidden_row">{{field.field_value}}</span>
                        </span>                
                    </td>
                </tr>
            </template>
        </tbody>
    </table>
`,
    props: {
        heads: [],
        rows: [],
        border_columns: { // объект содержит order колонок вида Фрахт => ['left' => 11, 'right' => 14]
            type: Object,
            default: {}
        },
        filter_key: null,
        bold_rows: {
            type: Array,
            default: []
        },
        editable_rows: {
            type: Array,
            default: []
        },
        responsive: {
            type: Boolean,
            default: false
        },
        sortable: {
            type: Boolean,
            default: false
        },
        hidden_rows: {
            type: Boolean,
            default: false
        },
        checkboxes: {
            type: Boolean,
            default: false
        },
        edite: {
            type: Boolean,
            default: false
        },
        dbl_click_editable: {
            type: Boolean,
            default: false
        },
        error_editable_cell: {
            type: Array,
            default: []
        },
    },
    data: function() {
        return {
            sortKey: '',
            sortOrdersData: new Map(),
            checked_all: false,
            edite_options: {}
        }
    },
    computed: {
        sortOrders: function() {
            if(this.sortOrdersData.size == 0) {
                for(let key in this.heads) {
                    this.sortOrdersData.set(key, 1);
                }
            }
            return this.sortOrdersData;
        },
    },
    methods: {
        /**
         * Рендерит ячейку либо с разбивкой по блокам если в формате JSON
         * либо её содержимое
         *
         * @param index
         * @param key
         * @param value
         * @returns {*}
         */
        renderCell: function(index, key, value) {
            if (typeof  value !== 'object') {
                try {
                    value = JSON.parse(value)
                } catch(e) {
                    return value;
                }
            };

            const result = this.getValuesWithCodes(value);

            if (result.length > 0) {
                return result;
            }

            return value;
        },

        /**
         * @param values
         * @returns {Array}
         * [value1 with code, value2 with code,]
         */
        getValuesWithCodes: function(values) {
            let result = [];
            let math_sign = ' + ';
            for (let currency_sign in values) {
                if (result.length === 0) {
                    result = values[currency_sign] + ' ' + currency_sign;
                    continue;
                }
                math_sign = values[currency_sign] < 0 ? ' - ' : ' + ';
                result = result + math_sign + Math.abs(values[currency_sign]) + this.decodeHTML('&nbsp') + currency_sign;
            }

            return result;
        },

        getError: function(number_row, order_cell_in_row) {
            let error = '';
            if (this.hasError(number_row, order_cell_in_row)) {
                error = this.error_editable_cell[number_row][order_cell_in_row];
            }

            return error;
        },
        hasError:  function(number_row, order_cell_in_row) {
            return this.error_editable_cell[number_row] && this.error_editable_cell[number_row][order_cell_in_row]
        },
        getStyle: function() {
            return "font-family: Roboto; font-size: 12px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: 1.83; letter-spacing: normal; text-align: left;"
        },
        ShowHiddenRow: function(row, has_hidden_rows) {
            if(has_hidden_rows){
                if(row.additional_params.shown_hidden_row) {
                    this.$set(row.additional_params, 'shown_hidden_row', false);
                    return;
                }
                this.$set(row.additional_params, 'shown_hidden_row', true);
            }
            return;
        },
        toggleAll: function() {
            this.checked_all = !this.checked_all;
            this.rows.map(item => item['checked'] = this.checked_all);
        },
        sortRows() {
            if (this.sortable) {
                let order = this.sortOrders.get(this.sortKey) || 1;
                if (this.sortKey) {
                    this.rows = this.rows.slice().sort((a, b) => {
                        a = a[this.sortKey];
                        b = b[this.sortKey];
                        if (isNaN(a) || isNaN(b)) {  // Если один из сортируемых элементов не является числом, то сортируем как строки
                            return (a === b ? 0 : a > b ? 1 : -1) * order; // если сравниваемые значения равны return 0 - не нужно сортировать, return 1 - сортировать по возрастанию return - 1 - сортировать по убыванию
                        }
                        return order === 1 ? (a - b) : (b - a);   // Если строка является числом, то сортируем как числа
                    });
                    this.$emit('sortby', this.rows);
                }
            }
            if (this.filter_key) {
                this.rows = this.rows.filter(item => {
                    return JSON.stringify(Object.values(item)).indexOf(this.filter_key) !== -1;
                });
            }
        },
        sortBy: function(key) {
            this.sortKey = null;
            this.sortKey = key;
            let order = this.sortOrders.get(this.sortKey);
            this.sortOrders.set(this.sortKey, order * -1);
            this.sortRows();
        },
        getColumnClass: function(index) {
            let css_class = [];
            if (typeof this.border_columns === "undefined") {
                return '';
            }

            index = parseInt(index);

            if (this.border_columns.left && this.border_columns.left.hasOwnProperty(index)) {
                css_class.push('bordered-l');
            } else if (this.border_columns.right && this.border_columns.right.hasOwnProperty(index)) {
                css_class.push('bordered-r');
            }

            if (this.sortKey == index) {
                css_class.push('column-active');
            }

            return css_class.join(' ');
        },
        isShown: function (index) {
            return this.rows[index]['shown'];
        },
        showCell: function (key) {
            return key !== 'checked' && key !== 'shown' && key !== 'additional_params' && key !== 'disabled';
        },
        getRowStyle: function(row) {
            return row.disabled ? {'color': 'gray'} : '';
        },
        updateCell: function(index, key, value) {
            if (!this.hasError(index, key) && this.edite_options[index] && this.edite_options[index][key]) {
                this.edite_options[index][key].new_value = false;
            }
            this.$emit('update_cell', index, key, value);
        },
        updateCol: function(key, $event) {
            let self = this;
            let all_rows = [];
            this.rows.forEach(function (item, index) {
                let value = $event.target.value;
                item[key] = value;
                all_rows.push({index: index, key: key, value: value});
            });

            if (all_rows.length > 0) {
                self.$emit('update_col', all_rows);
            }

            $event.target.value = '';

        },
        cellDblClick: function(index, key, value) {
            if(this.dbl_click_editable && this.isEditableCell(key)) {
                if(!this.edite_options[index]) {
                    this.$set(this.edite_options, index, {});
                }
                this.$set(this.edite_options[index], key, { new_value: true, old_value: value });
            }
        },
        editable: function(index, key) {
            return ((this.edite || (this.dbl_click_editable && this.edite_options[index] && this.edite_options[index][key] && this.edite_options[index][key].new_value))
                && this.isEditableCell(key) || (this.hasError(index, key)));
        },
        isEditableCell: function(key) {
            if(this.editable_rows.length === 0) {
                return false;
            }
            return (this.editable_rows.indexOf(key) !== -1 || this.editable_rows.indexOf(parseInt(key)) !== -1);
        },
        isBoldCell: function(key) {
            if(this.bold_rows.length === 0) {
                return false;
            }
            return (this.bold_rows.indexOf(key) !== -1 || this.bold_rows.indexOf(parseInt(key)) !== -1);
        }
    },
    mounted: function() {
        for(let key in this.rows) {
            this.rows[key]['checked'] = false;
        }
    }

});
