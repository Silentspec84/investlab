"use strict";

/**
 * Компонент таблиц без возможности редактирования ячеек.
 * @see grid-editable
 */
Vue.component('grid', {
    template: `
    <table id="grid_table" :class="[responsive !== false ? 'table-responsive' : '', 'table', 'table-sm', 'border', 'table-hover']">
        <thead :class="getHeaderClass()" style="cursor: pointer">
        <tr v-if="typeof border_columns.left !== 'undefined'" class="text-nowrap block-line">
            <th v-if="checkboxes !== false"></th>
            <th v-if="hidden_rows !== false"></th>
            <th v-show="isShowColumn(index)" v-for="(title, index) in heads" :class="getBlockNameByNumberColumn(index) ? 'hamburger ' + getColumnClass(index) : getColumnClass(index)">
                <span  v-if="getBlockNameByNumberColumn(index)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-light btn-sm oi oi-list"></span>
                <ul aria-labelledby="dropdownMenuButton" class="dropdown-menu">
                    <li v-for="(col_order, index) in orders_by_blocks[getBlockNameByNumberColumn(index)]" class="ml-2" :style="index === 0 ? 'color: gray' : ''">
                        <div class="checkbox checkbox-primary" @click.stop>
                            <input :id="getColumnCheckboxId(col_order)" :value="col_order" v-model="cols_checked" :disabled="index === 0" :style="index > 0 ? 'cursor: pointer' : ''" type="checkbox" @change="saveColumnState()">
                            <label :for="getColumnCheckboxId(col_order)">{{heads[col_order]}}</label>
                        </div>
                    </li>
                </ul>
            </th>
        </tr>
        <tr :class="[typeof border_columns.left !== 'undefined' ? 'header-line' : 'block-line', 'text-nowrap']">
            <th v-if="checkboxes !== false">
                <div class="checkbox checkbox-primary checkbox-single">
                    <input id="toggle_all" @click="toggleAll" v-model="checked_all" type="checkbox">
                    <label for="toggle_all"></label>
                </div>
            </th>
            <th v-if="hidden_rows != false" class="empty-text"></th>
            <th v-if="isColumnCanBeSorted(index)" v-show="isShowColumn(index)" v-for="(title, index) in heads" v-on:click="sortBy(index)" :class="getColumnClass(index)">
                {{title}}
                <i v-if="sortable != false && !!title" :class="sortOrders.get(index) > 0 ? 'fa fa-sort-up' : 'fa fa-sort-down'"></i>
            </th>  
            <th v-if="isColumnCanBeSorted(index) === false" v-show="isShowColumn(index)" v-for="(title, index) in heads" :class="getColumnClass(index)">
                {{title}}
            </th>
        </tr>
        </thead>
        <tbody>
        <template v-for="(row, index) in getRows()">
            <tr 
                v-show="row['shown'] === true" 
                :data-hint="getHint(row)"
                :class="getRowClass(row)" 
                :style="getRowStyle(row)" 
                v-on:dblclick="ShowHiddenRow(row, hidden_rows)" 
                :key="index"
            >
                <td v-if="checkboxes !== false">
                    <div class="checkbox checkbox-primary checkbox-single">
                        <input type="checkbox" v-model="row.checked" :disabled="row.disabled === true"  :name = "'ch_' + index" :id="'ch_' + index">
                        <label :for="'ch_' + index"></label>
                    </div>
                </td>
                <td v-if="hidden_rows !== false">
                    <i :style="hidden_rows !== false && row.additional_params.shown_hidden_row ? 'width: 5px;' : 'width: 5px; color: #888888;'" :class="hidden_rows !== false && row.additional_params.shown_hidden_row ? 'fa fa-caret-down' : 'fa fa-caret-right'" v-on:click="ShowHiddenRow(row, hidden_rows)"></i>
                </td>
                <slot name="rows" :row="row">
                    <td 
                        v-show="isShowColumn(key)" 
                        v-for="(cell, key) in row" 
                        v-if="showCell(key)" 
                        :class="getColumnClass(key)"
                    >
                            <span :style="isBoldCell(key) ? 'font-weight: bold;' : ''">
                                {{renderCell(index, key, cell)}}
                            </span>
                    </td>
                </slot>
            </tr>
            <tr v-if="row['shown'] === true && hidden_rows !== false && row.additional_params.shown_hidden_row" :class="{'text-nowrap': index % 2 !== 0}">
                <td colspan="1"></td>
                <td colspan="100">
                        <span v-for="(field, index_hidden_row) in row.additional_params.hidden_row" :style="getStyle()">
                            <span class="field_title_hidden_row">{{field.title}}:</span>
                            <span class="field_value_hidden_row">{{field.field_value}}</span>
                        </span>
                </td>
            </tr>
        </template>
        </tbody>
    </table>
`,
    props: {
        heads: [],
        rows: [],
        border_columns: { // объект содержит order колонок вида Фрахт => ['left' => 11, 'right' => 14]
            type: Object,
            default: {}
        },
        orders_by_blocks: { // объект содержит порядковые номера столбцов по блокам: {field_keys: [], fraht:[], zhd:[], auto:[], itogo:[]}
            type: Object,
            default: {}
        },
        filter_key: null,
        bold_rows: {
            type: Array,
            default: []
        },
        responsive: {
            type: Boolean,
            default: false
        },
        sortable: {
            type: Boolean,
            default: false
        },
        columns_not_to_sort: {
            type: Object,
            default: {}
        },
        custom_values_to_sorted_columns: {
            type: Object,
            default: {}
        },
        hidden_rows: {
            type: Boolean,
            default: false
        },
        checkboxes: {
            type: Boolean,
            default: false
        },
        header_fixed: {
            type: Boolean,
            default: false
        },
        use_paginator: {
            type: Boolean,
            default: false
        },
        /** должно ли появлятся меню-сэндвич, позволяющее у каждого блока скрывать столбцы */
        are_hidden_columns: {
            type: Boolean,
            default: false
        },
        cols_checked: {
            type: Array,
            default: []
        },
        show_hints: {
            type: Boolean,
            default: false
        }
    },
    data: function() {
        return {
            sortKey: '',
            sortOrdersData: new Map(),
            checked_all: false,
        }
    },
    computed: {
        sortOrders: function() {
            if(this.sortOrdersData.size === 0) {
                for(let key in this.heads) {
                    this.sortOrdersData.set(key, 1);
                }
            }
            return this.sortOrdersData;
        },
    },
    methods: {
        /**
         * Рендерит ячейку либо с разбивкой по блокам если в формате JSON
         * либо её содержимое
         * @param index
         * @param key
         * @param value
         * @returns {*}
         */
        renderCell: function(index, key, value) {
            if (typeof value === 'string') {
                try {
                    value = JSON.parse(value);
                } catch(e) {
                    return value;
                }
            }

            if (typeof value !== 'object') {
                return value;
            }

            const result = this.getValuesWithCodes(value);
            // приводим результат к строке. Если больше одного элемента в массиве
            // ставим между ними знак +
            if (result.length >= 1) {
                return result.join(" + ");
            }

            return '';
        },

        /**
         * @param values
         * @returns {Array}
         * [value1 with code, value2 with code,]
         */
        getValuesWithCodes: function(values) {
            let result = [];
            for (let code in values) {
                result.push(values[code] + ' ' + code);
            }

            return result;
        },
        getStyle: function() {
            return "font-family: Roboto; font-size: 12px; font-weight: normal; font-style: normal; font-stretch: normal; line-height: 1.83; letter-spacing: normal; text-align: left;"
        },
        ShowHiddenRow: function(row, has_hidden_rows) {
            if(has_hidden_rows){
                if(row.additional_params.shown_hidden_row) {
                    this.$set(row.additional_params, 'shown_hidden_row', false);
                    return;
                }
                this.$set(row.additional_params, 'shown_hidden_row', true);
            }
            return;
        },
        toggleAll: function() {
            this.checked_all = !this.checked_all;
            this.getRows().map(item => {
                if (item['shown']) {
                    item['checked'] = item['disabled'] !== true ? this.checked_all : item['checked'];
                }
            });
        },
        getRows: function() {
            let sorted_rows = this.rows;
            if (this.use_paginator) {
                return sorted_rows;
            }
            if (this.sortable && this.isColumnCanBeSorted(this.sortKey)) {
                let order = this.sortOrders.get(this.sortKey) || 1; // отвечает за порядок сортировки 1 - по возрастанию, -1 по убыванию
                if (this.sortKey) {
                    sorted_rows = sorted_rows.slice().sort((a, b) => {
                        let cell_val_1 = a[this.sortKey];
                        let cell_val_2 = b[this.sortKey];

                        if (this.isColumnHasCustomValuesForSort(this.sortKey)) {
                            a = this.custom_values_to_sorted_columns[this.sortKey][cell_val_1];
                            b = this.custom_values_to_sorted_columns[this.sortKey][cell_val_2];
                        } else {
                            a = cell_val_1;
                            b = cell_val_2;
                        }
                        if (isNaN(a) || isNaN(b)) {  // Если один из сортируемых элементов не является числом, то сортируем как строки
                            a = a === null ? '' : a.toLowerCase();
                            b = b === null ? '' : b.toLowerCase();
                            return (a === b ? 0 : a > b ? 1 : -1) * order; // если сравниваемые значения равны return 0 - не нужно сортировать, return 1 - сортировать по возрастанию return - 1 - сортировать по убыванию
                        }
                        return order === 1 ? (a - b) : (b - a);   // Если строка является числом, то сортируем как числа
                    });
                    this.$emit('sortby', sorted_rows);
                }
            }
            if (this.filter_key) {
                sorted_rows = sorted_rows.filter(item => {
                    return JSON.stringify(Object.values(item)).toUpperCase().indexOf(this.filter_key.toUpperCase()) !== -1;
                });
            }

            return sorted_rows;
        },
        /** Колонка будет сортироваться не по номинальному значению, а по связанному. Например: колонка содержит числовые константы, а сортироваться будет по их текстовым значениям по алфавиту*/
        isColumnHasCustomValuesForSort: function (col_name) {
            return this.custom_values_to_sorted_columns.hasOwnProperty(this.sortKey);
        },
        isColumnCanBeSorted: function (col_name) {
            return col_name && !this.columns_not_to_sort.hasOwnProperty(col_name);
        },
        sortBy: function(key) {
            this.sortKey = null;
            this.sortKey = key;
            let order = this.sortOrders.get(this.sortKey);
            this.sortOrders.set(this.sortKey, order * -1);
            if (this.use_paginator) {
                this.$emit('sortby_with_paginator', order, key);
            }

        },
        getColumnClass: function(index) {
            let css_class = [];
            if (typeof this.border_columns === "undefined") {
                return '';
            }

            index = parseInt(index);

            if (this.border_columns.left && this.border_columns.left.hasOwnProperty(index)) {
                css_class.push('bordered-l');
            } else if (this.border_columns.right && this.border_columns.right.hasOwnProperty(index)) {
                css_class.push('bordered-r');
            }

            if (this.sortKey == index) {
                css_class.push('column-active');
            }

            return css_class.join(' ');
        },
        getHeaderClass: function() {
            if (this.header_fixed) {
                return 'fixed';
            }

            return '';
        },
        showCell: function (key) {
            return key !== 'checked' && key !== 'shown' && key !== 'additional_params' && key !== 'disabled';
        },
        getRowStyle: function(row) {
            return row.disabled ? {'color': 'gray'} : '';
        },
        getRowClass(row) {
            let css_class = 'text-nowrap';
            if (!this.show_hints) {
                return css_class;
            }
            if (row['additional_params']['empty_time_on_terminals'] || row['additional_params']['empty_time_delivery_between_terminals']) {
                css_class += ' alert-warning has-hint';
            }

            return css_class;
        },
        getHint(row) {
            if (!this.show_hints) {
                return '';
            }
            let hint = '';
            if (row['additional_params']['empty_time_on_terminals']) {
                hint = `Не установлено время для терминалов: ${row['additional_params']['empty_time_on_terminals'].join(', ')}.`;
            }
            if (row['additional_params']['empty_time_delivery_between_terminals']) {
                let p2p_deliveries = row['additional_params']['empty_time_delivery_between_terminals'].map(el => {
                   return `${el.loading} - ${el.discharge}`;
                });
                hint += `Не установлено время для плеч: ${p2p_deliveries.join(', ')}.`;
            }

            return hint;
        },
        isBoldCell: function(key) {
            if(this.bold_rows.length === 0) {
                return false;
            }
            return (this.bold_rows.indexOf(key) !== -1 || this.bold_rows.indexOf(parseInt(key)) !== -1);
        },

        /**
         * Возвращает название блока по номеру колонки в загаловке таблицы
         * ВАЖНО! Если колонка является первым элементом блока, то вернет название блока, иначе false
         **/
        getBlockNameByNumberColumn: function (num_column) {
            for (let block_name in this.orders_by_blocks) {
                if (this.orders_by_blocks[block_name][0] && String(this.orders_by_blocks[block_name][0]) === String(num_column)) {
                    return block_name;
                }
            }
            return false;
        },

        getColumnCheckboxId: function (col_order) {
            return 'ch_col_toggle' + col_order;
        },

        isShowColumn: function (col_order) {
            if (!this.are_hidden_columns) {
                return true;
            }
            return this.cols_checked.indexOf(parseInt(col_order)) >= 0;
        },
        saveColumnState: function () {
            this.$emit('column_state_change', this.cols_checked);
        },


    },
    mounted: function() {

        /*if (this.checkboxes) {
            for(let key in this.rows) {
                this.rows[key]['checked'] = false;
            }
        }*/
    }
});

