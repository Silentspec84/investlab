"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#table",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            data_list: [],
            drop_link: '/table-api/delete',
            add_link: '/table-api/save',
            form: {},
            new_item_form: {},
            error_list: {}
        },
        components: {vuejsDatepicker},
        methods: {
            getInitials(row) {
                let initials = '';
                if (row.last_name) {
                    initials += row.last_name + ' ';
                }
                if (row.first_name) {
                    initials += row.first_name.charAt(0).toUpperCase() + '. ';
                }
                if (row.middle_name) {
                    initials += row.middle_name.charAt(0).toUpperCase() + '. ';
                }

                if (!initials) {
                    return "нет";
                }

                return initials;
            },
            getDate: function(date) {
                if (date) {
                    return moment(date).format('DD.MM.YYYY');
                } else {
                    return 'Нет';
                }
            },
            getDatetime: function(date) {
                if (date) {
                    return moment(date).format('DD.MM.YYYY HH:mm:ss');
                } else {
                    return 'Нет';
                }
            },
            getTimestamp: function(date) {
                return new Date(date).getTime();
            },
            getClass: function(field) {
                return 'form-control form-control-sm' + (this.getError(field) ? ' is-invalid' : '');
            },
            getErrorText: function(field) {
                if(this.error_list[field] === undefined) {
                    return '';
                }
                return this.error_list[field];
            },
            addNewItem: function() {
                this.data_list.push({
                    edit: true,
                    is_new: true,
                    data: {
                        title: '',
                        type: null,
                        date_actual_: null,
                        date_load: null,
                        date_upload_since: null,
                        date_upload_by: null,
                    }});
            },
            validate: function() {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['title', 'type'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    min: {
                        fields: ['title'],
                        min: 2,
                    },
                    max: {
                        fields: ['title'],
                        max: {title: 50},
                    }
                };

                return this.validateForm(config, this.new_item_form);
            },
            startEdit: function(item, index) {
                item.old_data = Object.assign({}, item.data);
                item.edit = true;
            },
            cancelEdit: function(item, index) {
                item.edit = false;
                this.error_list = {};
                if(item.hasOwnProperty('old_data')) {
                    item.data = item.old_data;
                }
                if(item.is_new) {
                    this.data_list.splice(index, 1);
                }
            },
            saveItem: function(item) {
                this.new_item_form = Object.assign({}, item);
                this.new_item_form.date_load = this.getTimestamp(item.date_load);
                this.new_item_form.date_upload_since = this.getTimestamp(item.date_upload_since);
                this.new_item_form.date_upload_by = this.getTimestamp(item.date_upload_by);
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить таблицу! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                this.addItem();
            },
            load: function() {
                axios.get('/table-api/get').then(response => {
                    let data = response.data.content;
                    this.data_list = data.map(item => {
                        item.date_load = item.date_created  ? new Date(item.date_created * 1000) : '';
                        item.date_upload_since = item.date_upload_since ? new Date(item.date_upload_since * 1000) : '';
                        item.date_upload_by = item.date_upload_by ? new Date(item.date_upload_by * 1000) : '';
                        return {
                            edit: false,
                            is_new: false,
                            data: item
                        };
                    });
                });
            }
        },
        mounted: function () {
            try {
                this._csrf = this.$refs.config.dataset.csrf;
                this.types = JSON.parse(this.$refs.config.dataset.types);
                this.onLoad();
            }
            catch(e){
                console.log(e);
            };
        }

    });
});
