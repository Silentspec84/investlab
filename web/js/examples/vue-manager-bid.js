"use strict";
document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: '#bid',
        mixins: [listMixins],
        data: {
            shared: {},
            table: [],
            heads: {},
            error_list: [],
            usd_rate: null,
            edite_mode: false,
            form: {
                id: null,
                bid_comment: "",
                notify: "",
                consignee: "",
                cargo_code: "",
                cargo_description: "",
                shipper: [],
                date_way_from: "",
                date_way_till: "",
                date_cargo_ready: "",
                order_number: "",
                delivery_basis: "",
                delivery_address: "",
                amount: 1,
                client_company: "",
                client_full_name: "",
                client_email: "",
                client_phone: ""
            },
            invalid_inputs: [],
            invalid_inputs_for_highlight: [],
            bid_title_fields: {},
            rows_bid_table: {},
            bid_cost_heads: {},
            bid_cost_rows: {},
            _csrf: null,
            status: null,
            status_new: null,
            status_draft: null,
            bid_is_archived: null,
            bid_table_link: '/manager-bid-api/bid-table-data',
            bid_cost_table_link: '/manager-bid-api/bid-cost-table-data',
            bid_list_link: '/manager-bid/',
            bid_save_link: '/manager-bid-api/set-bid-status'
        },
        mounted: function() {
            this._csrf = this.$refs.config.dataset.csrf;
            const configElement = document.getElementById('sharedData');
            this.shared = JSON.parse(configElement.innerHTML);
            this.bid_status = JSON.parse(this.$refs.config.dataset.bid_status);
            this.status_new = JSON.parse(this.$refs.config.dataset.status_new);
            this.status_draft = JSON.parse(this.$refs.config.dataset.status_draft);
            this.bid_is_archived = JSON.parse(this.$refs.config.dataset.bid_is_archived);
            if (this.bid_status === this.status_new || this.bid_status === this.status_draft) {
                this.edite_mode = true;
            }
            this.form = this.shared;
            this.status = this.shared.status;
            this.usd_rate = this.shared.usd_rate;
            this.load();
        },
        methods: {
            getBidCostTableBody: function () {
                let rows = {};
                let i = 1;
                for(let key in this.bid_cost_rows) {
                    rows[i] = this.bid_cost_rows[key];
                    i++;
                }
                return rows;
            },
            getBidTableHeads: function () {
                let heads = {};
                for(let key in this.bid_title_fields) {

                    heads[key] = this.bid_title_fields[key]["title"];
                }
                return heads;
            },
            getBidTableBody: function () {
                let rows = {};
                let i = 1;
                for(let key in this.rows_bid_table) {
                    rows[i] = this.rows_bid_table[key];
                    i++;
                }
                return rows;
            },
            getBidCostTableHeads: function() {
                return {
                    title: 'Услуга',
                    amount: 'Кол-во',
                    currency_sign: 'Валюта',
                    price: 'Цена',
                    subtotal: 'Подытог',
                }
            },
            load: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(this.bid_table_link, {_csrf:this._csrf, id: this.shared.id}).then(response => {
                    if (response.data.is_success) {
                        this.bid_title_fields = response.data.content.bid_heads;
                        this.rows_bid_table =  Object.values(response.data.content.bid_rows);
                    } else {
                        this.error_list = response.data.errors;
                    }
                    this.loadBidCostTableData();
                }).catch(e => {
                    console.warn('cant load data');
                });
            },
            loadBidCostTableData: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(this.bid_cost_table_link, {_csrf:this._csrf, id: this.shared.id, amount: this.form.amount}).then(response => {
                    if (response.data.is_success) {
                        this.bid_cost_heads = response.data.content.bid_cost_heads;
                        this.bid_cost_rows = Object.values(response.data.content.bid_cost_rows);
                    } else {
                        this.error_list = response.data.errors;
                    }
                }).catch(e => {
                    console.warn('cant load data');
                });
            },
            setStatus: function (approve) {
                this.error_list = {};
                axios.post(this.bid_save_link, {_csrf:this._csrf, approve: approve, form: this.form}).then(response => {
                    if (response.data.is_success) {
                        this.returnOnList();
                    } else {
                        this.error_list = response.data.errors;
                    }
                });
            },
            returnOnList: function () {
                window.location.replace(this.bid_list_link);
            }
        }
    });
});
