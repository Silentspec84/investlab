"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('offer-list', {
        mixins: [listMixins, dateMixins],
        template: '#offer-list-template',
        data: function () {
            return {
                offers: [],
                filters_checkboxes: null,
                query: '',
                _csrf: null,
                show_modal: false,
                show_duplicate_modal: false,
                archive_candidates: [],
                selected_offer_id: 0,
                error_list: {},
                warning_list: {},
                status_list: [],
                status_modes: [],
                status_for_delete: [],
                drop_link: '/manager-offer-api/delete'
            };
        },
        methods: {
            onSearch(query) {
              this.query = query;
            },
            onChangeFilters(data) {
                this.filters_checkboxes.forEach((el, i) => {
                    if (el.title === data) {
                        this.filters_checkboxes[i].is_active = true;
                    } else {
                        this.filters_checkboxes[i].is_active = false;
                    }
                });
                this.load();
            },
            archiveModal: function(id) {
                this.archive_candidates = [];
                if(id === undefined) {
                    for(let i = 0; i < this.offers.length; i++) {
                        if(this.offers[i]['checked']) {
                            this.archive_candidates.push(this.offers[i].id);
                        }
                    }
                }
                else {
                    this.archive_candidates.push(id);
                }
                this.show_modal = true;
            },
            archive: function() {
                axios.post('/manager-offer-api/archive', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    ids: this.archive_candidates
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if(response.data.is_success) {
                        this.load();
                    }
                }).catch(e => {
                    console.warn('archiving error');
                });
                this.show_modal = false;
                this.load();
            },
            getTableHeads: function() {
                let table_heads = {
                    id: '№ КП',
                    client: 'Клиент',
                    bets: 'Кол-во ставок',
                    date_created: 'Создано',
                    date_sent_to_client: 'Отправлено',
                    date_till: 'Срок действия',
                    manager_name: 'Менеджер',
                    status: 'Статус',
                    actions: ''
                };

                return table_heads;
            },
            load() {
                axios.post('/manager-offer-api/list', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    filters_checkboxes: this.filters_checkboxes
                },{
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                        if (response.data.is_success) {
                            this.error_list = {};
                            this.offers = response.data.content;
                            for(let i = 0; i < this.offers.length; i++) {
                                this.offers[i]['shown'] = true;
                            }
                            this.warning_list = response.data.warnings;
                        } else {
                            this.error_list = response.data.errors;
                        }
                    })
                    .catch(e => {
                        console.warn('cant load dates');
                    });
            },
            getStatusClass: function(status) {
                let css_style = status ? this.status_modes[status] : '';
                let mode = status ? 'badge-' + css_style  : '';
                return 'badge ' + mode + ' text-white small py-1 px-2';
            },
            getStatusTitle: function(status) {
                return status ? this.status_list[status] : '';
            },
            isOfferForDelete(status) {
                return this.status_for_delete.indexOf(status) !== -1;
            },
            duplicateModal: function(id) {
                if(!id) {
                    return;
                }
                this.selected_offer_id = id;
                this.show_duplicate_modal = true;
            },
            duplicate: function() {
                axios.post('/manager-offer-api/duplicate', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    id: this.selected_offer_id
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if(response.data.is_success) {
                        this.load();
                    } else {
                        swal("При дублировании произошла ошибка. Обратитесь к администратору.", {
                            buttons: false,
                            icon: "error",
                            timer: 3000,
                        })
                    }
                }).catch(e => {
                    swal("При дублировании произошла ошибка. Обратитесь к администратору.", {
                        buttons: false,
                        icon: "error",
                        timer: 3000,
                    })
                });
                this.show_duplicate_modal = false;
                this.load();
            },
        },
        mounted: function () {
            this.load();
            this._csrf = this.$refs.configs.dataset.csrf;
            this.filters_checkboxes = JSON.parse(this.$refs.configs.dataset.filters_checkboxes);
            this.status_list = JSON.parse(this.$refs.configs.dataset.status_list);
            this.status_modes = JSON.parse(this.$refs.configs.dataset.status_modes);
            this.status_for_delete = JSON.parse(this.$refs.configs.dataset.status_for_delete);
        }
    });


    //index page
    new Vue({
        el: '#offer'
    });
});
