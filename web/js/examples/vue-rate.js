"use strict";

document.addEventListener("DOMContentLoaded", function() {
    const currency = new Vue({
        mixins: [listMixins],
        el: "#currency",
        data: {
            _csrf: null,
            data_list: [],
            types: [],
            currency: '',
            rate: '',
            type: '',
            new_item_form: {
                _csrf: this._csrf,
                type: null,
                rate: null,
            },
            form: {
                data_list: this.data_list,
                _csrf: null
            },
            error_list: [],
            warning_list: [],
            add_link: '/rate-api/save-currency',
            get_link: null,
            drop_link: '/rate-api/drop-currency',
            update_link: '/rate-api/update-currency',
            check_delete_link: '/rate-api/check-delete-currency'
        },
        mounted() {
            let dataset = this.$refs.config.dataset;
            this.id = dataset.id;
            this.get_link = dataset.get_link;
            this.types = JSON.parse(this.$refs.config.dataset.types);
            this.form._csrf = dataset.csrf;
            this._csrf = dataset.csrf;
            this.onLoad();
        },
        methods: {
            updateItem(item) {
                if (item.edit === true) {
                    item.data._csrf = this.form._csrf;

                    return axios.post(this.update_link, item.data)
                        .then(response => {
                            if (!response.data.is_success) {
                                this.error_list = {};
                                for (var key in response.data.errors[0]) {
                                    this.error_list[key] = response.data.errors[0][key];
                                }
                            } else {
                                swal("Курс валюты обновлен", {
                                    buttons: false,
                                    timer: 3000,
                                });
                                item.edit = false;
                                this.onLoad();
                            }
                        });
                }
            },
            /** Проверяет можно ли удалить валюту */
            checkDeleteCurrency: function(id) {
                axios.post(this.check_delete_link, {_csrf:this._csrf, id: id})
                    .then(response => {
                        this.warning_list = response.data.warnings;
                        if (!this.warning_list['check_delete']) {
                            this.showDropDialog(id);
                        }
                    });
            },
            startEdit: function(item) {
                item.data.old_rate = item.data.rate;
                item.data.old_type = item.data.type;
                item.edit = true;
            },
            cancelEdit: function(item) {
                item.edit = false;
                this.error_list = {};
                if(item.data.hasOwnProperty('old_rate') || item.data.hasOwnProperty('old_type')) {
                    item.data.rate = item.data.old_rate;
                    item.data.type = item.data.old_type;
                }
            },
            getErrorText: function(field) {
                if(this.getError(field).length == 0) {
                    return '';
                }
                return this.getError(field)[0];
            },
            getClass: function(field, is_inline) {
                if (is_inline) {
                    return 'form-control-inline form-control form-control-sm' + (this.getError(field) ? ' is-invalid' : '');
                }
                return 'form-control form-control-sm' + (this.getError(field) ? ' is-invalid' : '');
            }
        }
    });

    const rate = new Vue({
        mixins: [listMixins, arrayObjectMixins],
        el: "#rate",
        data: {
            data_list: {
                order_rates: [],
                currencies: [],
                types: [],
                order_rate_types: [],
            },
            order_rate_data: {},
            currencies: [],
            order_rates: [],
            types: [],
            cargo_list: [],
            order_rate_types: [],
            new_item_form: {
                _csrf: this._csrf,
                data: {},
                title: null,
                status: null,
            },
            form: {
                data_list: this.data_list,
                _csrf: null
            },
            _csrf: null,
            base_is_set: false,
            error_list: {},
            get_link: '/rate-api/get-rate-list',
            add_link: '/rate-api/update-rate-list',
            drop_link: '/rate-api/drop-rate',
            set_base_link: '/rate-api/set-base-rate',
        },
        mounted() {
            this.onLoad();
        },
        methods: {
            async onLoad() {
                this.error_list = {};
                await this.load();
                this.form._csrf = this.$refs.config.dataset.csrf;
                this._csrf = this.$refs.config.dataset.csrf;
                $('[data-toggle="tooltip"]').tooltip();
            },
            async load() {
                const response = await axios.get(this.get_link);

                this.data_list = response.data.content;
                this.order_rates = this.data_list.order_rates;
                this.currencies = this.data_list.currencies;
                this.types = this.data_list.types;
                this.order_rate_types = this.data_list.order_rate_types;
                this.base_is_set = this.data_list.base_is_set;
                this.cargo_list = this.data_list.cargo_list;

                this.clearNewItemForm();

            },
            addItem() {
                this.new_item_form._csrf = this.form._csrf;
                return axios.post(this.add_link, this.new_item_form)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = {};
                            for (var key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];
                            }
                        } else {
                            this.error_list = {};
                            this.clearNewItemForm();

                            swal("Сохранено", {
                                buttons: false,
                                timer: 1500,
                            });
                            this.load();
                        }
                    });
            },
            clearNewItemForm() {
                for (let cargo in this.cargo_list) {
                    for (let type in this.types) {
                        this.$set(this.new_item_form.data, this.types[type].id + "_" + this.cargo_list[cargo].id, {
                            cargo_type_id: this.cargo_list[cargo].id,
                            type: this.types[type].id,
                            value: null,
                            currency_id: null,
                        })
                    }
                }
                this.new_item_form.status = null;
                this.new_item_form.title = null;
            },
            updateRow(order_rate) {
                if (order_rate.edit === true) {
                    for (let type in this.types) {
                        for (let cargo in this.cargo_list) {
                            this.$set(this.order_rate_data, this.types[type].id+"_"+this.cargo_list[cargo].id, {
                                type: this.types[type].id,
                                cargo_type_id: this.order_rate_types[order_rate['id']][this.types[type].id][this.cargo_list[cargo].id].cargo_type_id,
                                value: this.order_rate_types[order_rate['id']][this.types[type].id][this.cargo_list[cargo].id].value,
                                currency_id: this.order_rate_types[order_rate['id']][this.types[type].id][this.cargo_list[cargo].id].currency_id,
                            })
                        }
                    }
                    return axios.post(this.add_link, {_csrf: this.form._csrf, data: this.order_rate_data, title: order_rate.title, status: order_rate.status, id: order_rate.id})
                        .then(response => {
                            if (!response.data.is_success) {
                                this.error_list = {};
                                for (var key in response.data.errors) {
                                    this.error_list[key] = response.data.errors[key];
                                }
                            } else {
                                swal("Маржа обновлена", {
                                    buttons: false,
                                    timer: 3000,
                                });
                                order_rate.edit = false;
                                this.onLoad();
                            }
                        });
                }
            },
            startEdit: function(order_rate, order_rate_type_id) {
                this.$set(order_rate, 'edit', true);
                order_rate.old_data = Object.assign({}, order_rate);
                this.order_rate_types[order_rate_type_id].old_data = [];
                this.order_rate_types[order_rate_type_id].old_data = this.copyObject(this.order_rate_types[order_rate_type_id]);
                if('show_rates' in order_rate) {
                    order_rate.show_rates = true;
                } else {
                    this.$set(order_rate, 'show_rates', true);
                }
            },

            cancelEdit: function(order_rate, order_rate_type_id) {
                order_rate.old_data.edit = false;
                this.error_list = {};
                this.order_rates[order_rate['id']] = order_rate.old_data;
                this.order_rate_types[order_rate_type_id] = this.order_rate_types[order_rate_type_id].old_data;
                if('show_rates' in order_rate) {
                    order_rate.show_rates = false;
                }
            },

            setAsBase: function(order_rate_id) {
                return axios.post(this.set_base_link, {_csrf: this.form._csrf, id: Number(order_rate_id)})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = {};
                            for (var key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];
                            }
                        } else {
                            swal("Тариф установлен в качестве Тарифа для клиентского поиска", {
                                buttons: false,
                                timer: 3000,
                            });
                            this.onLoad();
                        }
                    });
            },
            isBase: function (order_rate) {
                if (order_rate.is_base == 1) {
                    return 'alert alert-warning'
                }
            },
            getStarIfBase: function (order_rate) {
                if (order_rate.is_base == 1) {
                    return order_rate.title + "<span class='font-weight-bold' style='font-size:20px'>*</span>";
                } else {
                    return order_rate.title;
                }
            },
            getError: function (filed, id = false, type_id = false) {
                var result = '';

                if (id === false && type_id === false && (filed in this.error_list)) {
                    result = this.error_list[filed];
                }
                if (id === false && (type_id in this.error_list) && (filed in this.error_list[type_id])) {
                    result = this.error_list[type_id][filed];
                }
                if ((id in this.error_list) && type_id === false  && (filed in this.error_list[id])) {
                    result = this.error_list[id][filed];
                }
                if ((id in this.error_list) && (type_id in this.error_list[id]) && (filed in this.error_list[id][type_id])) {
                    result = this.error_list[id][type_id][filed];
                }
                $('[data-toggle="tooltip"]').tooltip();

                return result;
            },
            showRates: function (order_rate) {
                if(!('show_rates' in order_rate)) {
                    this.$set(order_rate, 'show_rates');
                }
                order_rate.show_rates = !order_rate.show_rates;
            },
            getOrderRateId: function (id) {
                return "order_rate_"+id;
            }
        },

    });
});