"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#reset-data",
        mixins: [listMixins],
        data: {
            _csrf: null,
            error_reset_data: null
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
        },
        methods: {
            resetData() {
                this.createSwal("Идет откат к заводским настройкам", "info", 300000);
                return axios.post('/reset-data-api/reset-table-data',{_csrf: this._csrf})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.closeSwal(response);
                             this.error_reset_data = response.data.errors['error_reset_data'];
                            this.createSwal(this.error_reset_data, "error", 2000);
                        } else {
                            this.error_reset_data = null;
                            this.closeSwal(response);
                            swal("Данные успешно сброшены", {
                                buttons: false,
                                icon: "success",
                                timer: 1500,
                            }).then(() => {
                                window.location.href = "/";
                            });
                        }
                    });
            },
        }
    });
});
