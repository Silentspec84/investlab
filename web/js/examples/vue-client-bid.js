"use strict";
document.addEventListener("DOMContentLoaded", function() {
    Vue.directive('mask', VueMask.VueMaskDirective);
    new Vue({
        el: '#bid',
        mixins: [listMixins, validatorMixins],
        data: {
            shared: {},
            table: [],
            heads: {},
            error_list: [],
            usd_rate: null,
            form: {
                id: null,
                bid_comment: "",
                notify: "",
                consignee: "",
                cargo_code: "",
                cargo_description: "",
                shipper: [],
                date_shipping_from: "",
                date_shipping_till: "",
                date_cargo_ready: "",
                order_number: "",
                delivery_basis: "",
                delivery_address: "",
                amount: 1,
                client_company: "",
                client_full_name: "",
                client_email: "",
                client_phone: "",
                status: null
            },
            invalid_inputs: [],
            bid_title_fields: {},
            rows_bid_table: {},
            bid_cost_heads: {},
            bid_cost_rows: {},
            _csrf: null,
            status_new: 10,
            status_draft: 20,
            status_on_confirm: 30,
            bid_is_archived: null,
            bid_table_link: '/client-bid-api/bid-table-data',
            bid_cost_table_link: '/client-bid-api/bid-cost-table-data',
            bid_list_link: '/client-bid/',
            search_link: '/client-search/',
            bid_save_link: '/client-bid-api/save-bid',
            drop_link: '/client-bid-api/delete-bid',
            redirect_link_after_drop: '/client-bid/index'
        },
        mounted: function() {
            this._csrf = this.$refs.config.dataset.csrf;
            const configElement = document.getElementById('sharedData');
            this.shared = JSON.parse(configElement.innerHTML);
            this.form = this.shared;
            this.usd_rate = this.shared.usd_rate;
            this.status_new = JSON.parse(this.$refs.config.dataset.status_new);
            this.status_draft = JSON.parse(this.$refs.config.dataset.status_draft);
            this.status_on_confirm = JSON.parse(this.$refs.config.dataset.status_on_confirm);
            this.bid_is_archived = JSON.parse(this.$refs.config.dataset.bid_is_archived);
            if (this.bid_is_archived) {
                this.form.is_editable = false;
            }
            this.setDates();
            this.load();
        },
        methods: {
            addShipper: function () {
                this.form.shipper.push({number: this.form.shipper.length + 1, text: ""});
            },
            deleteLastShipper: function () {
                this.form.shipper.pop();
            },
            /**
             * Установка текущей даты при первом создании заявки или если даты по какой-то причине нет
             */
            setDates: function () {
                if (!this.isDate('', {format: 'YYYY-MM-DD'}, this.form.date_shipping_from)
                    || !this.dateAfter('', {format: 'YYYY-MM-DD', date: moment().format("YYYY-MM-DD")}, this.form.date_shipping_from)) {
                    this.form.date_shipping_from = moment().format("YYYY-MM-DD");
                }
                if (!this.isDate('', {format: 'YYYY-MM-DD'}, this.form.date_shipping_till)
                    || !this.dateAfter('', {format: 'YYYY-MM-DD', date: moment().format("YYYY-MM-DD")}, this.form.date_shipping_till)) {
                    this.form.date_shipping_till = moment().format("YYYY-MM-DD");
                }
                if (!this.isDate('', {format: 'YYYY-MM-DD'}, this.form.date_cargo_ready)
                    || !this.dateAfter('', {format: 'YYYY-MM-DD', date: moment().format("YYYY-MM-DD")}, this.form.date_cargo_ready)) {
                    this.form.date_cargo_ready = moment().format("YYYY-MM-DD");
                }
            },
            getBidCostTableBody: function () {
                let rows = {};
                let i = 1;
                for(let key in this.bid_cost_rows) {
                    rows[i] = this.bid_cost_rows[key];
                    i++;
                }
                return rows;
            },
            getBidTableHeads: function () {
                let heads = {};
                for(let key in this.bid_title_fields) {

                    heads[key] = this.bid_title_fields[key]["title"];
                }
                return heads;
            },
            getBidTableBody: function () {
                let rows = {};
                let i = 1;
                for(let key in this.rows_bid_table) {
                    rows[i] = this.rows_bid_table[key];
                    i++;
                }
                return rows;
            },
            getBidCostTableHeads: function() {
                return {
                    title: 'Услуга',
                    amount: 'Кол-во',
                    currency_sign: 'Валюта',
                    price: 'Цена',
                    subtotal: 'Подытог',
                }
            },
            load: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(this.bid_table_link, {_csrf:this._csrf, id: this.shared.id}).then(response => {
                    if (response.data.is_success) {
                        this.bid_title_fields = response.data.content.bid_heads;
                        this.rows_bid_table =  Object.values(response.data.content.bid_rows);
                    } else {
                        this.error_list = response.data.errors;
                    }
                    this.loadBidCostTableData();
                }).catch(e => {
                    console.warn('cant load data');
                });
            },
            loadBidCostTableData: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(this.bid_cost_table_link, {_csrf:this._csrf, id: this.shared.id, amount: this.form.amount}).then(response => {
                    if (response.data.is_success) {
                        this.bid_cost_heads = response.data.content.bid_cost_heads;
                        this.bid_cost_rows = Object.values(response.data.content.bid_cost_rows);
                    } else {
                        this.error_list = response.data.errors;
                    }
                }).catch(e => {
                    console.warn('cant load data');
                });
            },
            validate: function(status) {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['client_full_name', 'client_email', 'client_phone', 'date_shipping_from', 'date_shipping_till', 'date_cargo_ready', 'order_number', 'delivery_basis', 'delivery_address', 'cargo_code', 'cargo_description', 'notify', 'consignee'],
                        message: 'Данное поле обязательно к заполнению!',
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    customValidation: {fields: ['shipper'], method: 'validateShipper', params: {status: status}},
                    min: {
                        fields: ['client_email', 'client_full_name', 'order_number', 'delivery_basis', 'bid_comment', 'delivery_address', 'cargo_code', 'cargo_description', 'notify', 'consignee'],
                        min: 2,
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    max: {
                        fields: ['client_phone', 'cargo_code', 'client_email', 'client_company', 'client_full_name', 'order_number', 'delivery_basis', 'bid_comment', 'delivery_address', 'cargo_description', 'notify', 'consignee'],
                        max: {client_phone: 50, cargo_code: 50, client_email: 255, client_company: 255, client_full_name: 100, order_number: 100, delivery_basis: 100, bid_comment: 1000, delivery_address: 1000, cargo_description: 1000, notify: 1000, consignee: 1000},
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    isDate: {
                        fields: ['date_shipping_from', 'date_shipping_till', 'date_cargo_ready'],
                        format: 'YYYY-MM-DD',
                        message: 'Неверный формат даты',
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    dateAfter: {
                        fields: ['date_shipping_from', 'date_shipping_till', 'date_cargo_ready'],
                        format: 'YYYY-MM-DD',
                        date: moment().subtract(1, 'd'),
                        message: 'Дата должна быть не раньше текущей',
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    email: {
                        fields: ['client_email'],
                        message: 'Неправильный формат почты (xxx@xxx.xx)',
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    phone: {
                        fields: ['client_phone'],
                        message: 'Неправильный формат телефона (+#(###)###-##-##)',
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                    userName: {
                        fields: ['client_full_name'],
                        message: 'Поле Контактное лицо должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 2-х символов и больше 100.',
                        needValidate: 'isNeedValidate',
                        params: {status: status}
                    },
                };

                return this.validateForm(config, this.form);
            },
            isNeedValidate: function (field_name, field_value, params) {
                let required_fields = ['client_email', 'client_phone', 'client_full_name'];

                if (field_name === 'bid_comment') {
                    return this.required(field_name, {}, field_value); // bid_comment нужно валидировать, только если он заполнен
                }
                if (params.status === this.status_draft) {
                    if (required_fields.indexOf(field_name) !== -1) {
                        return true; // для черновика нужно валидировать обязательные поля (required_fields)
                    } else {
                        return this.required(field_name, {}, field_value);  // для черновика нужно валидировать остальные поля, если они заполнены
                    }
                }
                return true;
            },
            validateShipper: function(params)
            {
                let validation_succeed = true;
                for (let key in this.form.shipper) {
                    let shipper_value = this.form.shipper[key]['text'];
                    if (!shipper_value && params.status !== this.status_draft) {
                        this.error_list.shipper = [];
                        this.error_list.shipper[key] = 'Данное поле обязательно к заполнению!';
                        validation_succeed = false; // чистовик и поле не заполнено
                    }
                    if (!this.min('shipper', {min: 2}, shipper_value)) {
                        this.error_list.shipper = [];
                        this.error_list.shipper[key] = 'Минимальная длина поля - 2 символа';
                        validation_succeed = false; // поле заполнено некорректно
                    }
                    if (!this.max('shipper', {max: 1000}, shipper_value)) {
                        this.error_list.shipper = [];
                        this.error_list.shipper[key] = 'Максимальная длина поля - 1000 символов';
                        validation_succeed = false; // поле заполнено некорректно
                    }
                    if (!shipper_value && params.status === this.status_draft) {
                        this.error_list.shipper = [];
                        validation_succeed = true;
                    }
                }
                return validation_succeed;
            },
            amountChange: function () {
                this.loadBidCostTableData();
            },
            saveBid: function (status) {
                if (!this.validate(status)) {
                    this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                this.form.amount = parseInt(this.form.amount);
                axios.post(this.bid_save_link, {_csrf:this._csrf, status: status, form: this.form}).then(response => {
                    if (response.data.is_success) {
                        window.location.replace(this.bid_list_link);
                    } else {
                        this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                        this.error_list = response.data.errors;
                    }
                });
            },
            returnOnList: function () {
                if (this.form.status === this.status_new) {
                    window.location.replace(this.search_link);
                    axios.post(this.bid_delete_link, {_csrf:this._csrf, id: this.form.id}).then(response => {
                        if (response.data.is_success) {
                            window.location.replace(this.search_link);
                        } else {
                            this.error_list = response.data.errors;
                        }
                    });
                } else {
                    window.location.replace(this.bid_list_link);
                }
            }
        }
    });
});
