"use strict";
document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: '#offer',
        mixins: [listMixins, formulasMixins],
        data: {
            is_loading: true,
            shared: {},
            table: [],
            heads: {},
            editable_rows: [],
            formulas: null,
            currencies: [],
            error_list: [],
            _csrf: null,
            set_create_bid_link: "/client-bid-api/create-bid",
            get_table_data_link: "/client-offer-api/table-data",
            set_view_bid_link: "/client-bid/view/",
            not_exist_value: '',
        },
        mounted: function() {
            this._csrf = this.$refs.config.dataset.csrf;
            const configElement = document.getElementById('sharedData');
            this.shared = JSON.parse(configElement.innerHTML);
            this.not_exist_value = this.$refs.config.dataset.not_exist_value;
            this.load();
        },
        methods: {
            load: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.get(this.get_table_data_link, {params: {id: this.shared['id']}}).then(response => {
                    if (response.data.is_success) {
                        let response_heads = response.data.content.heads;
                        for(let key in response.data.content.heads) {
                            this.heads[key] = response.data.content.heads[key]['title'];
                        }
                        this.formulas = response.data.content.formulas;
                        this.currencies = response.data.content.currencies;
                        this.table = Object.values(response.data.content.rows);
                        this.table.map(item => item.shown = true);
                        this.is_loading = false;
                        this.table = this.getCalculateRows(response_heads, this.table, this.formulas);
                    }
                }).catch(e => {
                    console.warn('cant load data');
                });
            },
            createNewBid: function (row, bid_from_offer) {
                this.error_list = {};
                axios.post(this.set_create_bid_link, {_csrf:this._csrf, row: row, bid_from_offer: bid_from_offer}).then(response => {
                    if (response.data.is_success) {
                        window.location.replace(this.set_view_bid_link + response.data.content.id);
                    } else {
                        this.error_list = {};
                        for (var key in response.data.errors) {
                            this.error_list[key] = response.data.errors[key];
                        }
                    }
                });
            }
        }
    });
});
