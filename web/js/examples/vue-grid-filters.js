Vue.component('vue-grid-filters', {
    template: `<div id="grid-filters" class="row">
                    <div class="col-md-4">
                        <button v-if="primary_button.title" class="btn btn-primary mr-2">{{primary_button.title}}</button>
                        <button v-for="button in secondary_buttons" class="btn btn-secondary mr-2">{{button.title}}</button>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="row d-flex align-items-center justify-content-end" style="height: 100%;">
                            <div v-if="checkboxes" class="col-md-4 d-flex align-items-center">
                                <span v-if="header_filters" class="mr-2">{{header_filters}}</span>
                                <div v-for="(filter, index) in filters" class="checkbox checkbox-primary form-check-inline mr-3">
                                    <input type="checkbox" :id="'checkbox_' + index" :checked="filter.is_active" @change="changeFilter($event, index)">
                                    <label class="ml-2 mr-2" :for="'checkbox_' + index">{{filter.title}}</label>
                                </div>
                            </div>
                            
                            <div v-if="radio" class="col-md-4 d-flex align-items-center">
                                 <span v-if="header_filters" class="mr-2">{{header_filters}}</span>
                                 <div v-for="(filter, index) in filters" class="radio radio-primary form-check-inline mr-3">
                                    <input type="radio" :id="'radio_' + index" :checked="filter.is_active" @change="changeFilter($event, index)">
                                    <label :for="'radio_' + index">{{filter.title}}</label>
                                </div>
                            </div>
                            
                            <div v-if="dropdown.options" class="col-md-2">
                                <div class="form-group">
                                    <label for="status-select">{{dropdown.name}}</label>
                                    <select class="form-control form-control-sm" @change="optionSelected($event)">
                                        <option v-for="option in dropdown.options" :value="option.title">{{option.title}}</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-xl-5 mb-3" v-if="datepicker">
                                <span class="period-label">Период:</span>
                                <label class="mr-2 mb-2">
                                    <span class="mr-2">с</span>
                                    <input id="date_from"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           data-original-title="" title=""
                                           :data-original-title="getError('date_from')"
                                           :class="getError('date_from') ? 'form-control form-control-inline form-control-sm is-invalid' : 'form-control form-control-inline form-control-sm'"
                                           type="text"
                                           style="width: auto;"
                                           class="d-inline-block"
                                           v-model="filters.date_from">
                                </label>
                                
                                <label class="mb-2">по
                                    <input id="date_to"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           data-original-title=""
                                           title=""
                                           :data-original-title="getError('date_to')"
                                           :class="getError('date_to') ? 'form-control form-control-inline form-control-sm is-invalid' : 'form-control form-control-inline form-control-sm'"
                                           type="text"
                                           style="width: auto;"
                                           class="d-inline-block"
                                           v-model="filters.date_to" >
                                </label>
                            </div>
                            
                            <div v-if="search.placeholder" class="col-md-3 mt-2">
                                <input :placeholder="search.placeholder" class="form-control form-control-sm" v-model="search_query" @input="onSearch">
                            </div>
                        </div>
                        
                    </div>
               </div>`,
    props: {
        checkboxes: {
            type: Boolean,
            default: false,
        },
        radio: {
            type: Boolean,
            default: false,
        },
        datepicker: {
            type: Boolean,
            default: false,
        },
        header_filters: {
            type: String,
            default: '',
        },
        search: {
            /* Объект вида : {
            *   placeholder: 'Искать',
            * }
            */
            type: Object,
            default() {
                return {};
            }
        },
        dropdown:{
            /* Объект вида : {
            *   name: 'Статус',
            *   options: [
            *       {id: 1, title: 'Подготовлен'},
            *       {id: 2, title: 'Отправлен'},
            *   ]
            * }
            */
            type: Object,
            default() {
                return {};
            }
        },
        primary_button: {
            /* Объект вида : {
            *    title: 'Отправить',
            * }
            */
            type: Object,
            default() {
                return {};
            },
        },
        secondary_buttons: {
            /* Массив вида : [
            *       {title: 'Удалить'},
            *       {title: 'Архивировать'},
            *   ]
            */
            type: Array,
            default() {
                return [];
            },
        },
        /** Массив вида:
         * [
         *      0 => [
         *          title => 'Архивные',
         *          set_value => Offer::IS_ARCHIVED,
         *          is_active => false
         *      ],
         *          ...
         *      N => [
         *          title => 'Активные',
         *          set_value => Offer::NOT_ARCHIVED,
         *          is_active => true
         *      ]
         * ]
         * */
        filter_fields: {
            type: Array,
            default() {
                return [];
            }
        }
    },
    data() {
        return {
            error_list: '',
            search_query: '',
        }
    },
    computed: {
        filters() {
            return this.filter_fields;
        }
    },
    methods: {
        getError(field, id = false) {
            let result = '';
            if (typeof this.error_list === 'string') {
                return this.error_list;
            }
            if (id === false && (field in this.error_list)) {
                result = this.error_list[field];
            } else if (id in this.error_list && (field in this.error_list[id])) {
                result = this.error_list[id][field];
            }
            $('[data-toggle="tooltip"]').tooltip();

            return result;
        },
        changeFilter($event, index) {
            if (this.radio) {
                let checked = $event.target.checked;
                this.filters.forEach((filter, i) => {
                    if (index === i) {
                        this.filters[i].is_active = checked;
                    } else {
                        this.filters[i].is_active = false;
                    }
                });
            }

            if (this.checkboxes) {
                let checked = $event.target.checked;
                this.filters.forEach((filter, i) => {
                    if (index === i) {
                        this.filters[i].is_active = checked;
                    }
                });
            }

            this.$emit('changeFilters', this.filters);
        },
        optionSelected(event) {
            this.$emit('optionSelected', event.target.value);
        },
        onSearch() {
            this.$emit('onSearch', this.search_query);
        }
    }
});