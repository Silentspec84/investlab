"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        mixins: [listMixins, rowsSearchFilterMixins],
        el: "#client_view_search_filter",
        data: {
            link_get_search_rows_by_hash_filter: '/client-search-filter-api/get-search-rows-by-hash-filter',
        },
    });
});