"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#result-table",
        mixins: [listMixins, paginatorMixin, seacrchComponentMixins, formulasMixins],
        data: {
            export_in_progress: false,
            title_fields: [],
            rows_result_table: [],
            error_list: {},
            is_loading_data: null,
            page_links: {},
            current_page: 1,
            formulas: null,
            currencies: [],
            not_exist_value: '',
            link_get_search_result: '/result-table-api/get',
        },
        methods: {
            exportCSV() {
                this.export_in_progress = true;
                axios({
                    url: '/result-table/',
                    method: 'POST',
                    responseType: 'blob',
                    data: {
                        is_export: true
                    },
                }).then(response => {
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', 'result_table_export.csv');
                    document.body.appendChild(link);
                    link.click();
                    this.export_in_progress = false;
                }).catch(error => alert(error.message));
            },
            async onSearch() {
                if (!this.query) {
                    this.error_list = {};
                    return this.loadData(this.link_get_search_result);
                }
                this.is_loading_data = true;
                if (await this.search()) {
                    this.title_fields = this.search_result.title_fields;
                    this.rows_result_table = this.search_result.rows_result_table;
                    this.page_links = this.search_result.page_links;
                    this.current_page = this.search_result.current_page;
                    this.pages = this.getPages(this.page_links);
                }
                this.is_loading_data = false;
            },
            getSearchResultResponse() {
                return axios.get(this.link_get_search_result, {params: {query: this.query}});
            },
            getTableHeads: function() {
                let heads = {};
                for(let key in this.title_fields) {
                    heads[key] = this.title_fields[key]['title'];
                }
                return heads;
            },

            hasPage(page) {
                return (typeof this.pages[page] !== 'undefined');
            },

            loadData(link, params) {
                if (link) {
                    this.link_get_search_result = link;
                }
                this.is_loading_data = true;
                axios.get(this.link_get_search_result, params).then(response => {
                    if (response.data.is_success) {
                        this.title_fields = response.data.content.title_fields;
                        this.rows_result_table = response.data.content.rows_result_table;
                        this.page_links = response.data.content.page_links;
                        this.current_page = response.data.content.current_page;
                        this.pages = this.getPages(this.page_links);
                        this.formulas = response.data.content.formulas;
                        this.currencies = response.data.content.currencies;
                        this.rows_result_table = this.getCalculateRows(this.title_fields, this.rows_result_table, this.formulas);
                    } else {
                        this.error_list = response.data.errors;
                    }
                    this.is_loading_data = false;
                }).catch(e => {
                    this.$set(this.error_list, 'all_fields', 'Ошибка получения данных, обратитесь к администратору!');
                    this.is_loading_data = false;
                });
            },
            sortbyWithPaginator: function(order, key) {
                let params = {params: {sort_direction: order, field_order:key, query: this.query}};
                window.history.pushState(null, null, this.reBuildGetLink(window.location.href, params.params));
                this.loadData( this.link_get_search_result, params);
            },
            getError: function (field, id = false) {
                var result = '';
                if (typeof this.error_list === 'string') {
                    return this.error_list;
                }
                if (id === false && (field in this.error_list)) {
                    result = this.error_list[field];
                } else if (id in this.error_list && (field in this.error_list[id])) {
                    result = this.error_list[id][field];
                }
                $('[data-toggle="tooltip"]').tooltip();

                return result;
            },
        },

        mounted: function () {
            let link = this.link_get_search_result;
            let parse_url = this.parseUrl(window.location, true);
            if (parse_url.search) {
                let get_params = parse_url.searchObject;
                link = this.link_get_search_result + '?' + this.httpBuildQuery(get_params);
            }
            this.loadData(link);
            this.not_exist_value = this.$refs.config.dataset.not_exist_value;
        }
    });
});
