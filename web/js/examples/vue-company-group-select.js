Vue.component('vue-company-group-select', {
    components: {
        multiselect: window.VueMultiselect.default,
    },
    props: ['owner_id', 'client_list','value', 'all_child_companies'],
    template: `<div>
                   <button class="btn btn-link" v-if="!edit_mode && !isChildCompany" @click="edit_mode = !edit_mode">
                        <span v-if="!child_companies.length" @click="getCompaniesList">Добавить</span>
                        <span v-else @click="getCompaniesList">Показать <i class="dripicons dripicons-chevron-down"></i></span>
                   </button>
                   <button class="btn btn-link" v-if="edit_mode" @click="edit_mode = !edit_mode">Скрыть <i class="dripicons dripicons-chevron-up"></i></button>
                   <button class="btn btn-primary btn-sm" v-if="edit_mode" @click="updateCompaniesList"">Сохранить</button>
                   
                   <div v-if="edit_mode" class="vue-multiselect-custom d-block">
                        <multiselect
                                v-model="child_companies"
                                :options="list"
                                :multiple="true"
                                :limit="15"
                                :max-height="350"
                                :close-on-select="false"
                                placeholder="🔍Поиск"
                                track-by="full_name"
                                label="full_name"
                                selected-label="Выбрано"
                                select-label="Выбрать"
                                deselect-label="Удалить"
                                @select="onSelect($event)"
                                @remove="onRemove($event)">
                            <span slot="noResult">Результатов не найдено...</span>
                        </multiselect>
                    </div>
               </div>`,
    data() {
        return {
            initial_value: this.value,
            list: [],
            child_companies: [],
            child_company_ids: [],
            edit_mode: false,
        }
    },
    computed: {
        isChildCompany() {
            return this.all_child_companies.indexOf(this.owner_id.toString()) !== -1;
        },
    },
    created() {
        if (this.initial_value) {
            this.child_companies = [...this.initial_value];
            this.initial_value.forEach(el => {
                this.child_company_ids.push(el.id);
            });
        }
    },
    methods: {
        onSelect($event) {
            this.child_company_ids.push($event.id);
        },
        onRemove($event) {
            let index = this.child_company_ids.indexOf($event.id);
            this.child_company_ids.splice(index, 1);
        },
        getCompaniesList() {
            axios.get('/client-api/get-options-child-companies', {_csrf: this.$csrfToken})
                .then(({data}) => {
                   data.content.options_child_companies.forEach((el, index) => {
                       if (parseInt(el.id) == this.owner_id) {
                          data.content.options_child_companies.splice(index, 1);
                       }
                   });
                   this.list = data.content.options_child_companies;
                });
        },
        updateCompaniesList() {
            axios.post('/client-api/update-child-companies-for-owner', {
                _csrf: document.querySelector("meta[name='csrf-token']").getAttribute('content'),
                owner_company_id: this.owner_id,
                child_company_ids: this.child_company_ids,
            }).then(({data}) => {
                if (data.is_success === false) {
                    swal({
                        text: data.errors[0],
                        value: true,
                        visible: true,
                        className: "error",
                        closeModal: true,
                    });
                } else {
                    let oldList = [];
                    for (let company in this.value) {
                        oldList.push(this.value[company].id);
                    }
                    this.$emit('updateChildCompanies', {oldList: oldList, newList: this.child_company_ids});
                    this.edit_mode = false;
                    this.initial = this.child_company_ids;
                    this.value = swal({
                        text: "Группа успешно сохранена",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true,
                    });
                }
            });
        },
    },
});