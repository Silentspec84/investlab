document.addEventListener("DOMContentLoaded", function() {

    Vue.directive('mask', VueMask.VueMaskDirective);

    erp_client_config = new Vue({
        mixins: [listMixins],
        el: "#erp_client_config",
        data: {
            form: {
                _csrf: null,
                data: null
            },
            client_list: [],
            field_headers: [],
            client_erp_config_list: [],
            error_list: [],
            isDocumentLoaded: false,
            get_erp_config: '/manager-erp-api/get-client-fields',
            save_erp_config: '/manager-erp-api/save-client-fields',
        },

        methods: {
            isClientsLoaded() {
                return (Object.keys(this.client_erp_config_list).length > 0);
            },
            selectAll(client){
               console.log(this.getClientEnabledFields(client));
            },
            getClientEnabledFields(client){
                let client_config = this.client_erp_config_list[client.inn].enabled_fields;
                return client_config;
            },

            getClients() {
                let clients = [];
                for (let inn in this.client_list) {
                    clients.push(this.client_list[inn]);
                }

                return clients;
            },

            getHeaders() {
                let headers = [];
                for (let key in this.field_headers) {
                    headers.push(this.field_headers[key]);
                }
                return headers;
            },

            getHeaderKeys(){
                let header = [];
                for (let key in this.field_headers) {
                    header.push(key);
                }
                return header;
            },

            loadForm(data) {
                this.form.data = this.client_erp_config_list;
                return this.form
            },

            save() {
                let self = this;
                return axios.post(self.save_erp_config, this.loadForm())
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = {};
                            for (var key in response.data.errors[0]) {
                                self.error_list[key] = response.data.errors[0][key];
                            }
                        } else {
                            swal("Сохранено", {
                                buttons: false,
                                timer: 2500,
                            });
                        }
                    }).catch(e => {
                        swal("Ошибка сохранения, попробуйте позже", {
                            buttons: false,
                            timer: 2500,
                        });
                        console.warn(e);
                    });
            },


            getClass(field) {
                return 'form-control form-control-sm' + (this.getError(field) ? ' is-invalid' : '');
            },

        },
        mounted: function() {
            this.onLoad();
            const vue = this;
            axios.get(this.get_erp_config)
                .then(function (response) {
                    console.log(response.data);
                    vue.client_list = response.data.content.client_list;
                    vue.field_headers = response.data.content.field_headers;
                    vue.client_erp_config_list = response.data.content.client_erp_config_list;
                }).catch(e => {
                console.warn(e);
            });

        },
    });


});