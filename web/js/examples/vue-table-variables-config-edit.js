document.addEventListener("DOMContentLoaded", function(event) {
    vm = new Vue({
        mixins: [listMixins],
        el: "#main",
        data: {
            get_link: '',
            set_link: '',
            order_field: '',
            groups: [],
            error_list: [],
            data_list: {
                all_fields: {},
                table_keys: {},
                simple_fields: {},
                FRAH: {},
                RAILWAY: {},
                AUTO: {},
                table_fields_other: {}
            },
            form: {
                _csrf:  null,
                data_list: [],
            },
            borders: {
                for_all_fields: '',
                for_table_keys: '',
                for_simple_fields: '',
                for_fraht: '',
                for_rail: '',
                for_auto: '',
                for_other: '',
            },
            need_to_check_field: false,
            evt_data: []
        },
        methods: {
            load: function () {
                let field = {
                    order_field: this.order_field
                };
                this.form.data_list = field;
                let self = this;
                axios.post(this.get_link, this.form)
                    .then(function (response) {
                        if (!response.data.is_success) {
                            this.error_list = [];
                            for (let key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];
                            }
                        } else {
                            self.data_list = response.data.content;
                        }
                    });
            },
            modify: function (field_id, action_id, old_index, new_index, group) {
                let field = {
                    id:  field_id,
                    action: action_id,
                    old_index: old_index,
                    new_index: new_index,
                    group: group,
                    order_field: this.order_field
                };
                this.form.data_list = field;
                this.createSwal("Идет сохранение данных", "info", 300000);
                return axios.post(this.set_link, this.form)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = [];
                            for (let key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];

                            }
                            swal("Не удалось сохранить изменения!", {
                                buttons: false,
                                icon: "error",
                                timer: 2500,
                            });
                        } else {
                            this.error_list = [];
                            swal("Сохранено", {
                                buttons: false,
                                icon: "success",
                                timer: 2500,
                            });
                        }
                        this.form.data_list = [];
                    });
            },
            modifyField: function (evt) {
                if (!this.isChangeNeeded()) {
                    return;
                }
                if ( typeof evt.removed !== 'undefined') {
                    this.modify(evt.removed.element.field.id, 1, evt.removed.oldIndex, 0, evt.removed.element.group);
                }
                if ( typeof evt.added !== 'undefined') {
                    this.modify(evt.added.element.field.id, 0, 0, evt.added.newIndex, evt.added.element.group);
                }
                if ( typeof evt.moved !== 'undefined') {
                    this.modify(evt.moved.element.field.id, 2, evt.moved.oldIndex, evt.moved.newIndex, evt.moved.element.group);
                }
            },
            getBorderGroupClassArray:function() {
                let border_group_class = {
                    'all_fields': this.borders.for_all_fields,
                    'table_keys': this.borders.for_table_keys,
                    'simple_fields': this.borders.for_simple_fields,
                    'FRAH': this.borders.for_fraht,
                    'RAIL': this.borders.for_rail,
                    'AUTO': this.borders.for_auto,
                    'table_fields_other': this.borders.for_other
                };
                return border_group_class;
            },
            getBorderForGroup: function (cur_group) {
                let border_class = this.getBorderGroupClassArray();
                return border_class[cur_group];
            },
            getBorderField: function (cur_group) {
                let border_group_class_name = {
                    'all_fields': 'for_all_fields',
                    'table_keys': 'for_table_keys',
                    'simple_fields': 'for_simple_fields',
                    'FRAH': 'for_fraht',
                    'RAIL': 'for_rail',
                    'AUTO': 'for_auto',
                    'table_fields_other': 'for_other'
                };
                return border_group_class_name[cur_group];
            },
            setBorders: function (real_group, target_group) {
                for (let border_type in this.borders) {
                    if (border_type === this.getBorderField(target_group)) {
                        this.borders[border_type] = 'is-valid-div';
                        if (target_group !== real_group && target_group !== 'all_fields') {
                            this.borders[border_type] = 'is-invalid-div';
                        }                    }
                    else {
                        this.borders[border_type] = '';
                    }
                }
            },
            clearBorders: function() {
                this.borders.for_all_fields = '';
                this.borders.for_table_keys = '';
                this.borders.for_simple_fields = '';
                this.borders.for_fraht = '';
                this.borders.for_rail = '';
                this.borders.for_auto = '';
                this.borders.for_other = '';
            },
            removeFromGroupTo: function(evt_end) {
                let evt_move = this.evt_data;
                let moved_element = evt_move.draggedContext.element;
                let from_group = this.data_list[evt_end.to.parentElement.id];
                let index = 0;
                for (let element in from_group) {
                    if (from_group[element]['field']['code'] === moved_element.field.code) {
                        this.data_list[evt_end.to.parentElement.id].splice(index, 1);
                    }
                    index++;
                }
                return true;
            },
            addToGroupFrom: function() {
                let moved_element = this.evt_data.draggedContext.element;
                this.data_list[this.evt_data.from.parentElement.id].push(moved_element);
            },
            revertData:function(evt_end) {
                this.removeFromGroupTo(evt_end);
                this.addToGroupFrom();
            },
            isChangeNeeded:function () {
                return this.evt_data.draggedContext.element.group === this.evt_data.to.parentElement.id
                    || this.evt_data.to.parentElement.id === 'all_fields';
            },
            onEnd: function(evt) {
                if (!this.isChangeNeeded()) {
                    this.revertData(evt);
                }
                this.clearBorders();
            },
            onMove: function(evt) {
                this.setBorders(evt.draggedContext.element.group, evt.to.parentElement.id);
                this.evt_data = evt;
                if (evt.draggedContext.element.group !== evt.to.parentElement.id
                    && evt.to.parentElement.id !== 'all_fields') {
                    return false;
                }

                return true;
            },
            getAbsoluteIndex: function (group) {
                let absolute_index = 0;
                for (let field_group in this.data_list) {
                    if (field_group === 'all' || field_group === 'all_fields') {
                        continue;
                    }
                    if (field_group === group.name) {
                        return absolute_index;
                    }
                    absolute_index += this.data_list[field_group].length;
                }
            }
        },
        mounted() {
            this.form._csrf = this.$refs.config.dataset.csrf;
            this.set_link = this.$refs.config.dataset.set_link;
            this.order_field = this.$refs.config.dataset.order_field;
            this.get_link = this.$refs.config.dataset.get_link;
            this.groups = JSON.parse(this.$refs.config.dataset.groups);
            this.load();
        }
    });

});