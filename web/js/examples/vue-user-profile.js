document.addEventListener("DOMContentLoaded", function() {
    Vue.directive('mask', VueMask.VueMaskDirective);
    const user_profile = new Vue({
        el: "#user_profile",
        mixins: [listMixins, validatorMixins],
        data: {
            form: {
                id: null,
                first_name: "",
                last_name: "",
                middle_name: "",
                phone: "",
                new_password: "",
                old_password: "",
                new_password_confirm: "",
                email: ""
            },
            is_employee_profile: false,
            is_new_password_set: false,
            error_list: {},
            user_id: null,
            get_link: '/config-api/get-user-profile',
            add_link: '/config-api/add-user-profile',
            _csrf: null,
        },
        mounted() {
            this.error_list = {};
            var dataset = this.$refs.config.dataset;
            this.user_id = JSON.parse(dataset.user_id);
            this.is_employee_profile = JSON.parse(dataset.is_employee_profile);
            this._csrf = dataset.csrf;
            this.loadUserProfile();
        },
        methods: {
            validate: function() {
                this.error_list = {};

                if (this.form.new_password !== this.form.new_password_confirm) {
                    this.error_list['new_password'] = 'Введенные пароли различаются!';
                    this.error_list['new_password_confirm'] = 'Введенные пароли различаются!';
                    return false;
                }

                let config = {
                    required: {
                        fields: ['last_name', 'first_name', 'middle_name', 'email', 'phone'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    min: {
                        fields: ['last_name', 'first_name', 'middle_name', 'email'],
                        min: 2,
                    },
                    max: {
                        fields: ['last_name', 'first_name', 'middle_name', 'email'],
                        max: {last_name: 30, first_name: 30, middle_name: 30, email: 60},
                    },
                    email: {
                        fields: ['email'],
                        message: 'Неправильный формат почты',
                    },
                    phone: {
                        fields: ['phone'],
                        message: 'Неправильный формат телефона (+#(###)###-##-##)',
                    },
                    userName: {
                        fields: ['first_name', 'last_name', 'middle_name'],
                        message: 'Поле Контактное лицо должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 2-х символов и больше 88.',
                    },
                };

                return this.validateForm(config, this.form);
            },
            addItem: function() {
                this.form._csrf = this._csrf;
                this.createSwal("Идет сохранение", "info", 60000);
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                return axios.post(this.add_link, this.form)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = {};
                            if (typeof response.data.errors[0] === "string") {
                                this.createSwal(response.data.errors[0], "error", 3000);
                                return;
                            }
                            for (var key in response.data.errors[0]) {
                                this.error_list[key] = response.data.errors[0][key];
                            }
                            this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                        } else {
                            this.createSwal("Сохранено", "success", 1500);
                            this.error_list = {};
                            swal.close();
                            if (this.is_employee_profile) {
                                window.location.replace('/config');
                            } else if (response.data.content.is_new_password_set) {
                                this.createSwal("Пароль успешно изменён", "success", 1500);
                                this.error_list = {};
                                swal.close();
                                this.clearFormPassword();
                            }
                        }
                    });
            },
            clearFormPassword() {
                this.form.new_password = "";
                this.form.old_password = "";
                this.form.new_password_confirm = "";
            },
            loadUserProfile: function () {
                var self = this;
                self.createSwal("Ожидайте загрузки данных", "info", 60000);
                return axios.post(self.get_link, {_csrf: self._csrf, id: self.user_id})
                    .then(function (response) {
                        self.form = response.data.content;
                        swal.close();
                    });
            },
            getErrorClass: function(field) {
                return this.getErrorText(field) === "" ? "col-md-12" : "col-md-12  border border-danger";
            },
            getErrorText: function(field) {
                if (this.getError(field).length === 0) {
                    return "";
                }
                return this.getError(field)[0];
            }
        }

    });
});