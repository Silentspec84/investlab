"use strict";

document.addEventListener("DOMContentLoaded", function() {

    const field_config = new Vue({
        el: "#field_config",
        mixins: [listMixins, validatorMixins],
        data: {
            data_list: [],
            type_const: {
                type: 'Nubmer',
                default: null
            },
            _csrf: null,
            currency_id: 0,
            currencies: {},
            types: {},
            data_types: {},
            data_deliveries: {},
            drop_link: '/field-config-api/delete',
            add_link: '/field-config-api/save',
            get_order_link: '/field-config-api/get',
            form: {},
            new_item_form: {},
            error_list: {},
            loaded_sort_orders: [], // содержит порядок полей при загрузке. Нужен, чтобы запомнить какой изначально порядок был у поля, когда поля нужно поменять местами.
            error_type_duplicate_field: null
        },

        methods: {
            currencyTitle: function(id) {
                for(let cur of this.currencies) {
                    if(cur['id'] == id) {
                        return cur['title'];
                    }
                }
                return '';
            },
            typeTitle: function(id) {
                for(let type of this.types) {
                    if(type['id'] == id) {
                        return type['title'];
                    }
                }
                return '';
            },
            dataTypeTitle: function(item) {
                if(item.data.type != this.type_const) {
                    return 'не применимо';
                }
                for(let type of this.data_types) {
                    if(type['id'] == item.data.data_type) {
                        return type['title'];
                    }
                }
                return '';
            },
            dataDeliveriesTitle: function(item) {
                if(item.data.type_of_delivery == undefined) {
                    return 'не применимо';
                }

                return this.data_deliveries[item.data.type_of_delivery] ? this.data_deliveries[item.data.type_of_delivery]['title'] : '';
            },
            formulaTitle: function(data) {
                if(data.type == this.type_const) {
                    return 'не применимо';
                }
                if(data.formula_title) {
                    return data.formula_title;
                }

                return 'формула не задана';
            },
            getClass: function(field) {
                return 'form-control form-control-sm' + (this.getError(field) ? ' is-invalid' : '');
            },
            validate: function(status) {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['full_title', 'title', 'code'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    min: {
                        fields: ['full_title', 'title', 'code'],
                        min: 3,
                    },
                    max: {
                        fields: ['full_title', 'title', 'code'],
                        max: {full_title: 100, title: 50, code: 10},
                    },
                };

                return this.validateForm(config, this.new_item_form);
            },
            getErrorText: function(field) {
                if (this.getError(field).length == 0) {
                    return '';
                }
                return this.getError(field)[0];
            },
            addNewItem: function() {
                this.data_list.push({
                    edit: true,
                    is_new: true,
                    data: {
                        full_title: '',
                        title: '',
                        code: '',
                        type: 1,
                        data_type: 0,
                        type_of_delivery: '',
                        currency_id: 0,
                        is_shown: true,
                        is_key: false,
                        is_geo_point: false,
                        is_simple_field: false,
                        formula_title: null
                }});
            },
            setCurrencyIdForItem(item, code) {
                if (this.data_deliveries[code]) {
                    this.$set(item.data, 'currency_id', this.data_deliveries[code].currency_id);
                }
            },
            startEdit: function(item) {
                item.old_data = Object.assign({}, item.data);
                item.edit = true;
            },
            cancelEdit: function(item, index) {
                item.edit = false;
                this.error_list = {};
                if(item.hasOwnProperty('old_data')) {
                    item.data = item.old_data;
                }
                if(item.is_new) {
                    this.data_list.splice(index, 1);
                }
            },
            saveItem: function(item) {
                this.new_item_form = item;
                this.addItem();
            },
            addItem: async function (item) {
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить переменную! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                this.new_item_form.is_shown = parseInt(this.new_item_form.is_shown);
                this.new_item_form.is_geo_point = parseInt(this.new_item_form.is_geo_point);
                this.new_item_form.is_key = parseInt(this.new_item_form.is_key);
                this.new_item_form.is_simple_field = parseInt(this.new_item_form.is_simple_field);
                this.new_item_form._csrf = this.form._csrf;
                this.createSwal("Идет сохранение...", "info", 60000);
                const response = await axios.post(this.add_link, this.new_item_form);
                if (!response.data.is_success) {
                    this.error_list = {};
                    for (let key in response.data.errors[0]) {
                        this.error_list[key] = response.data.errors[0][key];
                    }
                    this.createSwal("Невозможно сохранить переменную! Заполните выделенные красным поля.", "error", 3000);
                } else {
                    for (var key in this.error_list) {
                        this.error_list[key] = null;
                    }
                    for (var key in this.new_item_form) {
                        this.new_item_form[key] = null;
                    }
                    this.createSwal("Изменения сохранены!", "success", 3000);
                    await this.load();
                }
            },
            load: async function() {
                this.createSwal("Данные загружаются...", "info", 60000);
                const response = await axios.get(this.get_order_link);

                let data = response.data.content;
                this.data_list = data.map(item => new Object({edit: false, is_new: false, data: item}));
                this.loaded_sort_orders = [];
                for (let i = 0; i < this.data_list.length; i++) {
                    let sort_objects = [];
                    if (typeof this.data_list[i] === "undefined") {
                        continue;
                    }
                    for (let j = 0; j < this.data_list[i].data.sort_order.length; j++) {
                        if (typeof this.data_list[i].data.sort_order[j] === "undefined") {
                            continue;
                        }
                        let sort_object = {field_title: this.data_list[i].data.sort_order[j].id, field_order: this.data_list[i].data.sort_order[j].value};
                        sort_objects.push(sort_object);

                        let field_id = this.data_list[i].data.id;
                        this.loaded_sort_orders[field_id] = sort_objects;
                    }
                }
                swal.close();
            },
            async onLoad() {
                this.form._csrf = this.$refs.config.dataset.csrf;
                await this.load();
            },
        },
        mounted: async function () {
            try {
                this._csrf = this.$refs.config.dataset.csrf;
                this.currencies = JSON.parse(this.$refs.config.dataset.currencies);
                this.types = JSON.parse(this.$refs.config.dataset.types);
                this.data_types = JSON.parse(this.$refs.config.dataset.data_types);
                this.type_const = JSON.parse(this.$refs.config.dataset.type_const);
                this.data_deliveries = JSON.parse(this.$refs.config.dataset.data_deliveries);
                this.error_type_duplicate_field = JSON.parse(this.$refs.config.dataset.error_type_duplicate_field);
                await this.onLoad();
            }
            catch(e){}
        }

    });
});
