"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.directive('ApexCharts', ApexCharts);

    new Vue({
        el: "#accounts",
        mixins: [listMixins, validatorMixins],
        components: {
            apexchart: VueApexCharts,
        },
        data: {
            _csrf: null,
            id: null,
            account: {},
            get_account_link: '/accounts-api/get-account',
            error_list: {},
        },
        methods: {
            load: function() {
                let self = this;
                self.createSwal("Идет загрузка данных...", "info", 3000);
                return axios.post(this.get_account_link, {_csrf: this._csrf, id: this.id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.account = response.data.content.account;
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
            switchTableView: function() {
                console.log('sdfsdf');
                this.table_view = !this.table_view;
            },
            getUserAvatar: function(account) {
                if (!account.user_data.avatar) {
                    return 'images/avatar/unknown.png'
                }
                return account.user_data.avatar;
            },
            closeModal: function() {
                this.new_account_form = {};
                this.error_list = {};
            },
            all_accounts_state: function () {
                this.all_accounts = true;
                this.my_accounts = false;
                this.favourites = false;
            },
            my_accounts_state: function () {
                this.all_accounts = false;
                this.my_accounts = true;
                this.favourites = false;
            },
            favourites_state: function () {
                this.all_accounts = false;
                this.my_accounts = false;
                this.favourites = true;
            },
            getStateClass: function (nav_element) {
                if (nav_element === 'all_accounts' && this.all_accounts) {
                    return 'nav-link active';
                }
                if (nav_element === 'my_accounts' && this.my_accounts) {
                    return 'nav-link active';
                }
                if (nav_element === 'favourites' && this.favourites) {
                    return 'nav-link active';
                }
                return 'nav-link';
            },
            getStateTitle: function () {
                if (this.all_accounts) {
                    return 'Все счета';
                }
                if (this.my_accounts) {
                    return 'Мои счета';
                }
                if (this.favourites) {
                    return 'Избранное';
                }
            },
            validateAccountForm: function() {
                this.error_list = {};
                this.new_account_form.account = Number(this.new_account_form.account);

                let config = {
                    required: {
                        fields: ['acc_name', 'account', 'acc_type'],
                        message: 'Данное поле обязательно к заполнению!'
                    },
                    min: {
                        fields: ['acc_name', 'account'],
                        min: {acc_name: 3, account: 3}
                    },
                    max: {
                        fields: ['acc_name', 'account'],
                        max: {acc_name: 50, account: 12}
                    },
                    isInteger: {
                        fields: ['account'],
                        message: 'Данное поле должно быть целым числом!'
                    },

                };

                return this.validateForm(config, this.new_account_form);
            },
            saveAccount: function () {
                if (!this.validateAccountForm()) {
                    this.createSwal("Невозможно сохранить счет! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                swal('Идет создание счета', {
                    buttons: false,
                    icon: 'info',
                    timer: 300000,
                });
                this.new_account_form._csrf = this._csrf;
                return axios.post(this.create_account_link, this.new_account_form)
                    .then(response => {
                        this.error_list = {};
                        if (!response.data.is_success) {
                            swal.close();
                            for (let key in response.data.errors[0]) {
                                this.error_list[key] = response.data.errors[key];
                            }
                            swal('Ошибка регистрации!', {
                                buttons: false,
                                icon: 'error',
                                timer: 3000,
                            });
                        } else {
                            swal.close();
                            let user_account = response.data.content.user_account;
                            this.accounts.inactive_user_accounts = {user_account};
                            swal('Регистрация прошла успешно!', {
                                buttons: false,
                                icon: 'success',
                                timer: 3000,
                            });
                        }
                    });
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.id = this.$refs.config.dataset.id;
            this.load();
        }

    });
});
