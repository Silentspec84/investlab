/**
 * Mixin полезных функций для работы с объектами и массивами в js.
 * Постоянно дополняется...
 */

export const arrayObjectMixins = {
    methods: {
        isEmptyObject: function(obj) {
            for (let i in obj) {
                if (obj.hasOwnProperty(i)) {
                    return false;
                }
            }
            return true;
        },
        isEmptyArray: function(array) {
            return array.length === 0;
        },
        copyObject: function (obj) {
            return JSON.parse(JSON.stringify(obj));
        }
    }
};