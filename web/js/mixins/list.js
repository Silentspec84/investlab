export const listMixins = {
    methods: {
        getError: function (field, id = false) {
            var result = '';

            if (id === false && (field in this.error_list)) {
                result = this.error_list[field];
            } else if (id in this.error_list && (field in this.error_list[id])) {
                result = this.error_list[id][field];
            }
            $('[data-toggle="tooltip"]').tooltip();

            return result;
        },
        getWarning: function (field, id = false) {
            var result = '';
            if (id === false && (field in this.warning_list)) {
                return this.warning_list[field];
            }
            if (id in this.warning_list && (field in this.warning_list[id])) {
                return this.warning_list[id][field];
            }

            return result;
        },
        load: function () {
            var self = this;
            axios.get(self.get_link)
                .then(function (response) {
                    self.data_list = response.data.content;
                });
        },

        saveAll: function () {
            var self = this;
            self.form.data_list = self.data_list;
            return axios.post(self.save_link, self.form)
                .then(function (response) {
                    if (!response.data.is_success) {
                        self.error_list = {};
                        for (var key in response.data.errors) {
                            self.error_list[key] = {};
                            for (var field in response.data.errors[key]) {
                                self.error_list[key][field] = response.data.errors[key][field];
                            }
                        }
                    } else {
                        self.data_list.forEach(function (item) {
                            self.error_list[item.id] = {};
                            for (var field in item) {
                                self.error_list[item.id][field] = false;
                            }
                        });
                        swal("Сохранено", {
                            buttons: false,
                            timer: 1000,
                        });
                        self.load();
                    }
                });
        },

        showDropDialog(id) {
            var self = this;

            swal({
                className: "drop-clients",
                title: "Удалить позицию?",
                text: "Удалённая информация не может быть восстановлена",
                buttons: {
                    confirm: {
                        text: "Удалить",
                        value: true,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true
                    },
                    cancel: {
                        text: "Отмена",
                        value: null,
                        visible: true,
                        className: "btn btn-secondary",
                        closeModal: true,
                    }
                }
            }).then((willDelete) => {
                if (willDelete) {
                    self.dropItem(id);
                }
            });

        },

        dropItem: function (id) {
            var self = this;
            axios.post(self.drop_link, {id: parseInt(id), _csrf: this._csrf})
                .then(function (response) {
                    if (!response.data.is_success) {
                        self.error_list = {};
                        for (var key in response.data.errors) {
                            self.error_list[key] = response.data.errors[key];
                        }
                    } else {
                        swal("Удалено", {
                            buttons: false,
                            timer: 3000,
                        });
                        self.load();
                    }
                    if (self.redirect_link_after_drop) {
                        window.location.href = self.redirect_link_after_drop;
                    }
                });
        },

        switchItem:function (id) {
            var self = this;
            axios.get(self.switch_link + '?id=' + id)
                .then(function (response) {
                    self.data_list = response.data;
                });
        },

        addItem() {
            var self = this;
            self.new_item_form._csrf = self.form._csrf;
            return axios.post(self.add_link, self.new_item_form)
                .then(function (response) {
                    if (!response.data.is_success) {
                        self.error_list = {};
                        for (var key in response.data.errors[0]) {
                            self.error_list[key] = response.data.errors[0][key];
                        }
                    } else {
                        for (var key in self.error_list) {
                            self.error_list[key] = null;
                        }
                        for (var key in self.new_item_form) {
                            self.new_item_form[key] = null;
                        }
                        swal("Сохранено", {
                            buttons: false,
                            timer: 1500,
                        });
                        self.load();
                    }
                });
        },

        createSwal: function (title, icon_style, timer) {
            swal(title, {
                buttons: false,
                icon: icon_style,
                timer: timer,
            });
        },

        closeSwal: function(response) {
            swal.close();
        },

        onLoad() {
            this._csrf = this.$csrfToken;
            this.load();
        },
        /**
         * Рендерит ячейку либо с разбивкой по блокам если в формате JSON
         * либо её содержимое
         *
         * @param value
         * @returns {*}
         */
        renderCell: function(value) {
            if (typeof value === 'string') {
                try {
                    value = JSON.parse(value);
                } catch(e) {
                    return value;
                }
            }

            if (typeof value !== 'object') {
                return value;
            }

            const result = this.getValuesWithCodes(value);
            // приводим результат к строке. Если больше одного элемента в массиве
            // ставим между ними знак +
            if (result.length >= 1) {
                return result.join(" + ");
            }

            return '';
        },

        /**
         * @param values
         * @returns {Array}
         * [value1 with code, value2 with code,]
         */
        getValuesWithCodes: function(values) {
            let result = [];
            for (let code in values) {
                result.push(values[code] + " " + code);
            }

            return result;
        },
    }
}
