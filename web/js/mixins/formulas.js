/**
 * Миксин для работы с формулами на страницах (например поиска, корзины, кп, заявок, итоговой таблицы)
 */
import {stringMixins} from './string';
import {calculateRpnFormulaMixins} from './calculate-rpn-formula';

export const formulasMixins = {
    mixins: [stringMixins, calculateRpnFormulaMixins],
    data: function () {
        return {
            headers_rows: null,
            calculate_rows: null,
            /**
             *  Массив вида [
             *      result_formula_order1 => [
             *          'formula' => '10 4 5 + *',   //записанные в обратной польской нотации, где цифры это номер колонки в таблице
             *          'is_consider_currency': false  // Учитывать валюту? т.е. поддерживает мультивалютность
             *          'is_convert_result_to_rub': false // Конвертировать результирующее поле формулы в рубли? Работает если is_consider_currency: true
             *      ] ,
             *         ...
             *      result_formula_orderN => [
             *          'formula' => '10 4 5 + * 2 +',
             *          'is_consider_currency': false
             *          'is_convert_result_to_rub': false
             *      ]
             *  ]
             **/
            calculate_formulas: null,
            not_exist_value: '-', // должны быть в компоненте к которому будет подмешен миксин, символ для обозначения не существующего значения
        };
    },
    methods: {
        getCalculateRows(headers_rows, rows, formulas) {
            if (!headers_rows) {
                this.setErrorInErrorList("Произошла ошибка. Не найдены заголовки строк, для подсчета формул!");
                return [];
            }
            this.headers_rows = headers_rows;
            if (!rows) {
                this.setErrorInErrorList("Произошла ошибка. Не найдены строки, для подсчета формул!");
                return [];
            }
            this.calculate_rows = rows;
            if (!formulas) {
                this.setErrorInErrorList("Произошла ошибка. Не найдены формулы!");
                return [];
            }

            this.calculate_formulas = formulas;
            this.calculateFormulas();

            return this.calculate_rows;
        },
        setErrorInErrorList(error_msg) {
            let html_error_msg = "<div class=\"alert alert-danger alert-dismissible fade show\">" + error_msg + "</div>";
            this.form_errors !== "undefined" ? this.form_errors.push(html_error_msg) : alert(error_msg);
        },
        /**
         * Пересчитывает формулы для this.calculate_rows
         * */
        calculateFormulas() {
            if (!this.calculate_rows || !this.calculate_formulas || !this.not_exist_value || !this.currencies) {
                if (this.form_errors !== "undefined") {
                    this.form_errors = [];
                    return this.form_errors.push("<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Не хватает данных, для подсчета формул!</div>");
                } else {
                    return alert('Произошла ошибка. Не хватает данных, для подсчета формул!');
                }
            }

            let sorted_formulas = this.getSortedFormulas();
            for (let i in sorted_formulas) {
                let formula_operands_and_operators = sorted_formulas[i]['formula'].split(' ');
                this.calculateFormula(formula_operands_and_operators, sorted_formulas[i].result);
            }
        },
        /**
         * Перекладывает формулы с бэка из объекта в массив и сортирует этот массив методом sortFormulas,
         * чтобы "подытоги" вычислялись раньше "итогов".
         * @returns {*}
         */
        getSortedFormulas() {
            let formulas = [];
            for (let result_formula_order in this.calculate_formulas) { // перекладываем формулы из объекта в массив
                this.calculate_formulas[result_formula_order].result = result_formula_order;
                formulas.push(this.calculate_formulas[result_formula_order]);
            }

            return this.sortFormulas(formulas);
        },
        /**
         * Сортирует формулы, чтобы подытоги вычислялись раньше итогов:
         * Если в слагаемых формулы_1 используется результат формулы_2,
         * то формула_2 должна быть вычеслена раньше, чем формула_1.
         * Сортировка происходит в соотвествии с этим принципом.
         * @param formulas
         * @returns {*}
         */
        sortFormulas(formulas) {
            let max_iterations = 100; // защитимся от впадания в бесконечный цикл
            let iteration = 1;
            let is_sort_formula_needed = true;
            let temp; // меняем формулы местами через эту переменную
            while (is_sort_formula_needed && iteration <= max_iterations) {
                for (let i = 0; i < formulas.length; i++) {
                    is_sort_formula_needed = false;
                    let dependent_formula_index = this.isDependentFormula(formulas, formulas[i].result, i);
                    if (dependent_formula_index !== false) {
                        temp = formulas[dependent_formula_index];
                        formulas[dependent_formula_index] = formulas[i];
                        formulas[i] = temp;
                        is_sort_formula_needed = true;
                    }
                }
                iteration++;
            }
            return formulas;
        },
        /**
         * Вернет true, если в слагаемых формулы используется результат другой формулы
         * @param formulas
         * @param formula_result
         * @param current_index
         * @returns {*}
         */
        isDependentFormula(formulas, formula_result, current_index) {
            for (let i = 0; i < formulas.length; i++) {
                let formula_parts = formulas[i].formula.split(' ');
                if (current_index > i && formula_parts.indexOf(formula_result) !== -1) {
                    return i;
                }
            }
            return false;
        },
        /**
         * Считывает результат по формуле и записывает во ВСЕ строки
         * */
        calculateFormula(formula_operands_and_operators, result_formula_order) {
            for (let num_row = 0; num_row < this.calculate_rows.length; num_row++) {
                if (typeof this.calculate_rows[num_row][result_formula_order] === 'undefined') {
                    return;
                }
                let original_formula = Object.assign([], formula_operands_and_operators);
                if (this.calculate_formulas[result_formula_order]['is_consider_currency']) {
                    this.calculateFormulaForRowWithConsiderCurrency(num_row, formula_operands_and_operators, result_formula_order);
                } else {
                    this.calculateFormulaForRow(num_row, formula_operands_and_operators, result_formula_order);
                }
                formula_operands_and_operators = Object.assign([], original_formula);
            }
        },
        /**
         * Считывает результат по формуле и записывает в ОДНУ конкретную строку С УЧЕТОМ валюты
         * */
        calculateFormulaForRowWithConsiderCurrency(num_row, formula_operands_and_operators, result_formula_order) {
            let formula_currencies = {};
            let type_of_delivery = '';
            let currency_sign = '₽';

            for (let i in formula_operands_and_operators) {
                let operand_or_operator = formula_operands_and_operators[i];

                if (!this.isNumeric(operand_or_operator)) {
                    continue;
                }

                type_of_delivery = this.headers_rows[operand_or_operator]['type_of_delivery'] ? this.headers_rows[operand_or_operator]['type_of_delivery'] : '';
                currency_sign = this.headers_rows[operand_or_operator]['currency_sign'] ? this.headers_rows[operand_or_operator]['currency_sign'] : '₽';
                if (typeof formula_currencies[type_of_delivery] === 'undefined') {
                    formula_currencies[type_of_delivery] = {};
                }
                if (typeof formula_currencies[type_of_delivery]['currencies_values'] === 'undefined') {
                    formula_currencies[type_of_delivery]['currencies_values']  = {};
                }
                if (typeof formula_currencies[type_of_delivery]['has_not_exist_value'] === 'undefined') {
                    formula_currencies[type_of_delivery]['has_not_exist_value']  = false;
                }
                if (typeof formula_currencies[type_of_delivery]['currencies_values'][currency_sign] === 'undefined') {
                    formula_currencies[type_of_delivery]['currencies_values'][currency_sign]  = 0;
                }
                if (typeof this.calculate_rows[num_row][operand_or_operator] === 'undefined'
                    || this.calculate_rows[num_row][operand_or_operator] === this.not_exist_value) {
                    formula_currencies[type_of_delivery]['has_not_exist_value'] = true;
                    formula_operands_and_operators[i] = '0';
                } else {
                    formula_operands_and_operators[i] = this.calculate_rows[num_row][operand_or_operator]; // заменяем порядок поля на его значение
                }

                if (formula_currencies[type_of_delivery]['currencies_values'][currency_sign] === 0) {
                    formula_currencies[type_of_delivery]['currencies_values'][currency_sign] = this.getFloatByString(formula_operands_and_operators[i]);
                    continue;
                }

                if (formula_operands_and_operators[i-1] === '+') {
                    formula_currencies[type_of_delivery]['currencies_values'][currency_sign] =  formula_currencies[type_of_delivery]['currencies_values'][currency_sign] + this.getFloatByString(formula_operands_and_operators[i]);
                } else if(formula_operands_and_operators[i-1] === '-') {
                    formula_currencies[type_of_delivery]['currencies_values'][currency_sign] =  formula_currencies[type_of_delivery]['currencies_values'][currency_sign] - this.getFloatByString(formula_operands_and_operators[i]);
                }
            }

            if (!formula_currencies) {
                return;
            }

            let result_values = this.getResultValuesForFormulaCurrency(formula_currencies);

            if (!result_values) {
                return this.calculate_rows[num_row][result_formula_order] = this.not_exist_value;
            }

            let result_value = '';
            if (this.calculate_formulas[result_formula_order]['is_convert_result_to_rub']) {
                result_value = this.getResultValueInRub(result_values);
            } else {
                result_value = this.getResultValueMultiCurrencies(result_values);
            }
            return this.calculate_rows[num_row][result_formula_order] = result_value;
        },
        /**
         * Получить результат из "валютной" формулы
         * {
         *      currency_sign1(₽): result_value1(100500)
         *          ...
         *      currency_signN($): result_valueN(500)
         * }
         *
         * */
        getResultValuesForFormulaCurrency(formula_currencies) { //todo CH-2619 Нужно делать проверку, что плечо присутствует в маршруте заданном в поиске. Те которые не пристутсвуют выкинуть из результат, если has_not_exist_value, то делать результат this.not_exist_value
            let result_values = {};
            for (let type_of_delivery in formula_currencies) {
                if (formula_currencies[type_of_delivery]['has_not_exist_value']) {
                    continue;
                }
                for (let currency_sign in formula_currencies[type_of_delivery]['currencies_values']) {
                    if (typeof result_values[currency_sign] === 'undefined'){
                        result_values[currency_sign] = 0;
                    }
                    result_values[currency_sign] = result_values[currency_sign] + formula_currencies[type_of_delivery]['currencies_values'][currency_sign];
                }
            }

            return result_values;
        },
        /**
         * Получить мультивалютный результат формулы,
         * Пример 100500 ₽ + 500$
         * */
        getResultValueMultiCurrencies(result_values) {
            let result_value = '';
            for (let currency_sign in result_values) {
                let operator = ' + ';
                if (result_values[currency_sign] < 0) {
                    operator = ' - ';
                }
                if (!result_value) {
                    result_value = result_values[currency_sign] + ' ' + currency_sign;
                    continue;
                }

                result_value = result_value + operator + Math.abs(result_values[currency_sign]) + ' ' + currency_sign;
            }

            return result_value;
        },
        /**
         * Получить результат формулы конвертированный в рубли,
         * Пример 133000 ₽
         * */
        getResultValueInRub(result_values) {
            let rub_sign = '₽';
            let result_value = 0;
            for (let currency_id in this.currencies) {
                let currency_code = this.currencies[currency_id]['code'] ? this.currencies[currency_id]['code'] : null;
                if (!currency_code || !result_values[currency_code]) {
                    continue;
                }
                if (result_values[currency_code] && currency_code !== rub_sign) {
                    result_values[currency_code] = parseFloat(result_values[currency_code]) * parseFloat(this.currencies[currency_id]['rate'])
                }

                result_value = result_value + parseFloat(result_values[currency_code]);
            }
            return result_value + ' ' + rub_sign;
        },
        /**
         * Считывает результат по формуле и записывает в ОДНУ конкретную строку БЕЗ учета валюты
         * */
        calculateFormulaForRow(num_row, formula_operands_and_operators, result_formula_order) {
            let formula_with_replace_orders_by_values = this.getFormulaWithReplaceOrdersByValues(num_row, formula_operands_and_operators);
            if (!formula_with_replace_orders_by_values) {
                this.calculate_rows[num_row][result_formula_order] = this.not_exist_value;
                return;
            }
            let rpn_formula = formula_with_replace_orders_by_values.join(' ');

            this.calculate_rows[num_row][result_formula_order] = this.calculateRpnFormula(rpn_formula);
        },
        /**
         *  Возвращает формулу с замененным порядком полей на их значения
         * */
        getFormulaWithReplaceOrdersByValues(num_row, formula_operands_and_operators) {
            for (let i in formula_operands_and_operators) {
                let operand_or_operator = formula_operands_and_operators[i];
                if (this.isNotExistValueForOperand(num_row, operand_or_operator)) {
                    return null;
                }
                if (this.isNumeric(operand_or_operator)) { // operand_or_operator является операндом
                    formula_operands_and_operators[i] = this.calculate_rows[num_row][operand_or_operator]; // заменяем порядок поля на его значение
                }
            }

            return formula_operands_and_operators;
        },
        /**
         * Если операнда не существует или он равен this.not_exist_value
         * */
        isNotExistValueForOperand(num_row, operand_or_operator) {
            return this.isNumeric(operand_or_operator)
                && (typeof this.calculate_rows[num_row][operand_or_operator] === 'undefined'
                    || this.calculate_rows[num_row][operand_or_operator] === this.not_exist_value);
        }
    }
};