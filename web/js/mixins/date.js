import moment from 'moment';

export const dateMixins = {
    methods: {
        /**
         * Преобразует timestamp в date
         *
         * @param format //Пример 'DD.MM.YYYY'
         * @param timestamp
         * @returns string
         */
        date: function(format, timestamp) {
            if (!timestamp) {
                return '';
            }

            return moment(timestamp * 1000).format(format);
        }
    }
};