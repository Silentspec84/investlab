<?php

namespace app\assets;

use yii\web\AssetBundle;

class SocialProfilesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        "js/pages/profiles.js",
        'https://cdn.jsdelivr.net/npm/apexcharts@latest',
        'https://cdn.jsdelivr.net/npm/vue-apexcharts'
    ];
    public $depends = [
        'app\assets\AppAsset'
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
