<?php

namespace app\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{
    public $js = [
        "js/core/vue.js",
        "js/core/vuex.min.js",
        "js/core/vue-router.js",
        "js/core/vue-cookies.js",
        "js/core/vue-multiselect.min.js",
        "js/core/vue-draggable.min.js",
        "js/core/app.js",
        "js/core/init.js",
        "js/mixins/validator.js",
        "js/plugins/moment.min.js",
        "js/plugins/sortable.min.js",
        "js/plugins/sweetalert/dist/sweetalert.min.js",
        "js/plugins/axios/dist/axios.min.js",
        "js/plugins/popper.min.js",
        "js/plugins/bootstrap.min.js",
    ];

    public $css = [
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

}
