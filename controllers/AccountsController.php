<?php


namespace app\controllers;


use app\components\AccessRule;
use app\models\User;
use app\models\UserAccount;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;

class AccountsController extends BaseController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }

    /**
     * @return string
     * @throws InvalidParamException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view', ['account_id' => $id]);
    }

    public static function getJsConfig()
    {
        return [
            'ACCOUNT_TRADE_MODE_DEMO'  => UserAccount::ACCOUNT_TRADE_MODE_DEMO,
            'ACCOUNT_TRADE_MODE_CONTEST'  => UserAccount::ACCOUNT_TRADE_MODE_CONTEST,
            'ACCOUNT_TRADE_MODE_REAL'  => UserAccount::ACCOUNT_TRADE_MODE_REAL,
            'STATUS_ACTIVE' => User::STATUS_ACTIVE,
            'STATUS_INACTIVE' => User::STATUS_INACTIVE,
            'STATUS_UNCONFIRMED' => User::STATUS_UNCONFIRMED,
            'ROLE_ADMIN' => User::ROLE_ADMIN,
            'ROLE_MODERATOR' => User::ROLE_MODERATOR,
            'ROLE_USER' => User::ROLE_USER,
        ];
    }
}