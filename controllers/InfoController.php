<?php


namespace app\controllers;


use app\components\AccessRule;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;

class InfoController extends BaseController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'docs'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'docs', 'support'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }

    /**
     * @return string
     * @throws InvalidParamException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     * @throws InvalidParamException
     */
    public function actionDocs()
    {
        return $this->render('index');
    }

    /**
     * @return string
     * @throws InvalidParamException
     */
    public function actionSupport()
    {
        return $this->render('index');
    }
}