<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\web\Response;

class SiteController extends BaseController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'error', 'recover', 'register-success', 'confirm-email', 'recovery'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'error', 'logout', 'recover', 'register-success', 'confirm-email', 'recovery'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }

    /**
     * Страница счетов по умолчанию
     *
     * @return string
     * @throws InvalidParamException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Форма восстановления пароля, отсылка письма со ссылкой на восстановление
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionRecover()
    {
        $this->layout = 'main';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $message = '';
        $recover_form = new RecoverForm();

        if ($recover_form->load(Yii::$app->request->post()) && $recover_form->setRecoveryKey()) {
            $user = $recover_form->getUser();
            if ($user->status === User::STATUS_BANNED || $user->role === User::ROLE_UNCONFIRMED_CLIENT) {
                $message = 'Ваш аккаунт не активен, восстановление пароля невозможно. Вероятно, вы были заблокированы администрацией. Пожалуйста, обратитесь в техподдержку.';
                return $this->render('forms/recover_message', [
                    'message' => $message,
                    'is_success' => false
                ]);
            }

            $content['link'] = Url::to('recovery?key='.$user->recovery_key, true);
            $content['user'] = $user->login_name;

            Yii::$app->mailer->compose('recovery', ['content' => $content])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject('Восстановление пароля InvestLab.ru')
                ->send();

            return $this->render('recover_success', [
                'recover_form' => $recover_form,
                'message' => $message
            ]);
        }

        return $this->render('forms/recover', [
            'recover_form' => $recover_form,
            'message' => $message
        ]);
    }

    /**
     * Восстановление пароля
     * @param bool $key
     * @return string
     */
    public function actionRecovery($key = false)
    {
        $is_success = false;
        $message = 'Срок действия ключа восстановления истёк';

        /** @var User|null $user */
        $user = $key ? User::findOne(['recovery_key' => $key]) : null;
        $password = User::generatePassword();

        if ($user && $user->setPassword($password)->save()) {
            $message = "Ваш новый пароль - $password" . ". Вы сможете установить свой пароль из личного кабинета.";
            $is_success = true;

            $content['user'] = $user->login_name;
            $content['password'] = $password;
            Yii::$app->mailer->compose('welcome', ['content' => $content])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject('Новый пароль для InvestLab.ru')
                ->send();
        }

        return $this->render('forms/recover_message', [
            'message' => $message,
            'is_success' => $is_success
        ]);

    }

    /*
     * Успешная регистрация
     */
    public function actionRegisterSuccess()
    {
        if (Yii::$app->user->isGuest && Yii::$app->request->cookies->get(SiteApiController::NEW_REGISTRATION_SESSION_ID)) {
            Yii::$app->response->cookies->remove(SiteApiController::NEW_REGISTRATION_SESSION_ID);

            return $this->render('forms/success_register');
        }

        $this->goHome();
    }



    public function actionConfirmEmail($hash)
    {
        $user = User::findOne(['hash' => $hash]);

        if (!$user) {
            Yii::$app->getSession()->setFlash('error', 'Пользователь с таким email не найден, email подтвердить не удалось.');
            return $this->render('confirm_email_fail');
        }

        $content['user'] = $user->login_name;
        $content['password'] = $user->unsafe_password;
        $user->setPassword($user->unsafe_password);
        $user->unsafe_password = '';

        Yii::$app->mailer->compose('welcome', ['content' => $content])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($user->email)
            ->setSubject('Добро пожаловать на InvestLab.ru')
            ->send();

        $user->status = User::STATUS_ACTIVE;
        $user->save();

        Yii::$app->user->login($user, 3600 * 24 * 30);

        return $this->render('confirm_email', ['user' => $user]);
    }

    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if ($exception->statusCode == 404) {
                return $this->render('error404', ['exception' => $exception]);
            }
            $error_message = 'На окружении произошла фатальная ошибка!'."\n";
            $error_message .= $exception->getFile().':'.$exception->getLine()."\n";
            $error_message .= 'Error code: '. $exception->getCode()."\n";
            $error_message .= 'Error msg: '.$exception->getMessage();

            return $this->render('error', ['message' => $exception, 'name' => 'Фатальная ошибка']);
        }
    }

}
