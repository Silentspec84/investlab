<?php

namespace app\controllers;

use app\models\AccountCloseOrder;
use app\models\AccountOpenOrder;
use app\models\User;
use app\models\UserAccount;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\Response;

class Mt4Controller extends Controller
{
    const RESPONSE_OK = 1;
    const RESPONSE_ERR_WRONG_LOGIN = 2;
    const RESPONSE_ERR_WRONG_PASSWORD = 3;
    const RESPONSE_ERR_NO_ACCOUNT = 4;
    const RESPONSE_FIRST_CONNECT = 5;
    const RESPONSE_ERR_SAVE_DATA = 6;

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter' => [
                'class' => VerbFilter::className(),
                'actions' => $this->verbs(),
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
            ],
            'rateLimiter' => [
                'class' => RateLimiter::className(),
            ],
        ];
    }

    /**
     * Авторизация и проверка пользователя и его счета
     *
     * @return int код результата запроса
     */
    public function actionAuth()
    {
        $request_params = Yii::$app->getRequest()->bodyParams;
        $user = User::find()->where(['login_name' => $request_params['login_name']])->one();
        if (!$user) {
            return self::RESPONSE_ERR_WRONG_LOGIN;
        }
        /** @var User $user */
        if (!$user->validatePassword($request_params['password'])) {
            return self::RESPONSE_ERR_WRONG_PASSWORD;
        }
        $account = UserAccount::find()->where(['account' => $request_params['account'], 'user_id' => $user->id])->one();
        if (!$account) {
            return self::RESPONSE_ERR_NO_ACCOUNT;
        }

        if (!$account->date_first_connected || $account->date_first_connected === 0) {
            return self::RESPONSE_FIRST_CONNECT;
        }

        return self::RESPONSE_OK;
    }

    /**
     * Первое подключение счета, заполнение всех данных о счете
     *
     * @return int код результата запроса
     */
    public function actionFirstConnect()
    {
        $request_params = Yii::$app->getRequest()->bodyParams;
        $user = User::find()->where(['login_name' => $request_params['login_name']])->one();
        if (!$user) {
            return self::RESPONSE_ERR_WRONG_LOGIN;
        }
        /** @var User $user */
        if (!$user->validatePassword($request_params['password'])) {
            return self::RESPONSE_ERR_WRONG_PASSWORD;
        }
        /** @var UserAccount $account */
        $account = UserAccount::find()->where(['account' => $request_params['account'], 'user_id' => $user->id])->one();
        if (!$account) {
            return self::RESPONSE_ERR_NO_ACCOUNT;
        }
        $account->fillAccountDataFirstTime($request_params);

        if (!$account->save()) {
            return self::RESPONSE_ERR_SAVE_DATA;
        }
        return self::RESPONSE_OK;
    }

    /**
     * Все последующие подключения счета, обновление данных о счете
     *
     * @return int код результата запроса
     */
    public function actionRefreshAccount()
    {
        $request_params = Yii::$app->getRequest()->bodyParams;
        $user = User::find()->where(['login_name' => $request_params['login_name']])->one();
        if (!$user) {
            return self::RESPONSE_ERR_WRONG_LOGIN;
        }
        /** @var User $user */
        if (!$user->validatePassword($request_params['password'])) {
            return self::RESPONSE_ERR_WRONG_PASSWORD;
        }
        /** @var UserAccount $account */
        $account = UserAccount::find()->where(['account' => $request_params['account'], 'user_id' => $user->id])->one();
        if (!$account) {
            return self::RESPONSE_ERR_NO_ACCOUNT;
        }

        $account->fillAccountData($request_params);

        if (!$account->save()) {
            return self::RESPONSE_ERR_SAVE_DATA;
        }
        return self::RESPONSE_OK;
    }

    public function actionHistory()
    {
        $request_params = Yii::$app->getRequest()->bodyParams;
        $user = User::find()->where(['login_name' => $request_params['login_name']])->one();
        if (!$user) {
            return self::RESPONSE_ERR_WRONG_LOGIN;
        }
        /** @var User $user */
        if (!$user->validatePassword($request_params['password'])) {
            return self::RESPONSE_ERR_WRONG_PASSWORD;
        }
        /** @var UserAccount $account */
        $account = UserAccount::find()->where(['account' => $request_params['account'], 'user_id' => $user->id])->one();
        if (!$account) {
            return self::RESPONSE_ERR_NO_ACCOUNT;
        }

        // Получаем массив сделок, которые еще не сохранены в базе
        $data = AccountCloseOrder::formOrdersDataForImport($request_params, $user->id, $account->id);

        // Если такое сделки есть, сохраняем их
        if ($data) {
            AccountCloseOrder::saveOrdersData($data);
        }

        return self::RESPONSE_OK;
    }

    public function actionOpened()
    {
        $request_params = Yii::$app->getRequest()->bodyParams;
        $user = User::find()->where(['login_name' => $request_params['login_name']])->one();
        if (!$user) {
            return self::RESPONSE_ERR_WRONG_LOGIN;
        }
        /** @var User $user */
        if (!$user->validatePassword($request_params['password'])) {
            return self::RESPONSE_ERR_WRONG_PASSWORD;
        }
        /** @var UserAccount $account */
        $account = UserAccount::find()->where(['account' => $request_params['account'], 'user_id' => $user->id])->one();
        if (!$account) {
            return self::RESPONSE_ERR_NO_ACCOUNT;
        }

        // Получаем массив сделок, которые еще не сохранены в базе
        $data = AccountOpenOrder::formOrdersDataForImport($request_params, $user->id, $account->id);

        // Если такое сделки есть, сохраняем их
        if ($data) {
            AccountOpenOrder::saveOrdersData($data);
        }

        return self::RESPONSE_OK;
    }

}