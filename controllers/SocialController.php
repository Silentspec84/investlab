<?php

namespace app\controllers;

use app\components\AccessRule;
use yii\filters\AccessControl;

class SocialController extends BaseController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['profile', 'profiles'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }

    public function actionProfiles()
    {
        return $this->render('profiles');
    }

    public function actionProfile($id)
    {
        return $this->render('profile', ['profile_id' => $id]);
    }
}