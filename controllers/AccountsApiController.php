<?php

namespace app\controllers;

use app\classes\helpers\AccountsStatsHelper;
use app\components\AccessRule;
use app\models\UserAccount;
use yii\filters\AccessControl;

class AccountsApiController extends BaseApiController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-accounts'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-all-active-accounts', 'create-account'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    public function actionGetAllActiveAccounts()
    {
        if (!$this->validateRequestWithOnlyCsrf()) {
            return $this->response;
        }

        $active_accounts = UserAccount::find()->onlyActive()->onlyId()->all();
        $active_accounts_data = [];
        foreach ($active_accounts as $active_account) {
            $account = new AccountsStatsHelper($active_account->id);
            $account->getAccountMainStats()->getAccountCharts(AccountsStatsHelper::GRAPH_TYPE_MONTH)->unsetCloseOrders();
            $active_accounts_data[] = $account;
            unset($account);
        }
        unset($active_accounts);

        return $this->response->setContent([
            'accounts' => $active_accounts_data,
            'platforms' => UserAccount::getPlatformsArray(),
            'user' => $this->user
        ]);
    }

    public function actionGetAccount()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $account_data = UserAccount::findOne($this->request_post['id']);
        $account = new AccountsStatsHelper($account_data->id);
        $account->getAccountCharts(AccountsStatsHelper::GRAPH_TYPE_MONTH);


        return $this->response->setContent(['account' => $account]);
    }

    public function actionCreateAccount()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'account' => ['type' => 'integer', 'required' => true],
            'acc_name' => ['type' => 'string', 'required' => true],
            'acc_type' => ['type' => 'integer', 'required' => true],
            'acc_descr' => ['type' => 'string', 'required' => false],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $user_account = new UserAccount();
        $user_account->account = $this->request_post['account'];
        $user_account->acc_name = $this->request_post['acc_name'];
        $user_account->acc_type = $this->request_post['acc_type'];
        $user_account->acc_descr = $this->request_post['acc_descr'];
        $user_account->user_id = $this->user->id;

        if (!$user_account->save()) {
            return $this->response->addError($user_account->errors);
        }
        return $this->response->setContent([
            'user_account' => $user_account,
        ]);
    }


}