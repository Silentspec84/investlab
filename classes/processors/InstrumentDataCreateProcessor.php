<?php

namespace app\classes\processors;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\parsers\InstrumentDataParserInterface;
use app\classes\parsers\InstrumentEquityDataParser;
use app\models\instrument_info\InstrumentInfo;
use app\models\instrument_info\main_info\Exchange;
use app\models\InstrumentHistory;
use app\models\instrument_info\Instrument;
use app\models\instrument_info\main_info\MarketType;
use Yii;
use yii\db\Exception;

/**
 * Самостоятельно определяет, какой парсер запустить для переданного инструмента.
 * Получает данные от парсера, обрабатывает их и сохраняет в нужные таблицы.
 *
 * Class InstrumentDataCreateProcessor
 * @package app\classes\processors
 */

class InstrumentDataCreateProcessor
{

    private const PARSERS_NAMESPACE = 'app\classes\parsers\\';
    private const APIS_NAMESPACE = 'app\classes\connectors\\';

    /**
     * Метод обновляет историю котировок инструмента с $secid в таблице истории котировок этого инструмента
     *
     * @param string $secid
     * @param int $exchange_id
     */
    public function updateInstrumentHistory(string $secid, int $exchange_id): void
    {
        /** @var Instrument $instrument_link */
        $instrument_link = Instrument::find()->where(['secid' => $secid, 'exchange_id' => $exchange_id])->one();

        $parser = $this->getParser($instrument_link);
        $instrument_history_data = $parser->getInstrumentHistoryDataArray();
        $history = new InstrumentHistory;
        $history->setTableName($instrument_link->secid);
        $history->batchInsertHistoryData($instrument_history_data);
    }

    public function updateInstrumentInfo(string $secid, int $exchange_id): void
    {
        /** @var Instrument $instrument_link */
        $instrument_link = Instrument::find()->where(['secid' => $secid, 'exchange_id' => $exchange_id])->one();
        $parser = $this->getParser($instrument_link);
        $instrument_info = $parser->getInstrumentCommonDataArray();

        $instrument_link->currency = $instrument_info['currency'];
        $instrument_link->isin = $instrument_info['isin'];
        $instrument_link->sn = $instrument_info['sn'];
        $instrument_link->industry_id = $instrument_info['industry_id'];
        $instrument_link->sector_id = $instrument_info['sector_id'];

        $instrument_link->save();
    }

    public function saveHistory()
    {
        $instruments = Instrument::find()->all();
        $moex_connector = $this->getApiConnector(1);
        /** @var Instrument $instrument */
        foreach ($instruments as $instrument) {
            $instrument_history = $moex_connector->getInstrumentHistoryFromApi($instrument->secid);
            $history_table = new InstrumentHistory();
            $history_table->setTableName($instrument->secid);
            $history_table->batchInsertHistoryData($instrument_history);
            unset($instrument_history, $history_table);
        }
    }

    public function deleteHistory()
    {
        $instruments = Instrument::find()->all();
        $moex_connector = $this->getApiConnector(1);
        /** @var Instrument $instrument */
        foreach ($instruments as $instrument) {
            $history_table = new InstrumentHistory();
            $history_table->setTableName($instrument->secid);
            $history_table->deleteHistoryTable();
        }
    }

    /**
     * Метод-фабрика, сетит подходящий для конкретного инструмента экземпляр парсера с уже привязанным инструментом
     *
     * @param Instrument $instrument_link
     * @return InstrumentDataParserInterface
     */
    private function getParser(Instrument $instrument_link): InstrumentDataParserInterface
    {
        $market_type = MarketType::findOne($instrument_link->market_type_id);
        $parser_class_name = self::PARSERS_NAMESPACE . $market_type->parser_class_name;
        /** @var InstrumentDataParserInterface $parser_class */
        $parser_class = new $parser_class_name();
        return $parser_class->setInstrumentSettings($instrument_link);
    }

    /**
     * Метод-фабрика, сетит подходящий для конкретного инструмента экземпляр коннектора к апи биржи с уже привязанным инструментом
     *
     * @param Instrument $instrument_link
     * @return ApiConnectorInterface
     */
    private function getApiConnector($exchange_id): ApiConnectorInterface
    {
        $exchange = Exchange::findOne($exchange_id);
        $connector_class_name = self::APIS_NAMESPACE . $exchange->connector_api_class;
        /** @var ApiConnectorInterface $connector_class */
        return new $connector_class_name();
    }
}