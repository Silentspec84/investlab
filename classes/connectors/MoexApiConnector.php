<?php

namespace app\classes\connectors;

use app\classes\helpers\ArrayHelper;
use app\models\instrument_info\Instrument;
use app\models\instrument_info\main_info\MarketType;
use app\models\InstrumentHistory;
use app\models\MoexInstrument;
use Yii;

/**
 * Класс для работы с api московской биржи http://iss.moex.com/iss/reference/
 *
 * Class MoexApiConnector
 * @package app\classes\helpers
 */
class MoexApiConnector implements ApiConnectorInterface
{
    const SECURITIES_LIST_URL = 'http://iss.moex.com/iss/securities.json?is_trading=true';
    const SECURITY_HISTORY_URL = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/securities/';

    // Доски инструментов (тут пока только основные)
    public static $boards = [
        'TQBR' => 'Т+ Акции и ДР',
        'TQDE' => 'Т+ Акции Д',
        'TQIF' => 'Т+ Паи',
        'TQTF' => 'Т+ ETF',
    ];

    public static $boards_to_market_type_codes = [
        'TQBR' => 'equities',
        'TQDE' => 'equities',
        'TQIF' => 'funds',
        'TQTF' => 'etfs',
    ];

    /** @var string $error */
    public $error = '';

    /** @var Instrument $instrument_link*/
    private $instrument_link;

    /**
     * @param Instrument $instrument_link
     * @return ApiConnectorInterface
     */
    public function setInstrument(Instrument $instrument_link) : ApiConnectorInterface
    {
        $this->instrument_link = $instrument_link;
        return $this;
    }

    /**
     * @return bool
     */
    public function getInstrumentsArray(): bool
    {
        $exist_instruments = Instrument::find()->indexBy('secid')->all();
        $instruments_from_api = $this->getAllSecuritiesListFromApi();
        $market_types = MarketType::find()->indexBy('code')->all();
        /** @var Instrument $exist_instrument */
        /** @var MarketType $market_type */
        foreach ($instruments_from_api as $instrument_from_api_code => $instrument_from_api) {
            if (array_key_exists($instrument_from_api_code, $exist_instruments)) {
                $exist_instrument = $exist_instruments[$instrument_from_api_code];
                $exist_instrument->board_code =  $instrument_from_api[12];
                $exist_instrument->title =  $instrument_from_api[3];
                $exist_instrument->exchange_id = 1;
                $market_type_code = self::$boards_to_market_type_codes[$exist_instrument->board_code];
                $market_type = $market_types[$market_type_code];
                $exist_instrument->market_type_id = $market_type->id;
                if (!$exist_instrument->save()) {
                    return false;
                }
            } else {
                $instrument = new Instrument();
                $instrument->secid = $instrument_from_api[0];
                $instrument->title =  $instrument_from_api[3];
                $instrument->exchange_id = 1;
                $exist_instrument->board_code =  $instrument_from_api[12];
                $market_type_code = self::$boards_to_market_type_codes[$exist_instrument->board_code];
                $market_type = $market_types[$market_type_code];
                $instrument->market_type_id = $market_type->id;
                if (!$instrument->save()) {
                    return false;
                }
            }
        }

        return true;
    }

    public function getInstrumentHistory(string $instr_secid) : array
    {
        $instrument_history_model = new InstrumentHistory();
        $instrument_history_model->setTableName($instr_secid);
        $exist_instrument_history = ArrayHelper::index($instrument_history_model->getHistory(), 'trade_date');
        $instrument_history_from_api = $this->getInstrumentHistoryFromApi($instr_secid);
        if (empty($exist_instrument_history)) {
            return $instrument_history_from_api;
        }
        foreach ($instrument_history_from_api as $instrument_history_date => $instrument_history_raw) {
            if (array_key_exists($instrument_history_date, $exist_instrument_history)) {
                unset($instrument_history_from_api[$instrument_history_date]);
            }
        }

        return $instrument_history_from_api;
    }

    private function getAllSecuritiesListFromApi()
    {
        $limit = 100;
        $start = 0;
        $result = [];

        do {
            try {
                $security_list_json = file_get_contents(self::SECURITIES_LIST_URL . '&start=' . $start . '&limit=' . $limit);
            } catch (yii\base\ErrorException $e) {
                $start += $limit;
                continue;
            }

            $security_list = json_decode($security_list_json, true);

            if (empty($security_list['securities']['data'])) {
                break;
            }

            foreach ($security_list['securities']['data'] as $security_key => $security) {
                if (!array_key_exists($security[15], self::$boards) || $security[6] !== 1) {
                    continue;
                }
                unset($security[0]);
                unset($security[6]);
                unset($security[7]);
                $security = array_values($security);
                $result[$security[0]] = $security;
            }

            $start += $limit;

        } while (!empty($security_list['securities']['data']));

        return $result;
    }

    /**
     * @param string $instr_secid
     * @return array
     */
    public function getInstrumentHistoryFromApi(string $instr_secid) : array
    {
        $limit = 100;
        $start = 0;
        $result = [];

        do {
            // Получили список всех бумаг
            try {
                $security_history_json = file_get_contents(self::SECURITY_HISTORY_URL . $instr_secid . '.json?start=' . $start . '&limit=' . $limit);
            } catch (yii\base\ErrorException $e) {
                $start += $limit;
                continue;
            }

            $security_history = json_decode($security_history_json, true);

            if (empty($security_history['history']['data'])) {
                break;
            }

            // Ходим по бумагам
            foreach ($security_history['history']['data'] as $raw_key => $raw) {
                if($raw[4] < 1 && $raw[5] < 1 && $raw[6] === null && $raw[10] === null && $raw[12] < 1) {
                    continue;
                }
                if (array_key_exists(($raw_key + 1), $security_history['history']['data'])) {
                    if ($security_history['history']['data'][$raw_key + 1][1] === $raw[1]
                        &&$security_history['history']['data'][$raw_key + 1][4] > $raw[4]) {
                        continue;
                    }
                }
                unset($raw[0], $raw[2], $raw[4], $raw[5], $raw[9], $raw[10], $raw[13], $raw[14], $raw[15], $raw[16], $raw[17], $raw[18], $raw[19]);
                $raw[1] = strtotime($raw[1]);
                $raw[6] = (float)$raw[6];
                $raw[7] = (float)$raw[7];
                $raw[8] = (float)$raw[8];
                $raw[11] = (float)$raw[11];
                $raw[12] = (int)$raw[12];
                $raw = array_values($raw);
                $result[$raw[0]] = $raw;
            }

            $start += $limit;

        } while (!empty($security_history['history']['data']));

        return $result;
    }

}