<?php

namespace app\classes\connectors;

use app\models\instrument_info\Instrument;

interface ApiConnectorInterface
{
    /**
     * Метод возвращает массив всех инструментов, доступных на данной бирже
     *
     * @return array
     */
    public function getInstrumentsArray() : bool;

    /**
     * Метод возвращает всю историю по инструменту, secid которого передано
     *
     * @param string $secid
     * @return array
     */
    public function getInstrumentHistory(string $secid) : array;

    /**
     * Сетит объект InstrumentLink
     *
     * @param Instrument $instrument_link
     * @return ApiConnectorInterface
     */
    public function setInstrument(Instrument $instrument_link) : ApiConnectorInterface;

    public function getInstrumentHistoryFromApi(string $instr_secid) : array;
}