<?php

namespace app\classes\helpers;


use app\models\Inflation;
use app\models\instrument_info\Instrument;
use app\models\InstrumentHistory;

class PortfolioOptimizator
{
    private $initial_balance = 100000;
    private $withdraw_sum = 3000;
    private $min_sectors = 3;
    private $max_instruments = 7;
    private $years_testing = 10;

    public function optimize()
    {
        $current_year = (int)date('Y', time());
        $start_year = $current_year - $this->years_testing;
        $start_date = strtotime('01.01.' . $start_year);
        $instruments_to_filter = Instrument::find()->asArray()->select(['secid', 'min_lot', 'sector_id'])->all();
        $instruments_to_use = [];
        $withdraw_sum_with_inflation = $this->getSumWithInflation($this->withdraw_sum, $this->years_testing);
        // Отсеиваем из всех инструментов те, что не подходят по количеству истории и по цене покупки
        foreach ($instruments_to_filter as $instrument_to_filter) {
            $history = new InstrumentHistory();
            $history->setTableName($instrument_to_filter['secid']);
            $history_dates = $history->getHistoryDates();
            $history_first_price = $history->getFirstPriceOfYear($start_year);
            $date = strtotime($history_dates['from']);
            if ($date == 0 || $date > $start_date) {
                continue;
            }
            if ($history_first_price == 0
                || $history->getLastPrice() * $instrument_to_filter['min_lot'] > $this->withdraw_sum
                || $history_first_price * $instrument_to_filter['min_lot'] > $withdraw_sum_with_inflation) {
                continue;
            }
            $instruments_to_use[$instrument_to_filter['secid']] = $instrument_to_filter;
            $instruments_to_use[$instrument_to_filter['secid']]['first_price'] = $history_first_price;
            unset($history, $instrument_to_filter, $history_dates, $history_first_price, $date);
        }
        unset($start_year, $current_year, $start_date, $instruments_to_filter);

        $base_sum = $this->getSumWithNoInvest();

        foreach ($instruments_to_use as $instrument_to_use_name => $instrument_to_use) {
            $result = $this->getInstrumentResult($instrument_to_use);
            if ($result > $base_sum) {
                $instruments_to_use[$instrument_to_use_name]['result'] = $result;
            } else {
                unset($instruments_to_use[$instrument_to_use_name]);
            }
            unset($instrument_to_use_name, $instrument_to_use);
        }

        $instruments_to_use = ArrayHelper::index($instruments_to_use, 'result');

        ksort($instruments_to_use);
        $instruments_to_use = array_reverse($instruments_to_use);
        $instruments_to_use = array_slice($instruments_to_use, 0, $this->max_instruments * 2);

        $instruments_count = count($instruments_to_use);
        $variants_count = $this->getVariantsCount($instruments_count);
        $instruments_to_use = ArrayHelper::index($instruments_to_use, 'secid');

        $portfolios = $this->getPortfolioSamples($instruments_to_use, $this->max_instruments, $this->max_instruments);
        unset($instruments_to_use);

        foreach ($portfolios as $portfolio_name => $portfolio) {
            $result = 0;
            foreach ($portfolio as $instrument_name => $instrument) {
                $result += $instrument['result'];
                unset($portfolios[$portfolio_name][$instrument_name]['result']);
            }
            $portfolios[$portfolio_name]['result'] = $result;
        }

        $portfolios = ArrayHelper::index($portfolios, 'result');
        ksort($portfolios);
        $portfolios = array_reverse($portfolios);
        $portfolios = array_values(array_slice($portfolios, 0, 50));

        $depo = $this->getSumWithInflation($this->initial_balance, $this->years_testing);

        $portfolios_results = [];
        foreach ($portfolios as $portfolio) {
            $portfolios_results[] = $this->getPortfolioResult($portfolio);
        }

        $portfolios_results = ArrayHelper::index($portfolios_results, 'portfolio_cost');
        ksort($portfolios_results);
        $portfolios_results = array_reverse($portfolios_results);
        $best_portfolio = reset($portfolios_results);
        ea($best_portfolio);
    }

    private function getSumWithNoInvest()
    {
        $current_year = (int)date('Y', time());
        $start_year = $current_year - $this->years_testing;
        $depo = $this->initial_balance;
        for ($cur_year = $start_year; $cur_year <= $current_year; $cur_year++) {
            $depo += $this->withdraw_sum * 12;
        }
        return $depo;
    }

    private function getInstrumentResult($instrument)
    {
        $history = new InstrumentHistory();
        $history->setTableName($instrument['secid']);
        $current_year = (int)date('Y', time());
        $start_year = $current_year - $this->years_testing;
        $instrument_history = $history->getHistoryFromDate($start_year);
        $last_month = 0;
        $shares = 0;
        $depo = 0;
        foreach ($instrument_history as $instrument_quote) {
            $month = (int)date('m',$instrument_quote['trade_date']);
            if ($last_month!= 0 && $month != $last_month) {
                $share_price = $instrument['min_lot'] * $instrument_quote['close'];
                if ($share_price == 0) {
                    continue;
                }
                $last_month = $month;
                $year = $current_year - (int)date('Y', $instrument_quote['trade_date']);
                $depo += $this->getSumWithInflation($this->withdraw_sum, $year);
                $shares_to_buy = floor($depo / $share_price);
                if ($shares_to_buy > 0) {
                    $shares += $shares_to_buy;
                    $depo -= $shares_to_buy * $share_price;
                }
            }
            if ($last_month == 0) {
                $share_price = $instrument['min_lot'] * $instrument_quote['close'];
                $depo = $this->getSumWithInflation($this->initial_balance, $this->years_testing);
                $last_month = (int)date('m', $instrument_quote['trade_date']);
                $month = $last_month;
                $shares_to_buy = floor($depo / $share_price);
                $shares += $shares_to_buy;
                $depo -= $shares_to_buy * $share_price;
            }
        }
        $last_price = $history->getLastPrice();
        $depo += $shares * $last_price * $instrument['min_lot'];
        unset($history, $current_year, $start_year, $instrument_history, $instrument_quote, $last_month, $shares);
        return $depo;
    }

    private function getPortfolioResult($portfolio)
    {
        unset($portfolio['result']);
        $instruments = array_keys($portfolio);
        $instruments_lots = ArrayHelper::getColumn(ArrayHelper::index(Instrument::find()->where(['secid' => $instruments])->select(['min_lot', 'secid'])->asArray()->all(), 'secid'), 'min_lot');
        $current_year = (int)date('Y', time());
        $start_year = $current_year - $this->years_testing;

        $history = new InstrumentHistory();
        $instruments_history = $history->getHistoryFromDateByMonthsForInstruments($start_year, $instruments);

        $last_month = [];
        $shares = [];
        $portfolio_info = [];
        foreach ($instruments_history as $instrument_history_date => $instrument_history_quote) {
            $curr_year = (int)substr($instrument_history_date, 3, 4);

            if (!array_key_exists($instrument_history_date, $portfolio_info)) {
                $portfolio_info[$instrument_history_date] = [];
                $portfolio_info[$instrument_history_date]['balance'] = 0;
            }

            if ($instrument_history_date == '01.' . $start_year) {
                $depo = $this->getSumWithInflation($this->initial_balance, $this->years_testing);
                for ($balancing = 0; $balancing < 1000; $balancing++) {
                    foreach ($instrument_history_quote as $instrument_name => $instrument_quote) {
                        $share_price = $instruments_lots[$instrument_name] * $instrument_quote['open'];
                        if ($depo - $share_price > 0) {
                            $depo -= $share_price;
                            if (!array_key_exists($instrument_name, $portfolio_info[$instrument_history_date])) {
                                $portfolio_info[$instrument_history_date][$instrument_name] = [];
                                $portfolio_info[$instrument_history_date][$instrument_name]['lots'] = 0;
                                $portfolio_info[$instrument_history_date][$instrument_name]['value'] = 0;
                                $portfolio_info[$instrument_history_date][$instrument_name]['price'] = 0;
                                $portfolio_info[$instrument_history_date]['value'] = 0;

                            }
                            if (!array_key_exists($instrument_name, $portfolio_info)) {
                                $portfolio_info[$instrument_name] = [];
                                $portfolio_info[$instrument_name]['lots'] = 0;
                                $portfolio_info[$instrument_name]['value'] = 0;
                            }
                            $portfolio_info[$instrument_history_date][$instrument_name]['lots'] += $instruments_lots[$instrument_name];
                            $portfolio_info[$instrument_history_date][$instrument_name]['value'] += $share_price;
                            $portfolio_info[$instrument_history_date][$instrument_name]['price'] = $instrument_quote['open'];
                            $portfolio_info[$instrument_history_date]['balance'] = $depo;
                            $portfolio_info[$instrument_history_date]['value'] += $share_price;
                            $portfolio_info['balance'] = $depo;
                            $portfolio_info[$instrument_name]['lots'] += $instruments_lots[$instrument_name];
                            $portfolio_info[$instrument_name]['value'] += $share_price;
                        }
                    }
                    $balancing++;
                    if ($depo <= 0.05 * $this->getSumWithInflation($this->initial_balance, $this->years_testing)) {
                        break;
                    }
                }
                continue;
            }
            $withdraw = $this->getSumWithInflation($this->withdraw_sum,  $current_year - $curr_year) + $portfolio_info['balance'];

            for ($balancing = 0; $balancing < 1000; $balancing++) {
                foreach ($instrument_history_quote as $instrument_name => $instrument_quote) {
                    $share_price = $instruments_lots[$instrument_name] * $instrument_quote['open'];
                    if ($withdraw - $share_price > 0) {
                        $withdraw -= $share_price;
                        if (!array_key_exists($instrument_name, $portfolio_info[$instrument_history_date])) {
                            $portfolio_info[$instrument_history_date][$instrument_name] = [];
                            $portfolio_info[$instrument_history_date][$instrument_name]['lots'] = 0;
                            $portfolio_info[$instrument_history_date][$instrument_name]['value'] = 0;
                            $portfolio_info[$instrument_history_date][$instrument_name]['price'] = 0;
                            $portfolio_info[$instrument_history_date]['value'] = 0;
                        }
                        $portfolio_info[$instrument_history_date][$instrument_name]['lots'] += $instruments_lots[$instrument_name];
                        $portfolio_info[$instrument_history_date][$instrument_name]['value'] += $share_price;
                        $portfolio_info[$instrument_history_date][$instrument_name]['price'] = $instrument_quote['open'];
                        $portfolio_info[$instrument_history_date]['balance'] = round($withdraw, 2);
                        $portfolio_info[$instrument_history_date]['value'] += $share_price;
                        $portfolio_info['balance'] = round($withdraw, 2);
                        $portfolio_info[$instrument_name]['lots'] += $instruments_lots[$instrument_name];
                        $portfolio_info[$instrument_name]['value'] += $share_price;
                    }
                }
                $balancing++;
                if ($withdraw <= 0.1 * $this->getSumWithInflation($this->withdraw_sum, $current_year - $curr_year)) {
                    break;
                }
            }
        }

        $portfolio_info['portfolio_cost'] = 0;
        foreach ($instruments as $instrument) {
            $history = new InstrumentHistory();
            $history->setTableName($instrument);
            $portfolio_info['portfolio_cost'] += $portfolio_info[$instrument]['lots'] * $history->getLastPrice();
        }

        return $portfolio_info;
    }

    private function getVariantsCount($instruments_count)
    {
        return $this->getFact($instruments_count) / ($this->getFact($this->max_instruments) * $this->getFact(($instruments_count - $this->max_instruments)));
    }

    private function getFact($number)
    {
        $ffact = 1;
        while($number >= 1) {
            $ffact = $number * $ffact;
            $number--;
        }
        return $ffact;
    }

    private function getPortfolioSamples($instruments, $size, $elements_size, $combinations = array()) {

        # if it the first iteration, the first set
        # of combinations is the same as the set of characters
        if (empty($combinations)) {
            $combinations = $instruments;
        }

        # we're done if we're at size 1
        if ($size == 1) {
            return $combinations;
        }

        # initialise array to put new values in
        $new_combinations = [];

        # loop through existing combinations and character set to create strings
        foreach ($combinations as $combination_name => $combination) {
            foreach ($instruments as $instrument_name => $instrument) {

                if (in_array($instrument_name, $combination)
                    || (array_key_exists('secid', $combination) && $combination['secid'] == $instrument_name)) {
                    continue;
                }

                $new_combination = [];
                $new_instrument = [];

                if (array_key_exists('secid', $combination)) {
                    $new_combination[$combination_name] = $combination;
                    $new_instrument[$instrument_name] = $instrument;
                    $new_combination = array_merge($new_instrument, $new_combination);
                } else {
                    $new_instrument[$instrument_name] = $instrument;
                    $new_combination = array_merge($combination, $new_instrument);
                }

                ksort($new_combination);
                $new_combination_key = implode('', array_keys($new_combination));

                if ($size != 2 || ($size == 2 && count($new_combination) == $elements_size)) {
                    $new_combinations[$new_combination_key] = $new_combination;
                }
            }
        }

        # call same function again for the next iteration
        return $this->getPortfolioSamples($instruments, $size - 1, $elements_size, $new_combinations);

    }

    private function getSumWithInflation(float $sum, int $years) : float
    {
        $current_year = (int)date('Y', time());
        $start_year = $current_year - $years;
        $inflation = Inflation::find()->where(['currency' => 'RUB'])->andWhere(['>', 'year', ($start_year - 1)])->indexBy('year')->all();
        for ($year = $current_year; $year >= $start_year; $year--) {
            $sum = round($sum * (1 - $inflation[$year]['inflation'] / 100), 2);
        }
        return $sum;
    }
}