<?php

namespace app\classes\helpers;

use app\models\AccountCloseOrder;
use app\models\UserAccount;
use app\models\User;

class AccountsStatsHelper
{
    public $account_id;

    /* Инфа из базы, сетится при создании экземпляра класса. */
    public $user_data = [];
    public $account_data = [];
    public $account_close_orders = [];

    /* Вычисляемые поля */
    public $account_main_stats = []; // Основная статистика счета
    public $account_trading_stats = []; // Торговая статистика счета

    public $account_risk_stats = []; // Статистика риска счета
    public $account_efficiency_stats = []; // Статистика эффективности счета

    /* Графики для мониторинга, сетятся специальными методами (могут быть не созданы, если этого не требуется) */
    public $graph_balance; /* График прироста депозита */

    public const GRAPH_TYPE_DEALS = 1;
    public const GRAPH_TYPE_DAY = 2;
    public const GRAPH_TYPE_WEEK = 3;
    public const GRAPH_TYPE_MONTH = 4;

    /**
     * Разложить на бай и селл можно. Разложить по магикам, инструментам и системам (ручками определять системы или же из комментария по правилам тянуть).
     * Переключить на свечи, бары, линию или столбики (где уместно). Дневки, недели, месяцы, годы (масштаб).
     * Прироста, Прибыли, Просадки,
     * Кол-во сделок по инструментам в % от всего - круговая.
    Соотношение профит лосс по инструментам - горизонтальные столбики.
    Среднее время удержания по инструментам - горизонтальная.
    Прибыль - убыток, количество, проценты, деньги, пункты по часам, дням недели, месяца, по месяцам. Длинные короткие
     * по часам дням недели, месяца, по месяцам.
     * Длинные и короткие прибыльные по часам дням недели, месяца, по месяцам. Убыточные по часам дням недели, месяца, по месяцам.
     * МАе МФЕ
     * https://drive.google.com/file/d/1k4p8jCOaZ5zHihOTP2ioso3IqCROnQ10/view?usp=sharing
     * Стабильность чистой прибыли WF -Net Profit Stability&gt; 60% .
    Drawdown Stability -
    Return / DD Stability -
    Sharpe Ratio Stability -
     * http://tlap.com/strategiya-investirovaniya-ot-dzheka-singera/#more-33423
     *
     * Distribution of Winning Trades
     * Distribution of Losing Trades
     * Лучшие/худшие часы, дни, месяцы, года
     * pf
     *
     */

    /* Вычисляемые данные по счету Риск - это массив */
    /**
     * Annualized Standard Deviation
    Downside Deviation
    Max Drawdown
    Maximum dd duration
    Peak to Through
    Максимальный внутридневной нарастающий убыток (Maximum Intraday DrawDown, MIDD).
    Показатель MIDD отражает наибольшую величину
    снижения депозита за весь период тестирования торговой системы.
    Фиксированная просадка – убыток при закрытии всех сделок.
    Относительная просадка – в % от депозита.
    Risk of Ruin Риск разорения RoR = (1-w)^R, где R - risk percent
    Отношение max-ного падения капитала к его среднему падению
    Динамика агрессивности торгов за период. Вычисляется как MIDD/Avg DD.
    Значение: не более 3.
     */

    /* Вычисляемые данные по счету Эффективность - это массив */
    /**
     * Sharpe Ratio
    Information Ratio
    Sortino Ratio
    Соотношение средней прибыльной к средней убыточной
    Мат ожидание
    Treynor Ratio
    Jensen Measure
    Fama’s Return Decomposition
    Mae mfe
    Omega
    𝑏1−𝐹𝑥𝑑𝑥 𝐶𝐿
    Ω𝐿=𝐿𝑏𝐹𝑥𝑑𝑥 =𝑃𝐿 𝐿
     Thehighertheratio;thebetter.
     Thisistheratiooftheprobabilityofhavingagainto
    the probability of having a loss.
     Donotassumenormality.
     Usethewholereturnsdistribution.
     */



    /**
     * Индикаторы к графикам. Будем передавать константы и параметры в метод, строящий график.
     * Если не переданы, индикаторы не нужны. Если переданы, строим и кладем массив в свойство нужного графика.
     */

    public const IN_CURRENCY = 1;
    public const IN_PIPS = 2;
    public const IN_PERCENT = 3;

    public function __construct($account_id)
    {
        $this->account_id = $account_id;
        $this->account_data = UserAccount::findOne($this->account_id);
        $this->user_data = User::findOne($this->account_data->user_id);
        $this->account_close_orders = AccountCloseOrder::find()->where(['account_id' => $this->account_data->id])->orderByDateClosed()->all();
    }

    /**
     * Метод ансетит массив объектов AccountCloseOrder (они могут быть не нужны для некоторых случаев)
     *
     * @return $this
     */
    public function unsetCloseOrders()
    {
        unset($this->account_close_orders);
        return $this;
    }

    /**
     * Метод расчитывает основные параметры счета
     *
     * @return $this
     */
    public function getAccountMainStats()
    {
        $result = ['ea_orders' => 0, 'all_orders' => 0, 'account_profit' => 0,
            'withdrawal' => 0, 'deposit' => 0, 'av_holding_time' => 0,
            'profit_factor' => 0, 'authentic_profit_factor' => 0, 'wins' => 0, 'losses' => 0,
            'win_currency' => 0, 'loss_currency' => 0, 'biggest_win' => 0, 'biggest_loss' => 0,
            'annualized_return' => 0];

        /** Отклонение от доходности
         *Геометрическое стандартное отклонение от доходности — показатель отклонения от средней геометрической доходности за выбранный период. Если 2 ПАММ-счета имеют одинаковую среднюю геометрическую доходность, менее рисковым будет вложение в ПАММ-счет с меньшим стандартным отклонением.
         * Волатильность
         *Показатель выражен в процентах, рассчитывается по формуле: Среднее арифметическое из ежедневных значений цены пая (HIGH-LOW) / OPEN × 100%
         * Просадка
         * Время существования системы всего, верифицированное (с момента первого подключения)
         * Агрессивность торговли
         * Агрессивность торговли рассчитывается на основании волатильности среднедневного торгового результата ПАММ-счета. Диапазоны агрессивности:
        1 уровень — от 0 до 1%;
        2 уровень — от 1 до 3%;
        3 уровень — от 3 до 5%;
        4 уровень — от 5 до 7%;
        5 уровень — от 7% и более.
         * Доходность/Риск
         *Прибыль/Волатильность
         * Убыточные торговые дни
         * Процент убыточных торговых дней — доля убыточных дней относительно количества торговых дней существования ПАММ-счета.
         * Лучшая/худшая сделка в $ и пп
         */

        /** @var AccountCloseOrder $account_close_order */
        foreach ($this->account_close_orders as $account_close_order) {
            if ($account_close_order->order_magic !== 0) {
                $result['ea_orders']++;
            }
            if ($account_close_order->order_type === AccountCloseOrder::OP_BALANCE) {
                if ($account_close_order->order_profit >= 0) {
                    $result['deposit'] += $account_close_order->order_profit;
                }
                if ($account_close_order->order_profit < 0) {
                    $result['withdrawal'] += $account_close_order->order_profit;
                }
            }
            if ($account_close_order->isOrderStatistical()) {
                $order_profit = $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
                if ($order_profit < 0) {
                    $result['losses'] ++;
                    $result['loss_currency'] += $order_profit;
                    if ($result['biggest_loss'] > $order_profit) {
                        $result['biggest_loss'] = $order_profit;
                    }
                } else {
                    $result['wins'] ++;
                    $result['win_currency'] += $order_profit;
                    if ($result['biggest_win'] < $order_profit) {
                        $result['biggest_win'] = $order_profit;
                    }
                }

                $result['av_holding_time'] += $account_close_order->getOrderHoldingTime();
                $result['account_profit'] += $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
                $order_balance = round($result['deposit'] + $result['withdrawal'] + $result['account_profit'],2);
                $result['annualized_return'] += ($account_close_order->order_profit / $order_balance) * 100;
                $result['all_orders']++;
            }
        }

        $result['manual_orders'] = $result['all_orders'] - $result['ea_orders'];
        $result['ea_orders_ratio'] = $result['all_orders'] !== 0 ? round(($result['ea_orders'] / $result['all_orders']) * 100, 2) : 0;
        $result['winning_perc'] = $result['all_orders'] !== 0 ? round(($result['wins'] / $result['all_orders']) * 100, 2) : 0;
        $result['average_size']['all'] = $result['all_orders'] !== 0 ? round($result['account_profit'] / $result['all_orders'], 2) : 0;
        $result['average_size']['win'] = $result['wins'] !== 0 ? round($result['win_currency'] / $result['wins'], 2) : 0;
        $result['average_size']['loss'] = $result['losses'] !== 0 ? round($result['loss_currency'] / $result['losses'], 2) : 0;
        $result['authentic_profit_factor'] = $result['loss_currency'] !== 0 ? round(- ($result['win_currency'] - $result['biggest_win']) / $result['loss_currency'], 2) : 0;
        $result['profit_factor'] = $result['loss_currency'] !== 0 ? round(- $result['win_currency'] / $result['loss_currency'], 2) : 0;
        if ($result['av_holding_time'] < 1) {
            $result['time_frame'] = 'Высокочастотная торговля';
        } elseif ($result['av_holding_time'] < 1440) {
            $result['time_frame'] = 'Торговля внутри для';
        } elseif ($result['av_holding_time'] < 43200) {
            $result['time_frame'] = 'Краткосрочная торговля';
        } else {
            $result['time_frame'] = 'Долгосрочная торговля';
        }
        $result['annualized_return'] = round($result['annualized_return'], 2);
        $result['withdrawal'] = round($result['withdrawal'],2);
        $result['deposit'] = round($result['deposit'],2);
        $result['balance'] = round($result['deposit'] + $result['withdrawal'] + $result['account_profit'],2);
        $result['account_profit'] = round($result['account_profit'],2);
        $result['acc_type'] = $this->account_data->getPlatformNameById();
        $result['account_trade_mode_num'] = $this->account_data->account_trade_mode;
        $result['account_trade_mode'] = $this->account_data->getAccountTradeModeTitle();
        if ($result['deposit'] !== 0 || $result['withdrawal'] !== 0) {
            $result['total_return'] = round(($result['account_profit'] / ($result['deposit'] + $result['withdrawal'])) * 100, 2);
        }

        $this->account_main_stats = $result;

        return $this;
    }

    public function getAccountTradingStats()
    {
        $result = [
            'total_return' => round(($this->account_main_stats['account_profit'] / ($this->account_main_stats['deposit'] + $this->account_main_stats['withdrawal'])) * 100, 2) ,
            'transaction_costs' => 0,
            'time_frame' => '',
            'profit_factor' => 0,
            'authentic_profit_factor' => 0,
            'total_ammount' => [
                'all_loss' => 0, 'all_win' => 0,
                'all_short' => 0, 'all_long' => 0,
                'all_loss_short' => 0, 'all_loss_long' => 0,
                'all_win_short' => 0, 'all_win_long' => 0,
            ],
            'all_win_loss' => [
                'perc' => [
                    'all' => 0, 'all_loss' => 0, 'all_win' => 0,
                    'all_loss_short' => 0, 'all_loss_long' => 0,
                    'all_win_short' => 0, 'all_win_long' => 0,
                    'all_short' => 0, 'all_long' => 0,
                ],
                'currency' => [
                    'all' => 0, 'all_loss' => 0, 'all_win' => 0,
                    'all_loss_short' => 0, 'all_loss_long' => 0,
                    'all_win_short' => 0, 'all_win_long' => 0,
                    'all_short' => 0, 'all_long' => 0,
                ],
                'pips' => [
                    'all' => 0, 'all_loss' => 0, 'all_win' => 0,
                    'all_loss_short' => 0, 'all_loss_long' => 0,
                    'all_win_short' => 0, 'all_win_long' => 0,
                    'all_short' => 0, 'all_long' => 0,
                ],
            ],
            'trades_perc' => [
                'winning_all' => 0, 'losing_all' => 0,
                'winning_short' => 0, 'winning_long' => 0,
                'losing_short' => 0, 'losing_long' => 0,
            ],
            'average_size' => [
                'perc' => [
                    'all' => 0, 'win' => 0, 'loss' => 0,
                    'win_short' => 0, 'win_long' => 0,
                    'loss_short' => 0, 'loss_long' => 0,
                    'all_short' => 0, 'all_long' => 0,
                ],
                'currency' => [
                    'all' => 0, 'win' => 0, 'loss' => 0,
                    'win_short' => 0, 'win_long' => 0,
                    'loss_short' => 0, 'loss_long' => 0,
                    'all_short' => 0, 'all_long' => 0,
                ],
                'pips' => [
                    'all' => 0, 'win' => 0, 'loss' => 0,
                    'win_short' => 0, 'win_long' => 0,
                    'loss_short' => 0, 'loss_long' => 0,
                    'all_short' => 0, 'all_long' => 0,
                ]
            ],
            'biggest' => [
                'perc' => [
                    'win' => 0, 'loss' => 0,
                    'win_short' => 0, 'win_long' => 0,
                    'loss_short' => 0, 'loss_long' => 0,
                ],
                'currency' => [
                    'win' => 0, 'loss' => 0,
                    'win_short' => 0, 'win_long' => 0,
                    'loss_short' => 0, 'loss_long' => 0,
                ],
                'pips' => [
                    'win' => 0, 'loss' => 0,
                    'win_short' => 0, 'win_long' => 0,
                    'loss_short' => 0, 'loss_long' => 0,
                ]
            ],
            'sd' => [
                'all' => 0, 'win' => 0, 'loss' => 0,
                'win_short' => 0, 'win_long' => 0,
                'loss_short' => 0, 'loss_long' => 0,
                'all_short' => 0, 'all_long' => 0,
            ],
            'av_holding_time' => [
                'all' => 0, 'win' => 0, 'loss' => 0,
                'win_short' => 0, 'win_long' => 0,
                'loss_short' => 0, 'loss_long' => 0,
                'all_short' => 0, 'all_long' => 0,
            ]
        ];

        $order_balance = 0;

        /** @var AccountCloseOrder $account_close_order */
        foreach ($this->account_close_orders as $account_close_order) {
            if (!$account_close_order->isOrderStatistical()) {
                $order_balance += $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
                continue;
            }
            $order_profit = $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
            $result['transaction_costs'] = $account_close_order->commission + $account_close_order->order_swap;
            $order_balance += $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;

            if ($account_close_order->order_type === AccountCloseOrder::OP_BUY) {
                if ($account_close_order->isOrderInProfit()) {
                    if ($result['biggest']['perc']['win'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['win'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['perc']['win_long'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['win_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['currency']['win'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['win'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['currency']['win_long'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['win_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['pips']['win'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['win'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }
                    if ($result['biggest']['pips']['win_long'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['win_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }

                    $result['av_holding_time']['win'] += $account_close_order->getOrderHoldingTime();
                    $result['av_holding_time']['win_long'] += $account_close_order->getOrderHoldingTime();

                    $result['total_ammount']['all_win']++;
                    $result['total_ammount']['all_win_long']++;
                    $result['total_ammount']['all_long']++;
                    $result['all_win_loss']['perc']['all_win'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['perc']['all_win_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['currency']['all_win'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['currency']['all_win_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['pips']['all_win'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    $result['all_win_loss']['pips']['all_win_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);

                } else {
                    if ($result['biggest']['perc']['loss'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['loss'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['perc']['loss_long'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['loss_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['currency']['loss'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['loss'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['currency']['loss_long'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['loss_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['pips']['loss'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['loss'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }
                    if ($result['biggest']['pips']['loss_long'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['loss_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }

                    $result['av_holding_time']['loss'] += $account_close_order->getOrderHoldingTime();
                    $result['av_holding_time']['loss_long'] += $account_close_order->getOrderHoldingTime();

                    $result['total_ammount']['all_loss']++;
                    $result['total_ammount']['all_loss_long']++;
                    $result['total_ammount']['all_long']++;
                    $result['all_win_loss']['perc']['all_loss'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['perc']['all_loss_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['currency']['all_loss'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['currency']['all_loss_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['pips']['all_loss'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    $result['all_win_loss']['pips']['all_loss_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                }
                $result['all_win_loss']['perc']['all_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                $result['all_win_loss']['currency']['all_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                $result['all_win_loss']['pips']['all_long'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                $result['av_holding_time']['all_long'] += $account_close_order->getOrderHoldingTime();
            }
            if ($account_close_order->order_type === AccountCloseOrder::OP_SELL) {
                if ($account_close_order->isOrderInProfit()) {
                    if ($result['biggest']['perc']['win'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['win'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['perc']['win_short'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['win_short'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['currency']['win'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['win'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['currency']['win_short'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['win_short'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['pips']['win'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['win'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }
                    if ($result['biggest']['pips']['win_long'] < $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['win_long'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }

                    $result['av_holding_time']['win'] += $account_close_order->getOrderHoldingTime();
                    $result['av_holding_time']['win_short'] += $account_close_order->getOrderHoldingTime();

                    $result['total_ammount']['all_win']++;
                    $result['total_ammount']['all_win_short']++;
                    $result['total_ammount']['all_short']++;
                    $result['all_win_loss']['perc']['all_win'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['perc']['all_win_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['currency']['all_win'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['currency']['all_win_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['pips']['all_win'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    $result['all_win_loss']['pips']['all_win_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                } else {
                    if ($result['biggest']['perc']['loss'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['loss'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['perc']['loss_short'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance)) {
                        $result['biggest']['perc']['loss_short'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    }
                    if ($result['biggest']['currency']['loss'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['loss'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['currency']['loss_short'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR)) {
                        $result['biggest']['currency']['loss_short'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    }
                    if ($result['biggest']['pips']['loss'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['loss'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }
                    if ($result['biggest']['pips']['loss_short'] > $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS)) {
                        $result['biggest']['pips']['loss_short'] = $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    }

                    $result['av_holding_time']['loss'] += $account_close_order->getOrderHoldingTime();
                    $result['av_holding_time']['loss_short'] += $account_close_order->getOrderHoldingTime();

                    $result['total_ammount']['all_loss']++;
                    $result['total_ammount']['all_loss_short']++;
                    $result['total_ammount']['all_short']++;
                    $result['all_win_loss']['perc']['all_loss'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['perc']['all_loss_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                    $result['all_win_loss']['currency']['all_loss'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['currency']['all_loss_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                    $result['all_win_loss']['pips']['all_loss'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                    $result['all_win_loss']['pips']['all_loss_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                }
                $result['all_win_loss']['perc']['all_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
                $result['all_win_loss']['currency']['all_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
                $result['all_win_loss']['pips']['all_short'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
                $result['av_holding_time']['all_short'] += $account_close_order->getOrderHoldingTime();
            }
            $result['all_win_loss']['perc']['all'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PERC, $order_balance);
            $result['all_win_loss']['pips']['all'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_PIPS);
            $result['all_win_loss']['currency']['all'] += $account_close_order->getOrderProfit(AccountCloseOrder::PROFIT_CUR);
            $result['av_holding_time']['all'] += $account_close_order->getOrderHoldingTime();
        }

        $all_orders = $result['total_ammount']['all_loss'] + $result['total_ammount']['all_win'];

        $result['av_holding_time']['all'] = round($result['av_holding_time']['all'] / $all_orders / 60, 0);
        $result['av_holding_time']['win'] = round($result['av_holding_time']['win'] / $result['total_ammount']['all_win'] / 60, 0);
        $result['av_holding_time']['loss'] = round($result['av_holding_time']['loss'] / $result['total_ammount']['all_loss'] / 60, 0);
        $result['av_holding_time']['win_short'] = round($result['av_holding_time']['win_short'] / $result['total_ammount']['all_win_short'] / 60, 0);
        $result['av_holding_time']['win_long'] = round($result['av_holding_time']['win_long'] / $result['total_ammount']['all_win_long'] / 60, 0);
        $result['av_holding_time']['loss_short'] = round($result['av_holding_time']['loss_short'] / $result['total_ammount']['all_loss_short'] / 60, 0);
        $result['av_holding_time']['loss_long'] = round($result['av_holding_time']['loss_long'] / $result['total_ammount']['all_loss_long'] / 60, 0);
        $result['av_holding_time']['all_short'] = round($result['av_holding_time']['all_short'] / $result['total_ammount']['all_short'] / 60, 0);
        $result['av_holding_time']['all_long'] = round($result['av_holding_time']['all_long'] / $result['total_ammount']['all_long'] / 60, 0);

        if ($result['av_holding_time']['all'] < 1) {
            $result['time_frame'] = 'Высокочастотная торговля';
        } elseif ($result['av_holding_time']['all'] < 1440) {
            $result['time_frame'] = 'Торговля внутри для';
        } elseif ($result['av_holding_time']['all'] < 43200) {
            $result['time_frame'] = 'Краткосрочная торговля';
        } else {
            $result['time_frame'] = 'Долгосрочная торговля';
        }

        $result['trades_perc']['winning_all'] = round(($result['total_ammount']['all_win'] / $all_orders) * 100, 2);
        $result['trades_perc']['losing_all'] = round(($result['total_ammount']['all_loss'] / $all_orders) * 100, 2);
        $result['trades_perc']['winning_short'] = round(($result['total_ammount']['all_win_short'] / $result['total_ammount']['all_short']) * 100, 2);
        $result['trades_perc']['winning_long'] = round(($result['total_ammount']['all_win_long'] / $result['total_ammount']['all_long']) * 100, 2);
        $result['trades_perc']['losing_short'] = round(($result['total_ammount']['all_loss_short'] / $result['total_ammount']['all_short']) * 100, 2);
        $result['trades_perc']['losing_long'] = round(($result['total_ammount']['all_loss_long'] / $result['total_ammount']['all_long']) * 100, 2);

        $result['average_size']['perc']['all'] = round($result['all_win_loss']['perc']['all'] / $all_orders, 2);
        $result['average_size']['perc']['win'] = round($result['all_win_loss']['perc']['all_win'] / $result['total_ammount']['all_win'], 2);
        $result['average_size']['perc']['loss'] = round($result['all_win_loss']['perc']['all_loss'] / $result['total_ammount']['all_loss'], 2);
        $result['average_size']['perc']['win_short'] = round($result['all_win_loss']['perc']['all_win_short'] / $result['total_ammount']['all_win_short'], 2);
        $result['average_size']['perc']['win_long'] = round($result['all_win_loss']['perc']['all_win_long'] / $result['total_ammount']['all_win_long'], 2);
        $result['average_size']['perc']['loss_short'] = round($result['all_win_loss']['perc']['all_loss_short'] / $result['total_ammount']['all_loss_short'], 2);
        $result['average_size']['perc']['loss_long'] = round($result['all_win_loss']['perc']['all_loss_long'] / $result['total_ammount']['all_loss_long'], 2);

        $result['average_size']['currency']['all'] = round($result['all_win_loss']['currency']['all'] / $all_orders, 2);
        $result['average_size']['currency']['win'] = round($result['all_win_loss']['currency']['all_win'] / $result['total_ammount']['all_win'], 2);
        $result['average_size']['currency']['loss'] = round($result['all_win_loss']['currency']['all_loss'] / $result['total_ammount']['all_loss'], 2);
        $result['average_size']['currency']['win_short'] = round($result['all_win_loss']['currency']['all_win_short'] / $result['total_ammount']['all_win_short'], 2);
        $result['average_size']['currency']['win_long'] = round($result['all_win_loss']['currency']['all_win_long'] / $result['total_ammount']['all_win_long'], 2);
        $result['average_size']['currency']['loss_short'] = round($result['all_win_loss']['currency']['all_loss_short'] / $result['total_ammount']['all_loss_short'], 2);
        $result['average_size']['currency']['loss_long'] = round($result['all_win_loss']['currency']['all_loss_long'] / $result['total_ammount']['all_loss_long'], 2);

        $result['average_size']['pips']['all'] = round($result['all_win_loss']['currency']['all'] / $all_orders, 2);
        $result['average_size']['pips']['win'] = round($result['all_win_loss']['pips']['all_win'] / $result['total_ammount']['all_win'], 2);
        $result['average_size']['pips']['loss'] = round($result['all_win_loss']['pips']['all_loss'] / $result['total_ammount']['all_loss'], 2);
        $result['average_size']['pips']['win_short'] = round($result['all_win_loss']['pips']['all_win_short'] / $result['total_ammount']['all_win_short'], 2);
        $result['average_size']['pips']['win_long'] = round($result['all_win_loss']['pips']['all_win_long'] / $result['total_ammount']['all_win_long'], 2);
        $result['average_size']['pips']['loss_short'] = round($result['all_win_loss']['pips']['all_loss_short'] / $result['total_ammount']['all_loss_short'], 2);
        $result['average_size']['pips']['loss_long'] = round($result['all_win_loss']['pips']['all_loss_long'] / $result['total_ammount']['all_loss_long'], 2);

        $squere_sum = [
            'all' => 0, 'win' => 0, 'loss' => 0,
            'win_short' => 0, 'win_long' => 0,
            'loss_short' => 0, 'loss_long' => 0,
            'all_short' => 0, 'all_long' => 0,
        ];
        foreach ($this->account_close_orders as $account_close_order) {
            if (!$account_close_order->isOrderStatistical()) {
                $order_balance += $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
                continue;
            }
            $order_profit = $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
            $order_balance += $account_close_order->order_profit + $account_close_order->order_swap + $account_close_order->commission;
            $squere_sum['all'] += pow(($order_profit - $result['average_size']['perc']['all']), 2);
            if ($account_close_order->order_type === AccountCloseOrder::OP_BUY) {
                if ($account_close_order->isOrderInProfit()) {
                    $squere_sum['win_long'] += pow(($order_profit - $result['average_size']['perc']['win_long']), 2);
                    $squere_sum['win'] += pow(($order_profit - $result['average_size']['perc']['win']), 2);
                } else {
                    $squere_sum['loss_long'] += pow(($order_profit - $result['average_size']['perc']['loss_long']), 2);
                    $squere_sum['loss'] += pow(($order_profit - $result['average_size']['perc']['loss']), 2);
                }
                $squere_sum['all_long'] += pow(($order_profit - $result['average_size']['perc']['all_long']), 2);
            }
            if ($account_close_order->order_type === AccountCloseOrder::OP_SELL) {
                if ($account_close_order->isOrderInProfit()) {
                    $squere_sum['win_short'] += pow(($order_profit - $result['average_size']['perc']['win_short']), 2);
                    $squere_sum['win'] += pow(($order_profit - $result['average_size']['perc']['win']), 2);
                } else {
                    $squere_sum['loss_short'] += pow(($order_profit - $result['average_size']['perc']['loss_short']), 2);
                    $squere_sum['loss'] += pow(($order_profit - $result['average_size']['perc']['loss']), 2);
                }
                $squere_sum['all_short'] += pow(($order_profit - $result['average_size']['perc']['all_short']), 2);
            }
        }

        $result['sd']['all'] =  round(sqrt($squere_sum['all'] / ($all_orders - 1)), 2);

        $result['sd']['win'] =  round(sqrt($squere_sum['win'] / ($result['total_ammount']['all_win'] - 1)), 2);
        $result['sd']['loss'] =  round(sqrt($squere_sum['loss'] / ($result['total_ammount']['all_loss'] - 1)), 2);
        $result['sd']['win_short'] =  round(sqrt($squere_sum['win_short'] / ($result['total_ammount']['all_win_short'] - 1)), 2);
        $result['sd']['win_long'] =  round(sqrt($squere_sum['win_long'] / ($result['total_ammount']['all_win_long'] - 1)), 2);
        $result['sd']['loss_short'] =  round(sqrt($squere_sum['loss_short'] / ($result['total_ammount']['all_loss_short'] - 1)), 2);
        $result['sd']['loss_long'] =  round(sqrt($squere_sum['loss_long'] / ($result['total_ammount']['all_loss_long'] - 1)), 2);
        $result['sd']['all_short'] =  round(sqrt($squere_sum['all_short'] / ($result['total_ammount']['all_short'] - 1)), 2);
        $result['sd']['all_long'] =  round(sqrt($squere_sum['all_long'] / ($result['total_ammount']['all_long'] - 1)), 2);

        $result['profit_factor'] = round(- ($result['all_win_loss']['currency']['all_win'] - $result['biggest']['currency']['win']) / $result['all_win_loss']['currency']['all_loss'], 2);
        $result['authentic_profit_factor'] = round(- $result['all_win_loss']['currency']['all_win'] / $result['all_win_loss']['currency']['all_loss'], 2);
        $this->account_trading_stats = $result;

        return $this;
    }

    /* Вычисляемые данные по счету Торговая стата - это массив */
    /**

    Наивысшее значение баланса
    Максимальное количество непрерывных выигрышей (прибыль и кол-во)
    Максимальное количество непрерывных проигрышей (убыток и кол-во)
    Макс непрерывная прибыль (число выигрышей и баксы)
    Макс непрерывный убыток (число проигрышей и баксы)
    Средний непрерывный выигрыш и проигрыш – кол-во
    Сделок в день, неделю, месяц
     */

    public function getAccountCharts($graph_type = 4)
    {
        $data = $this->getProfitsIndexedByDates($graph_type);
        $categories = array_keys(array_values($data));

        $this->graph_balance['series'] = [
            'name' => 'Прибыль',
            'data' => array_values($data)
        ];

        $this->graph_balance['chartOptions'] = [
            'chart' => [
                'sparkline' => [
                    'enabled' => true
                ],
            ],
            'xaxis' => [
                'categories' => $categories
            ],

        ];

        return $this;
    }

    private function getProfitsIndexedByDates($graph_type = 2)
    {
        if ($graph_type === self::GRAPH_TYPE_DEALS) {
            return $this->getAllProfitsIndexedByTradeNums();
        }
        $data = [];
        foreach ($this->account_close_orders as $closed_trade_key => $closed_trade) {
            switch ($graph_type) {
                case self::GRAPH_TYPE_DAY:
                    $date = date('Y.m.d', $closed_trade['order_open_time']);
                    break;
                case self::GRAPH_TYPE_WEEK:
                    $date = date('Y.m.N', $closed_trade['order_open_time']);
                    break;
                case self::GRAPH_TYPE_MONTH:
                    $date = date('Y.m', $closed_trade->order_open_time);
                    break;
            }
            if ($closed_trade_key === 0) {
                $last_date = $date;
                $data[$date] = round($closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
                continue;
            }
            if (!array_key_exists($date, $data)) {
                $data[$date] = round($data[$last_date] + $closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
                $last_date = $date;
                continue;
            }
            $data[$date] = round($data[$last_date] + $closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
            $last_date = $date;
        }
        return $data;
    }

    private function getAllProfitsIndexedByHours()
    {
        $data = [];
        foreach ($this->account_close_orders as $closed_trade_key => $closed_trade) {
            $date = date('Y.m.d H', $closed_trade['order_open_time']);
            if ($closed_trade_key === 0) {
                $last_date = $date;
                $data[$date] = round($closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
                continue;
            }
            if (!array_key_exists($date, $data)) {
                $data[$date] = round($data[$last_date] + $closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
                $last_date = $date;
                continue;
            }
            $data[$date] = round($data[$last_date] + $closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
            $last_date = $date;
        }
        return $data;
    }

    private function getAllProfitsIndexedByTradeNums()
    {
        $data = [];
        foreach ($this->account_close_orders as $closed_trade_key => $closed_trade) {
            if ($closed_trade_key === 0) {
                $data[$closed_trade_key] = round($closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
                continue;
            }
            $data[$closed_trade_key] = round($data[$closed_trade_key - 1] + $closed_trade->getOrderProfit(AccountCloseOrder::PROFIT_CUR), 2);
        }
        return $data;
    }

}