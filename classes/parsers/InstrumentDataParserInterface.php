<?php

namespace app\classes\parsers;

use app\models\instrument_info\Instrument;

interface InstrumentDataParserInterface
{
    /**
     * Возвращает массив исторических котировок инструмента позже переданной даты.
     *
     * @param int $last_date
     * @return array
     */
    public function getInstrumentHistoryDataArray(int $last_date = 0) : array;

    /**
     * Возвращает массив всех данных по инструменту - разную финансовую информацию, дивиденды и прочее
     *
     * @return array
     */
    public function getInstrumentCommonDataArray() : array;

    /**
     * Сетит объект InstrumentLink
     *
     * @param Instrument $instrument_link
     * @return InstrumentDataParserInterface
     */
    public function setInstrumentSettings(Instrument $instrument_link) : InstrumentDataParserInterface;

}