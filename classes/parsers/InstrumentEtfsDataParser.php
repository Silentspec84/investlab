<?php

namespace app\classes\parsers;

use app\classes\helpers\StringFormatter;
use DOMDocument;

class InstrumentEtfsDataParser extends InstrumentGlobalDataParser
{

    private const CURRENCY_POSITION = 79;
    private const EMITENT_POSITION = 86;
    private const ISIN_POSITION = 88;
    private const SN_POSITION = 90;

    private $fields = [
        self::CURRENCY_POSITION => 'currency',
        self::EMITENT_POSITION => 'emitent',
        self::ISIN_POSITION => 'isin',
        self::SN_POSITION => 'sn'
    ];

    public function getInstrumentCommonDataArray() : array
    {
        return $this->getMainPageInfo();
    }

    /**
     * Возвращает массив данных по инструменту с основной страницы
     *
     * @return array
     */
    private function getMainPageInfo() : array
    {
        $result = [];
        $link = self::MAIN_LINK . $this->market_type->code . '/' . $this->instrument_link->link;
        $doc = $this->getPage($link);
        $result['main_data']['title'] = $this->getTitle($doc);
        $result['main_data'] = array_merge($result['main_data'], $this->getMainData($doc));

        return $result;
    }

    /**
     * Получает основные данные по инструменту
     *
     * @param DOMDocument $doc
     * @return array
     */
    private function getMainData(DOMDocument $doc) : array
    {
        $searchNodes = $doc->getElementsByTagName( "span" );
        foreach ($this->fields as $field_id => $field_value) {
            $result[$field_value] = StringFormatter::convertString($searchNodes->item($field_id)->textContent) ?? '';
        }
        return $result;
    }

}