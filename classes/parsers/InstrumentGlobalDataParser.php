<?php

namespace app\classes\parsers;

use app\classes\helpers\StringFormatter;
use app\classes\parsers\elements\TableElement;
use app\models\instrument_info\main_info\Country;
use app\models\instrument_info\Instrument;
use app\models\instrument_info\main_info\MarketType;
use DOMDocument;

class InstrumentGlobalDataParser implements InstrumentDataParserInterface
{

    protected const MAIN_LINK = 'https://ru.investing.com/';
    protected const HISTORY_LINK = '-historical-data';                //Прошлые данные
    protected const COMPANY_PROFILE_LINK = '-company-profile';        //Сведения о компании
    protected const RELATED_INDICES_LINK = '-related-indices';        //Компонент индексов
    protected const FINANCIAL_SUMMARY_LINK = '-financial-summary';    //Финансовая сводка
    protected const INCOME_STATEMENT_LINK = '-income-statement';      //Отчет о доходах
    protected const BALANCE_SHEET_LINK = '-balance-sheet';            //Балансовый отчет
    protected const CASH_FLOW_LINK = '-cash-flow';                    //Отчет о движении денежных средств
    protected const RATIOS_LINK = '-ratios';                          //Коэффициенты
    protected const DIVIDENDS_LINK = '-dividends';                    //Дивиденды по акциям
    protected const EARNINGS_LINK = '-earnings';                      //Отчетность

    /** @var string $error */
    public $error = '';

    /** @var Instrument $instrument_link*/
    protected $instrument_link;

    /** @var MarketType $market_type*/
    protected $market_type;

    /**
     * @param Instrument $instrument_link
     * @return InstrumentDataParserInterface
     */
    public function setInstrumentSettings(Instrument $instrument_link) : InstrumentDataParserInterface
    {
        $this->instrument_link = $instrument_link;
        $this->market_type = MarketType::findOne($this->instrument_link->market_type_id);
        if (!$this->market_type) {
            $this->error = 'Рынок с id ' . $this->instrument_link->market_type_id . ' не найден!';
        }
        return $this;
    }

    /**
     * Возвращает массив истории котировок инструмента
     *
     * @param int $last_date
     * @return array
     */
    public function getInstrumentHistoryDataArray(int $last_date = 0) : array
    {
        $link = self::MAIN_LINK . $this->market_type->code . '/' . $this->instrument_link->link . self::HISTORY_LINK;
        $doc = $this->getPage($link);

        return $this->getHistory($doc);
    }

    /**
     * Возвращает массив всех данных по инструменту - разную финансовую информацию, дивиденды и прочее
     *
     * @return array
     */
    public function getInstrumentCommonDataArray() : array
    {
        return [];
    }

    public function getInstrumentLinks()
    {
        $countries = Country::find()->all();
        https://ru.investing.com/stock-screener/?sp=country::25|sector::a|industry::a|equityType::a|exchange::18%3Ceq_market_cap;1
    }


    /**
     * Забирает всю страничку целиком по переданной ссылке и возвращает ее уже в виде экземпляра класса DOMDocument
     *
     * @param string $link
     * @return DOMDocument
     */
    protected function getPage(string $link)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HEADER, 0);           // возвращает заголовки
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);        // таймаут ответа
        $page = curl_exec($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);
        if ($curl_error) {
            \Yii::error('Ошибка: ' . $curl_error . ' !');
        }

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML ($page);

        return $doc;
    }

    /**
     * Получает название инструмента
     *
     * @param DOMDocument $doc
     * @return string
     */
    protected function getTitle(DOMDocument $doc) : string
    {
        $title = $doc->getElementsByTagName( "h1" )->item(0)->textContent;
        return trim(substr($title, 0, strpos($title, '(')));
    }

    /**
     * Парсит таблицу истории и возвращает чистые готовые данные
     *
     * @param DOMDocument $doc
     * @return array $clear_history
     */
    private function getHistory(DOMDocument $doc) : array
    {
        $table_columns = [
            0 => 'date',
            1 => 'close',
            2 => 'open',
            3 => 'high',
            4 => 'low',
            5 => 'volume'
        ];

        $clear_history = [];

        $raw_history = (new TableElement)
            ->setDoc($doc)
            ->setTableNumber(1)
            ->setColumnNamesAndCount($table_columns, 7)
            ->getResultArray();

        foreach ($raw_history as $raw_history_row_key => $raw_history_row) {
            $clear_history[$raw_history_row_key]['date'] = strtotime($raw_history_row['date']);
            $clear_history[$raw_history_row_key]['close'] = StringFormatter::convertCommasAndDots($raw_history_row['close']);
            $clear_history[$raw_history_row_key]['open'] = StringFormatter::convertCommasAndDots($raw_history_row['open']);
            $clear_history[$raw_history_row_key]['high'] = StringFormatter::convertCommasAndDots($raw_history_row['high']);
            $clear_history[$raw_history_row_key]['low'] = StringFormatter::convertCommasAndDots($raw_history_row['low']);
            $clear_history[$raw_history_row_key]['volume'] = StringFormatter::convertCommasAndDots($raw_history_row['volume']);
        }

        return $clear_history;
    }

}