<?php

namespace app\classes\parsers\elements;

use DOMDocument;

/**
 * Класс парсит данные из указанной таблицы, обрабатывает их и возвращает готовый результат
 *
 */

class TableElement
{

    /** @var DOMDocument $doc */
    private $doc;

    /** @var integer $table_num */
    private $table_num;

    /** @var array $column_names */
    private $column_names = [];

    /** @var integer $column_count */
    private $column_count;

    /** @var array $column_count */
    private $row_names = [];

    /**
     * Метод сетит DOMDocument для поиска и работы с таблицами, возвращает экземпляр TableElement
     *
     * @param DOMDocument $doc
     * @return TableElement
     */
    public function setDoc(DOMDocument $doc):TableElement
    {
        $this->doc = $doc;
        return $this;
    }

    /**
     * Метод сетит $table_num - номер таблицы в списке Dom элементов, возвращает экземпляр TableElement
     *
     * @param int $table_num
     * @return TableElement
     */
    public function setTableNumber(int $table_num):TableElement
    {
        $this->table_num = $table_num;
        return $this;
    }

    /**
     * Метод сетит $table_num - номер таблицы в списке Dom элементов, и $columns_count - количество колонок в таблице и
     * возвращает экземпляр TableElement.
     * Принимает массив, где ключ - номер нужной колонки, значение - ее название в итоговом массиве и
     * количество колонок в исходной таблице
     *
     * @param array $column_names
     * @param int $columns_count
     * @return TableElement
     */
    public function setColumnNamesAndCount(array $column_names, int $columns_count) : TableElement
    {
        $this->column_names = $column_names;
        $this->column_count = $columns_count;
        return $this;
    }

    public function setRowNames(array $row_names) : TableElement
    {
        $this->row_names = $row_names;
        return $this;
    }

    /**
     * @return array
     */
    public function getResultArray()
    {
        $result = [];
        $table_content = $this->doc->getElementsByTagName("tbody")->item($this->table_num);
        $rows_in_table_count = $table_content->getElementsByTagName('tr')->length;

        for ($current_row_num = 0; $current_row_num < $rows_in_table_count; $current_row_num++) {
            $current_row = $table_content->getElementsByTagName('tr')->item($current_row_num);

            for ($cell_num = 0; $cell_num < $this->column_count; $cell_num++) {
                if (array_key_exists($cell_num, $this->column_names)) {
                    $current_cell = $current_row->getElementsByTagName('td')->item($cell_num);
                    if (!empty($this->row_names)) {
                        if (!array_key_exists($current_row_num, $this->row_names)) {
                            continue;
                        }
                        $result[$this->row_names[$current_row_num]][$this->column_names[$cell_num]] = $current_cell->textContent;
                    } else {
                        $result[$current_row_num][$this->column_names[$cell_num]] = $current_cell->textContent;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Метод чистит результат парсинга таблицы от пустых строк и пробелов и возвращает очищенный результат
     *
     * @param \DOMNodeList $searchNodes
     * @return array
     */
    private function clearTableData(\DOMNodeList $searchNodes)
    {
        $cleared_result = [];
        $table = explode(' ', $searchNodes->item($this->table_num)->textContent);
        foreach ($table as $table_key => $table_element) {
            $cleared_element = preg_replace('/\s+/', '', $table_element);
            if ($cleared_element !== '') {
                $cleared_result[] = $cleared_element;
            }
        }
        return $cleared_result;
    }

}