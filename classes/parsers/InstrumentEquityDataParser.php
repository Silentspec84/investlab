<?php

namespace app\classes\parsers;

use app\classes\helpers\ArrayHelper;
use app\classes\helpers\StringFormatter;
use app\classes\parsers\elements\TableElement;
use app\models\instrument_info\main_info\Country;
use app\models\instrument_info\main_info\Exchange;
use app\models\instrument_info\main_info\Industry;
use app\models\instrument_info\Instrument;
use app\models\instrument_info\main_info\MarketType;
use app\models\instrument_info\main_info\Sector;
use DOMDocument;

/** С investing берем общие данные, как брали
С investfunds пока лотность
С conomy берем дивиденды пока
 *Историю берем с ммвб
 */

class InstrumentEquityDataParser extends InstrumentGlobalDataParser
{

    private const CURRENCY_POSITION = 79;
    private const ISIN_POSITION = 86;
    private const SN_POSITION = 88;

    private $main_fields = [
        self::CURRENCY_POSITION => 'currency',
        self::ISIN_POSITION => 'isin',
        self::SN_POSITION => 'sn'
    ];

    /**
     * Возвращает массив всех данных по инструменту - разную финансовую информацию, дивиденды и прочее
     *
     * @return array
     */
    public function getInstrumentCommonDataArray() : array
    {
        $result = $this->getMainPageInfo();
        $info_data = $this->getCompanyProfilePageInfo();
        $result = array_merge($result, $info_data);
        ea($result);
        return $result;
    }

    /**
     * Возвращает массив данных по инструменту с основной страницы
     *
     * @return array
     */
    private function getMainPageInfo() : array
    {
        $link = self::MAIN_LINK . $this->market_type->code . '/' . $this->instrument_link->investing_link;
        $doc = $this->getPage($link);
        return $this->getMainData($doc);
    }

    /**
     * Получает основные данные по инструменту
     *
     * @param DOMDocument $doc
     * @return array
     */
    private function getMainData(DOMDocument $doc) : array
    {
        $searchNodes = $doc->getElementsByTagName( "span" );
        foreach ($this->main_fields as $field_id => $field_value) {
            $result[$field_value] = StringFormatter::convertString($searchNodes->item($field_id)->textContent) ?? '';
        }
        return $result;
    }

    /**
     * Возвращает массив данных по инструменту со страницы профиля компании (для акций)
     *
     * @return array
     */
    private function getCompanyProfilePageInfo() : array
    {
        $result = [];
        $link = self::MAIN_LINK . $this->market_type->code . '/' . $this->instrument_link->investing_link . self::COMPANY_PROFILE_LINK;
        $doc = $this->getPage($link);
        $searchNodes = $doc->getElementsByTagName("a");
        $industry = $searchNodes->item(368)->textContent;
        $result['industry_id'] = Industry::find()->where(['title' => $industry])->one()->id;
        $sector = $searchNodes->item(369)->textContent;
        $result['sector_id'] = Sector::find()->where(['title' => $sector])->one()->id;

        return $result;
    }

    /**
     *
     * @param DOMDocument $doc
     * @return array
     */
    private function getDividends(DOMDocument $doc) : array
    {
        $table_columns = [
            0 => 'expiration',      // Дата экспирации дивидендов
            1 => 'dividends',       // Сумма дивидендов в валюте акции
            3 => 'pay_date',        // Дата выплаты
            4 => 'profit'           // Прибыль в % от стоимости акции
        ];

        $dividends_table =  (new TableElement)
            ->setDoc($doc)
            ->setTableNumber(1)
            ->setColumnNamesAndCount($table_columns, 9)
            ->getResultArray();

        foreach ($dividends_table as $dividend_row_key => $dividend_row) {
            $result[$dividend_row_key]['expiration'] = strtotime($dividend_row['expiration']);
            $result[$dividend_row_key]['dividends'] = StringFormatter::convertCommasAndDots($dividend_row['dividends']);
            $result[$dividend_row_key]['pay_date'] = strtotime($dividend_row['pay_date']);
            $result[$dividend_row_key]['profit'] = StringFormatter::convertPercentString($dividend_row['profit']);
        }

        return $result;
    }

    private function convertTable(array $table) {
        foreach ($table as $subtable_name => $subtable) {
            foreach ($subtable as $date => $value) {
                if (strrpos($value, '%') !== false) {
                    $result[$subtable_name][strtotime($date)]
                        = StringFormatter::convertPercentString($value);
                } elseif ($value === '-') {
                    $result[$subtable_name][strtotime($date)] = 0;
                } else {
                    $result[$subtable_name][strtotime($date)]
                        = StringFormatter::convertCommasAndDots($value);
                }
            }
        }
        return $result;
    }


    private function getAllNodes($searchNodes)
    {
        for ($i = 0; $i < 500; $i++) {
            $result[] = $searchNodes->item($i)->textContent;
        }
        ea($result);
    }
}