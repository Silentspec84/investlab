<?php

namespace app\classes\parsers;

use app\classes\helpers\StringFormatter;
use DOMDocument;

class InstrumentFundsDataParser extends InstrumentGlobalDataParser
{
    protected const CURRENCY_POSITION = 79;
    protected const EMITENT_POSITION = 86;
    protected const ISIN_POSITION = 88;
    protected const SN_POSITION = 90;

    private $fields = [
        self::CURRENCY_POSITION => 'currency',
        self::EMITENT_POSITION => 'emitent',
        self::ISIN_POSITION => 'isin',
        self::SN_POSITION => 'sn'
    ];

    public function getInstrumentCommonDataArray() : array
    {
        return $this->getMainPageInfo();
    }

    /**
     * Возвращает массив данных по инструменту с основной страницы
     *
     * @return array
     */
    private function getMainPageInfo() : array
    {
        $result = [];
        $link = self::MAIN_LINK . $this->market_type->code . '/' . $this->instrument_link->link;
        $doc = $this->getPage($link);
        $result['main_data']['title'] = $this->getTitle($doc);
        $result['main_data'] = array_merge($result['main_data'], $this->getMainData($doc));

        return $result;
    }

    /**
     * Получает название инструмента
     *
     * @param DOMDocument $doc
     * @return string
     */
    private function getTitle(DOMDocument $doc) : string
    {
        $title = $doc->getElementsByTagName( "h1" )->item(0)->textContent;
        return trim(substr($title, 0, strpos($title, '(')));
    }

    /**
     * Получает основные данные по инструменту
     *
     * @param DOMDocument $doc
     * @return array
     */
    private function getMainData(DOMDocument $doc) : array
    {
        $searchNodes = $doc->getElementsByTagName( "span" );
        foreach ($this->fields as $field_id => $field_value) {
            $result[$field_value] = StringFormatter::convertString($searchNodes->item($field_id)->textContent) ?? '';
        }
        return $result;
    }

}