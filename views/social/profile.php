<?php

use app\assets\SocialProfileAsset;

SocialProfileAsset::register($this);

?>

<div class="container-fluid" id="profile" ref="config"
     data-csrf='<?= Yii::$app->request->csrfToken?>'
    data-profile_id='<?=$profile_id?>'
>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" aria-label="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/social/profiles">Сообщество</a></li>
            <li class="active">Профиль пользователя такого-то</li>
        </ol>
    </nav>

    <div class="row">

        <div class="col-2">
            <div class="card mb-4">
                <div class="card-body">
                    <ul class="nav flex-column nav-pills">
                        <li class="nav-item">
                            <a :class="getStateClass('all_accounts')" v-on:click="all_accounts_state()">
                                <i class="fas fa-globe mr-2 btn-outline-info"></i>
                                Все счета
                            </a>
                        </li>
                        <li class="nav-item">
                            <a :class="getStateClass('my_accounts')" v-on:click="my_accounts_state()">
                                <i class="fas fa-chart-pie mr-2 btn-outline-primary"></i>
                                Мои счета
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#add_account">
                                <i class="fas fa-plus mr-2 btn-outline-success"></i>
                                Добавить счет
                            </a>
                        </li>
                        <li class="nav-item">
                            <a :class="getStateClass('favourites')" v-on:click="favourites_state()">
                                <i class="fas fa-star mr-2 btn-outline-warning"></i>
                                Избранное
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-10">

        </div>
    </div>
</div>


