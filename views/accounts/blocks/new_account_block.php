<?php
?>

<!--Избранное-->
<div class="modal fade" id="add_account" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Добавить счет</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="closeModal()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label class="small text-nowrap mb-0 pull-left">Номер счета</label>
                            <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Номер счета" data-original-title="" title=""
                                    class="form-control col-12"
                                    v-bind:data-original-title="getError('account')"
                                    v-bind:class="getError('account') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                    v-model="new_account_form.account"/>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label class="small text-nowrap mb-0 pull-left">Название счета</label>
                            <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Название счета" data-original-title="" title=""
                                    class="form-control col-12"
                                    v-bind:data-original-title="getError('acc_name')"
                                    v-bind:class="getError('acc_name') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                    v-model="new_account_form.acc_name"/>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label class="small text-nowrap mb-0 pull-left">Тип счета</label>
                            <select data-toggle="tooltip"
                                    data-placement="right"
                                    data-original-title=""
                                    title=""
                                    class="form-control col-12 text-center"
                                    v-model="new_account_form.acc_type"
                                    v-bind:data-original-title="getError('acc_type')"
                                    v-bind:class="getErrorClass('acc_type')"
                            >
                                <option v-if="!new_account_form.acc_type" disabled :value="null">Выберите платформу</option>
                                <option v-for="platform in platforms"
                                        :disabled="platform.disabled"
                                        :title="platform.message"
                                        :data-thumbnail="platform.icon"
                                        :value="platform.id"
                                        :selected="new_account_form.acc_type">
                                    {{platform.title}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="small text-nowrap mb-0 pull-left">Описание счета</label>
                            <textarea rows="3" data-toggle="tooltip" data-placement="right" placeholder="Описание счета" data-original-title="" title=""
                                      class="form-control col-12"
                                      v-bind:data-original-title="getError('acc_descr')"
                                      v-bind:class="getError('acc_descr') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                      v-model="new_account_form.acc_descr">
                                </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary btn-sm" v-on:click="closeModal()">Отменить</button>
                <button  v-on:click="saveAccount()" type="button" data-dismiss="modal" class="btn btn-primary btn-sm">
                    Сохранить
                </button>
            </div>
        </div>
    </div>
</div>
