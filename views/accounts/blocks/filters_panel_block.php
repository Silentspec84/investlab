<?php
?>

<!--Панель фильтров-->
<div id="filters_panel">
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-outline-primary mr-2" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <span class="oi oi-magnifying-glass mr-3"></span>Поиск и фильтрация
                    </button>
                    <button v-show="table_view" class="btn btn-outline-secondary ml-2" type="button" v-on:click="switchTableView()">
                        <span class="oi oi-grid-two-up mr-3"></span>Карточки
                    </button>
                    <button v-show="!table_view" class="btn btn-outline-secondary ml-2" type="button" v-on:click="switchTableView()">
                        <span class="oi oi-list mr-3"></span>Таблица
                    </button>
                </h5>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Минимальный срок существования счета</span>
                        </div>
                        <input type="text" class="form-control" aria-label="Text input with dropdown button">
                        <div class="input-group-append">
                            <select class="custom-select" id="inputGroupSelect02">
                                <option selected>Время...</option>
                                <option value="1">Дней</option>
                                <option value="2">Месяцев</option>
                                <option value="3">Лет</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">Прибыль</span>
                        </div>
                        <input type="text" class="form-control" placeholder="от">
                        <input type="text" class="form-control" placeholder="до">
                        <div class="input-group-append">
                            <span class="input-group-text">%</span>
                        </div>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="">Просадка</span>
                        </div>
                        <input type="text" class="form-control" placeholder="от">
                        <input type="text" class="form-control" placeholder="до">
                        <div class="input-group-append">
                            <span class="input-group-text">%</span>
                        </div>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-primary" id="">Поиск</button>
                        </div>
                        <div class="input-group-append">
                            <button class="btn btn-outline-dark">Очистить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
