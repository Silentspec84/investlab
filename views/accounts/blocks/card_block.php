<?php
?>
<div class="card mt-4">
    <div class="card-header">
        <div class="row">
            <div class="col-3">
                <a :href="'/accounts/view/' + account.account_data.id"><h5 class="pull-left">{{account.account_data.acc_name}}</h5></a>
            </div>
            <div class="col-3">

            </div>
            <div class="col-1">
            </div>
            <div class="col-5">
                <ul class="nav nav-tabs card-header-tabs pull-right">
                    <li class="nav-item">
                        <a class="nav-link active">Основные данные</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">Подробнее</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="row">
                    <div class="col-2">
                        <div class="btn-group-vertical">
                            <a :href="'/accounts/view/' + account.account_data.id">
                                <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Просмотр">
                                    <i class="fas fa-desktop"></i>
                                </button>
                            </a>
                            <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="В избранное">
                                <i class="far fa-star"></i>
                                <!--                                                <i class="fas fa-star"></i>-->
                            </button>
                            <button v-if="userCanManageAccount(account.user_data, user)" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Редактировать">
                                <i class="far fa-edit"></i>
                            </button>
                            <button v-if="userCanManageAccount(account.user_data, user)" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Удалить">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-4"><img class="pull-left" :src="getUserAvatar(account)" style="width: 90%;" alt=""></div>
                    <div class="col-6">
                        <div>
                            Прибыль: {{account.account_main_stats.account_profit}} {{account.account_data.account_currency}}
                        </div>
                        <div>
                            Баланс: {{account.account_main_stats.balance}} {{account.account_data.account_currency}}
                        </div>
                        <div>
                            Общая прибыль: {{account.account_main_stats.total_return}} %
                        </div>
                        <div>
                            Реальная прибыль: {{account.account_main_stats.annualized_return}} %
                        </div>
                        <div>
                            Характер торговли: {{account.account_main_stats.time_frame}}
                        </div>
                        <span class="badge badge-light pull-right" data-toggle="tooltip" data-placement="bottom" title="" :title="account.account_main_stats.acc_type.title">
                            <img :src="account.account_main_stats.acc_type.icon" style="width: 15%;" alt="" >
                        </span>
                        <span v-if="account.account_main_stats.account_trade_mode_num === $config.ACCOUNT_TRADE_MODE_DEMO"
                              class="oi oi-signal pull-right" data-toggle="tooltip" data-placement="bottom" title="" :title="account.account_main_stats.account_trade_mode">
                        </span>
                        <span v-if="account.account_main_stats.account_trade_mode_num === $config.ACCOUNT_TRADE_MODE_CONTEST"
                              class="oi oi-monitor pull-right" data-toggle="tooltip" data-placement="bottom" title="" :title="account.account_main_stats.account_trade_mode">
                        </span>
                        <span v-if="account.account_main_stats.account_trade_mode_num === $config.ACCOUNT_TRADE_MODE_REAL"
                              class="oi oi-dollar pull-right" data-toggle="tooltip" data-placement="bottom" title="" :title="account.account_main_stats.account_trade_mode">
                        </span>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6">
                        <h6 class="text-center">
                            <a :href="'/social/profile/' + account.user_data.id">
                                <button class="btn btn-outline-dark btn-block">
                                    <span class="oi oi-person mr-2"></span>
                                    {{account.user_data.login_name}}
                                </button>
                            </a>
                        </h6>
                    </div>
                    <div class="col-6"></div>
                </div>
            </div>
            <div class="col-4">

                <div>
                    Последнее подключение: {{account.account_data.date_last_connected}}
                </div>
                <div>
                    Профит фактор: {{account.account_main_stats.authentic_profit_factor}}
                </div>
                <div>
                    Средняя сделка: {{account.account_main_stats.average_size.all}} {{account.account_data.account_currency}}
                </div>
                <div>
                    % выигрышных: {{account.account_main_stats.winning_perc}} %
                </div>
                <p class="card-text">{{account.account_data.acc_descr}}</p>
            </div>
            <div class="col-4">
                <apexchart type=area width="70%" :options="account.graph_balance.chartOptions" :series="[account.graph_balance.series]"></apexchart>
            </div>
        </div>
    </div>
</div>
