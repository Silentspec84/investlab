<?php
?>

<!--Все счета-->
<div>
    <div class="row">
        <div class="col-6" v-show="all_accounts" v-for="account in accounts">
            <!--Карточка счета-->
            <?=$this->render('@app/views/accounts/blocks/card_block');?>
        </div>
    </div>
</div>


<!--            <div>-->
<!--                Плечо: 1:{{account.account_data.account_leverage}}-->
<!--            </div>-->
<!--            <div>-->
<!--                Сделок: {{account.account_main_stats.all_orders}}-->
<!--            </div>-->
<!--            <div>-->
<!--                Из них EA: {{account.account_main_stats.ea_orders_ratio}} %-->
<!--            </div>-->
<!--            <div>-->
<!--                Среднее время удержания: {{account.account_main_stats.av_holding_time}} минут-->
<!--            </div>-->
<!--            <div>-->
<!--                Брокер: {{account.account_data.account_company}}-->
<!--            </div>-->
<!--            <div>-->
<!--                Сервер: {{account.account_data.account_server}}-->
<!--            </div>-->