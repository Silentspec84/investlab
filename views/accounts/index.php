<?php

use app\assets\AccountsAsset;

AccountsAsset::register($this);

?>

<div class="container-fluid" id="accounts" ref="config" data-csrf='<?= Yii::$app->request->csrfToken?>'>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" aria-label="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li>Счета</li>
            <li class="active">{{getStateTitle()}}</li>
        </ol>
    </nav>

    <div class="row">
        <!--Боковое левое меню-->
        <?=$this->render('@app/views/accounts/blocks/left_menu_block');?>
        <div class="col-10">
            <!--Панель фильтров-->
            <?=$this->render('@app/views/accounts/blocks/filters_panel_block');?>
            <!--Все счета-->
            <?=$this->render('@app/views/accounts/blocks/all_accounts_block');?>
            <!--Мои счета-->
            <?=$this->render('@app/views/accounts/blocks/my_accounts_block');?>
            <!--Избранное-->
            <?=$this->render('@app/views/accounts/blocks/favourites_block');?>
        </div>
    </div>

    <!--Избранное-->
    <?=$this->render('@app/views/accounts/blocks/new_account_block');?>

</div>


