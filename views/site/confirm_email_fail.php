<?php

?>
<br xmlns="http://www.w3.org/1999/html">
<br><br><br><br>
<div class="container-fluid mt-5">
    <div class="row align-middle">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="card">
                <h3 class="card-header text-center">
                    Email не был подтвержден!
                </h3>
                <div class="card-body text-center">
                    <p>Что-то пошло не так и ваша регистрация не была подтверждена.<p>
                    <p>Пожалуйста, обратитесь в службу технической поддержки.</p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
