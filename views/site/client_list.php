<?php

use app\models\Company;
use \app\models\CompanyClient;

\app\assets\VueClientAsset::register($this);

$this->title = 'Мои клиенты';
?>

<h2><?= $this->title ?></h2>

<main class="page__main" id="client">
    <client-list></client-list>
</main>
<script type="text/x-template" id="client-list-template">
    <div class="bg-white rounded p-2" ref="config"
         data-csrf="<?= \Yii::$app->request->csrfToken?>"
         data-business_type_list = '<?=json_encode(Company::$business_type_list, JSON_NUMERIC_CHECK)?>'
         data-show_modal = '<?=json_encode($show_modal)?>'
         data-base_order_rate_id = '<?=json_encode($base_order_rate_id)?>'
         data-status_list='<?= json_encode(CompanyClient::getClientCompanyStatusList())?>'
         data-status_modes='<?=json_encode(CompanyClient::getClientCompanyStatusModes())?>'
    >
        <div class="bg-white rounded p-2">
            <div class="mb-3">
                <button type="button"
                        v-on:click="show_modal=true"
                        class="btn btn-primary"
                >
                    + &nbsp;&nbsp;&nbsp;Добавить клиента
                </button>
            </div>
            <div class="row m-t-20 filter-options">
                <div class="col-2">
                    <div class="form-group">
                        <label for="client-type-select">Тип клиента</label>
                        <select class="form-control" id="client-type-select" v-model="business_type_filter">
                            <option v-for="type in getBusinessTypes(true)" :value="type.id">{{type.title}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label for="client-status-select">Статус клиента</label>
                        <select class="form-control" id="client-status-select" v-model="status_filter">
                            <option v-for="status in getClientStatuses()" :value="status.value">{{status.title}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <grid class="mb-0" :heads="getTableHeads()" :rows="getFilteredList()"
                              :filter_key="query" sortable="true" :columns_not_to_sort = "getNotSortableColumns()"
                              :custom_values_to_sorted_columns = "getCustomValuesToSortedColumns()">
                            <template slot="rows" slot-scope="client">
                                <td>{{client.row.index}}</td>
                                <td>
                                    <span v-html="getBusinessType(client.row)"></span>
                                </td>
                                <td>{{client.row.full_name}}</td>
                                <td>{{client.row.phone}}</td>
                                <td>{{client.row.email}}</td>
                                <td>
                                    <span v-html="getClientInitials(client.row)"></span>
                                </td>
                                <td>
                                    <span v-html="getManagerInitials(client.row.manager_user_id)"></span>
                                </td>
                                <td>
                                    <span :class="getStatusClass(client.row)" v-html="getStatusTitle(client.row)"></span>
                                </td>
                                <td>
                                    <span v-if="client.row.status !== $config.STATUS_NOT_CONFIRM" v-html="getAction(client.row)" :disabled="email_is_sending" v-on:click="makeAction(client.row)"></span>
                                    <span v-if="client.row.status === $config.STATUS_NOT_CONFIRM">
                                        <span class="btn btn-link btn-sm p-0" v-on:click="changeClientStatus(client.row.manager_client_rel, true, true)">Подтвердить</span>/
                                        <span class="btn btn-link btn-sm p-0" v-on:click="changeClientStatus(client.row.manager_client_rel, false, true)">Отменить</span>
                                    </span>
                                </td>
                                <td>
                                    <div class="dropleft grid_dropdown_menu">
                                        <button type="button" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-light btn-sm">
                                            <span class="oi oi-list"></span>
                                        </button>
                                        <div aria-labelledby="dropdownMenuButton" class="dropdown-menu" x-placement="top">
                                            <button type="button" class="dropdown-item" v-on:click="showModal(client.row)">Редактировать</button>
                                            <button type="button" class="dropdown-item" v-on:click="showDropDialog(client.row)">Удалить</button>
                                        </div>
                                    </div>
                                </td>
                            </template>
                        </grid>
                    </div>

                    <modal :show="show_modal">
                        <template slot="body">
                            <div class="modal-dialog m-0 p-0" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">{{form.title}}</h2>
                                        <button type="button" class="close">
                                            <span aria-hidden="true" v-on:click="closeModal()">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Фамилия</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Фамилия" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('last_name')"
                                                            v-bind:class="getError('last_name') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.last_name"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Имя</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Имя" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('first_name')"
                                                            v-bind:class="getError('first_name') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.first_name"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Отчество</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Отчество" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('middle_name')"
                                                            v-bind:class="getError('middle_name') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.middle_name"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Телефон</label>
                                                    <input type="text" data-toggle="tooltip" data-placement="right" placeholder="+7(###)###-##-##" data-original-title="" title="" id="phone"
                                                           class="form-control col-12"
                                                           v-model="new_client_form.phone"
                                                           v-mask="'+7(###)###-##-##'"
                                                           v-bind:data-original-title="getError('phone')"
                                                           v-bind:class="getErrorClass('phone')"
                                                    >
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">E-mail</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="E-mail" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('email')"
                                                            v-bind:class="getError('email') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.email"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Менеджер</label>
                                                    <select class="form-control" v-model="new_client_form.manager_user_id"
                                                            data-toggle="tooltip" data-placement="right"
                                                            v-bind:data-original-title="getError('manager_user_id')"
                                                            v-bind:class="getError('manager_user_id') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                    >
                                                        <option v-for="manager in getManagers()" v-bind:value="manager.id">{{manager.title}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Компания</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Компания" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('client_company_name')"
                                                            v-bind:class="getError('client_company_name') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.client_company_name"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">ИНН</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="ИНН" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('inn')"
                                                            v-bind:class="getError('inn') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.inn"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Тип клиента</label>
                                                    <select class="form-control col-12" v-model="new_client_form.client_company_business_type"
                                                            data-toggle="tooltip" data-placement="right"
                                                            v-bind:data-original-title="getError('client_company_business_type')"
                                                            v-bind:class="getError('client_company_business_type') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                    >
                                                        <option selected v-for="type in getBusinessTypes(false)" :value="type.id">{{type.title}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Наценка</label>
                                                    <select class="form-control col-12" v-model="new_client_form.order_rate_id"
                                                            data-toggle="tooltip" data-placement="right"
                                                            v-bind:data-original-title="getError('order_rate_id')"
                                                            v-bind:class="getError('order_rate_id') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                    >
                                                        <option selected v-for="type in getRates()" :value="type.id">{{type.title}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="small text-nowrap mb-0 pull-left">Номер договора</label>
                                                    <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Номер договора" data-original-title="" title=""
                                                            class="form-control col-12"
                                                            v-bind:data-original-title="getError('contract')"
                                                            v-bind:class="getError('contract') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                            v-model="new_client_form.contract"/>
                                                </div>
                                                <div v-if="showCheckoboxCreateAccount(new_client_form)" class="form-group">
                                                    <div class="mb-4 mt-2 col-12">
                                                        <input class="mr-2" type="checkbox" v-model="new_client_form.create_account"
                                                               data-toggle="tooltip" data-placement="right" data-original-title="" title=""
                                                               v-bind:data-original-title="getError('create_account')"
                                                               v-bind:class="getError('create_account') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                                        >
                                                        <span>Создать аккаунт</span>
                                                        <img width="25" height="25" src="/../images/info.svg" data-toggle="tooltip" data-placement="left" title="Создать личный кабинет клиента">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-sm" v-on:click="closeModal()">Отменить</button>
                                        <button  v-on:click="addClient()" type="button" class="btn btn-primary btn-sm">
                                            Сохранить
                                        </button>
                                        <div v-if="new_client_form.client_company_id" class="col-md-5 p-t-40"><small><small>Дата регистрации: <br>{{new_client_form.date_created}}</small></small></div>
                                    </div>
                                </div>
                            </div>
                        </template>
                    </modal>

                    <div v-show="show_modal" class="modal-backdrop fade show"></div>

                </div>
            </div>
        </div>
</script>