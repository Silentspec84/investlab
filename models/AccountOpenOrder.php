<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id
 * @property integer $ticket
 * @property integer $order_open_time
 * @property integer $order_type
 * @property float $order_lots
 * @property string $order_symbol
 * @property float $order_open_price
 * @property float $order_sl
 * @property float $order_tp
 * @property float $commission
 * @property float $order_swap
 * @property float $order_profit
 * @property string $order_comment
 * @property string $order_user_comment
 * @property string $order_screenshot
 * @property integer $order_magic
 * @property string $order_hash
 * @property integer $date_modified
 * @property integer $date_created
 */

class AccountOpenOrder extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_open_orders';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
            $this->order_hash = $this->getHashForOrder();
        }

        $this->date_modified = time();
        return parent::beforeValidate();
    }

    public function getHashForOrder() {
        return md5($this->ticket . $this->order_open_time . $this->order_lots . $this->order_open_price);
    }

    public function getUserAccount()
    {
        return $this->hasOne(UserAccount::className(), ['id' => 'account_id']);
    }

    public static function formOrdersDataForImport($orders, $user_id, $account_id)
    {
        try {
            self::clearOldOpenedOrders($user_id, $account_id);
        } catch (Exception $e) {
            return [];
        }

        $data = [];
        foreach ($orders as $key => $order) {
            if (!is_array($order)) {
                continue;
            }
            if (!array_key_exists('ticket', $order) || in_array($key, ['login_name', 'password', 'account'])) {
                continue;
            }
            $order_hash = md5($order['ticket'] . $order['order_open_time'] . $order['order_lots'] . $order['order_open_price']);
            $data[] = [
                'user_id' => $user_id,
                'account_id' => $account_id,
                'ticket' => (int)$order['ticket'],
                'order_open_time' => (int)$order['order_open_time'],
                'order_type' => (int)$order['order_type'],
                'order_lots' => (float)$order['order_lots'],
                'order_symbol' => $order['order_symbol'],
                'order_open_price' => (float)$order['order_open_price'],
                'sl' => (float)$order['sl'],
                'tp' => (float)$order['tp'],
                'commission' => (float)$order['commission'],
                'order_swap' => (float)$order['order_swap'],
                'order_profit' => (float)$order['order_profit'],
                'order_comment' => $order['order_comment'],
                'order_user_comment' => '',
                'order_screenshot' => '',
                'order_magic' => (int)$order['order_magic'],
                'order_hash' => $order_hash,
                'date_created' => time(),
                'date_modified' => time()
            ];
        }
        return $data;
    }

    /**
     * @param $user_id
     * @param $account_id
     * @throws \yii\db\Exception
     */
    public static function clearOldOpenedOrders($user_id, $account_id)
    {
        $current_account_orders = self::find()->where(['user_id' => $user_id, 'account_id' => $account_id])->indexBy('id')->asArray()->all();
        Yii::$app->db->createCommand()->delete(self::tableName(), ['id' => array_keys($current_account_orders)])->execute();
    }

    public static function saveOrdersData($data)
    {
        $fields_array = [
            'user_id',
            'account_id',
            'ticket',
            'order_open_time',
            'order_type',
            'order_lots',
            'order_symbol',
            'order_open_price',
            'order_sl',
            'order_tp',
            'commission',
            'order_swap',
            'order_profit',
            'order_comment',
            'order_user_comment',
            'order_screenshot',
            'order_magic',
            'order_hash',
            'date_created',
            'date_modified',
        ];

        try {
            Yii::$app->db->createCommand()->batchInsert(self::tableName(), $fields_array, $data)->execute();
        } catch (Exception $e) {
            return $e;
        }
    }

}