<?php

namespace app\models;

use app\models\queries\AccountCloseOrderQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id
 * @property integer $ticket
 * @property integer $order_open_time
 * @property integer $order_type
 * @property float $order_lots
 * @property string $order_symbol
 * @property float $order_open_price
 * @property float $order_sl
 * @property float $order_tp
 * @property integer $order_close_time
 * @property float $order_close_price
 * @property float $commission
 * @property float $order_swap
 * @property float $order_profit
 * @property string $order_comment
 * @property string $order_user_comment
 * @property string $order_screenshot
 * @property integer $order_magic
 * @property string $order_hash
 * @property integer $date_modified
 * @property integer $date_created
 */

class AccountCloseOrder extends ActiveRecord
{

    public const OP_BALANCE = 6;    // Пополнения и снятия
    public const OP_SELLSTOP = 5;   // Отложенный ордер SELL STOP
    public const OP_BUYSTOP = 4;    // Отложенный ордер BUY STOP
    public const OP_SELLLIMIT = 3;  // Отложенный ордер SELL LIMIT
    public const OP_BUYLIMIT = 2;   // Отложенный ордер BUY LIMIT
    public const OP_SELL = 1;       // Продажа
    public const OP_BUY = 0;        // Покупка

    public const PROFIT_PIPS = 10;
    public const PROFIT_PERC = 11;
    public const PROFIT_CUR = 12;

    public static $order_type_names = [
        self::OP_BUY => 'Покупка',
        self::OP_SELL => 'Продажа',
        self::OP_BUYLIMIT => 'Отложенный ордер BUY LIMIT',
        self::OP_SELLLIMIT => 'Отложенный ордер SELL LIMIT',
        self::OP_BUYSTOP => 'Отложенный ордер BUY STOP',
        self::OP_SELLSTOP => 'Отложенный ордер SELL STOP',
        self::OP_BALANCE => 'Пополнения и снятия',
    ];

    public static $non_statistic_orders = [
        self::OP_BUYLIMIT, self::OP_SELLLIMIT, self::OP_BUYSTOP, self::OP_SELLSTOP, self::OP_BALANCE
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_close_orders';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
            $this->order_hash = $this->getHashForOrder();
        }

        $this->date_modified = time();
        return parent::beforeValidate();
    }

    public static function find()
    {
        return new AccountCloseOrderQuery(get_called_class());
    }

    public function isOrderStatistical()
    {
        return !in_array($this->order_type, self::$non_statistic_orders);
    }

    public function isOrderInProfit()
    {
        return $this->order_profit + $this->order_swap + $this->commission >= 0;
    }

    public function getOrderProfit($profit_type, $balance = 0)
    {
        $order_profit = $this->order_profit + $this->order_swap + $this->commission;
        switch ($profit_type)
        {
            case self::PROFIT_PIPS:
                return $this->order_type === self::OP_BUY
                    ? ($this->order_close_price - $this->order_open_price)
                    : ($this->order_open_price - $this->order_close_price);

            case self::PROFIT_PERC:
                return ($order_profit / $balance) * 100;

            case self::PROFIT_CUR:
                return $order_profit;
        }
        return 0;
    }

    public function getOrderHoldingTime()
    {
        return $this->order_close_time - $this->order_open_time;
    }

    public function getHashForOrder() {
        return md5($this->ticket . $this->order_open_time . $this->order_close_time . $this->order_lots . $this->order_open_price . $this->order_close_price);
    }

    public function getUserAccount()
    {
        return $this->hasOne(UserAccount::className(), ['id' => 'account_id']);
    }

    public static function formOrdersDataForImport($orders, $user_id, $account_id)
    {
        foreach ($orders as $key => $order) {

            if (in_array($key, ['login_name', 'password', 'account'])) {
                continue;
            }
            $order_hash = md5($order['ticket'] . $order['order_open_time'] . $order['order_lots'] . $order['order_open_price']);
            $data[$order_hash] = [
                'user_id' => $user_id,
                'account_id' => $account_id,
                'ticket' => (int)$order['ticket'],
                'order_open_time' => (int)$order['order_open_time'],
                'order_type' => (int)$order['order_type'],
                'order_lots' => (float)$order['order_lots'],
                'order_symbol' => $order['order_symbol'],
                'order_open_price' => (float)$order['order_open_price'],
                'order_sl' => (float)$order['sl'],
                'order_tp' => (float)$order['tp'],
                'order_close_time' => (int)$order['order_close_time'],
                'order_close_price' => (float)$order['order_close_price'],
                'commission' => (float)$order['commission'],
                'order_swap' => (float)$order['order_swap'],
                'order_profit' => (float)$order['order_profit'],
                'order_comment' => $order['order_comment'],
                'order_user_comment' => '',
                'order_screenshot' => '',
                'order_magic' => (int)$order['order_magic'],
                'order_hash' => $order_hash,
                'date_created' => time(),
                'date_modified' => time()
            ];
        }
        return self::removeDuplicateOrders($data, $user_id, $account_id);
    }

    public static function removeDuplicateOrders($new_orders, $user_id, $account_id)
    {
        $current_account_orders = self::find()->where(['user_id' => $user_id, 'account_id' => $account_id])->indexBy('order_hash')->asArray()->all();
        return array_values(array_diff_key($new_orders, $current_account_orders));
    }

    public static function saveOrdersData($data)
    {
        $fields_array = [
            'user_id',
            'account_id',
            'ticket',
            'order_open_time',
            'order_type',
            'order_lots',
            'order_symbol',
            'order_open_price',
            'order_sl',
            'order_tp',
            'order_close_time',
            'order_close_price',
            'commission',
            'order_swap',
            'order_profit',
            'order_comment',
            'order_user_comment',
            'order_screenshot',
            'order_magic',
            'order_hash',
            'date_created',
            'date_modified'
        ];

        Yii::$app->db
            ->createCommand()
            ->batchInsert(self::tableName(), $fields_array, $data)
            ->execute();
    }

}