<?php

namespace app\models;

use app\models\queries\UserQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Модель пользователя.
 *
 * @property integer $id 	        int(11)
 * @property string $email 	        char(88)
 * @property string $login_name 	char(88)
 * @property string $password 	    char(88)
 * @property string $unsafe_password char(88)
 * @property int $status            int(11)
 * @property int $role 	            int(11)
 * @property string $hash           varchar(255)
 * @property string $recovery_key    varchar(256)
 * @property string $avatar          varchar(256)
 * @property int $date_modified 	int(11)
 * @property int $date_created 	    int(11)
 */
class User extends ActiveRecord implements IdentityInterface
{
    private $auth_key;

    public static $user;

    public const STATUS_ACTIVE = 10;
    public const STATUS_INACTIVE = 20;
    public const STATUS_UNCONFIRMED = 30;

    public static $status_names = [
        self::STATUS_ACTIVE => 'Активен',
        self::STATUS_INACTIVE => 'Не активен',
        self::STATUS_UNCONFIRMED => 'Не подтвержден',
    ];

    public const ROLE_ADMIN = 1;
    public const ROLE_MODERATOR = 2;
    public const ROLE_USER = 3;

    public static $role_names = [
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_MODERATOR => 'Модератор',
        self::ROLE_USER => 'Пользователь',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_name'], 'required'],
            [['email'], 'required'],
            [['date_created', 'date_modified', 'status', 'role'], 'integer'],
            [['unsafe_password', 'login_name', 'hash', 'recovery_key', 'password', 'avatar', 'email', 'avatar'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email', 'login_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'email' => 'Эл. почта',
            'login_name' => 'Имя',
            'password' => 'Пароль',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $this->status = $this->status ?? self::STATUS_UNCONFIRMED;
            $this->role = $this->role ?? self::ROLE_USER;
            $this->date_created = time();
            $this->hash = $this->getHashForUser();
        }
        $this->date_modified = time();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        //Yii::$app->session->setFlash('success', 'Запись сохранена');
    }

    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        if (empty(static::$user)) {
            static::$user = static::findOne($id);
        }

        return static::$user;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() == $authKey;
    }

    /**
     * @param $role
     * @return bool
     */
    public function can($role) {
        return $this->role === $role;
    }

    public static function getPasswordSalt()
    {
        return Yii::$app->params['salt'];
    }

    /**
     * @return string
     */
    public static function generatePassword()
    {
        return  substr(md5(time().rand(0,253)),0, 8);
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->recovery_key = NULL;
        $this->password = md5($password . self::getPasswordSalt());
        return $this;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password . self::getPasswordSalt());
    }

    /**
     * @return string
     */
    public static function getSalt()
    {
        return 'it_project_007';
    }

    /**
     * Find User by email
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Find User by email
     * @param string $email
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login_name' => $login]);
    }

    /**
     * @return string
     */
    public function getHashForUser() {
        return md5($this->email . User::getSalt().mt_rand());
    }

    /**
     * @return $this
     * @throws \yii\base\Exception
     */
    public function setRecoveryKey() {
        $this->recovery_key = Yii::$app->security->generateRandomString();
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin() : bool {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @param User $user
     * @return bool
     */
    public static function login(User $user)
    {
        return $user ? \Yii::$app->user->login($user, 3600 * 30 * 24) : false;
    }

    public function getUserAccounts()
    {
        return $this->hasMany(UserAccount::className(), ['user_id' => 'id']);
    }

}
