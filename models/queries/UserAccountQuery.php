<?php

namespace app\models\queries;

use app\models\UserAccount;
use yii\db\ActiveQuery;

class UserAccountQuery extends ActiveQuery
{
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }

    public function orderByDateCreated($sort = SORT_DESC)
    {
        return $this->orderBy(['date_created' => $sort]);
    }

    public function orderByDateFirstConnected($sort = SORT_DESC)
    {
        return $this->orderBy(['date_first_connected' => $sort]);
    }

    public function orderByDateModified($sort = SORT_DESC)
    {
        return $this->orderBy(['date_modified' => $sort]);
    }

    public function orderByAccountBalance($sort = SORT_DESC)
    {
        return $this->orderBy(['account_balance' => $sort]);
    }

    public function orderByAccountProfit($sort = SORT_DESC)
    {
        return $this->orderBy(['account_profit' => $sort]);
    }

    public function orderByBroker($sort = SORT_DESC)
    {
        return $this->orderBy(['account_company' => $sort]);
    }

    public function orderByAccountName($sort = SORT_DESC)
    {
        return $this->orderBy(['acc_name' => $sort]);
    }

    public function withUser()
    {
        return $this->with('user');
    }

    /**
     * Только активные счета
     *
     * @return UserAccountQuery
     */
    public function onlyActive()
    {
        return $this->andWhere(['>', 'date_first_connected', 0]);
    }

    /**
     * Только неподключенные счета
     *
     * @return UserAccountQuery
     */
    public function onlyInactive()
    {
        return $this->andWhere(['date_first_connected' => null]);
    }

    /**
     * Только активные счета пользователя
     *
     * @param $user_id
     * @return UserAccountQuery
     */
    public function onlyUserActive($user_id)
    {
        return $this->andWhere(['user_id' => $user_id])->andWhere(['not', ['date_first_connected' => null]]);
    }

    /**
     * Только активные счета не этого пользователя
     *
     * @param $user_id
     * @return UserAccountQuery
     */
    public function onlyActiveExcludeUser($user_id)
    {
        return $this->andWhere(['not', ['user_id' => $user_id]])->andWhere(['not', ['date_first_connected' => null]]);
    }

    public function onlyId()
    {
        return $this->select(['id']);
    }

    /**
     * Только неподключенные счета пользователя
     *
     * @param $user_id
     * @return UserAccountQuery
     */
    public function onlyUserInactive($user_id)
    {
        return $this->andWhere(['user_id' => $user_id])->andWhere(['date_first_connected' => null]);
    }

}