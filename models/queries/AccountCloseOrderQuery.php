<?php

namespace app\models\queries;

use app\models\AccountCloseOrder;
use yii\db\ActiveQuery;

class AccountCloseOrderQuery extends ActiveQuery
{
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }

    public function orderByDateOpened($sort = SORT_ASC)
    {
        return $this->orderBy(['order_open_time' => $sort]);
    }

    public function orderByDateClosed($sort = SORT_ASC)
    {
        return $this->orderBy(['order_open_time' => $sort]);
    }
}