<?php

namespace app\models;

use app\models\queries\UserAccountQuery;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $account
 * @property string $acc_name
 * @property string $acc_descr
 * @property string $acc_type
 * @property integer $terminal_bild
 * @property string $terminal_company_name
 * @property integer $account_trade_mode
 * @property integer $account_leverage
 * @property integer $account_limit_orders
 * @property integer $account_margin_so_mode
 * @property integer $account_trade_allowed
 * @property integer $account_trade_expert
 * @property float $account_balance
 * @property float $account_credit
 * @property float $account_profit
 * @property float $account_equity
 * @property float $account_margin
 * @property float $account_margin_free
 * @property float $account_margin_level
 * @property float $account_margin_so_call
 * @property float $account_margin_so_so
 * @property string $client_name
 * @property string $account_company
 * @property string $account_server
 * @property string $account_currency
 * @property integer $date_modified
 * @property integer $date_created
 * @property integer $date_first_connected
 * @property integer $date_last_connected
 */

class UserAccount extends ActiveRecord
{

    public const PLATFORM_MT4 = 'MetaTrader4';
    public const PLATFORM_MT4_ID = 1;
    public const PLATFORM_MT5 = 'MetaTrader5';
    public const PLATFORM_MT5_ID = 2;
    public const PLATFORM_LIBERTEX = 'Libertex';
    public const PLATFORM_LIBERTEX_ID = 3;
    public const PLATFORM_SPOTOPTION = 'SpotOption';
    public const PLATFORM_SPOTOPTION_ID = 4;
    public const PLATFORM_TRANSAQ = 'TRANSAQ';
    public const PLATFORM_TRANSAQ_ID = 5;
    public const PLATFORM_QUIK = 'Quik';
    public const PLATFORM_QUIK_ID = 6;
    public const PLATFORM_NINJATRADER = 'Ninja Trader';
    public const PLATFORM_NINJATRADER_ID = 7;
    public const PLATFORM_CTRADER = 'cTrader';
    public const PLATFORM_CTRADER_ID = 8;

    public const ACCOUNT_TRADE_MODE_DEMO = 0;
    public const ACCOUNT_TRADE_MODE_CONTEST = 1;
    public const ACCOUNT_TRADE_MODE_REAL = 2;

    public const ACCOUNT_TRADE_MODE_DEMO_TITLE = 'Демо счет';
    public const ACCOUNT_TRADE_MODE_CONTEST_TITLE = 'Конкурсный счет';
    public const ACCOUNT_TRADE_MODE_REAL_TITLE = 'Реальный счет';

    //account_margin_so_mode
    public const ACCOUNT_STOPOUT_MODE_PERCENT = 0; //Уровень задается в процентах
    public const ACCOUNT_STOPOUT_MODE_MONEY = 1; //Уровень задается в деньгах

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_accounts';
    }

    public function rules()
    {
        return [
            [['account', 'user_id', 'acc_type', 'acc_name'], 'required'],
            [['id', 'user_id', 'account', 'acc_type'], 'integer'],
            [['acc_name'], 'string', 'max' => 50],
            [['acc_descr'], 'string', 'max' => 300],
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();

        return parent::beforeValidate();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getAccountCloseOrders()
    {
        return $this->hasMany(AccountCloseOrder::className(), ['account_id' => 'id']);
    }

    public function getAccountOpenOrders()
    {
        return $this->hasMany(AccountOpenOrder::className(), ['account_id' => 'id']);
    }

    public static function find()
    {
        return new UserAccountQuery(get_called_class());
    }

    public static function getPlatformsArray()
    {
        return [
            ['id' => self::PLATFORM_MT4_ID, 'title' => self::PLATFORM_MT4, 'disabled' => false, 'message' => '', 'icon' => '../web/images/terminals/MetaTrader4.png'],
            ['id' => self::PLATFORM_MT5_ID, 'title' => self::PLATFORM_MT5, 'disabled' => false, 'message' => '', 'icon' => '../web/images/terminals/MetaTrader5.png'],
            ['id' => self::PLATFORM_LIBERTEX_ID, 'title' => self::PLATFORM_LIBERTEX, 'disabled' => true, 'message' => 'В разработке', 'icon' => '../web/images/terminals/Libertex.png'],
            ['id' => self::PLATFORM_SPOTOPTION_ID, 'title' => self::PLATFORM_SPOTOPTION, 'disabled' => true, 'message' => 'В разработке', 'icon' => '../web/images/terminals/SpotOption.png'],
            ['id' => self::PLATFORM_TRANSAQ_ID, 'title' => self::PLATFORM_TRANSAQ, 'disabled' => true, 'message' => 'В разработке', 'icon' => '../web/images/terminals/TRANSAQ.png'],
            ['id' => self::PLATFORM_QUIK_ID, 'title' => self::PLATFORM_QUIK, 'disabled' => true, 'message' => 'В разработке', 'icon' => '../web/images/terminals/Quik.png'],
            ['id' => self::PLATFORM_NINJATRADER_ID, 'title' => self::PLATFORM_NINJATRADER, 'disabled' => true, 'message' => 'В разработке', 'icon' => '../web/images/terminals/NinjaTrader.png'],
            ['id' => self::PLATFORM_CTRADER_ID, 'title' => self::PLATFORM_CTRADER, 'disabled' => true, 'message' => 'В разработке', 'icon' => '../web/images/terminals/cTrader.png'],
        ];
    }

    public function getPlatformNameById()
    {
        $platforms = [
            self::PLATFORM_MT4_ID => ['title' => self::PLATFORM_MT4, 'icon' => '../web/images/terminals/MetaTrader4.png'],
            self::PLATFORM_MT5_ID => ['title' => self::PLATFORM_MT5, 'icon' => '../web/images/terminals/MetaTrader5.png'],
            self::PLATFORM_LIBERTEX_ID => ['title' => self::PLATFORM_LIBERTEX, 'icon' => '../web/images/terminals/Libertex.png'],
            self::PLATFORM_SPOTOPTION_ID => ['title' => self::PLATFORM_SPOTOPTION, 'icon' => '../web/images/terminals/SpotOption.png'],
            self::PLATFORM_TRANSAQ_ID => ['title' => self::PLATFORM_TRANSAQ, 'icon' => '../web/images/terminals/TRANSAQ.png'],
            self::PLATFORM_QUIK_ID => ['title' => self::PLATFORM_QUIK, 'icon' => '../web/images/terminals/Quik.png'],
            self::PLATFORM_NINJATRADER_ID => ['title' => self::PLATFORM_NINJATRADER, 'icon' => '../web/images/terminals/NinjaTrader.png'],
            self::PLATFORM_CTRADER_ID => ['title' => self::PLATFORM_CTRADER, 'icon' => '../web/images/terminals/cTrader.png']
        ];
        return $platforms[$this->acc_type];
    }

    public function getAccountTradeModeTitle()
    {
        $trade_modes = [
            self::ACCOUNT_TRADE_MODE_DEMO => self::ACCOUNT_TRADE_MODE_DEMO_TITLE,
            self::ACCOUNT_TRADE_MODE_CONTEST => self::ACCOUNT_TRADE_MODE_CONTEST_TITLE,
            self::ACCOUNT_TRADE_MODE_REAL => self::ACCOUNT_TRADE_MODE_REAL_TITLE
        ];
        return $trade_modes[$this->account_trade_mode];
    }

    public function fillAccountDataFirstTime($data)
    {
        $this->terminal_bild = $data['terminal_bild'];
        $this->terminal_company_name = $data['terminal_company_name'];
        $this->account_trade_mode = $data['account_trade_mode'];
        $this->account_leverage = $data['account_leverage'];
        $this->account_limit_orders = $data['account_limit_orders'];
        $this->account_margin_so_mode = $data['account_margin_so_mode'];
        $this->account_trade_allowed = $data['account_trade_allowed'];
        $this->account_trade_expert = $data['account_trade_expert'];
        $this->account_balance = $data['account_balance'];
        $this->account_credit = $data['account_credit'];
        $this->account_profit = $data['account_profit'];
        $this->account_equity = $data['account_equity'];
        $this->account_margin = $data['account_margin'];
        $this->account_margin_free = $data['account_margin_free'];
        $this->account_margin_level = $data['account_margin_level'];
        $this->account_margin_so_call = $data['account_margin_so_call'];
        $this->account_margin_so_so = $data['account_margin_so_so'];
        $this->client_name = $data['account_name'];
        $this->account_company = $data['account_company'];
        $this->account_server = $data['account_server'];
        $this->account_currency = $data['account_currency'];
        $this->date_first_connected = time();
        $this->date_last_connected = time();
        $this->date_created = time();
        $this->date_modified = time();
    }

    public function fillAccountData($data)
    {
        $this->account_balance = $data['account_balance'];
        $this->account_credit = $data['account_credit'];
        $this->account_profit = $data['account_profit'];
        $this->account_equity = $data['account_equity'];
        $this->account_margin = $data['account_margin'];
        $this->account_margin_free = $data['account_margin_free'];
        $this->account_margin_level = $data['account_margin_level'];
        $this->account_margin_so_call = $data['account_margin_so_call'];
        $this->account_margin_so_so = $data['account_margin_so_so'];
        $this->date_last_connected = time();
        $this->date_modified = time();
    }

}