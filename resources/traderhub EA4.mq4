//+------------------------------------------------------------------+
//|                                                  Master_Copy.mq4 |
//+------------------------------------------------------------------+
#property copyright "profxsignal"
#property link      "profxsignal.com"
#property version   "1.00"

#include <WinUser32.mqh>
#import "kernel32.dll"
   void GetLocalTime(int& a0[]);
   int GetTimeZoneInformation(int& a0[]);
#import

#define DELAY 50

//Внешние переменные
extern string Login = "Silentspec";       // Логин от аккаунта TraderHub
extern string Password = "09111984-=qwePOI";    // Пароль от аккаунта TraderHub
int RatesUpdate = 300;     // Периодичность в секундах, с которой отправдяются данные
// по уже открытым позам на сервер (чтобы на сайте обновлялись текущие прибыль/убыток)

string auth_link = "http://investlab.test/mt4/auth"; // URL
string first_connect_link = "http://investlab.test/mt4/first-connect"; // URL
string refresh_account_link = "http://investlab.test/mt4/refresh-account"; // URL
string history_link = "http://investlab.test/mt4/history"; // URL
string opened_link = "http://investlab.test/mt4/opened"; // URL

bool firstrun = true;

enum tradeApiResponse
{
    RESPONSE_OK = 1,
    RESPONSE_ERR_WRONG_LOGIN = 2,
    RESPONSE_ERR_WRONG_PASSWORD = 3,
    RESPONSE_ERR_NO_ACCOUNT = 4,
    RESPONSE_FIRST_CONNECT = 5,
    RESPONSE_ERR_SAVE_DATA = 6
};

enum responseStatus
{
    RESPONSE_TRUE = 901,
    RESPONSE_FAIL = 902,
    RESPONSE_NEED_FC = 903,
};

enum stateStatus
{
   STATE_NO_CHANGE = 1000,
   STATE_CHANGED = 1001,
   STATE_MODIFIED = 1002,
};

int account_number; //Номер счета
int terminal_bild; //Номер билда запущенного терминала
string terminal_company_name; //Возвращает наименование компании-владельца клиентского терминала.
int account_trade_mode; //Тип торгового счета
int account_leverage; //Размер предоставленного плеча
int account_limit_orders; //Максимально допустимое количество открытых позиций и действующих отложенных ордеров (суммарно), 0 — ограничений нет
int account_margin_so_mode; //Режим задания минимально допустимого уровня залоговых средств
bool account_trade_allowed; //Разрешенность торговли для текущего счета
bool account_trade_expert; //Разрешенность торговли для эксперта
double account_balance; //Баланс счета в валюте депозита
double account_credit; //Размер предоставленного кредита в валюте депозита
double account_profit; //Размер текущей прибыли на счете в валюте депозита
double account_equity; //Значение собственных средств на счете в валюте депозита
double account_margin; //Размер зарезервированных залоговых средств на счете  в валюте депозита
double account_margin_free; //Размер свободных средств на счете  в валюте депозита, доступных для открытия ордера
double account_margin_level; //Уровень залоговых средств на счете в процентах
double account_margin_so_call; //Уровень залоговых средств, при котором требуется пополнение счета (Margin Call). В зависимости от установленного ACCOUNT_MARGIN_SO_MODE выражается в процентах либо в валюте депозита
double account_margin_so_so; //Уровень залоговых средств, при достижении которого происходит принудительное закрытие самого убыточного ордера (Stop Out). В зависимости от установленного ACCOUNT_MARGIN_SO_MODE выражается в процентах либо в валюте депозита
string account_name; //Имя клиента
string account_server; //Имя торгового сервера
string account_currency; //Валюта депозита
string account_company; //Имя компании, обслуживающей счет

string open_orders[1][13];
int state;

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   account_number = AccountInfoInteger(ACCOUNT_LOGIN);
   terminal_company_name = TerminalCompany();
   terminal_bild = TerminalInfoInteger(TERMINAL_BUILD);
   account_trade_mode = AccountInfoInteger(ACCOUNT_TRADE_MODE);
   // ACCOUNT_TRADE_MODE_DEMO Демонстрационный торговый счет
   // ACCOUNT_TRADE_MODE_CONTEST Конкурсный торговый счет
   // ACCOUNT_TRADE_MODE_REAL Реальный торговый счет
   account_leverage = AccountInfoInteger(ACCOUNT_LEVERAGE);
   account_limit_orders = AccountInfoInteger(ACCOUNT_LIMIT_ORDERS);
   account_margin_so_mode = AccountInfoInteger(ACCOUNT_MARGIN_SO_MODE);
   // ACCOUNT_STOPOUT_MODE_PERCENT Уровень задается в процентах
   // ACCOUNT_STOPOUT_MODE_MONEY Уровень задается в деньгах
   account_trade_allowed = AccountInfoInteger(ACCOUNT_TRADE_ALLOWED);
   account_trade_expert = AccountInfoInteger(ACCOUNT_TRADE_EXPERT);
   account_trade_mode = AccountInfoInteger(ACCOUNT_TRADE_MODE);
   account_trade_mode = AccountInfoInteger(ACCOUNT_TRADE_MODE);
   account_trade_mode = AccountInfoInteger(ACCOUNT_TRADE_MODE);
   account_balance = AccountInfoDouble(ACCOUNT_BALANCE);
   account_credit = AccountInfoDouble(ACCOUNT_CREDIT);
   account_profit = AccountInfoDouble(ACCOUNT_PROFIT);
   account_equity = AccountInfoDouble(ACCOUNT_EQUITY);
   account_margin = AccountInfoDouble(ACCOUNT_MARGIN);
   account_margin_free = AccountInfoDouble(ACCOUNT_MARGIN_FREE);
   account_margin_level = AccountInfoDouble(ACCOUNT_MARGIN_LEVEL);
   account_margin_so_call = AccountInfoDouble(ACCOUNT_MARGIN_SO_CALL);
   account_margin_so_so = AccountInfoDouble(ACCOUNT_MARGIN_SO_SO);
   account_name = AccountInfoString(ACCOUNT_NAME);
   account_server = AccountInfoString(ACCOUNT_SERVER);
   account_currency = AccountInfoString(ACCOUNT_CURRENCY);
   account_company = AccountInfoString(ACCOUNT_COMPANY);

   EventSetMillisecondTimer(DELAY * 10);
   open_orders[0][0] = " ";
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
   {
   EventKillTimer();
   // При остановке работы совы написать о причине остановки при помощи функции EAComment
   switch(_UninitReason)
     {
      case REASON_ACCOUNT:
         Print(" Советник остановлен. Номер аккаунта был изменен.");
         break;
      case REASON_CHARTCHANGE:
         Print(" Советник остановлен. Символ или период графика был изменен");
         break;
      case REASON_CHARTCLOSE:
         Print(" Советник остановлен. Окно графика было закрыто.");
         break;
      case REASON_PARAMETERS:
         Print(" Советник остановлен. Входящие настройки были изменены.");
         break;
      case REASON_RECOMPILE:
         Print(" Советник остановлен. Копировщик был перекомпилирован.");
         break;
      case REASON_REMOVE:
         Print(" Советник остановлен. Копировщик был удален с графика.");
         break;
      case REASON_TEMPLATE:
         Print(" Советник остановлен. К графику был применен новый шаблон.");
         break;
      default:
         Print(" Советник остановлен.");
     }
   }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
void OnTimer()
{
   if (firstrun) {                      // При первом коннекте
      Print("Первый запуск");
      if (Auth() == RESPONSE_NEED_FC) { // Авторизация, если данных о счете еще нет,
         FirstConnect();                // Отправляет данные о счете
      }
      sendHistoryOrders();              // Шлет массив всей истории сделок
      AccountRefresh();                 // Обновление данных о счете
      fillOpenOrdersArray();            // Заполняет массив открытых сделок
      sendOpenedOrders();               // Шлет открытые сделки
      // Нужно обновить данные о сделках
      firstrun = false;
   }
   state = checkCurrentState();
   if (state == STATE_CHANGED) {
      Print("Сделка открыта/закрыта");
      fillOpenOrdersArray();            // Заполняет массив открытых сделок
      sendOpenedOrders();               // Шлет открытые сделки
      sendHistoryOrders();              // Шлет массив всей истории сделок
   }
   if (state == STATE_MODIFIED) {
      Print("Сделка модифицирована");
      fillOpenOrdersArray();            // Заполняет массив открытых сделок
      sendOpenedOrders();               // Шлет открытые сделки
   }
   if (IsNewBar(1,PERIOD_M1)) {
      Print("Обновление данных о счете");
      AccountRefresh();                 // Обновление данных о счете
   }
   return;
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Веб запрос                                                        |
//+------------------------------------------------------------------+
string WebReq(string postdata, int speed, string req_link)
{
   char post[];
   char result[];
   string result_headers;
   string headers = "Content-Type: application/json";
   int res;

   ArrayResize(post,StringToCharArray(postdata,post,0,WHOLE_ARRAY,CP_UTF8)-1);
   ResetLastError();
   res = WebRequest("POST",req_link,headers,speed,post,result,result_headers);

   return CharArrayToString(result, 0);
}

//+------------------------------------------------------------------+
//|Первое подключение (пока тестовое)                                |
//+------------------------------------------------------------------+
int Auth()
{
   string response = WebReq("{" +
   "\"login_name\":" + "\"" + Login + "\"" +
   ",\"password\":" + "\"" + Password + "\"" +
   ",\"account\":" + account_number +
   "}",50, auth_link);

   int response_code = StrToInteger(response);

   switch (response_code)
   {
      case RESPONSE_OK:
         Print("Авторизация прошла успешно");
         return RESPONSE_OK;
      case RESPONSE_ERR_WRONG_LOGIN:
         Print("Вы указали неверный логин");
         ExpertRemove();
      case RESPONSE_ERR_WRONG_PASSWORD:
         Print("Вы указали неверный пароль");
         ExpertRemove();
      case RESPONSE_ERR_NO_ACCOUNT:
         Print("Вы еще не добавили данный счет в кабинет");
         ExpertRemove();
      case RESPONSE_FIRST_CONNECT:
         Print("Первичное соединение счета");
         return RESPONSE_NEED_FC;
   }
   return RESPONSE_FAIL;
}

// Отправляет данные о счете
int FirstConnect()
{
   Print("Подтверждаем регистрацию счета");

   string response = WebReq("{" +
   "\"login_name\":" + "\"" + Login + "\"" +
   ",\"password\":" + "\"" + Password + "\"" +
   ",\"account\":" + account_number +
   ",\"terminal_bild\":" + terminal_bild +
   ",\"terminal_company_name\":" + "\"" + terminal_company_name + "\"" +
   ",\"account_trade_mode\":" + account_trade_mode +
   ",\"account_leverage\":" + account_leverage +
   ",\"account_limit_orders\":" + account_limit_orders +
   ",\"account_margin_so_mode\":" + account_margin_so_mode +
   ",\"account_trade_allowed\":" + account_trade_allowed +
   ",\"account_trade_expert\":" + account_trade_expert +
   ",\"account_balance\":" + account_balance +
   ",\"account_credit\":" + account_credit +
   ",\"account_profit\":" + account_profit +
   ",\"account_equity\":" + account_equity +
   ",\"account_margin\":" + account_margin +
   ",\"account_margin_free\":" + account_margin_free +
   ",\"account_margin_level\":" + account_margin_level +
   ",\"account_margin_so_call\":" + account_margin_so_call +
   ",\"account_margin_so_so\":" + account_margin_so_so +
   ",\"account_name\":" + "\"" + account_name + "\"" +
   ",\"account_server\":" + "\"" + account_server + "\"" +
   ",\"account_currency\":" + "\"" + account_currency + "\"" +
   ",\"account_company\":" + "\"" + account_company + "\"" +
   "}",50, first_connect_link);

   int response_code = StrToInteger(response);

   switch (response_code)
   {
      case RESPONSE_OK:
         Print("Подтверждение регистрации счета прошло успешно");
         return RESPONSE_OK;
      case RESPONSE_ERR_WRONG_LOGIN:
         Print("Вы указали неверный логин");
         ExpertRemove();
      case RESPONSE_ERR_WRONG_PASSWORD:
         Print("Вы указали неверный пароль");
         ExpertRemove();
      case RESPONSE_ERR_NO_ACCOUNT:
         Print("Вы еще не добавили данный счет в кабинет");
         ExpertRemove();
      case RESPONSE_ERR_SAVE_DATA:
         Print("На сервере произошла ошибка. Попробуйте снова.");
         ExpertRemove();
   }

   return RESPONSE_FAIL;
}

// Обновляет информацию о счете
int AccountRefresh()
{
   string response = WebReq("{" +
   "\"login_name\":" + "\"" + Login + "\"" +
   ",\"password\":" + "\"" + Password + "\"" +
   ",\"account\":" + account_number +
   ",\"account_balance\":" + account_balance +
   ",\"account_credit\":" + account_credit +
   ",\"account_profit\":" + account_profit +
   ",\"account_equity\":" + account_equity +
   ",\"account_margin\":" + account_margin +
   ",\"account_margin_free\":" + account_margin_free +
   ",\"account_margin_level\":" + account_margin_level +
   ",\"account_margin_so_call\":" + account_margin_so_call +
   ",\"account_margin_so_so\":" + account_margin_so_so +
   "}",50, refresh_account_link);

   int response_code = StrToInteger(response);

   switch (response_code)
   {
      case RESPONSE_OK:
         return RESPONSE_OK;
      case RESPONSE_ERR_WRONG_LOGIN:
         Print("Вы указали неверный логин");
         ExpertRemove();
      case RESPONSE_ERR_WRONG_PASSWORD:
         Print("Вы указали неверный пароль");
         ExpertRemove();
      case RESPONSE_ERR_NO_ACCOUNT:
         Print("Вы еще не добавили данный счет в кабинет");
         ExpertRemove();
      case RESPONSE_ERR_SAVE_DATA:
         Print("На сервере произошла ошибка. Попробуйте снова.");
         ExpertRemove();
   }

   return RESPONSE_FAIL;
}

// Шлет массив всей истории сделок
int sendHistoryOrders()
{
   string request = "{" +
   "\"login_name\":" + "\"" + Login + "\"" +
   ",\"password\":" + "\"" + Password + "\"" +
   ",\"account\":" + account_number + ",";

   bool first_deal = true;
   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
   {
      if (!OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)) continue;
      if (first_deal) {
         request += "\"" + OrderTicket() + "\":" + "{" +
            "\"ticket\":" + "\"" + OrderTicket() + "\"" +
            ",\"order_open_time\":" + "\"" + OrderOpenTime() + "\"" +
            ",\"order_type\":" + "\"" + OrderType() + "\"" +
            ",\"order_lots\":" + "\"" + OrderLots() + "\"" +
            ",\"order_symbol\":" + "\"" + OrderSymbol() + "\"" +
            ",\"order_open_price\":" + "\"" + OrderOpenPrice() + "\"" +
            ",\"sl\":" + "\"" + OrderStopLoss() + "\"" +
            ",\"tp\":" + "\"" + OrderTakeProfit() + "\"" +
            ",\"order_close_time\":" + "\"" + OrderCloseTime() + "\"" +
            ",\"order_close_price\":" + "\"" + OrderClosePrice() + "\"" +
            ",\"commission\":" + "\"" + OrderCommission() + "\"" +
            ",\"order_swap\":" + "\"" + OrderSwap() + "\"" +
            ",\"order_profit\":" + "\"" + OrderProfit() + "\"" +
            ",\"order_comment\":" + "\"" + OrderComment() + "\"" +
            ",\"order_magic\":" + "\"" + OrderMagicNumber() + "\"" +
            "}";
         first_deal = false;
      } else {
         request += "," + "\"" + OrderTicket() + "\":" + "{" +
            "\"ticket\":" + "\"" + OrderTicket() + "\"" +
            ",\"order_open_time\":" + "\"" + OrderOpenTime() + "\"" +
            ",\"order_type\":" + "\"" + OrderType() + "\"" +
            ",\"order_lots\":" + "\"" + OrderLots() + "\"" +
            ",\"order_symbol\":" + "\"" + OrderSymbol() + "\"" +
            ",\"order_open_price\":" + "\"" + OrderOpenPrice() + "\"" +
            ",\"sl\":" + "\"" + OrderStopLoss() + "\"" +
            ",\"tp\":" + "\"" + OrderTakeProfit() + "\"" +
            ",\"order_close_time\":" + "\"" + OrderCloseTime() + "\"" +
            ",\"order_close_price\":" + "\"" + OrderClosePrice() + "\"" +
            ",\"commission\":" + "\"" + OrderCommission() + "\"" +
            ",\"order_swap\":" + "\"" + OrderSwap() + "\"" +
            ",\"order_profit\":" + "\"" + OrderProfit() + "\"" +
            ",\"order_comment\":" + "\"" + OrderComment() + "\"" +
            ",\"order_magic\":" + "\"" + OrderMagicNumber() + "\"" +
            "}";
      }
   }
   request += "}";
   string response = WebReq(request, 50, history_link);
}

// Шлет открытые ордера
int sendOpenedOrders()
{
   Print("Отправляем открытые сделки");
   string request = "{" +
   "\"login_name\":" + "\"" + Login + "\"" +
   ",\"password\":" + "\"" + Password + "\"" +
   ",\"account\":" + account_number + ",";

   bool first_deal = true;
   if (open_orders[0][0] == " ") {
      return RESPONSE_OK;
   }
   for(int i = ArrayRange(open_orders, 0) - 1; i >= 0; i--)
   {
      if (first_deal) {
         request += "\"" + open_orders[i][0] + "\":" + "{" +
            "\"ticket\":" + "\"" + open_orders[i][0] + "\"" +
            ",\"order_open_time\":" + "\"" + open_orders[i][1] + "\"" +
            ",\"order_type\":" + "\"" + open_orders[i][2] + "\"" +
            ",\"order_lots\":" + "\"" + open_orders[i][3] + "\"" +
            ",\"order_symbol\":" + "\"" + open_orders[i][4] + "\"" +
            ",\"order_open_price\":" + "\"" + open_orders[i][5] + "\"" +
            ",\"sl\":" + "\"" + open_orders[i][6] + "\"" +
            ",\"tp\":" + "\"" + open_orders[i][7] + "\"" +
            ",\"commission\":" + "\"" + open_orders[i][8] + "\"" +
            ",\"order_swap\":" + "\"" + open_orders[i][9] + "\"" +
            ",\"order_profit\":" + "\"" + open_orders[i][10] + "\"" +
            ",\"order_comment\":" + "\"" + open_orders[i][11] + "\"" +
            ",\"order_magic\":" + "\"" + open_orders[i][12] + "\"" +
            "}";
         first_deal = false;
      } else {
         request += "," + "\"" + open_orders[i][0] + "\":" + "{" +
            "\"ticket\":" + "\"" + open_orders[i][0] + "\"" +
            ",\"order_open_time\":" + "\"" + open_orders[i][1] + "\"" +
            ",\"order_type\":" + "\"" + open_orders[i][2] + "\"" +
            ",\"order_lots\":" + "\"" + open_orders[i][3] + "\"" +
            ",\"order_symbol\":" + "\"" + open_orders[i][4] + "\"" +
            ",\"order_open_price\":" + "\"" + open_orders[i][5] + "\"" +
            ",\"sl\":" + "\"" + open_orders[i][6] + "\"" +
            ",\"tp\":" + "\"" + open_orders[i][7] + "\"" +
            ",\"commission\":" + "\"" + open_orders[i][8] + "\"" +
            ",\"order_swap\":" + "\"" + open_orders[i][9] + "\"" +
            ",\"order_profit\":" + "\"" + open_orders[i][10] + "\"" +
            ",\"order_comment\":" + "\"" + open_orders[i][11] + "\"" +
            ",\"order_magic\":" + "\"" + open_orders[i][12] + "\"" +
            "}";
      }
   }
   request += "}";
   string response = WebReq(request, 50, opened_link);
   return RESPONSE_OK;
}

// Заполняет массив открытых сделок
void fillOpenOrdersArray()
{
   Print("Заполняем массив открытых сделок");
   for(int i = OrdersTotal() - 1; i >= 0; i--) {
      if (!OrderSelect(i,SELECT_BY_POS,MODE_TRADES)) continue;
      ArrayResize(open_orders, OrdersTotal());
      open_orders[i][0] = OrderTicket();
      open_orders[i][1] = OrderOpenTime();
      open_orders[i][2] = OrderType();
      open_orders[i][3] = OrderLots();
      open_orders[i][4] = OrderSymbol();
      open_orders[i][5] = OrderOpenPrice();
      open_orders[i][6] = OrderStopLoss();
      open_orders[i][7] = OrderTakeProfit();
      open_orders[i][8] = OrderCommission();
      open_orders[i][9] = OrderSwap();
      open_orders[i][10] = OrderProfit();
      open_orders[i][11] = OrderComment();
      open_orders[i][12] = OrderMagicNumber();
   }
}

int checkCurrentState()
{
   if ((ArrayRange(open_orders, 0) != OrdersTotal() && open_orders[0][0] != " ")
      || (ArrayRange(open_orders, 0) == OrdersTotal() && open_orders[0][0] == " ")
      || (OrdersTotal() == 0 && ArrayRange(open_orders, 0) == 1 &&open_orders[0][0] != " ")) {
      return STATE_CHANGED;
   }
   for (int i = OrdersTotal() - 1; i >= 0; i--) {
      if (!OrderSelect(i,SELECT_BY_POS,MODE_TRADES)) continue;
      if (open_orders[i][6] != OrderStopLoss()
         || open_orders[i][7] != OrderTakeProfit()
         || open_orders[i][9] != OrderSwap()) {
         return STATE_MODIFIED;
      }
   }
   return STATE_NO_CHANGE;
}

//+------------------------------------------------------------------+
//| GMT0 время                                                       |
//+------------------------------------------------------------------+
string GMTget() 
{
   int Date[4];
   int TimeZoneInfo[43];
   GetLocalTime(Date);
   int CurYear = Date[0] & 65535;
   int CurMonth = Date[0] >> 16;
   int CurDay = Date[1] >> 16;
   int CurHour = Date[2] & 65535;
   int CurMinute = Date[2] >> 16;
   int CurSecond = Date[3] & 65535;
   int CurMilisec = Date[3] >> 16;
   string CurDate = GetTimeString(CurYear, CurMonth, CurDay, CurHour, CurMinute, CurSecond);
   datetime CurDateInTime = StrToTime(CurDate);
   int TimeZoneCur = 0;
   int TimeZone = GetTimeZoneInformation(TimeZoneInfo);
   TimeZoneCur = TimeZoneInfo[0];
   if (TimeZone == 2) TimeZoneCur += TimeZoneInfo[42];
   datetime GMTTime = CurDateInTime + 60 * TimeZoneCur;
   string Time1 = (string)TimeYear(GMTTime)+"."+(string)TimeMonth(GMTTime)+"."+(string)TimeDay(GMTTime)+" "+
                  (string)TimeHour(GMTTime)+":"+(string)TimeMinute(GMTTime)+":"+(string)TimeSeconds(GMTTime);
   return (Time1);
}

//+------------------------------------------------------------------+
//| Конвертация времени в текстовый вид                              |
//+------------------------------------------------------------------+
string GetTimeString(int CurYear, int CurMonth, int CurDay, int CurHour, int CurMinute, int CurSecond) 
{
   string MonthStr = IntegerToString(CurMonth + 100);
   MonthStr = StringSubstr(MonthStr, 1);
   string DayStr = IntegerToString(CurDay + 100);
   DayStr = StringSubstr(DayStr, 1);
   string HourStr = IntegerToString(CurHour + 100);
   HourStr = StringSubstr(HourStr, 1);
   string MinuteStr = IntegerToString(CurMinute + 100);
   MinuteStr = StringSubstr(MinuteStr, 1);
   string SecondStr = IntegerToString(CurSecond + 100);
   SecondStr = StringSubstr(SecondStr, 1);
   return (StringConcatenate(CurYear, ".", MonthStr, ".", DayStr, " ", HourStr, ":", MinuteStr, ":", SecondStr));
}

//+------------------------------------------------------------------+
//|  New bar function                                                |
//+------------------------------------------------------------------+
bool IsNewBar(int mode=0,int period=0)
   {
   //0 - при первом запуске возвращает true
   //1 - при первом запуске ожидает следующий бар
   static datetime tm[10];
   int t=tfA(period);
   if(tm[t]==0&&mode==1) tm[t]=iTime(_Symbol,period,0);
   if(tm[t]==iTime(_Symbol,period,0)) return (false);
   tm[t]=iTime(_Symbol,period,0);
   return (true);
   }
   
int tfA(int tf)
   {
   switch (tf)
      {
      case PERIOD_M1: return (0);
      case PERIOD_M5: return (1);
      case PERIOD_M15: return (2);
      case PERIOD_M30: return (3);
      case PERIOD_H1: return (4);
      case PERIOD_H4: return (5);
      case PERIOD_D1: return (6);
      case PERIOD_W1: return (7);
      case PERIOD_MN1: return (8);
      default: return (9);
      }
   }