<?php

namespace app\classes\helpers;

use app\classes\detectCyrillicEncoding\classes\DetectCyrillic\Encoding;
use Iterator;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use League\Csv\CharsetConverter;
use League\Csv\Reader;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * Хелпер для работы с Csv файлами
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * @package app\classes\helpers
 */
class CsvHelper
{
	private static $encoding = '';
	const DELIMITER = ';';

	/**
	 * Создает csv файл и возвращает путь к файлу в случае успеха.
	 * @param string $file_path - полный путь к файлу
	 * @param array $headers
	 * @param array $rows
	 * @return string
	 * @throws Exception
	 */
	public static function createCsvFileFromArray(string $file_path, array &$rows, array $headers = []) :string
    {
		$writer = Writer::createFromPath($file_path, 'w+');
        self::writeCsv($writer, $rows, $headers);

        return $writer->getPathname();
    }

	/**
	 * Отдает Csv файл на скачивание
	 * @param string $file_path - полный путь к файлу
	 * @param array $headers
	 * @param array $rows
	 * @throws CannotInsertRecord
	 * @throws Exception
	 */
	public static function export(string $file_path, array &$rows, array $headers = []): void
    {
		if (self::isWindowsClientOs($_SERVER['HTTP_USER_AGENT'])) {
			self::$encoding = 'windows-1251';
		}
		$writer = Writer::createFromString();
		self::writeCsv($writer, $rows, $headers);
		$writer->output($file_path);
		die();
	}

    /**
     * @param Writer $writer
     * @param $headers
     * @param $rows
     * @throws Exception
     */
	private static function writeCsv(Writer $writer, array &$rows, array $headers): void
    {
        if (self::$encoding) {
			CharsetConverter::addTo($writer, 'utf-8', self::$encoding);
		}
        $writer->setDelimiter(self::DELIMITER);
        if ($headers) {
            $writer->insertOne($headers);
        }
        $writer->insertAll($rows);
    }

	/**
	 * @param string $file_path
	 * @param array $headers
	 * @return Iterator
	 * @throws Exception
	 */
	public static function readCsv(string $file_path, array $headers): Iterator
	{
		$file_content = file_get_contents($file_path);
		if(!$file_content) {
			throw new Exception('Не удалось получить содержимое файла: '.$file_path);
		}
		$csv = Reader::createFromString($file_content);
		$csv->setDelimiter(self::DELIMITER);
		$csv->setOutputBOM(Reader::BOM_UTF8);
		$encoding = self::getEncoding($file_content);
		if (!$encoding) {
			throw new Exception('Не определена кодировка файла: '.$file_path);
		}
		//let's convert the incoming data from iso-88959-15 to utf-8
		if ($encoding !== 'utf-8') {
			$csv->addStreamFilter('convert.iconv.'.$encoding.'/UTF-8');
		}
		$csv->setHeaderOffset(0);
		$csv_headers = $csv->getHeader();
		if (array_diff($csv_headers, $headers)) {
			throw new Exception ('Неверная структура загружаемого csv файла.');
		}

		return $csv->getRecords();
	}

	/**
	 * @param string $txt
	 * @return false|int|string
	 */
	public static function getEncoding(string $txt)
	{
		return (new Encoding())->detectMaxRelevant($txt);
	}

	/**
	 * Возвращиет true если на клиенте стоит ОС Windows
	 * @param $user_agent
	 * @return bool
	 */
	public static function isWindowsClientOs($user_agent):bool
	{
		if  (preg_match('/windows|win32|win64/i', $user_agent)) {
			return true;
		}

		return false;
	}

	/**
	 * @param UploadedFile $file
	 * @return void
	 * @throws \yii\base\Exception
	 */
	public static function saveFile(UploadedFile $file): void
	{
		if (mb_strtolower($file->getExtension()) !== 'csv') {
			throw new \yii\base\Exception('Расширение файла не соответствует csv.');
		}

		$file_path = self::getFilePath($file);
		if (!$file->saveAs($file_path)) {
			throw new \yii\base\Exception('Ошибка при сохранении файла.');
		}
	}

	public static function getFilePath(UploadedFile $file)
	{
		$upload_path = Yii::getAlias('@app').'/resources/temp_uploads';
		if (!is_dir($upload_path)) {
			FileHelper::createDirectory($upload_path, 0777, false);
		}
		/** @var $file UploadedFile */
		$temp_file_name = md5($file->name);
		return $upload_path.'/'.$temp_file_name;
	}

}