<?php

namespace app\commands;

use Yii;
use yii\console\controllers\MigrateController;
use app\classes\helpers\EnvironmentHelper;

class MigrateAllController extends MigrateController
{

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUp($limit = 0)
    {
        parent::actionUp($limit);
        if (\Yii::$app->params['environment']->isTest() || \Yii::$app->params['environment']->isLocal()) {
            $config = require(\Yii::getAlias('@app') . '/config/test_db.php');
            Yii::$app->set('db', Yii::createObject($config));
            $this->db = Yii::$app->db;
            parent::actionUp($limit);
        }
    }

}