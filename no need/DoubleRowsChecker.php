<?php

namespace app\classes\helpers;

use app\classes\helpers\RowProcessor\AbstractRowProcessor;
use app\models\FieldConfig;
use app\models\KpTablesData;

/**
 * Class DoubleRowsHelper
 * Класс для работы с дублями строк
 * Проверяет строки по хэшу hash_row_key_field. Удаляет дубли строк.
 *
 * @package app\classes\helpers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
class DoubleRowsChecker
{

    private $fields;
    private $company_id;
    private $intersect_key_field_ids;

    public function __construct($manager_company_id)
    {
        $this->fields = FieldConfig::find()->where(['company_id' => $manager_company_id])->indexBy('code')->all();
        $this->company_id = $manager_company_id;
        $this->intersect_key_field_ids = KpTablesData::getIntersectKeyFieldIdsForSearchAndOffers($manager_company_id);
    }

    /**
     * Производит операцию удаления дублирующих строк и извещения менеджера о неверных настройках фильтра поиска
     * @param array $rows - массив строк, которые нужно проверить на дубли
     * @return array $rows - очищенный от дублей и упорядоченный по ключам массив строк
     */
    public function removeSameRows(array $rows) : array
    {
        foreach ($rows as $row_index => $row) {
            if (isset($row['additional_params']['same_row_index'])) {
                $rows = $this->removeAllSameRowsByCondition($rows, $row_index);
            }
        }
        $rows = array_values($rows);
        return $rows;
    }

    /**
     * Есть ли дубли строк?
     * @param $rows
     * @return bool
     */
    public function hasSameRows(array $rows) : bool
    {
        foreach ($rows as $row_index => $row) {
            if (isset($row['additional_params']['same_row_index'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Отмечает дубли строк, добавляя в additional_params ключ same_row_index со значением - номером дублирующей строки
     * @param array $result_rows
     * @return array
     */
    public function markSameRows(array $result_rows) : array
    {
        $hashes = [];
        foreach ($result_rows as $result_row_key => $result_row) {
            $result_rows[$result_row_key]['additional_params']['hash_row_key_field'] = KpTablesData::getKeyFieldsRowHash($result_row, $this->intersect_key_field_ids);
            foreach ($hashes as $hash_key => $hash) {
                if ($hash === $result_rows[$result_row_key]['additional_params']['hash_row_key_field']) {
                    $result_rows[$result_row_key]['additional_params']['same_row_index'] = $hash_key;
                    $result_rows[$hash_key]['additional_params']['same_row_index'] = $result_row_key;
                }
            }
            $hashes[$result_row_key] = $result_rows[$result_row_key]['additional_params']['hash_row_key_field'];
        }
        return $result_rows;
    }

    /**
     * Сравнивать ли текущую строку с остальными?
     * @param int $row_index
     * @param array $row
     * @param int $cur_row_index
     * @param array $cur_row
     * @return bool
     */
    private function isRowToCompare(int $row_index, array $row, int $cur_row_index, array $cur_row) : bool
    {
        return $row_index === $cur_row_index || $row_index === $cur_row['additional_params']['same_row_index'] || $row['additional_params']['same_row_index'] === $cur_row_index ||
            $row['additional_params']['same_row_index'] === $cur_row['additional_params']['same_row_index'];
    }

    /**
     * Возвращает все строки, которые нужно сравнить между собой
     * @param array $result_rows
     * @param int $cur_row_index
     * @return array
     */
    private function getAllSameRows(array $result_rows, int $cur_row_index) : array
    {
        $compared_rows = [];
        $cur_row = $result_rows[$cur_row_index];
        if (!$cur_row) {
            return $compared_rows;  // если строку уже удалили, вернем пустой массив
        }
        foreach ($result_rows as $row_index => $row) {
            if ($this->isRowToCompare($row_index, $row, $cur_row_index, $cur_row)) {
                $row['additional_params']['row_index'] = $row_index;
                $compared_rows[] = $row;
            }
        }
        return $compared_rows;
    }

    /**
     * Возвращает массив строк с удаленными дублями
     * @param array $result_rows
     * @param int $cur_row_num
     * @return array
     */
    private function removeAllSameRowsByCondition(array $result_rows, int $cur_row_num) : array
    {
        $compared_rows = $this->getAllSameRows($result_rows, $cur_row_num);

        foreach ($compared_rows as $compared_row) {
            if ($compared_row['additional_params']['row_index'] === $cur_row_num) {
                continue;
            }
            $result_rows = $this->removeSameRowByCondition($result_rows, $cur_row_num, $compared_row['additional_params']['row_index']);
        }
        return $result_rows;
    }

    /**
     * Удаляет дубли строк из массива $result_rows по индексу в зависимости от условия
     * @param array $result_rows - массив строк, из которых нужно удалить дубли
     * @param int $same_row_index - индекс строки из массива $result_rows, которая дублирует текущую
     * @param int $row_index - индекс текущей строки из массива $result_rows
     * @return array $result_rows
     */
    private function removeSameRowByCondition(array $result_rows, int $row_index, int $same_row_index): array
    {
        $cur_row = $result_rows[$row_index];
        $same_as_cur_row = $result_rows[$same_row_index];

        if (empty($cur_row) || empty($same_as_cur_row)) { // Значит, дубль уже удален, идем дальше
            return $result_rows;
        }
        /** сравним маржу, удалим текущую строку, если она с меньшей маржой */
        if ($same_as_cur_row[$this->fields[FieldConfig::RESULT_RATE]['id']] > $cur_row[$this->fields[FieldConfig::RESULT_RATE]['id']]) {
            unset($result_rows[$row_index]);
            return $result_rows;
        }
        /** сравним маржу, удалим дублирующую строку, если она с меньшей маржой */
        if ($same_as_cur_row[$this->fields[FieldConfig::RESULT_RATE]['id']] < $cur_row[$this->fields[FieldConfig::RESULT_RATE]['id']]) {
            unset($result_rows[$same_row_index]);
            return $result_rows;
        }
        $cur_row_total_price = AbstractRowProcessor::getTotalPrice($cur_row, $this->company_id, $this->fields);
        $same_as_cur_row_total_price = AbstractRowProcessor::getTotalPrice($same_as_cur_row, $this->company_id, $this->fields);
        /** если маржа одинакова, сравним Итого. Удалим текущую строку, если она с большим Итого. */
        if ($cur_row_total_price > $same_as_cur_row_total_price) {
            unset($result_rows[$row_index]);
            return $result_rows;
        }
        /** Во всех остальных случаях удалим дублирующую строку*/
        unset($result_rows[$same_row_index]);
        return $result_rows;
    }

}