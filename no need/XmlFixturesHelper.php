<?php

namespace app\classes\helpers;

use Directory;
use DirectoryIterator;
use DOMDocument;
use DOMElement;
use Yii;
use yii\db\mysql\Schema;


/**
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * @package app\classes\helpers
 */
class XmlFixturesHelper
{
    const XML_PATH = '/app/tests/unit/fixtures/';
    const BASE_REL_DIR = '/app/models';

    /**
     * Метод генерирует xml файлы для всех моделей, у которых имеются таблицы в БД
     * @return bool
     */
    public static function generateXmlFilesFromDb()
    {
        $models_array = self::getArrayOfModelsWithTablesInDb();
        foreach ($models_array as $model_name) {
            self::generateXmlFileFromModelName($model_name);
        }

        return true;
    }

    public static function getArrayOfModelsWithTablesInDb()
    {
        $schema = Yii::$app->getDB()->createCommand('SHOW TABLES')->queryColumn();
        $models_array = self::getModelClassesArray();

        foreach ($models_array as $model_name) {
            if (!method_exists($model_name, 'tableName')) {
                continue;
            }
            if (in_array($model_name::tableName(), $schema)) {
                $result_array[] = $model_name;
            }
        }
        return $result_array;
    }

    /**
     * Метод генерирует xml файлы из моделей, имена которых переданы в массиве $model_names_array
     * Массив $model_names_array имеет вид [0 => 'app\models\User',
     *                                      1 => 'app\models\KpTablesData',
     *                                      2 => 'app\models\Currency',
     *                                      3 => 'app\models\data\Table',
     *                                      ........
     *                                      ]
     * @param array $model_names_array
     */
    public static function generateXmlFilesFromModelNamesArray($model_names_array)
    {
        if (!$model_names_array) {
            return;
        }
        foreach ($model_names_array as $model_name) {
            self::generateXmlFileFromModelName($model_name);
        }
    }

    /**
     * Метод возвращает массив всех моделей с неймспейсами в виде строк
     * @return array $classArr
     */
    private static function getModelClassesArray()
    {
        /** @var Directory $dir */
        $directories = self::getModelsDirectoryNamesArray();
        $matches = [];
        foreach ($directories as $dir) {
            while ($item = $dir->read()) {
                if (preg_match('/^(?<class>[^.].+)\.php$/', $item, $matches)) {
                    $classArr[] = self::getClassNameWithNamespaceByDirectoryAndClassName($dir, $matches['class']);
                }
            }
            $dir->close();
        }

        return $classArr;
    }

    /**
     * Метод возвращает массив объектов Directory из папки models
     * @return array $folders
     */
    private static function getModelsDirectoryNamesArray()
    {
        $dir = self::BASE_REL_DIR;
        $folders[] = dir($dir);
        foreach (new DirectoryIterator($dir) as $folder) {
            if (!$folder->isDot() && $folder->isDir()) {
                $folders[] = dir($folder->getPathname());
            }
        }

        return $folders;
    }

    /**
     * Метод создает один xml файл из модели, название которой принимается методом
     * @param string $model_name полное наименование класса в виде namespace/class_name
     */
    private static function generateXmlFileFromModelName($model_name)
    {
        $xml = new DOMDocument('1.0', 'UTF-8');
        $xml_dataset = $xml->appendChild($xml->createElement('dataset'));
        $is_replacing_needed = false;
        $models = $model_name::find()->all();
        if (!$models) {
            return;
        }
        $model_attributes = array_keys((new $model_name)->getAttributes());
        $table_name = (new $model_name)->tableName();
        foreach ($models as $model_field) {
            /** @var DOMElement $xml_field */
            $xml_field = $xml_dataset->appendChild($xml->createElement($table_name));
            foreach ($model_attributes as $model_attribute) {
                if (is_array($model_field->$model_attribute)) {
                    $xml_field->setAttribute($model_attribute, json_encode($model_field->$model_attribute, JSON_UNESCAPED_UNICODE));
                    $is_replacing_needed = true;
                } else {
                    $xml_field->setAttribute($model_attribute, $model_field->$model_attribute);
                }
            }
        }
        $xml->normalizeDocument();
        $xml_file_name = self::saveXmlFile($xml, $table_name);
        if ($is_replacing_needed) {
            self::replaceHtmlQuotes($xml_file_name);
        }
    }

    private static function saveXmlFile($xml, $table_name)
    {
        /** @var DOMDocument $xml */
        $xml->formatOutput = true;
        $xml->resolveExternals = true;
        $xml->normalizeDocument();
        $xml->saveXML();
        $xml_file_name = self::XML_PATH . $table_name . '.xml';
        $handle = fopen($xml_file_name, "w");
        $xml->save($xml_file_name);
        fclose($handle);
        return $xml_file_name;
    }

    private static function replaceHtmlQuotes($xml_file_name)
    {
        $file = fopen($xml_file_name, 'r+', false);
        $text = fread($file, filesize($xml_file_name));
        str_replace('"(', "'(", $text);
        str_replace(')"', ")'", $text);
        str_replace('&quot;', '"', $text);
        fwrite($file, $text);
        fclose($file);
    }

    /**
     * Метод возвращает название класса вместе с неймспейсом в виде строки
     * @param $dir
     * @param $class_name
     * @return string
     */
    private static function getClassNameWithNamespaceByDirectoryAndClassName($dir, $class_name)
    {
        $namespace = str_replace($_SERVER["DOCUMENT_ROOT"], '', $dir->path);
        $namespace = str_replace('/', '\\', $namespace);
        return $namespace . '\\' . $class_name;
    }
}