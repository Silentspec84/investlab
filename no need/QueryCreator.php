<?php

namespace app\classes\db\mysql;

use Yii;

/**
 * Трейт содержащий вспомогательные методы для вставки записей в mysql базу
 * Trait QueryCreator
 * @package app\classes\db\mysql
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
trait QueryCreator
{
	public static function getInsertTypeIgnore():string
	{
		return 'INSERT IGNORE';
	}

	public static function getReplaceTypeIgnore():string
	{
		return 'REPLACE INTO';
	}

	/**
	 * @param array $insert_array массив значений всех полей таблицы кроме id,
	 * в том же порядке, что и столбцы в таблице mysql
	 * @param string $insert_type
	 * @return int
	 * @throws yii\base\InvalidConfigException
	 * @throws yii\db\Exception
	 */
	public static function batchInsert(array $insert_array, string $insert_type): int
	{
		$insert_count = 0;
		if (count($insert_array) > 0) {
			$column_names = static::getTableSchema()->columnNames;
			array_shift($column_names); // удаляем id - он автоинкрементный и при инсерте не нужен
			$values = [];
			foreach ($insert_array as $row) {
				foreach ($row as $index => $field_val) {
					$row[$index] = isset($field_val) ? "'$field_val'" : "NULL";
				}
				$values[] = '(' . implode(', ', $row) . ')';
			}
			$sql_command =  $insert_type . ' ' . static::tableName()
				. ' (' . implode(', ', $column_names) . ') VALUES ' . implode(', ', $values);
			$insert_count = Yii::$app->db->createCommand($sql_command)->execute();
		}

		return $insert_count;
	}
}