<?php

namespace app\classes\helpers;

use yii\caching\MemCache as BaseMemCache;

class MainMemCache extends BaseMemCache
{
    // Определяет режим работы - true, если запущены Unit тесты
    public static $is_test = false;

    protected function setValue($key, $value, $duration)
    {
        if (MainMemCache::$is_test) {
            $key = 'test_' . $key;
        }
        return parent::setValue($key, $value, $duration);
    }
}