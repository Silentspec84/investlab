<?php

namespace app\classes\helpers;

/**
 * Содержит одинаковые методы для работы с фильтрами на разных страницах
 *
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * @package app\classes\helpers
 */
class FiltersHelper
{
    /**
     * @param array $filters
     * [
     *      0 => [
     *          title => 'Архивные',
     *          set_value => static::ARCHIVED, //устанавливаемое значение
     *          is_active => false
     *      ],
     *          ...
     *      N => [
     *          title => 'Активные',
     *          set_value => static::NOT_ARCHIVED,
     *          is_active => true
     *      ]
     * ]
     * @return array
     */
    public static function getFilterValues(array $filters): array
    {
        $filter_values = [];
        foreach ($filters as $filter) {
            if (!$filter['is_active']) {
                continue;
            }
            $filter_values[] = $filter['set_value'];
        }

        return $filter_values;
    }
}