<?php

namespace app\classes\helpers;

use app\models\FormulaList;

/**
 * Класс для работы с FormulaList, пока умеет преобразовывать формулы в обратной польской нотации,
 * заменяя операнды FieldConfig->code на FieldConfig->ORDER_FIELD_NAME
 *
 * Class FormulaHelper
 * @package app\classes\helpers
 */
class FormulaHelper
{
    /** @var int */
    private $company_id;
    /**
     * Массив порядка столбцов вида: [order_number => ['id' => field_id, 'code' => field_code]]
     * @var array
     */
    private $order_fields;

    public function __construct($company_id, $order_fields)
    {
        $this->company_id = $company_id;
        $this->order_fields = $order_fields;
    }

    /**
     * Возвращает операторы, которые может использовать обратная польская нотация(class Rpn.php)
     *
     * @return array
     */
    public static function getRPNOperators(): array
    {
        return ['+', '-', '/', '*', '^'];
    }

    /**
     *
     * Массив вида [
     *      result_formula_order1 => [
     *          'formula' => '10 4 5 + *',   //формула в обратной польской нотации, c замененными операндами FieldConfig->code на FieldConfig->ORDER_FIELD_NAME
     *          'is_consider_currency': false  // Учитывать валюту? т.е. поддерживает мультивалютность
     *          'is_convert_result_to_rub': false // Конвертировать результирующее поле формулы в рубли? Работает если is_consider_currency: true
     *      ] ,
     *         ...
     *      result_formula_orderN => [
     *          'formula' => '10 4 5 + * 2 +',
     *          'is_consider_currency': false
     *          'is_convert_result_to_rub': false
     *      ]
     *  ]
     *
     * @param int $type_formula
     * @return array
     */
    public function getFormulasReplacedCodesWithOrders(int $type_formula): array
    {
        $formulas = FormulaList::findAll(['company_id' =>  $this->company_id, 'type' => $type_formula]);
        if (!$formulas) {
            return [];
        }
        $fields_orders_keys_codes = $this->getFieldsOrdersKeysCodes();
        $formulas_replaced_codes_with_orders = [];
        foreach ($formulas as $formula) {
            $formula_replaced_codes_with_orders = $this->getFormulaReplacedCodeWithOrder($formula, $fields_orders_keys_codes);
            if (!$formula_replaced_codes_with_orders) {
                continue;
            }
            $order_result_formula = $this->getOrderByFieldId($formula->result_field_id);
            if (!$order_result_formula) {
                continue;
            }
            $formulas_replaced_codes_with_orders[$order_result_formula]['formula'] = $formula_replaced_codes_with_orders;
            $formulas_replaced_codes_with_orders[$order_result_formula]['is_consider_currency'] = (bool)$formula->is_consider_currency;
            $formulas_replaced_codes_with_orders[$order_result_formula]['is_convert_result_to_rub'] = (bool)$formula->is_convert_result_to_rub;
        }

        return $formulas_replaced_codes_with_orders;
    }

    /**
     * Возвращает формулу в обратной польской нотации,
     * c замененными операндами FieldConfig->code на FieldConfig->ORDER_FIELD_NAME
     * Например, '10 4 5 + *'
     *
     * @see getFormulasReplacedCodesWithOrders
     * @param FormulaList $formula
     * @param array $fields_code_on_order
     * @return string
     */
    private function getFormulaReplacedCodeWithOrder(FormulaList $formula, array $fields_code_on_order): string
    {
        $formula_operands_and_operators = $this->getFormulaOperandsAndOperators($formula);
        if (!$formula_operands_and_operators) {
            return '';
        }
        foreach ($formula_operands_and_operators as $index => $operand_or_operator) {
            if (array_key_exists($operand_or_operator, $fields_code_on_order)) {
                $formula_operands_and_operators[$index] = $fields_code_on_order[$operand_or_operator];
            }
            if (!$this->isCorrectOperandOrOperator($formula_operands_and_operators[$index])) {
                return '';
            }
        }

        return implode(' ', $formula_operands_and_operators);
    }

    /**
     * Преобразует строковую формулу, в массив операндов и операторов и возвращает этот массив
     *
     * @param FormulaList $formula
     * @return array
     */
    private function getFormulaOperandsAndOperators(FormulaList $formula): array
    {
        if ($formula->is_consider_currency === FormulaList::CONSIDER_CURRENCY) {
            $formula_with_spaces = str_replace(['+', '-'], [' + ', ' - '], $formula->formula);
        } else {
            $formula_with_spaces = $formula->rpn_formula;
        }

        return explode(' ', $formula_with_spaces);
    }

    /**
     * Проверяет что операнд, является is_numeric,
     * а оператор входит в список поддерживаемых операторов для RPN
     *
     * @param string $operand_or_operator
     * @return bool
     */
    private function isCorrectOperandOrOperator(string $operand_or_operator): bool
    {
        return is_numeric($operand_or_operator) || in_array($operand_or_operator, self::getRPNOperators());
    }

    /**
     * Возвращает массив вида: [FieldConfig->code => FieldConfig->ORDER_FIELD_NAME, ... , 'FRAH' => 10]
     *
     * @return array
     */
    private function getFieldsOrdersKeysCodes(): array
    {
        $fields_orders_keys_codes = [];
        foreach ($this->order_fields as $order => $order_field) {
            $fields_orders_keys_codes[$order_field['code']] = $order;
        }

        return $fields_orders_keys_codes;
    }

    /**
     * Возвращает FieldConfig->ORDER_FIELD_NAME по field_id
     *
     * @param int $field_id
     * @return int
     */
    private function getOrderByFieldId(int $field_id): int
    {
        foreach ($this->order_fields as $order => $order_field) {
            if ((int)$order_field['id'] === $field_id) {
                return (int)$order;
            }
        }

        return 0;
    }
}