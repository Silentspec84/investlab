<?php

namespace app\classes\locations;


use app\models\FieldConfig;

/**
 * Class LocationsToRoutes
 * Класс служит для вынесения логики получения валидных локаций для автокомплита маршрутов из SearchFilterApiController.
 *
 * Локация считается валидной, если в отфильтрованных маршрутов
 * она встречается среди существующих точек отправления (для self::TYPE_LOADING)
 * или точек прибытия (для self::TYPE_DISCHARGE)
 * Например, если загружен Маршрут Шанхай-Москва,
 * Москва будет являеться вадлидной локацией для self::TYPE_DISCHARGE, а Новосибирск - нет.
 */
class LocationsToRoutes {

    private const TYPE_LOADING = 1;
    private const TYPE_DISCHARGE = 2;

    private const LOCATION_CACHE_TTL = 2592000; // 1 month
    private const CACHE_ON = true; // ключает или выключает кэширование

    /**
     * Возвращает массив существующих id локаций согласно загруженным маршрутам.
     * Например, если из Шанхая мы не возим грузовиком - id терминала для шанхая не вернется.
     * @param array $result_rows - строки результирующей таблицы после применения фильтров
     * @param int $location_type
     * @param int $company_id
     * @return array
     */
    public static function getValidLocationIds(array $result_rows, int $location_type, int $company_id): array
    {
        $location_ids = [];
        $loading_filed_id = FieldConfig::getFieldIdByCode(FieldConfig::CODE_GEO_POINT_LOADING_PORT, $company_id);
        $discharge_filed_id = FieldConfig::getFieldIdByCode(FieldConfig::CODE_GEO_POINT_DELIVERY, $company_id);
        foreach ($result_rows as $result_row) {
            $locations = $result_row['additional_params']['row']['fields_json'];
            if ($location_type === self::TYPE_LOADING) {
                unset($locations[$discharge_filed_id]); // для места отправления выкидываем DELI (последнюю точку)
            } else {
                unset($locations[$loading_filed_id]); // для места назначения выкидываем PSIN (первую точку)
            }
            foreach ($locations as $number => $location) {
                $location_obj = json_decode($location, true);
                $location_ids[$location_obj['location']['id']] = $location_obj['location']['id'];
            }
        }
        return array_values($location_ids);
    }


    /**
     * Удаляет закешированные id валидных локаций
     *
     * @param $company_id
     */
    public static function invalidateLocationsCache(int $company_id): void
    {
        $location_types = self::getLocationTypes();
        foreach ($location_types as $location_type) {
            $cache_key = self::getLocationsCacheKey($company_id, $location_type);
            \Yii::$app->getCache()->delete($cache_key);
        }
    }

    public static function setToCache($valid_location_ids, int $company_id, int $location_type): void
    {
        if (!self::CACHE_ON) {
            return;
        }
        $cache_key = self::getLocationsCacheKey($company_id, $location_type);
        \Yii::$app->getCache()->set($cache_key, $valid_location_ids, self::LOCATION_CACHE_TTL);
    }

    public static function getFromCache(int $company_id, int $location_type)
    {
        if (!self::CACHE_ON) {
            return false;
        }
        $cache_key = self::getLocationsCacheKey($company_id, $location_type);
        return \Yii::$app->getCache()->get($cache_key);
    }

    public static function isValidLocationType(int $location_type): bool
    {
        $location_types = self::getLocationTypes();
        return in_array($location_type, $location_types, true);
    }

    private static function getLocationTypes(): array
    {
        return [self::TYPE_LOADING, self::TYPE_DISCHARGE];
    }


    private static function getLocationsCacheKey(int $company_id, int $location_type): string
    {
        return 'location_cache_key_'.$company_id.'_'.$location_type;
    }

}
