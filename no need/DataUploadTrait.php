<?php

namespace app\classes\helpers;

use app\classes\helpers\RowProcessor\SimpleTableRowProcessor;
use app\components\AlertWidget;
use app\models\FilesToFieldConfigs;
use app\models\Location;
use app\models\LocationAlias;
use app\models\P2PDeliveryTime;
use Yii;
use app\models\File;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\models\FieldConfig;
use app\models\RawTablesData;
use app\models\TablesData;
use app\models\data\Table;

/**
 * В трейт вынесены местоды общие для контроллеров controllers/DataUploadApiController.php
 * и controllers/ResetDataApiController.php
 * Trait DataUploadTrait
 * @package app\classes\helpers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
trait DataUploadTrait
{
    /** @var File */
    private $file;
    /** @var Table */
    private $table;

    private $row_processor;

    /**
     * возвращает массив строк таблицы с валидированными ячейками
     * @return \app\classes\api\Response
     * @throws \Exception
     */
    protected function getRowsWithValidateCells(): \app\classes\api\Response
    {
        $this->file = $this->getLastFileLoaded();
        if (!$this->file) {
            return $this->response->addError(AlertWidget::widget(['message' => self::ERROR_TEXT_FILE_LOST, 'type' => AlertWidget::TYPE_DANDER]), 'all_fields');
        }
        $raw_tables_data = RawTablesData::find()->where(['company_id' => $this->company->id, 'file_id' => $this->file['id']])->all();
        $fields_config = $this->getFieldConfigsByFileId((int)$this->file['id']);
        $this->table = $this->getTableByFile();
        if (!$raw_tables_data || !$fields_config || !$this->table['type']) {
            return $this->response->addError(AlertWidget::widget(['message' => self::ERROR_TEXT_LOADED_DATA_NOT_FOUND, 'type' => AlertWidget::TYPE_DANDER]), 'all_fields');
        }

        $rows = $this->getValidatedRows($raw_tables_data, $fields_config, $this->table['type']);
        $columns = $this->getColumnsOfFieldsConfig($fields_config);

        return $this->response->setContent([
            'columns' => $columns,
            'rows' => $rows,
            'document_name' => $this->file['source_name'] . '.' . $this->file['extension'],
        ]);
    }

    /**
     * Сохраняет данные скорректированные пользователем в форме загрузки
     * @param array $items
     * @return bool
     */
    protected function saveCheckedData(array $items): bool
    {
        $this->file = $this->getLastFileLoaded();
        if (!isset($this->file['id'])) {
            return $is_valid = false;
        }
        $this->table = $this->getTableByFile();

        $raw_tables_data = $this->getRawTablesDataByPost($items, $this->file['id']);
        $field_configs = $this->getFieldConfigsByFileId($this->file['id']);
        try {
            $is_valid = $this->updateValidatedRawDataFromPost($items, $raw_tables_data, $field_configs, $this->table['type']);
        } catch (Exception $e) {
            $is_valid = false;
            Yii::error('error update field on 3 step: ' . $e->getMessage());
        }

        if ($this->file) {
            File::saveAsCheked($this->file['id']);
        }

        return $is_valid;
    }


    /**
     * Проверяет, что данные из файла были сохранены. Если нет - пытается сохранить.
     * Ставит у файла статус Завершен.
     * @return \app\classes\api\Response
     * @throws Exception
     */
    protected function finish(): \app\classes\api\Response
    {
        $this->file = $this->getLastFileLoaded();
        if (!$this->file) {
            return $this->response->addError(AlertWidget::widget(['message' => self::ERROR_TEXT_FILE_LOST, 'type' => AlertWidget::TYPE_DANDER]), 'all_fields');
        }

        $save_result = true;
        $exist_table_data_for_current_file = TablesData::findOne(['file_id' => $this->file['id']]);
        if (!$exist_table_data_for_current_file) { // Если данные еще не были сохранены пытаемся сохранить
            $save_result = $this->saveDataFromFile();
        }
        if (!$save_result) {
            return $this->response->addError(AlertWidget::widget(['message' => self::ERROR_TEXT_NOT_SAVE_DATA, 'type' => AlertWidget::TYPE_DANDER]), 'all_fields');
        }

        //Получения данных для отрисовки таблицы загруженных данных
        $sql_data = $this->getRowProcessor()->getSqlData([$this->file['id']]);
        $result_rows = $this->getRowProcessor()->getResultRows($sql_data);
        $has_duplicate = $this->getRowProcessor()->hasDuplicate($result_rows);
        $result_rows = $this->decodeJsonFields($result_rows);
		$this->fillP2PDeliveryTimeTable($result_rows);

        $fields_ids = TablesData::find()
            ->distinct(true)
            ->select('field_id')
            ->where(['file_id' => $this->file['id']])
            ->column();
        $title_fields = $this->getRowProcessor()->getResultRowsHeaders($fields_ids);
        if (!$exist_table_data_for_current_file) {
            $this->saveFileToFieldsConfig($this->file['id'], $fields_ids);
        }

        if ($this->file) {
            File::saveAsFinished($this->file['id']);
        }

        return $this->response->setContent([
            'rows_with_index' => $result_rows,
            'title_fields' => $title_fields,
            'has_duplicate' => $has_duplicate
        ]);
    }

	/**
	 * Формирует массив из валидных плечей и заполняет таблицу p2p_delivery_time
	 * Валидными считаются плечи с двумя гео точками и типом загружаемой таблицы -
	 * перевозки(Table::TYPE_TRANSPORT)
	 * @param array $result_rows
	 * @return bool
	 */
	private function fillP2PDeliveryTimeTable(array $result_rows): bool
	{
		$p2p_delivery_times = [];
		foreach ($result_rows as $row) {
			$p2p_delivery_time = $this->getP2PDeliveryTimeAsArray($row);
			if (!$p2p_delivery_time) {
				continue;
			}
			$p2p_delivery_times[] = $p2p_delivery_time;
		}

		if (!$p2p_delivery_times) {
			return false;
		}

		return $this->saveP2PDeliveryTimes($p2p_delivery_times);
	}

	/**
	 * @param array $row
	 * @return array
	 */
	private function getP2PDeliveryTimeAsArray(array $row): array
	{
    	if (!Table::isTransportType((int)$row['type_table'])) {
    		return [];
		}
		$fields_ids_for_types_of_deliveries = FieldConfig::getFieldIdsForTypesOfDeliveries($this->company->id);
		$current_time = time();
		$p2p_delivery_time = [];
    	foreach ($fields_ids_for_types_of_deliveries as $type_of_delivery => $field_ids) {
    		if ($this->hasLoadingAndDischargePointsInRow($row, $field_ids)) {
				$p2p_delivery_time = [
					$type_of_delivery,
					json_decode($row['fields_json'][$field_ids['loading_id']], true)['location']['id'],
					json_decode($row['fields_json'][$field_ids['discharge_id']], true)['location']['id'],
					null,
					$current_time,
					$current_time,
				];
			}
		}

		return $p2p_delivery_time;
	}

	/**
	 * Проверяет, что в строке содержится валидное плечо
	 * @param array $row
	 * @param array $field_ids
	 * @return bool
	 */
	private function hasLoadingAndDischargePointsInRow(array $row, array $field_ids): bool
	{
		return array_key_exists($field_ids['loading_id'], $row['fields_json']) && array_key_exists($field_ids['discharge_id'], $row['fields_json']);
	}

	/**
	 * @param array $p2p_delivery_times
	 * @return bool
	 */
	private function saveP2PDeliveryTimes (array $p2p_delivery_times): bool
	{
		$count_insert_p2p_delivery_times = 0;
		$message = '';
		try {
			$count_insert_p2p_delivery_times = P2PDeliveryTime::batchInsert($p2p_delivery_times, P2PDeliveryTime::getInsertTypeIgnore());
			if ($count_insert_p2p_delivery_times > 0) {
				$message = 'В таблицу ' . P2PDeliveryTime::tableName() . ' добавлено ' . $count_insert_p2p_delivery_times . ' записей';
			}
		} catch (Exception | InvalidConfigException $e) {
			$message = $e->getMessage();
		}
		if ($message) {
			$this->sendMailAdmins($message);
		}

		return (bool)$count_insert_p2p_delivery_times;
	}

    /**
     * Сбрасывает текущий row процессор для случая
     * когда нужно подряд обработать более одного файла
     */
    public function resetRowProcessor(): void
    {
        $this->row_processor = null;
    }

    private function getRowProcessor()
    {
        if ($this->row_processor === null) {
            $this->row_processor = new SimpleTableRowProcessor($this->company->id, $this->user->id);
        }

        return $this->row_processor;
    }

    /**
     * @param int $data_type
     * @param string $value
     * @return bool
     */
    private function isNumberFieldValue(int $data_type, string $value): bool
    {
        return $data_type !== FieldConfig::DATA_TYPE_STRING && $value !== FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
    }

    /**
     * @return File|array|false
     */
    private function getLastFileLoaded()
    {
        if (!$this->file) {
            try {
                $this->file = File::getLastFileDataByUserId($this->user->id);
            } catch (Exception $e) {
                \Yii::error('error find last file load user: ' . $e->getMessage());
                return [];
            }
        }

        return $this->file;
    }

    /**
     * @return Table|array|null|static
     */
    private function getTableByFile()
    {
        if (!$this->table) {
            $this->file = $this->getLastFileLoaded();
            $this->table = Table::findOne(['id' => $this->file['table_id']]);
        }

        return $this->table;
    }

    /**
     * @param FieldConfig[] $fields_config
     * @return array
     */
    private function getColumnsOfFieldsConfig(array $fields_config): array
    {
        $columns = [];
        if (!$fields_config) {
            return $columns;
        }
        foreach ($fields_config as $field_config) {
            $columns[$field_config->code] = $field_config->title;
        }

        return $columns;
    }

    /**
     * @param array $raw_tables_data
     * @param array $fields_config из которых состоит строка
     * @param int $table_type
     * @return array
     */
    private function getValidatedRows(array $raw_tables_data, array $fields_config, int $table_type): array
    {
        $rows = [];
        $row = [];
        /** @var RawTablesData $item */
        $location_titles_to_field_codes = [];
        foreach ($raw_tables_data as $key => $item) {
            if (!isset($fields_config[$item->field_id])) {
                continue;
            }

            /** @var FieldConfig $field_config */
            $field_config = $fields_config[$item->field_id];
            $error = !$field_config->validateField($item->field_val, $table_type);

            $row[$field_config->code] = ['value' => $item->field_val, 'error' => $error, 'id' => $item->id];
            if ($field_config->is_geo_point == FieldConfig::IS_GEO_POINT) {
                $row[$field_config->code]['value'] = json_decode($item->field_val, true);
                $title = mb_strtolower($row[$field_config->code]['value']['raw_value']);
                $location_titles_to_field_codes[$title] = $field_config->code;
            }

            if (count($row) === count($fields_config)) {
                $rows[] = $row;
                $row = [];
            }
        }

        $rows = $this->getRowsWithValidatedLocation($rows, $location_titles_to_field_codes);

        return $rows;
    }

    /**
     * @param array $rows
     * @param array $location_titles_to_field_codes
     * $location_titles_to_field_codes = [
     *      (string)'location_title' => (array[DELI|POUT|PSIN|SOUT])'location_field_code',
     *      ...
     * ]
     *
     * @return array
     */
    private function getRowsWithValidatedLocation(array $rows, array $location_titles_to_field_codes): array
    {
        if (empty($location_titles_to_field_codes)) {
            return $rows;
        }
        $location_titles = array_keys($location_titles_to_field_codes);
        $location_types = [];

        foreach (array_unique($location_titles_to_field_codes) as $code) {
            if (isset(Location::getEndpointTypesWithFieldCodes()[$code])) {
                $location_types[] = Location::getEndpointTypesWithFieldCodes()[$code];
            }
        }

        $location_data_list = LocationAlias::getLocationsDataByTitles($location_titles, $location_types);
        foreach ($rows as $key => $original_row) {
            $row_with_add_location = $original_row;
            foreach ($location_titles_to_field_codes as $location_code) {
                $title = mb_strtolower($original_row[$location_code]['value']['raw_value']);
                if (isset($location_data_list[$title])) {
                    $row_with_add_location[$location_code]['value']['location'] = [
                        'id' => $location_data_list[$title]['location_id'],
                        'type' => $location_data_list[$title]['type'],
                        'title' => $location_data_list[$title]['location_title'],
                    ];
                    $row_with_add_location[$location_code]['value']['location_alias'] = [
                        'id' => $location_data_list[$title]['alias_id'],
                        'lang' => $location_data_list[$title]['lang'],
                        'title' => $location_data_list[$title]['alias_title'],
                    ];
                    $row_with_add_location[$location_code]['error'] = false;
                }
            }
            $rows[$key] = array_values($row_with_add_location);
        }

        return $rows;
    }

    /**
     * @param array $post
     * @param integer $file_id
     * @return RawTablesData[]|null
     */
    private function getRawTablesDataByPost(array $post, $file_id): ?array
    {
        $item_ids = [];
        foreach ($post as $key_row => $row) {
            if (!$row['checked']) {
                continue;
            }
            foreach ($row['items'] as $item) {
                $item_ids[] = $item['id'];
                if ($item['error']) {
                    break;
                }
            }
        }

        /** @var RawTablesData $models */
        $raw_tables_data = RawTablesData::find()
            ->where([
                'file_id' => $file_id,
                'id' => $item_ids
            ])->indexBy('id')
            ->all();

        return $raw_tables_data;
    }

    /**
     * Обновляет таблицу RAW на основнии даннах пришедших в запросе после проверки
     * @param array $post
     * @param RawTablesData[] $raw_table_data
     * @param FieldConfig[] $field_configs
     * @param int $table_type
     * @return bool
     *
     * Save RawTablesData
     * @throws \yii\db\Exception
     */
    private function updateValidatedRawDataFromPost($post, $raw_table_data, $field_configs, int $table_type): bool
    {
        $sql_field_values = [];
        $is_valid = false;
        foreach ($post as $key_row => $rows) {
            $checked = $rows['checked'] ?? null;
            if (!$checked) {
                continue;
            }
            if (!isset($rows['items'])) {
                return false;
            }
            $is_valid = true;
            foreach ($rows['items'] as $field) {
                /** @var RawTablesData $raw_table_item */
                $raw_table_item = $raw_table_data[$field['id']];
                if (!$raw_table_item) {
                    continue;
                }

                /** @var FieldConfig $field_config */
                $field_config = $field_configs[$raw_table_item->field_id];
                $raw_table_item->is_validate = (int)($field_config && $field_config->validateField($field['value'], $table_type));
                $raw_table_item->is_checked = (int)$checked;
                $field['value'] = $this->getJsonEncodeValue($field['value']);
                $raw_table_item->field_val = \Yii::$app->db->quoteValue($field['value']);
                $sql_field_values[] = "(" . implode(',', RawTablesData::asArray($raw_table_item)) . ")";
            }
        }

        if (count($sql_field_values) > 0) {
            $is_valid = true;
            $table_name = RawTablesData::tableName();
            $raw_table_fields = implode(",", array_keys(RawTablesData::asArray()));
            $column_values = implode(' ,', $sql_field_values);

            $sql = "REPLACE INTO $table_name ($raw_table_fields) VALUES $column_values";

            Yii::$app->getDb()->createCommand($sql)->execute();
        }

        return $is_valid;
    }

    /**
     * Если $value массив вернет закодированный json,
     * иначе вернет просто $value
     *
     * @param $value
     * @return string
     */
    private function getJsonEncodeValue($value): string
    {
        if (isset($value) && is_array($value)) {
            return json_encode($value);
        }

        return $value;
    }

    /**
     * @return bool
     */
    private function saveDataFromFile(): bool
    {
        $new_sql_data = $this->getRawSqlData();
        $new_valid_sql_data = $this->getValidRawSqlData($new_sql_data);
        $old_files_ids = $this->getOldFileIds();
        $old_rows = [];
        $new_rows = [];

        if (!$old_files_ids) { // Если нет старых файлов для конкретной таблицы, то перезаписываем данные
            return $this->rewriteTablesData($new_valid_sql_data);
        }
        if (!$this->isOverwriteData((int)$this->file['save_mode'])) { // выбран режим "update" т.е. старые записи будут объединены с новыми
            $new_rows = $this->getRowProcessor()->getRowsWithCompositeKey(ArrayHelper::toArray($new_valid_sql_data, ['id', 'field_id', 'field_val', 'file_id']));
            $old_sql_data = $this->getRowProcessor()->getSqlData($old_files_ids);
            $old_rows = $this->getRowProcessor()->getRowsWithCompositeKey($old_sql_data);
        }
        if ($old_rows && $new_rows) { // Если есть старые строки(из старых файлов) и новые строки, то обновляем(дополняем) данные
            $fields_empty = $this->getFieldsEmpty($old_files_ids);
            $result_rows = $this->getMergedRows($new_rows, $old_rows, $fields_empty);
            // Очищаем данные из старых файлов
            TablesData::deleteAll(['file_id' => $old_files_ids]);

            return $this->updateTablesData($result_rows);
        }
        // Очищаем данные из старых файлов
        TablesData::deleteAll(['file_id' => $old_files_ids]);

        return $this->rewriteTablesData($new_valid_sql_data);

    }

    /**
     * Сохраняем fields_ids добавленные в сохраняемом файле
     *
     * @param int $file_id
     * @param array $fields_ids
     * @return bool
     * @throws \yii\db\Exception
     */
    private function saveFileToFieldsConfig(int $file_id, array $fields_ids): bool
    {
        $batch_file_to_fields_config = [];
        foreach ($fields_ids as $field_id) {
            $batch_file_to_fields_config[] = [
                'file_id' => $file_id,
                'field_id' => $field_id
            ];
        }
        if (!$batch_file_to_fields_config) {
            return false;
        }

        $columns_file_to_fields_config = ['file_id', 'field_id'];
        $insertCount = Yii::$app->db->createCommand()
            ->batchInsert(FilesToFieldConfigs::tableName(), $columns_file_to_fields_config, $batch_file_to_fields_config)
            ->execute();


        return (bool)$insertCount;
    }

    /**
     * Метод собирает и возвращает валидные строки из полей таблицы raw_tables_data
     *
     * @param array $raw_sql_data
     * @return array
     */
    private function getValidRawSqlData(array $raw_sql_data): array
    {
        $valid_raw_table_data_ids = [];
        $raw_table_data_ids_one_row = [];
        $is_valid_row_ids = true;
        $first_element_row = reset($raw_sql_data);
        $last_element_row = end($raw_sql_data);

        foreach ($raw_sql_data as $raw_table_data_id => $field) {
            if ($this->isEndOfRow($first_element_row, $field)) {
                if ($is_valid_row_ids) {
                    $valid_raw_table_data_ids += $raw_table_data_ids_one_row;
                }

                $raw_table_data_ids_one_row = [];
                $first_element_row = $field;
                $is_valid_row_ids = true;
            }

            $raw_table_data_ids_one_row[$raw_table_data_id] = $field['is_validate'];
            if (!$field['is_validate']) {
                $is_valid_row_ids = false;
            }

            if ($last_element_row['id'] === $field['id']) {
                if ($is_valid_row_ids) {
                    $valid_raw_table_data_ids += $raw_table_data_ids_one_row;
                }
            }
        }

        if ($valid_raw_table_data_ids) {
            $raw_sql_data = array_intersect_key($raw_sql_data, $valid_raw_table_data_ids);
        }

        return $raw_sql_data;
    }

    /**
     * @param RawTablesData $first_element_row
     * @param RawTablesData $field
     * @return bool
     */
    protected function isEndOfRow(RawTablesData $first_element_row, RawTablesData $field): bool
    {
        return $first_element_row['id'] !== $field['id'] && ((int)$first_element_row['field_id'] === (int)$field['field_id'] || (int)$first_element_row['file_id'] !== (int)$field['file_id']);
    }

    /**
     * @param RawTablesData[] $result_rows
     * @return bool
     */
    private function updateTablesData($result_rows): bool
    {
        $this->file = $this->getLastFileLoaded();
        $column_values = [];
        foreach ($result_rows as $row) {
            foreach ($row as $field_id => $field_val) {
                if ($this->getRowProcessor()->isTechnicalField($field_id)) {
                    continue;
                }
                $model = new TablesData();
                $model->user_id = $this->user->id;
                $model->company_id = $this->company->id;
                $model->field_id = $field_id;
                $model->field_json = json_decode((string)$row['fields_json'][$field_id], true) ?? null;
                $model->field_val = $model->field_json['location']['id'] ?? $field_val;
                $model->file_id = $this->file['id'];

                $column_values[] = TablesData::asArray($model);
            }
        }

        return TablesData::batchInsertByData($column_values);
    }


    /**
     * @param RawTablesData[] $new_sql_data
     * @return bool
     */
    private function rewriteTablesData($new_sql_data): bool
    {
        $column_values = [];
        /** @var RawTablesData $new_model */
        foreach ($new_sql_data as $new_model) {
            if ($new_model->isGoodToSave()) {
                $column_values[] = TablesData::asArrayFromRaw($new_model);
            }
        }

        return TablesData::batchInsertByData($column_values);
    }

    /**
     * @param array $result_rows
     * @return mixed
     */
    private function decodeJsonFields($result_rows)
    {
        foreach ($result_rows as $key => $row) {
            foreach ($row as $id => $field) {
                if ($this->getRowProcessor()->isTechnicalField($id)) {
                    continue;
                }
                $data = json_decode($field, true);
                if ($data) {
                    $result_rows[$key][$id] = $data;
                }
            }
        }

        return $result_rows;
    }

    /**
     * @param int $file_id
     * @return FieldConfig[]
     */
    private function getFieldConfigsByFileId(int $file_id): array
    {
        $column_ids = RawTablesData::find()
            ->distinct(true)
            ->select(['field_id'])
            ->where(['company_id' => $this->company->id, 'file_id' => $file_id])
            ->column();

        if (!$column_ids) {
            return [];
        }
        return FieldConfig::find()
            ->where([
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'company_id' => $this->company->id,
                'id' => $column_ids,
            ])
            ->orderBy([new Expression('FIELD (id, ' . implode(',', $column_ids) . ')')])
            ->indexBy('id')
            ->all();
    }

    /**
     * @return array
     */
    private function getOldFileIds(): array
    {
        return File::find()
            ->distinct(true)
            ->select('files.id')
            ->innerJoin('tables_data', 'tables_data.file_id = files.id')
            ->where(['!=', 'files.id', $this->file['id']])
            ->andWhere([
                'files.company_id' => $this->file['company_id'],
                'files.table_id' => $this->file['table_id']
            ])
            ->column();
    }

    /**
     * @param int $save_mode
     * @return bool
     */
    private function isOverwriteData(int $save_mode): bool
    {
        return $save_mode === File::SAVE_MODE_REWRITE;
    }

    /**
     * Возвращает массив пустых полей вида
     * [field_id1 => '', ... ,field_idN => FieldConfig::FIELD_VAL_NOT_EXIST_VALUE]
     * Берутся поля обновляемого и нового файла
     *
     * @param array $old_files_ids
     * @return array
     */
    private function getFieldsEmpty(array $old_files_ids): array
    {
        $this->file = $this->getLastFileLoaded();
        $file_ids_current_table = $old_files_ids;
        array_push($file_ids_current_table, $this->file['id']);
        $fields_configs = FieldConfig::find()
            ->select(['id', 'data_type'])
            ->where([
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'company_id' => $this->company->id,
            ])
            ->indexBy('id')
            ->all();

        $all_fields_table = RawTablesData::find()
            ->distinct(true)
            ->select('field_id')
            ->where(['file_id' => $file_ids_current_table])
            ->asArray()->all();

        $fields_empty = [];
        foreach ($all_fields_table as $field) {
            if ($fields_configs[$field['field_id']] === FieldConfig::DATA_TYPE_STRING) {
                $fields_empty[$field['field_id']] = '';

                continue;
            }
            $fields_empty[$field['field_id']] = FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
        }

        return $fields_empty;
    }

    /**
     * Объединяет старые и новые строки, общие поля обновляет,
     * не совпадающие дополняет заглушкой $fields_empty
     * @see getFieldsEmpty()
     *
     * @param array $new_rows
     * @param array $old_rows
     * @param array $fields_empty
     * @return array
     */
    private function getMergedRows(array $new_rows, array $old_rows, array $fields_empty): array
    {
        $result_rows = [];
        foreach ($new_rows as $key => $row) {
            if (array_key_exists($key, $old_rows)) {
                $result_rows[$key] = $row + $old_rows[$key];
            } else {
                $result_rows[$key] = $row + $fields_empty;
            }
        }

        foreach ($old_rows as $key => $row) {
            if (!array_key_exists($key, $new_rows)) {
                $result_rows[$key] = $row + $fields_empty;
            }
        }

        return $result_rows;
    }

    /**
     * Получаем последние загруженные данные
     *
     * @return RawTablesData[]
     */
    private function getRawSqlData()
    {
        $new_sql_data = RawTablesData::find()
            ->where([
                'file_id' => $this->file['id']
            ])
            ->indexBy('id')
            ->all();

        return $new_sql_data;
    }
}