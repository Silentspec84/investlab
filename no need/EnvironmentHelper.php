<?php

namespace app\classes\helpers;

use app\models\erp\TekErpOrder;

require_once __DIR__ . '/SingletonTrait.php';

/**
 * Класс помогающий определять текущее окружение Anyport
 *
 * Class EnvironmentHelper
 * @package app\classes\helpers
 */
class EnvironmentHelper
{
    use SingletonTrait;

    /** Имена окружений */
    public const BETA = 'beta.anyport.ru'; // бета
    public const CALC = 'calc.anyport.ru'; // бой
    public const TEST = 'test.anyport.ru'; // тест
    public const LOCAL = 'anyport.test'; // локал

    /** Имена субдоменов */
    public const BETA_SUB_DOMAIN = 'beta'; // бета
    public const CALC_SUB_DOMAIN = 'calc'; // дефолтный домен с нашим юзером
    public const SECOND_SUB_DOMAIN = 'second'; // дефолтный домен 2 с нашим юзером
    public const TEK_SUB_DOMAIN = 'tek'; // Тэк оператор
    public const TEST_SUB_DOMAIN = 'test'; // тест

    public const LOCAL_ZONE = 'test';
    public const SERVER_ZONE = 'ru';
    public const DOMAIN_NAME = 'anyport';

    public const ERROR_NO_CONFIG = 'Ошибка конфигурации, обратитесь к администратору ресурса';

    /** @var string */
    protected $env_name;

    protected function init(...$args)
    {
        $this->env_name = $args[0] ?? '';
    }

    public function isLocal(): bool
    {
        return $this->env_name === self::LOCAL;
    }

    public function isBeta(): bool
    {
        return $this->env_name === self::BETA;
    }

    public function isProd(): bool
    {
        return $this->env_name === self::CALC;
    }

    public function isTest(): bool
    {
        return $this->env_name === self::TEST;
    }

    /**
     * @return string
     */
    public function getEnvName(): string
    {
        return $this->env_name;
    }

    public function getDomain($company_id = null, $scheme = false): string
    {
        $sub_domain = $company_id ? $this->getSubDomainByCompanyId($company_id) : $this->getSubDomain();
        if ($this->isLocal()) {
            $domain = $sub_domain ? $sub_domain.'.'.self::DOMAIN_NAME.'.'.self::LOCAL_ZONE : $this->getDefaultSubDomain() . '.' . self::DOMAIN_NAME.'.'.self::LOCAL_ZONE;
        } else {
            $domain = $sub_domain ? $sub_domain . '.' . self::DOMAIN_NAME . '.' . self::SERVER_ZONE : $this->getDefaultSubDomain() . '.' .self::DOMAIN_NAME . '.' . self::SERVER_ZONE;
        }
        return $scheme ? $this->getScheme().'://'.$domain : $domain;
    }

    /**
     * Возвращает дефолтный поддомен для окуржения.
     * Система должна существовать только на доменах 3-го уровня.
     * Такое правило нужно, чтобы не получилось редиректа, например, на anyport.ru/login - такой страницы нет, т.к. anyport.ru - это лендинг
     * @return string
     */
    private function getDefaultSubDomain(): string
    {
        if ($this->isLocal() || $this->isProd()) {
            return self::CALC_SUB_DOMAIN;
        }
        if ($this->isTest()) {
            return self::TEST_SUB_DOMAIN;
        }
        if ($this->isBeta()) {
            return self::BETA_SUB_DOMAIN;
        }
        return self::CALC_SUB_DOMAIN;
    }

    /**
     * @return array|null
     */
    public function getSubDomainCurCompanySettings(): ?array
    {
        $company_id = $this->getCompanyId();
        if (!$company_id) {
           return null;
        }
        $company_settings = $this->getSubDomainSettings($company_id);
        if (!$company_settings) {
           return null;
        }
        return $company_settings;
    }

    /**
     * Пренадлежит ли текущий поддомен залогиненой компании?
     * @param $company_id
     * @return bool
     */
    public function isCurDomainBelongsToAuthorizedCompany($company_id): bool
    {
        return $company_id === $this->getCompanyId();
    }

    /**
     * Возвращает протокол текущего url
     * @return mixed
     */
    private function getScheme() {
        return parse_url(\Yii::$app->request->getHostInfo())['scheme'];
    }

    /**
     * Возвращает домент 3-го уровня от текущего url
     * @return string
     */
    private function getSubDomain(): ?string
    {
        $url = explode('.', \Yii::$app->request->getHostName());
        if (!$url || count($url) != 3) {
            return null;
        }

        return array_shift($url);
    }

    /**
     * Возвращает домент 3-го уровня, для которого есть конфиг для company_id
     * @param $company_id
     * @return false|int|string
     */
    private function getSubDomainByCompanyId($company_id) {
        $config = $this->getSubDomainToCompnyIdConfig();
        $env_config = $config[$this->getEnvName()];
        return array_search($company_id, $env_config);
    }

    /**
     * Возвращает настройки субдомена для переданной company_id
     * @param $company_id
     * @return array
     */
    public function getSubDomainSettings($company_id): ?array
    {
        $settings = [
            36 => [
                'id' => 36, // id компании
                'colour' => '#38414a', // цвет хедера футера
                'icon_name' => 'logo-dark.svg', // logo
                'links' => [
                    ['link' => '/', 'name' => 'Контакты (ссылка 1)'],
                    ['link' => '/', 'name' => 'Контакты (ссылка 2)'],
                    ['link' => '/', 'name' => 'Контакты (ссылка 3)'],
                    ['link' => '/', 'name' => 'Контакты (ссылка 4)'],
                ],
                'phone' =>  '+x (xxx) xxx-xx-xx',
                'email' =>  'xxxx@xxxx.ru',
                'site' =>  'xxxxxx.ru',
            ],
            34 => [
                'id' => 34, // id компании
                'colour' => '#0039c2', // цвет хедера футера
                'icon_name' => 'logo-dark.svg', // logo
                'links' => [
                    ['link' => '/', 'name' => 'Контакты (ссылка 1)'],
                    ['link' => '/', 'name' => 'Контакты (ссылка 2)'],
                    ['link' => '/', 'name' => 'Контакты (ссылка 3)'],
                    ['link' => '/', 'name' => 'Контакты (ссылка 4)'],
                ],
                'phone' =>  '+x (xxx) xxx-xx-xx',
                'email' =>  'xxxx@xxxx.ru',
                'site' =>  'xxxxxx.ru',
            ],
            44 => [
                'id' => 44, // id компании
                'colour' => '#0039c2', // цвет хедера футера
                'icon_name' => 'logo-dark.svg', // logo
                'links' => [
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 1'],
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 2'],
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 3'],
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 4'],
                ],
                'phone' =>  '+7 (999) 333-22-11',
                'email' =>  'test_starbase@nayport.ru',
                'site' =>  'test_starbase.ru',
            ],
            40 => [
                'id' => 40, // id компании
                'colour' => '#0039c2', // цвет хедера футера
                'icon_name' => 'logo-dark.svg', // logo
                'links' => [
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 1'],
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 2'],
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 3'],
                    ['link' => '/', 'name' => 'Контакты test_starbase@nayport.ru 4'],
                ],
                'phone' =>  '+7 (999) 333-22-11',
                'email' =>  'test_starbase@nayport.ru',
                'site' =>  'test_starbase.ru',
            ],
            1 => [
                'id' => 1, // id компании
                'colour' => '#FFFFFF', // цвет хедера футера
                'menu_colour' => '#333333', //цвет ссылок в шапке
                'icon_name' => 'tec_logo.png', // logo
                'links' => [
                    ['link' => 'https://tekoperator.ru/', 'name' => 'Главная'],
                    ['link' => 'https://tekoperator.ru/ru/uslugi', 'name' => 'Услуги'],
                    ['link' => 'https://tekoperator.ru/ru/contacts', 'name' => 'Контакты'],
                ],
                'phone' =>  '+7 (495) 150-05-09',
                'email' =>  'info@tekoperator.ru',
                'site' =>  'tekoperator.ru',
                'erp_order_class' => TekErpOrder::class,
            ],
        ];

       return $settings[$company_id];

    }

    /**
     * Возвращает company_id для данного окружения и домена.
     * @return mixed
     */
    private function getCompanyId() {
        $config = $this->getSubDomainToCompnyIdConfig();
        $sub_domain = $this->getSubDomain();

        return $config[$this->getEnvName()][$sub_domain];
    }

    /**
     * Возвращает связь между company_id и доменом 3-го уровня для каждого из окружений
     * @return array
     */
    private function getSubDomainToCompnyIdConfig(): array
    {
        return [
            self::LOCAL => [
                self::CALC_SUB_DOMAIN => 36, // startbase@anyport.ru
                self::TEK_SUB_DOMAIN => 1, // zmm@tekoperator.ru
                self::SECOND_SUB_DOMAIN => 40, // test_startbase@anyport.ru
            ],
            self::BETA => [
                self::BETA_SUB_DOMAIN => 1,
            ],
            self::CALC => [
                self::CALC_SUB_DOMAIN => 36, // startbase@anyport.ru
                self::TEK_SUB_DOMAIN => 1, // zmm@tekoperator.ru
                self::SECOND_SUB_DOMAIN => 40, // test_startbase@anyport.ru
            ],
            self::TEST => [
                self::TEST_SUB_DOMAIN => 40,
            ],
        ];
    }
}