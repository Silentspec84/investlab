<?php

namespace app\classes\exceptions;

class FileCacheProcessException extends \Exception {
   public const FILE_CACHE_IN_PROGRESS_ERROR_FOR_LOG = 'Идет построение файлового кэша. Данные недоступны. ';
   public const FILE_CACHE_IN_PROGRESS_ERROR_FOR_USER = 'Идет обработка данных. Поторите попытку через минуту.';

}

