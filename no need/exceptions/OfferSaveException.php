<?php

namespace app\classes\exceptions;

/**
 * Исключение для ошибок сохранения Offer
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 *
 *
 * Class OfferSaveException
 * @package app\classes\exceptions
 */
class OfferSaveException extends \Exception
{
    public const MAIN_ERROR = 0;
    public const DATE_ERROR = 1;

    public static $error_code_keys = [
        self::MAIN_ERROR => 'all_fields',
        self::DATE_ERROR => 'date_till',
    ];

    public function __construct($message, $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}