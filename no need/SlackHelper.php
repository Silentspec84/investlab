<?php

namespace app\classes\helpers;

use Maknz\Slack\Client;

/**
 * Класс-обертка для работы с пакетом Maknz\Slack,
 * для быстрой и удобной отправки сообщений в слак
 *
 * Class SlackHelper
 * @package app\classes\helpers
 */
class SlackHelper
{
    /** @var string Incoming webhook нужен чтобы работать с входящими сообщениями slack */
    private $hookUrl  = 'https://hooks.slack.com/services/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';

    /** @var string канал в Slack куда отправлять сообщения*/
    public $channel = '#china-developers';

    /** @var string имя отправителя*/
    public $username = 'Anyport';

    /** @var string иконка отправителя задается в виде :emoji:*/
    public $icon = '';

    /**
     * Отправкак сообщения в канал Slack
     *
     * @param string $text
     */
    public function sendMessage(string $text)
    {
        $settings = [
            'channel' => $this->channel,
            'username' => $this->username,
            'icon' => $this->icon
        ];

        $client = new Client($this->hookUrl, $settings);
        $message = $client->createMessage()->setText($text);
        $client->sendMessage($message);
    }

    public function to($channel)
    {
        $this->setChannel($channel);

        return $this;
    }

    /**
     * @param string $channel
     * @return $this
     */
    public function setChannel(string $channel): SlackHelper
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username): SlackHelper
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string $icon
     * @return $this
     */
    public function setIcon(string $icon): SlackHelper
    {
        $this->icon = $icon;

        return $this;
    }
}