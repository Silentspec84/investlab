<?php


namespace app\classes\api;

/**
 * Class DaDataClient
 * Класс для работы с API DaData
 *
 * @package app\classes\api;
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
class DaDataClient
{
    private const URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/';
    private const TOKEN = '59d9b98c928caaaf09d8c9197aa615222f9b22bc';

    const TYPE_INDIVIDUAL = 'INDIVIDUAL';

    /**
     * Возвращает найденный в dadata данные по запросу($query)
     *
     * @param string $query
     * @param array $params (настройки запроса, например:
     *   $params = [
     *      'status' => ["ACTIVE"], получить только работающие компании
     *      'resource' => 'party', поиск по названию компании
     *      'count' => 5, количество компаний в результате
     *   ];
     * )
     * @return array
     */
    public function suggest(string $query, array $params): array
    {
        $resource = $params['resource'] ?? 'party';
        $params['query'] = $query;
        $options = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => array(
                    'Content-type: application/json',
                    'Authorization: Token ' . self::TOKEN,
                ),
                'content' => json_encode($params),
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents(self::URL . $resource, false, $context);
        $result = json_decode($result, true);

        return is_array($result) ? $result : [];
    }
}