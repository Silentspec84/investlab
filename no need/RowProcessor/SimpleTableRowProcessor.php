<?php

namespace app\classes\helpers\RowProcessor;
use app\models\File;
use app\models\TableConfig;
use app\models\TablesData;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * Класс содержит методы для работы с конкретной таблицей
 *
 * Class SimpleTableRowProcessor
 * @package app\classes\helpers
 */

class SimpleTableRowProcessor extends AbstractRowProcessor
{
    /** @var array id полей ключей конкретной таблицы */
    private $table_keys = [];

    public function __construct($company_id, $user_id, $table_id = null)
    {
        parent::__construct($company_id, $user_id);
        if ($table_id) {
            $this->table_keys = $this->getTableKeys($table_id);
        } else {
            try {
                $file = File::getLastFileDataByUserId($user_id);
            } catch (Exception $e) {
                \Yii::error('error getLastFileDataByUserId: '.$e->getMessage());
                $file = [];
            }
            $this->table_keys = $file ? $this->getTableKeys($file['table_id']) : [];
        }
    }

    public function getSqlData(array $file_ids = null): array
    {
        $condition = $file_ids ? ['company_id' => $this->company_id, 'file_id' => $file_ids] : ['company_id' => $this->company_id];

        $sql_data = TablesData::find()
            ->select(['id','field_id','field_json', 'field_val','file_id'])
            ->where($condition)
            ->asArray()->all();

        return $sql_data;
    }

    public function getResultRows(array $sql_data): array
    {
        $rows = $this->getRows($sql_data);
        return $this->getRowsMarkedDuplicate($rows);
    }

    public function getResultRowsHeaders(array $field_ids = null): array
    {
        return $this->getHeaderFieldsForIds($field_ids);
    }

    /**
     * Генерирует составной ключ из (FieldConfig::IS_KEY) строкам и возвращает их
     *
     * @param array $sql_data
     * @return array
     */
    public function getRowsWithCompositeKey(array $sql_data): array
    {
        $result_rows = [];
        $rows = $this->getRows($sql_data);
        foreach ($rows as $row) {
            $composite_key = [];
            foreach ($this->table_keys as $key) {
                if (isset($row[$key])) {
                    $composite_key[] = $row[$key];
                }
            }
            $composite_key = implode(',', $composite_key);
            if (isset($result_rows[$composite_key])) {
                $result_rows[$composite_key] = (array)$result_rows[$composite_key] + $row;
            } else {
                $result_rows[$composite_key] = $row;
            }
        }

        return $result_rows;
    }

    /**
     * Проверка есть ль дублирующие строчки по ключу таблицы в строках
     *
     * @param array $rows
     * @return bool
     */
    public function hasDuplicate(array $rows): bool
    {
        foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($rows), RecursiveIteratorIterator::LEAVES_ONLY) as $key => $value) {
            if ('is_duplicate' === $key) {
                return true;
            }
        }

        return false;
    }

    /**
     * Получаем строки где у дубликатов появляется соответствующие поля is_duplicate
     *
     * @param array $rows
     * @return array
     */
    private function getRowsMarkedDuplicate(array $rows): array
    {
        $count_rows = count($rows);
        $num_row = 0;
        $is_duplicate = true;
        while ($num_row != $count_rows) {
            foreach ($rows as $key_row => $row) {
                if ($key_row <= $num_row || array_key_exists('is_duplicate',$row)) {
                    continue;
                }
                foreach ($this->table_keys as $key) {
                    if (array_key_exists($key, $rows[$num_row]) && $rows[$num_row][$key] != $row[$key]) {
                        $is_duplicate = false;
                    }
                }

                if ($is_duplicate) {
                    $rows[$key_row]['is_duplicate'] = $is_duplicate;
                } else {
                    $is_duplicate = true;
                }
            }

            $num_row++;
        }

        return $rows;
    }

    /**
     * @param integer $table_id
     * @return array
     */
    private function getTableKeys(int $table_id): array
    {
        $fields = TableConfig::find()
            ->select('field_id')
            ->where([
                'table_id' => $table_id,
                'is_key' => TableConfig::IS_KEY
            ])
            ->asArray()->all();

        return ArrayHelper::getColumn($fields, 'field_id');
    }

    /**
     * @param array $field
     * @return mixed
     */
    protected function getFieldValFromField(array $field)
    {
        if (!json_decode($field['field_json'], true)['raw_value']) {
            return json_decode($field['field_val'], true)['raw_value'] ?? $field['field_val'];
        }

        return json_decode($field['field_json'], true)['raw_value'] ?? $field['field_val'];
    }
}