<?php

namespace app\classes\helpers\RowProcessor;

use app\classes\db\mysql\ActiveRecordVersion;
use app\models\KpTablesData;
use app\models\FieldConfig;
use app\models\store\Offer;
use yii\helpers\ArrayHelper;

/**
 * Класс содержит методы позволяющие собирать строки из данных
 * cart_tables_data
 *
 * Class OfferRowProcessor
 * @package app\classes\helpers
 */
class ManagerOfferRowProcessor extends ResultTableRowProcessor
{
    /** @var null  */
    protected $fields_for_show = null;

    /** @var int */
    protected $offer_id;

    /** Название аттрибута поля fields отвечающего за order
     */
    const ORDER_FIELD_NAME = 'order_in_kp';

    public function __construct($company_id, $user_id, $offer_id = null)
    {
        parent::__construct($company_id, $user_id);
        $this->offer_id = $offer_id;
        $this->setDateVersion();
    }

    protected function setDateVersion()
    {
        if (!$this->offer_id) {
            return;
        }
        $offer = Offer::findOne(['id' => $this->offer_id]);
        $date_version = $offer->date_sent_to_client ?? null;
        ActiveRecordVersion::$date_version = $date_version;
    }

    public function getSqlData(array $file_ids = null): array
    {
        $tables_data = KpTablesData::find()
            ->where([
                'offer_id' => $this->offer_id,
                'company_id' => $this->company_id
            ])
            ->orderBy([
                'hash_row' => 'ASC',
                'field_id' => 'DESC',
            ])->asArray()->all();

        return $tables_data;
    }

    public function getResultRows(array $sql_data): array
    {
        $title_fields = $this->getResultRowsHeaders();
        $title_ids = ArrayHelper::getColumn($title_fields, 'id');

        $rows = [];
        $row = [];
        $sql_first_row = reset($sql_data);
        $sql_last_row = end($sql_data);
        $in_way_field_id = (int)FieldConfig::getFieldIdByCode(FieldConfig::IN_WAY, $this->company_id);

        foreach ($sql_data as $sql_row) {
            if (!in_array((string)$sql_row['field_id'], $title_ids)
                    && $sql_last_row['id'] !== $sql_row['id']) {
                continue;
            }

            if ($this->isEndOfRow($sql_first_row, $sql_row)) {
                $row = $this->addEmptyInWay($row, $in_way_field_id);
                $row += $this->addRowAdditionalParams($sql_first_row);
                $rows[] = $row;
                $row = [];
                $sql_first_row = $sql_row;
            }

            $row[$sql_row['field_id']] = $this->getFieldValFromField($sql_row);

            if ($sql_last_row['id'] === $sql_row['id']) {
                $row = $this->addEmptyInWay($row, $in_way_field_id);
                $row += $this->addRowAdditionalParams($sql_first_row);
                $rows[] = $row;
            }
        }

        return $this->getOrderedRows($rows);
    }

    /**
     * Добавляет пустое значение В Пути, если оно не заполнено
     * Это нужно для получения пустого столбца В Пути при создании КП
     * @param array $row
     * @param int $in_way_field_id
     * @return array
     */
    protected function addEmptyInWay(array $row, int $in_way_field_id): array
    {
        if (empty($row[$in_way_field_id])) {
            $row[$in_way_field_id] = '';
        }
        return $row;
    }

    /**
     * Добавляет в строку дополнительные параметры, такие, как хэши, is_archived, offer_id
     * @param array $sql_first_row
     * @return array
     */
    protected function addRowAdditionalParams(array $sql_first_row): array
    {
        $offer_id = $sql_first_row['offer_id'];
        /** @var Offer $offer */
        $offer = Offer::findOne($offer_id);
        $row['additional_params']['hash_row'] = $sql_first_row['hash_row'];
        $row['additional_params']['is_archived'] = $offer->is_archived == Offer::ARCHIVED ? true : false;
        $row['additional_params']['hash_row_key_field'] = $sql_first_row['hash_row_key_field'];
        $row['additional_params']['offer_id'] = $offer->id;
        return $row;
    }

    public function getSpecialTitle(): array
    {
        return [
            FieldConfig::SUBTOTAL_FRAHT_OUT => 'Фрахт',
            FieldConfig::SUBTOTAL_RW_OUT => 'Жд',
            FieldConfig::SECURITY => 'Охрана',
            FieldConfig::SUBTOTAL_AUTO_OUT => 'Авто',
            FieldConfig::RESULT_OUT => 'ИТОГО'
        ];
    }
}