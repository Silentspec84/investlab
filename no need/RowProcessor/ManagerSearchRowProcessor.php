<?php

namespace app\classes\helpers\RowProcessor;
use app\controllers\ManagerSearchFilterApiController;
use app\controllers\PublicApiController;
use app\models\CartTablesData;
use app\models\CartVersion;
use app\models\FieldConfig;
use app\models\File;
use app\models\Location;
use app\models\SearchFilterData;
use app\models\SearchFiltersKeys;
use app\models\TablesData;
use app\models\UserSettings;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * Класс содержит методы для работы с таблицей на странице поиска
 *
 * Class ManagerSearchRowProcessor
 * @test ManagerSearchRowProcessorTest
 * @package app\classes\helpers
 */

class ManagerSearchRowProcessor extends ResultTableRowProcessor {
    protected const ERROR_NO_MANAGER_SEARCH_FILTER = 'Настройте <a href="/manager-search-filter/">менеджерские фильтры</a>';

    protected $field_codes = [
        FieldConfig::WEIGHT,
        FieldConfig::VTT,
        FieldConfig::SUBTOTAL_RW_IN,
    ];

    /**
     * @var array
     */
    protected $fields_by_codes = [];

    /**
     * Массив полей вида [field_id => field_val] по которым нужно отфильтровать данные итоговой таблицы (без локаций)
     * @var array
     */
    protected $filters = [];

    /**
     * Массив фильтров $this->filters, c той лишь разницей, что из них исключены поля, которых нет в fields_config.
     * Такого и не должно быть на практике, но на всякий случай...
     * @var array
     */
    protected $safe_filters = [];

    /**
     * Массив кодов полей по которым нужно отфильтровать данные итоговой таблицы
     * В $this->filters попадут только те поля, которые заданы в этом массиве
     * @var array
     */
    protected $filter_codes = [];

    /**
     * Все поисковые фильтры, пришедшие из поиска "как есть"
     * @var array
     */
    protected $post_filters = [];

    /**
     * Все доп параметры из формы поиска
     * @var array
     */
    protected $post_add_params = [];

    /**
     * Дата отгрузки из поиска
     * @var int
     */
    protected $actual_date;

    /**
     * @var int
     */
    protected $client_company_id;

    /**
     * @var []
     * Location list connected with locations from query
     */
    protected $location_id_list = [];

    /**
     * Данные лежащие в корзине для конкретного клиента
     * @var array
     */
    protected $cart_sql_data = [];

    /**
     * Массив field_config_id, для плеч участвующих в поиске
     * @var array
     */
    private $field_ids_by_type_of_delivery = [];

    /**
     * Массив настроенных фильтров менеджером для менеджерского поиска
     * @var array|null
     */
    protected $filters_manager_search = [];

    /** Название аттрибута поля fields отвечающего за order
     */
    const ORDER_FIELD_NAME = 'order_in_search';

    /**
     * ManagerSearchRowProcessor constructor.
     * @param int $company_id
     * @param int $user_id
     * @param array $filters
     * @param array $additional_params
     * @param null $actual_date
     */
    public function __construct($company_id, $user_id, $filters = [], $additional_params = [], $actual_date = null)
    {
        parent::__construct($company_id, $user_id);

        $this->filters_manager_search = $this->getManagerSearchFilter($filters);
        $this->post_filters = $filters;
        $this->post_add_params = $additional_params;
        $this->filter_codes = FieldConfig::getFilterSearchCodes();
        $this->filters = $this->replaceFilterKeysWithFiledIds($filters);
        $this->location_id_list = $this->getLocationIdList($filters);
        $this->actual_date = $actual_date;
        $this->client_company_id = $additional_params['client_company_id'] ?? null;
        $this->order_fields = $this->getOrderFields();
        $this->fields_by_codes = FieldConfig::getFieldsByCodes($this->field_codes, $company_id);
    }

    /**
     * Метод сетит массив id локаций начальных и конечных точек, по которым будет выполнен поиск
     *
     * @param array $filters - массив фильтров, пришедших из поисковой формы вида:
     * [
     *      loading_location => [0 => id0, 1 => id1, n => idn ...],
     *      discharge_location => [0 => id0, 1 => id1, n => idn ...],
     *      lang => 'RU',
     *      cargo => '20',
     *      loading_date => '2019-07-09',
     *      discharge_location_object => [
     *              alias_id => alias_id1 (int),
     *              id => [0 => 'id1', 1 => 'id2', ...],
     *              is_last_endpoint_and_exist_towns => false,
     *              is_related_location => false,
     *              is_all_locations => true,
     *              iso => 'RU',
     *              lang => 'RU',
     *              render_type => 'end_point',
     *              title => 'Москва (все жд станции)'
     *              type => [
     *                  title => 'Москва (все жд станции)',
     *                  icon => 'mdi mdi-tram',
     *                  type => 60 (Location::type),
     *              ],
     *              is_icon_needed => true
     *      ],
     *      loading_location_object => аналогично discharge_location_object,
     *      weight => 0,
     *      custom_type => 1,
     *      client_company_title => "Anyport test",
     *      client_company_id => 34
     * }
     *
     * @return array - массив id для поиска вида:
     * [
     *      loading_location => [0 => id0, 1 => id1, ....],
     *      discharge_location => [0 => id0, 1 => id1, ....]
     * ]
     *
     */
    private function getLocationIdList(array $filters): array
    {
        if (isset($filters[FieldConfig::GEO_POINT_LOADING]) && $filters[FieldConfig::GEO_POINT_DISCHARGE]) {
            $location_id_list = [
                FieldConfig::GEO_POINT_LOADING => $filters['loading_location_object']['id'],
                FieldConfig::GEO_POINT_DISCHARGE => $filters['discharge_location_object']['id']
            ];
        }
        return $location_id_list ?? [];
    }

    /**
     * Если в конструктор передан менеджерский фильтр, то возвращаем его,
     * если нет то получаем все менеджерские фильтры
     *
     * @param array $filters
     * @return array
     */
    protected function getManagerSearchFilter(array $filters): array
    {
        if (isset($filters[ManagerSearchFilterApiController::TITLE_FILTER])) {
            return $filters[ManagerSearchFilterApiController::TITLE_FILTER];
        }

        return $this->getSearchFilter(SearchFiltersKeys::SOURCE_TYPE_MANAGER_SEARCH);
    }

    /**
     * Получение всех поисковых фильтров менеджерских или клиентских
     * массив вида:
     * [
     *      'data' => [
     *          hash_filter_1 => [
     *              field_id1 => [field_val1, ..., field_valN],
     *                  ...
     *              field_idN => [field_val1, ..., field_valM]
     *          ],
     *              ...
     *          hash_filter_N => [
     *              field_id1 => [field_val1, ..., field_valN],
     *                  ...
     *              field_idK => [field_val1, ..., field_valM]
     *          ],
     *      ],
     *      'is_exception_rows' => false // Возвращать исключенные строки из поисковой выдачи
     * ]
     *
     * @param int $source_type
     * @return array
     */
    protected function getSearchFilter(int $source_type): array
    {
        $search_filter_data_table_name = SearchFilterData::tableName();
        $search_filters_keys_table_name = SearchFiltersKeys::tableName();
        $models = SearchFilterData::find()
            ->select([
                $search_filter_data_table_name . '.hash_filter',
                $search_filter_data_table_name . '.field_val',
                $search_filter_data_table_name . '.field_id'
            ])
            ->innerJoin($search_filters_keys_table_name, $search_filters_keys_table_name . '.id = ' . $search_filter_data_table_name . '.key_id' )
            ->where([
                $search_filters_keys_table_name . '.company_id' => $this->company_id,
                $search_filters_keys_table_name . '.source_type' => $source_type
            ])
            ->asArray()
            ->all();
        if (!$models) {
            return [];
        }
        $search_filter_data = [];
        foreach ($models as $model) {
            $search_filter_data[$model['hash_filter']][$model['field_id']][] = $model['field_val'];
        }

        return ['data' => $search_filter_data, 'is_exception_rows' => false];
    }

    /**
     * Возвращает id локаций родителя И детей  родительскому id и по типу локации
     *
     * @param int $parent_location_id
     * @param int $type
     * @return array
     */
    private function getLocationsIdsByParentIdAndType(int $parent_location_id, int $type): array {
        $location_ids = Location::find()->select('id, title')
            ->indexBy('title')
            ->where(['parent_location_id' => $parent_location_id])
            ->orWhere(['id' => $parent_location_id])
            ->andWhere(['type' => $type])
            ->column();

        return $location_ids ? $location_ids : [$parent_location_id];
    }

    /**
     * @param array $sql_data
     * @return array
     * @throws \app\classes\exceptions\FileCacheProcessException
     * @throws \Exception
     */
    public function getResultRows(array $sql_data): array
    {
        if (!$this->filters_manager_search || !$this->filters_manager_search['data']) {
            $this->errors[] = static::ERROR_NO_MANAGER_SEARCH_FILTER;
            return [];
        }
        if (!$this->actual_date) {
            $this->actual_date = date('Y-m-d');
        }

        $result_rows = [];
        $timestamp_range_actual = $this->getDateActualForData();
        if (!$timestamp_range_actual ||
            date('Y-m-d', $timestamp_range_actual['dt_actual_before']) < $this->actual_date ||
            date('Y-m-d', $timestamp_range_actual['dt_actual_after']) > $this->actual_date) {
            $this->errors[] = 'Нет данных за выбранную дату.';
            return $result_rows;
        }

        $result_rows = $this->getRowsOriginal($sql_data);
        $result_rows = $this->getRowsWithHashKey($result_rows, $timestamp_range_actual['dt_actual_before']);
        $result_rows = $this->getFilterRows($result_rows);
        if (empty($result_rows)) {
            $this->errors[] = 'Нет строк, удовлетворяющих параметрам поиска. Вы можете посмотреть все актуальные загруженные строки в <a href="/result-table/">Итоговой таблице</a> и проверить <a href="/manager-search-filter">Фильтр поиска</a>';
            return [];
        }
        $result_rows = $this->getRowsWithCorrectVtt($result_rows);
        $result_rows = $this->getRowsWithHiddenFields($result_rows);
        $result_rows = $this->getRowsWithLocationsTitlesInsteadIdsAndInWay($result_rows);
        $result_rows = $this->transformManagerSearchRowsToClientSearchRows($result_rows);

        return $this->getOrderedRows($result_rows);
    }

    /**
     * Проверяет, что в корзине есть записи для конкретной клиентской компании
     * @return bool
     */
    public function isCartNotEmptyForClientCompany(): bool
    {
        if ($this->cart_sql_data) {
            return true;
        }

        if (!$this->client_company_id) {
            return false;
        }

        $cart_version_model = CartVersion::getCurrentVersionModel($this->company_id, $this->client_company_id, $this->user_id);
        if (!$cart_version_model) {
            return false;
        }
        /** @var CartVersion $cart_version_model */
        CartTablesData::$date_version = $cart_version_model->version_date;

        return CartTablesData::find()
            ->distinct(true)
            ->select(['hash_row', 'is_weight_over_24t', 'is_security', 'is_danger_cargo', 'is_special_tariff', 'is_home_consignment', 'custom_type', 'date_actual'])
            ->where([
                'company_id' => $this->company_id,
                'client_company_id' => $this->client_company_id,
                'is_deleted' => CartTablesData::IS_NOT_DELETED
            ])
            ->exists();
    }

    protected function transformManagerSearchRowsToClientSearchRows($result_rows)
    {
        return $result_rows;
    }

    protected function getOrderedRowWithAddParams(array $row): array
    {
        $ordered_row = $this->getOrderedRow($row);

        $ordered_row['additional_params']['row'] = $row;
        if ($row['hash_row']) {
            $ordered_row['additional_params']['hash_row'] = $row['hash_row'];
        }
        $ordered_row['shown'] = true;

        $order_hash_row = $ordered_row['additional_params']['hash_row'];

        $ordered_row = $this->fillRowParamsFromCartData($ordered_row, $order_hash_row);

        return $ordered_row;
    }

    /**
     * @param array $ordered_row
     * @param string $order_hash_row
     * @return mixed
     */
    protected function fillRowParamsFromCartData($ordered_row, $order_hash_row)
    {
        if (!array_key_exists($order_hash_row, $this->getCartData())) {
            return $ordered_row;
        }

        $ordered_row['checked'] = true;
        $ordered_row['disabled'] = true; // Смысл этого поля что строка добавлена в корзину, поэтому она будет disabled
        $ordered_row['additional_params']['is_weight_over_24t'] = (bool)$this->cart_sql_data[$order_hash_row]['is_weight_over_24t'];
        $ordered_row['additional_params']['is_security'] = (bool)$this->cart_sql_data[$order_hash_row]['is_security'];
        $ordered_row['additional_params']['is_danger_cargo'] = (bool)$this->cart_sql_data[$order_hash_row]['is_danger_cargo'];
        $ordered_row['additional_params']['is_special_tariff'] = (bool)$this->cart_sql_data[$order_hash_row]['is_special_tariff'];
        $ordered_row['additional_params']['is_home_consignment'] = (bool)$this->cart_sql_data[$order_hash_row]['is_home_consignment'];
        $ordered_row['additional_params']['custom_type'] = $this->cart_sql_data[$order_hash_row]['custom_type'];
        $ordered_row['additional_params']['date_actual'] = $this->cart_sql_data[$order_hash_row]['date_actual'];

        return $ordered_row;
    }

    /**
     * Получаем диапозон валидности данных,
     * путем получения пересечения диапозонов всех файлов,
     * участвующих в составление результирующей таблицы
     *
     * @return array
     */
    private function getDateActualForData () {
        $files_ids_for_dt_actual = TablesData::find()
            ->select(['file_id'])
            ->where(['company_id' => $this->company_id])
            ->all();

        $files_ids_for_dt_actual = ArrayHelper::getColumn($files_ids_for_dt_actual, 'file_id');
        $files_for_dt_actual = File::find()
            ->select(['date_upload_since', 'date_upload_by'])
            ->where([
                'company_id' => $this->company_id,
                'id' => $files_ids_for_dt_actual
            ])
            ->all();

        $dt_max = 0;
        $dt_min = 0;
        foreach ($files_for_dt_actual as $key => $file) {
            if (!$dt_max || !$dt_min){
                $dt_min = $file['date_upload_since'];
                $dt_max = $file['date_upload_by'];
                continue;
            }

            if ($file['date_upload_by'] < $dt_max) {
                $dt_max = $file['date_upload_by'];
            }
            if ($file['date_upload_since'] > $dt_min) {
                $dt_min = $file['date_upload_since'];
            }

            if ($dt_max < $dt_min) {
                return [];
            }
        }

        if ($dt_max && (time() < $dt_max)) {
            return [
                'dt_actual_before' => $dt_max,
                'dt_actual_after' => $dt_min
            ];
        }

        return [];
    }

    /**
     * Возвращает строки с hash-ключом составленному из значений fields
     *
     * @param array $rows
     * @param int $timestamp_actual_before
     * @return array
     */
    private function getRowsWithHashKey(array $rows, int $timestamp_actual_before): array
    {
        $field_dt_actual = FieldConfig::find()
            ->select(['id'])
            ->where([
                'company_id' => $this->company_id,
                'code' => FieldConfig::DATE_ACTUAL_CODE,
                'is_deleted' => FieldConfig::IS_NOT_DELETED
            ])->one();

        foreach ($rows as $key => $row) {
            $field_values = [];
            /** @var [] $row */
            foreach ($row as $field_id => $field_val) {
                if (is_numeric($field_id)) {
                    $field_values[] = $field_val;
                }
            }
            $field_values[] = $this->post_filters['custom_type'] ?? 0;
            $rows[$key]['hash_row'] = $this->getRowHash($field_values);

            if ($field_dt_actual['id']) {
                $rows[$key][$field_dt_actual['id']] = date('d.m.Y', $timestamp_actual_before);
            }
        }

        return $rows;
    }

    /**
     * @param array $field_values
     * @return string
     */
    private function getRowHash(array $field_values): string
    {
        return hash('md5', implode(',', $field_values));
    }

    /**
     * Возвращает отфильтрованные строки
     * @param array $rows
     * @return array
     */
    private function getFilterRows(array $rows): array
    {
        $filtered_rows = [];
        foreach ($rows as $row) {
            if ($this->isGoodRow($row)) {
                $filtered_rows[] = $row;
            }
        }

        return $filtered_rows;
    }

    /**
     * Возвращает true, если строка удовлетворяет всем типам фильтров. Иначе - false.
     * @param $row
     * @return bool
     */
    protected function isGoodRow($row): bool
    {
        return
            $this->isGoodRowByFilters($row)
            && $this->isGoodRowByManagerSearchFilter($row)
            && $this->isGoodRowByWayPoints($row);
    }

    /**
     * Проверяет что строка соответствует настройкам менеджерских поисковых фильтров
     * Настраивается менеджерами
     *
     * @param array $row
     * @return bool
     */
    protected function isGoodRowByManagerSearchFilter(array $row): bool
    {
        return $this->isGoodRowBySearchFilter($row, $this->filters_manager_search);
    }

    /**
     * Проверяет что строка соответствует настройкам менеджерского или клиентских фильтров
     * Настраивается менеджерами
     *
     * @param array $row
     * @param array $filters
     * @return bool
     */
    protected function isGoodRowBySearchFilter(array $row, array $filters): bool
    {
        $filters_data = $filters['data'] ?? [];
        $is_exception_mode = $filters['is_exception_rows'] ?? false;
        $is_good_row = false; // Если не попадает под фильтры, строка считается не валидной
        foreach ($filters_data as $hash_filter => $filter) {
            foreach ($filter as $field_id => $field_values) {
                $is_good_row = true;
                if (in_array(SearchFilterData::FIELD_VAL_ALL, $field_values)) {
                    continue;
                }
                if (!isset($row[$field_id]) || !in_array($row[$field_id], $field_values)) {
                    $is_good_row = false; //строка не совпала с фильтром
                    break;
                }
            }
            if ($is_good_row) { //если попал хоть под один фильтр, то дальше смотреть не нужно
                break;
            }
        }

        return $is_exception_mode ? !$is_good_row : $is_good_row;
    }

    /**
     * Возвращает true, если поисковые фильтры существуют, и строка им удовлетворяет. Иначе - false.
     * @param array $row
     * @return bool
     */
    private function isGoodRowByFilters(array $row): bool
    {
        $save_filters = $this->getSafeFilters();
        if (!$save_filters) {
            $this->errors[] = 'Ошибка получения фильтров для поиска. Проверьте правильность заполнения таблицы <a href="/field-config/">Настройка данных</a>';
            return (bool)$this->filters_manager_search ['data'];
        }

        $field_weight =  $this->fields_by_codes[FieldConfig::WEIGHT]->id;
        foreach ($save_filters as $key => $filter) {
            if ($field_weight && $field_weight === $key) {
                if ($this->isCorrectWeight($row[$key], $filter)) {
                    continue;
                }
                return false;
            }

            if (!isset($row[$key]) || $row[$key] !== (string)$filter) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $row_weight
     * @param int $filter_weight
     * @return bool
     */
    private function isCorrectWeight(string $row_weight, int $filter_weight): bool
    {
        if (!$row_weight) {
            return true;
        }
        if ($row_weight === 'да' && $filter_weight >= FieldConfig::WEIGHT_TARIFF_CHANGE_BOUND) {
            return true;
        } else if ($row_weight === 'нет' && $filter_weight < FieldConfig::WEIGHT_TARIFF_CHANGE_BOUND) {
            return true;
        }

        return false;
    }

    /**
     * Возвращает true, если в строке содержится маршрут, переданный из поисковой формы,
     * с учетом алиасов м не имеет значения FieldConfig::NOT_EXIST_VALUE в геоточках маршрута
     * и в его плечах(type_of_delivery). Иначе - false.
     *
     * @param array $row
     * @return bool
     */
    private function isGoodRowByWayPoints(array $row): bool
    {
        if ($this->isQueryFromFilterForm()) {
            return true;
        }

        $way_points = $this->getWayPoints($row);
        if (!$way_points) {
            return false;
        }

        $types_of_delivery_for_way = $this->getTypesOfDeliveryForWay($way_points);
        if (!$types_of_delivery_for_way) {
            $this->errors[] = 'Не удалось построить плечи по маршруту. Вы можете посмотреть все возможные строки в <a href="/result-table/">результирующей таблице</a>';
            return false;
        }

        $this->field_ids_by_type_of_delivery = $this->getFieldIdsByTypesOfDelivery($types_of_delivery_for_way);
        if (!$this->field_ids_by_type_of_delivery) {
            $this->errors[] = 'Не удалось получить данные по плечам маршрута. Вы можете посмотреть все возможные строки в <a href="/result-table/">результирующей таблице</a>';
            return false;
        }

        return $this->isGoodRowByTypeOfDelivery($row);
    }

    /**
     * Если делаем запрос не из поисковой формы,
     * то у нас нет отправных точек, следовательно проверять их на корректность не нужно
     *
     * @return bool
     */
    private function isQueryFromFilterForm(): bool
    {
        return !$this->location_id_list && $this->filters_manager_search['data'];
    }


    /**
     * Метод возвращает массив геоточек вида
     * field_config_id => [
     *      'is_loading' => true, (или 'is_discharging' => 'true)
     *      'value' => location_id,
     *      'type_of_delivery' => TypesOfDeliveries::CODE_FRAHT
     * ]
     *
     * @param array $row
     * @return array
     */
    private function getWayPoints(array $row): array
    {
        $way_point_field_ids = FieldConfig::getFieldIdsForTypesOfDeliveries($this->company_id);
        $loading_ids = $this->location_id_list[FieldConfig::GEO_POINT_LOADING];
        $discharge_ids = $this->location_id_list[FieldConfig::GEO_POINT_DISCHARGE];
        $loading_way_point_exists = false;
        $discharge_way_point_exists = false;

        $way_points = [];
        foreach ($way_point_field_ids as  $type_delivery => $way) {
            $is_way_point_field =
                (
                    isset($row[$way['loading_id']])
                    && isset($row[$way['discharge_id']])
                );

            if ($is_way_point_field && in_array((int)$row[$way['loading_id']], $loading_ids)) {
                $way_points[$way['loading_id']]['is_loading'] = true;
                $loading_way_point_exists = true;
            }
            if ($is_way_point_field && in_array((int)$row[$way['discharge_id']], $discharge_ids)) {
                $way_points[$way['discharge_id']]['is_discharging'] = true;
                $discharge_way_point_exists = true;
            }

            $way_points[$way['loading_id']]['value'] = $row[$way['loading_id']];
            $way_points[$way['discharge_id']]['value'] = $row[$way['discharge_id']];

            if (!isset($way_points[$way['loading_id']]['type_of_delivery'])) {
                $way_points[$way['loading_id']]['type_of_delivery'] = $type_delivery;
            }
            if (!isset($way_points[$way['discharge_id']]['type_of_delivery'])) {
                $way_points[$way['discharge_id']]['type_of_delivery'] = $type_delivery;
            }
        }

        if ($loading_way_point_exists && $discharge_way_point_exists){
            return $way_points;
        } else {
            return [];
        }
    }

    /**
     * Возвращает массив плеч встретившихся внутри маршрута, пришедшего из поиска
     * Массив состоит из const TypesOfDeliveries::CODE_FRAHT, TypesOfDeliveries::CODE_RAILWAY, TypesOfDeliveries::CODE_AUTO
     *
     * @param array $way_points
     * @return array
     */
    private function getTypesOfDeliveryForWay(array $way_points): array
    {
        $check_value = false;
        $counter = 0;
        $types_of_delivery = [];
        foreach ($way_points as $field_id => $way_point) {
            if (isset($way_point['is_loading']) && $way_point['is_loading']) {
                $num_loading_point = $counter;
                $check_value = true;
            }

            if ($check_value && $way_point['value'] === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                return [];
            }
            if ($check_value) {
                $types_of_delivery[] = $way_point['type_of_delivery'] ?? null;
            }

            if (isset($way_point['is_discharging']) && $way_point['is_discharging']) {
                $num_discharging_point = $counter;
                $check_value = false;
            }
            $counter++;
        }

        if (!isset($num_loading_point) || !isset($num_discharging_point) || $num_loading_point >= $num_discharging_point) {
            return [];
        }

        array_shift($types_of_delivery); // Отбрасываем первый тип доставки, т.к. он принадлежит точке отправки, в ней движения не происходит.

        return $types_of_delivery;
    }

    /**
     * @param array $types_of_delivery
     * @return array
     */
    private function getFieldIdsByTypesOfDelivery(array $types_of_delivery): array
    {
        if (!$this->field_ids_by_type_of_delivery) {
            $this->field_ids_by_type_of_delivery = FieldConfig::find()
                ->select('id')
                ->where([
                    'company_id' => $this->company_id,
                    'type_of_delivery' => $types_of_delivery,
                    'type' => FieldConfig::TYPE_CONST,
                    'is_deleted' => FieldConfig::IS_NOT_DELETED
                ])->column();
        }

        return $this->field_ids_by_type_of_delivery;
    }

    /**
     * Проверка что в полях по заданным плечам(type_of_delivery) нет значений FieldConfig::NOT_EXIST_VALUE
     *
     * @param array $row
     * @return bool
     */
    private function isGoodRowByTypeOfDelivery(array $row): bool
    {
        foreach ($row as $field_id => $field_value) {
            if (!in_array($field_id, $this->field_ids_by_type_of_delivery)) {
                continue;
            }

            if ($field_value === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                return false;
            }
        }

        return true;
    }

    /**
     * Возвращает массив $this->filters, из которого удалены поля,
     * чьи id не найдены в таблице fields_config
     * @return array
     */
    private function getSafeFilters(): array
    {
        if ($this->safe_filters) {
            return $this->safe_filters;
        }
        if (!$this->filters) {
            return [];
        }
        $fields = FieldConfig::find()
            ->select('id')
            ->where(['company_id' => $this->company_id, 'is_deleted' => FieldConfig::IS_NOT_DELETED])
            ->groupBy('id')
            ->asArray()
            ->all();
        $fields = array_flip(ArrayHelper::getColumn($fields, 'id'));
        $this->safe_filters = array_intersect_key($this->filters, $fields);

        return $this->safe_filters;
    }

    /**
     * Изменяет поле VTT и подитог ЖД (в который входит VTT), в зависимости от значения 'custom_type', пришедшего из формы:
     * если custom_type порт, зануляем, иначе - оставляем без изменений.
     * @param array $result_rows
     * @return array
     */
    private function getRowsWithCorrectVtt(array $result_rows): array
    {
        if (!isset($this->post_filters['custom_type'])) {
            return $result_rows;
        }
        $vtt_id = $this->fields_by_codes[FieldConfig::VTT]->id;
        $subtotal_rw_in = $this->fields_by_codes[FieldConfig::SUBTOTAL_RW_IN]->id;
        foreach ($result_rows as $row_number => $result_row) {
            if ($this->post_filters['custom_type'] === PublicApiController::CUSTOM_TYPE_PORT) {
                if ($result_rows[$row_number][$subtotal_rw_in] !== FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                    $result_rows[$row_number][$subtotal_rw_in] -= (float)$result_rows[$row_number][$vtt_id]; // вычитаем VTT из подитога ЖД
                }
                $result_rows[$row_number][$vtt_id] = 0; // зануляем VTT
            }
        }

        return $result_rows;
    }

    /**
     * Получаем строки со скрытыми полями для выпадашки
     *
     * @param array $rows
     * @return array
     */
    private function getRowsWithHiddenFields(array $rows): array
    {
        $hidden_fields_key_code = FieldConfig::find()
            ->select(['id', 'title', 'code'])
            ->where([
                'company_id' => $this->company_id,
                'code' => self::getHiddenFields(),
                'is_deleted' => FieldConfig::IS_NOT_DELETED
            ])
            ->indexBy('code')
            ->orderBy([new Expression('FIELD (code, "' . implode('","', self::getHiddenFields()) . '")')])
            ->all();

        foreach ($rows as $key_row => $row) {
            $hidden_row = [];
            $i = 0;
            foreach ($hidden_fields_key_code as $field_code => $field) {
                $hidden_row[$i]['id'] = $field['id'];
                $hidden_row[$i]['title'] = $field['title'];
                if ($field_code === FieldConfig::HOME_BILL_CODE) {
                    $hidden_row[$i]['field_value'] = !empty($row[$field['id']]) ? 'Да' : 'Нет';
                } else {
                    $hidden_row[$i]['field_value'] = $row[$field['id']];
                }
                $i++;
            }
            $rows[$key_row]['additional_params']['hidden_row'] = $hidden_row;
        }

        return $rows;
    }

    /**
     * @return array
     */
    public static function getHiddenFields()
    {
        return [
            FieldConfig::PAYER_FRAHT,
            FieldConfig::OWNERSHIP_CONTAINER,
            FieldConfig::TERMS_OF_USE,
            FieldConfig::OWNER_CONTAINER,
            FieldConfig::HOME_BILL_CODE
        ];
    }

    /**
     * Возвращает массив фильтров вида:
     * [
     *      0 => [
     *          'title' => (string)field_title1,
     *          'values' => [
     *              (string)field_value1 => [
     *                  'value' => (string)field_value1,
     *                  'checked' => (bool)true
     *              ],
     *                  ...
     *              (string)field_valueN => [
     *                  'value' => (string)field_valueN,
     *                  'checked' => (bool)false
     *              ]
     *          ]
     *      ],
     *          ...
     *      N => [
     *          'title' => (string)field_titleN,
     *          'values' => [
     *              (string)field_value1 => [
     *                  'value' => (string)field_value1,
     *                  'checked' => (bool)false
     *              ],
     *                  ...
     *              (string)field_valueN => [
     *                  'value' => (string)field_valueN,
     *                  'checked' => (bool)true
     *              ]
     *          ]
     *      ]
     * ]
     * @param array $rows_result_table
     * @return array
     */
    public function getFiltersData(array $rows_result_table): array
    {
        $filters_data = [];
        $fields_for_filter_data = $this->getFieldsForFiltersData();
        if (!$fields_for_filter_data) {
            return $filters_data;
        }
        $original_rows = $this->getOriginalRows($rows_result_table);
        if (!$original_rows) {
            return $filters_data;
        }

        $filters_data_saved_state = $this->getFiltersState();
        foreach ($original_rows as $row) {
            foreach ($row as $key_field => $field) {
                if (is_numeric($key_field)) {
                    $filters_data = $this->addFilter($filters_data, $fields_for_filter_data, $field, $key_field, $filters_data_saved_state);
                } elseif ($key_field === 'additional_params') {
                    foreach ($row[$key_field]['hidden_row'] as $hidden_field) {
                        $filters_data = $this->addFilter($filters_data, $fields_for_filter_data, $hidden_field['field_value'], $hidden_field['id'], $filters_data_saved_state);
                    }
                }
            }
        }
        if (!$filters_data) {
            return $filters_data;
        }

        return $this->unsetFiltersIfFewValues($filters_data);
    }

    /**
     * Удаляет фильтр из набора фильтров, если в нем 1 и менее значений.
     * Такой фильтр из одного чекбокса не имеет смысла.
     * @param array $filters_data
     * @return array
     */
    protected function unsetFiltersIfFewValues(array $filters_data): array
    {
        foreach ($filters_data as $title => $filter) {
            if (count($filter['values']) <= 1) {
                unset($filters_data[$title]);
            }
        }
        return $filters_data;
    }

    /**
     * Возвращает массив $filters_data, в который добавился элемент с $field_title и $field_value
     *
     * @param array $filters_data
     * @param array $fields_for_filter_data
     * @param string $field_value
     * @param int $field_id
     * @param array $filters_data_saved_state
     * @return array
     */
    protected function addFilter(array $filters_data, array $fields_for_filter_data, string $field_value, int $field_id, array $filters_data_saved_state = []): array
    {
        if (!array_key_exists($field_id, $fields_for_filter_data)) {
            return $filters_data;
        }

        $filter_value = [$field_value => ['value' => $field_value, 'checked' => true]];
        $filters_data[$field_id]['title'] = $fields_for_filter_data[$field_id];
        $filters_data[$field_id]['values'] = $filters_data[$field_id]['values'] ?? [];
        $filters_data[$field_id]['values'] += $filter_value;
        $filters_data[$field_id]['show'] = $this->getFilterState($field_id, $filters_data_saved_state);

        if (!empty($filters_data_saved_state[$field_id]['values'][$field_value])) {
            $filters_data[$field_id]['values'][$field_value] = $filters_data_saved_state[$field_id]['values'][$field_value];
        }

        return $filters_data;
    }

    /**
     * Возвращает сохраненное состояние фильтров (скрыты/раскрыты) по умолчанию - скрыты.
     * @param int $field_id
     * @param array $filters_data_saved_state
     * @return bool
     */
    protected function getFilterState($field_id, $filters_data_saved_state): bool
    {
        return isset($filters_data_saved_state[$field_id]['show']) ? (bool)$filters_data_saved_state[$field_id]['show'] : false;
    }

    /**
     * Возвращает поля для бокового фильтра (FieldConfig::IS_KEY || getHiddenFields())
     *
     * @return array
     */
    protected function getFieldsForFiltersData(): array
    {
        return FieldConfig::find()
            ->select('title')
            ->where([
                'company_id' => $this->company_id,
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'is_key' => FieldConfig::IS_KEY
            ])
            ->orWhere([
                'company_id' => $this->company_id,
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'code' => self::getHiddenFields()
            ])
            ->indexBy('id')
            ->column();
    }

    /**
     * Заменяет в массиве фильтров, пришедших из поисковой формы
     * ключи field_title на filed_id.
     *
     * @param array $filters
     * @return array
     */
    private function replaceFilterKeysWithFiledIds(array $filters): array
    {
        $fields_data = FieldConfig::find()
            ->select(['id', 'code'])
            ->indexBy('code')
            ->where(['company_id' => $this->company_id, 'code' => $this->filter_codes, 'is_deleted' => FieldConfig::IS_NOT_DELETED])
            ->column();

        $filed_id_key_filters = [];
        foreach ($filters as $field_title => $filter) {
            $code = $this->filter_codes[$field_title] ?? null;
            if (!$code) {
                continue;
            }
            $filed_id_key_filters[$fields_data[$code]] = $filter;
        }

        return $filed_id_key_filters;
    }


    /**
     * Получаем строки участвующие в рассчете для конкретной клиенсткой компании
     *
     * @return array
     */
    protected function getCartData(): array
    {
        if (!$this->client_company_id) {
            return [];
        }
        if (!$this->cart_sql_data) {
            $cart_version_model = CartVersion::getCurrentVersionModel($this->company_id, $this->client_company_id, $this->user_id);
            if (!$cart_version_model) {
                return [];
            }
            /** @var CartVersion $cart_version_model */
            CartTablesData::$date_version = $cart_version_model->version_date;
            $this->cart_sql_data = CartTablesData::find()
                ->distinct(true)
                ->select(['hash_row', 'is_weight_over_24t', 'is_security', 'is_danger_cargo', 'is_special_tariff', 'is_home_consignment', 'custom_type', 'date_actual'])
                ->where([
                    'company_id' =>$this->company_id,
                    'client_company_id' => $this->client_company_id,
                    'is_deleted' => CartTablesData::IS_NOT_DELETED
                ])
                ->indexBy('hash_row')
                ->asArray()
                ->all();
        }

        return $this->cart_sql_data;
    }

    protected function getBoldFields() {
        return [
            FieldConfig::SUBTOTAL_FRAHT_IN,
            FieldConfig::SUBTOTAL_RW_IN,
            FieldConfig::SUBTOTAL_AUTO_IN,
            FieldConfig::RESULT_SEARCH
        ];
    }

    /**
     * Получаем номера столбцов которые нужно выделить bold
     *
     * @return array
     */
    public function getBoldFieldIds(): array
    {
        $fields_code = $this->getBoldFields();
        $bold_field_ids = [];
        foreach ($this->order_fields as $key => $field) {
            if (in_array($field['code'],$fields_code)) {
                $bold_field_ids[] = $key;
            }
        }

        return $bold_field_ids;
    }

    /**
     * @return int
     */
    private function getLoadingLocationType(): int
    {
        return $this->post_filters["loading_location_object"]['type']['type'] ?? 0;
    }

    /**
     * @return int
     */
    private function getDischargeLocationType(): int
    {
        return $this->post_filters["discharge_location_object"]['type']['type'] ?? 0;
    }

    /**
     * Возвращает столбцы, которые нужно показывать.
     * @return array
     */
    public function getColsChecked() :array
    {
        $checked_cols_orders_from_settings = UserSettings::get(UserSettings::SEARCH_CHECKED_COLS, $this->user_id);
        $checked_cols_orders = $checked_cols_orders_from_settings ?? array_keys($this->order_fields); // если в настройках пусто, вернем все поля, т.к. настройки может не быть.
        sort($checked_cols_orders);

        return $checked_cols_orders;
    }

    /**
     * Возвращает состояние меню фильтров поиска (свернут/раскрыт и стоит ли галочка)
     * @return array
     */
    protected function getFiltersState(): array
    {
        return (array)UserSettings::get(UserSettings::SEARCH_MENU_FILTERS_STATE, $this->user_id);
    }
}