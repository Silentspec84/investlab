<?php
namespace app\classes\helpers\RowProcessor;

use app\classes\db\mysql\ActiveRecordVersion;
use app\models\Bid;
use app\models\BidTablesData;
use app\models\FieldConfig;
use yii\helpers\ArrayHelper;

/**
 * Класс содержит методы позволяющие собирать строки из данных
 * bid_tables_data
 *
 * Class OfferRowProcessor
 * @package app\classes\helpers
 */
class BidRowProcessor extends ResultTableRowProcessor
{

    /** @var int */
    protected $bid_id;

    const ORDER_FIELD_NAME = 'order_in_client_bid';

    public function __construct($company_id, $user_id, $bid_id = null)
    {
        $this->bid_id = $bid_id;
        $this->setDateVersion();
        parent::__construct($company_id, $user_id);
    }

    protected function setDateVersion()
    {
        if (!$this->bid_id) {
            return;
        }

        $bid = Bid::find()
            ->select('offers.date_sent_to_client')
            ->where(['bids.id' => $this->bid_id])
            ->innerJoin('offers', 'offers.id = bids.offer_id')
            ->asArray()
            ->one();
        $date_version = $bid['date_sent_to_client'] ?? null;
        ActiveRecordVersion::$date_version = (int)$date_version;
    }

    public function getSqlData(array $file_ids = null): array
    {
        $tables_data = BidTablesData::find()
            ->where([
                'bid_id' => $this->bid_id,
            ])
            ->orderBy([
                'field_id' => 'DESC'
            ])->asArray()->all();

        return $tables_data;
    }

    public function getResultRows(array $sql_data): array
    {
        $rows = $this->getRows($sql_data);
        return $this->getOrderedRows($rows);
    }

    /**
     * Возвращает массив кодов, используемых для построения таблицы цен заявки
     * @return array
     */
    public static function getCodesForBidCostTable(): array
    {
        return [
            FieldConfig::SUBTOTAL_FRAHT_OUT,
            FieldConfig::SUBTOTAL_RW_OUT,
            FieldConfig::SUBTOTAL_AUTO_OUT,
            FieldConfig::SECURITY,
        ];
    }

    /**
     * Возвращает массив названий для кодов, используемых для построения таблицы цен заявки
     * @return array
     */
    public static function getCodeTitlesForBidCostTable(): array
    {
        return [
            FieldConfig::SUBTOTAL_FRAHT_OUT => 'Фрахт',
            FieldConfig::SUBTOTAL_RW_OUT => 'ЖД',
            FieldConfig::SUBTOTAL_AUTO_OUT => 'Авто',
            FieldConfig::SECURITY => 'Охрана',
        ];
    }
}