<?php

namespace app\classes\helpers\RowProcessor;

use app\models\Currency;
use app\models\FieldConfig;
use app\models\File;


/**
 * Класс служит для преобразования данных, хранящихся в "горизонтальном" виде field_id - field_val
 * к масиивам, для построения таблиц классического вида, состоящих из строк и столбцов.
 *
 * Class AbstractRowProcessor
 * @package app\classes\helpers
 */
abstract class AbstractRowProcessor {
    /** @var int */
    protected $company_id;

    /** @var int */
    protected $user_id;

    /**
     * Массив вида: file_id => table_type, нужен чтобы проставить строкам тип таблицы, для склейки строк
     * @var array
     */
    protected $file_ids_to_table_types = [];


    public function __construct($company_id, $user_id)
    {
        $this->company_id = $company_id;
        $this->user_id = $user_id;
        $this->file_ids_to_table_types = $this->getTableTypeKeyFileId();
    }

    /**
     * @param  array|null $file_ids
     * @return array
     */
    abstract public function getSqlData(array $file_ids = null): array;

    /**
     * Вовзращает массив результирующих строк
     *
     * @param  array $sql_data
     * @return array
     */
    abstract public function getResultRows(array $sql_data): array ;

    /**
     * Возвращает массив заголовков для результирующих строк
     *
     * @param  array|null $field_ids
     * @return array
     */
    abstract public function getResultRowsHeaders(array $field_ids = null): array;


    /**
     * Компонуем данные из загруженных таблиц по строкам и возвращаем их
     *
     * @param  array $sql_data
     * @return array
     */
    protected function getRows(array $sql_data): array
    {
        $rows = [];
        $row = [];
        $first_element_row = reset($sql_data);
        $last_element_row = end($sql_data);
        foreach ($sql_data as $key => $field) {
            if ($this->isEndOfRow($first_element_row, $field)) {
                $rows[] = $row;
                $row = [];
                $first_element_row = $field;
            }

            $row[$field['field_id']] = $this->getFieldValFromField($field);
            $row['type_table'] = $this->file_ids_to_table_types[$field['file_id']]['table_type'];
            $row['table_id'] = $this->file_ids_to_table_types[$field['file_id']]['table_id'];
            $field_json = $this->getFieldJsonFromField($field);
            if ($field_json) {
                $row['fields_json'][$field['field_id']] = $field_json;
            }

            if ($last_element_row['id'] === $field['id']) {
                $rows[] = $row;
            }
        }

        return $rows;
    }

    /**
     * Возвращает значение поля для json объекта, например raw_value - исходное значение
     * или id локации
     *
     * @param array $field - строка из таблицы table_data или raw_table_data из них строются rows
     * @return mixed
     */
    protected function getFieldValFromField(array $field)
    {
        return json_decode($field['field_json'], true)['raw_value'] ?? $field['field_val'];
    }

    /**
     * Возвращает json объект если поле явялется json иначе null
     *
     * @param array $field - строка из таблицы table_data или raw_table_data из них строются rows
     * @return mixed
     */
    private function getFieldJsonFromField(array $field)
    {
        if (isset(json_decode($field['field_json'], true)['raw_value'])) {
            return $field['field_json'];
        }
        if (isset(json_decode($field['field_val'], true)['raw_value'])) {
            return $field['field_val'];
        }

        return null;
    }

    /**
     * Определяет конец "горизонтальной строки"
     * @param array $sql_first_row
     * @param array $sql_current_row
     * @return bool
     */
    protected function isEndOfRow(array $sql_first_row, array $sql_current_row): bool
    {
        return
                $sql_first_row['id'] !== $sql_current_row['id']
                && (
                (
                    !empty($sql_first_row['field_id'])
                    && !empty($sql_current_row['field_id'])
                    && (int)$sql_first_row['field_id'] === (int)$sql_current_row['field_id'])
                ||
                (
                !empty($sql_first_row['file_id'])
                    && !empty($sql_current_row['file_id'])
                    && (int)$sql_first_row['file_id'] !== (int)$sql_current_row['file_id'])
                );
    }

    /**
     * Возвращает массив заголовоков для полей из $fields_ids
     *
     * @param array|null $fields_ids
     * @return array
     */
    protected function getHeaderFieldsForIds(array $fields_ids = null): array
    {
        $condition = $fields_ids ? ['is_shown' => FieldConfig::IS_SHOWN, 'is_deleted' => FieldConfig::IS_NOT_DELETED, 'company_id' => $this->company_id, 'id' => $fields_ids] : ['is_shown' => FieldConfig::IS_SHOWN, 'is_deleted' => FieldConfig::IS_NOT_DELETED, 'company_id' => $this->company_id];

        $fields = FieldConfig::find()
            ->select(['id', 'code', 'title', 'type', 'type_of_delivery'])
            ->where($condition)
            ->indexBy('id')
            ->asArray()->all();

        return $fields;
    }

    /**
     * возвращает массив вида [file_id => table_type]
     *
     */
    private function getTableTypeKeyFileId(): array
    {
        $files = File::getActualFilesByCompanyId($this->company_id);

        $files_ids_table_types = [];
        foreach ($files as $file) {
            $files_ids_table_types[$file['id']]['table_type'] = $file['type'];
            $files_ids_table_types[$file['id']]['table_id'] = $file['table_id'];
        }

        return $files_ids_table_types;
    }

    /**
     * Проверяет является ли поле в строке техническим,
     * к техническим полям относятся все поля, что не являюся id field_config
     * или порядковым номером столбца (order_in_cart, order_in_manager_search и т.д.)
     *
     * Примеры технических полей table_id, fields_json
     *
     * @param string $title_field
     * @return bool
     */
    public function isTechnicalField(string $title_field): bool
    {
        return !is_numeric($title_field);
    }

    /**
     * Метод вычисляет итоговую цену в рублях, конвертируя Подитоги по нужному курсу
     * Это нужно для сравнения итоговой суммы строки в виде одного числа независимо от валют
     * @param $row
     * @param $company_id
     * @param $fields
     * @return float|int
     */
    public static function getTotalPrice($row, $company_id, $fields)
    {
        $currencies = Currency::getCurrencies($company_id);
        $subtotal_fraht = $row[$fields[FieldConfig::SUBTOTAL_FRAHT_OUT]['id']];
        $subtotal_railway = $row[$fields[FieldConfig::SUBTOTAL_RW_OUT]['id']];
        $subtotal_auto = $row[$fields[FieldConfig::SUBTOTAL_AUTO_OUT]['id']];
        if (in_array(FieldConfig::FIELD_VAL_NOT_EXIST_VALUE, [$subtotal_fraht, $subtotal_railway, $subtotal_auto])) {
            return 0;
        }
        return $subtotal_fraht * $currencies[$fields[FieldConfig::SUBTOTAL_FRAHT_OUT]['currency_id']]['rate']
            + $subtotal_railway * $currencies[$fields[FieldConfig::SUBTOTAL_RW_OUT]['currency_id']]['rate']
            + $subtotal_auto * $currencies[$fields[FieldConfig::SUBTOTAL_AUTO_OUT]['currency_id']]['rate'];
    }
}