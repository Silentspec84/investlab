<?php

namespace app\classes\helpers\RowProcessor;

use app\classes\helpers\FiltersHelper;
use app\models\KpTablesData;
use app\models\store\Offer;

/**
 * Класс содержит методы позволяющие собирать строки из данных
 * cart_tables_data для клиента
 *
 * Class ClientOfferRowProcessor
 * @package app\classes\helpers
 */
class ClientOfferRowProcessor extends ManagerOfferRowProcessor
{
    /** Название аттрибута поля fields отвечающего за order
     */
    const ORDER_FIELD_NAME = 'order_in_client_kp';

    /**
     * @var int id компании клиента
     */
    private $client_company_id;

    /**
     * Пока это чекбоксы активный/архивный, но в будущем возможно их будет больше
     * @var array
     */
    private $filters;

    public function __construct($company_id, $user_id, $client_company_id, $offer_id = null, array $filters = null)
    {
        parent::__construct($company_id, $user_id, $offer_id);
        $this->client_company_id = $client_company_id;
        $this->filters = $filters ?? Offer::getFiltersForList();
    }

    /**
     * Метод возвращает все поля из таблицы КП по массиву заданных offer_ids
     *
     * @return array
     */
    public function getSqlDataForClientSearch(): array
    {
        $offer_ids = $this->getClientOfferIds();
        if (!$offer_ids) {
            return [];
        }

        return KpTablesData::find()
            ->where([
                'offer_id' => $offer_ids,
                'client_company_id' => $this->client_company_id
            ])->orderBy([
                'offer_id' => SORT_ASC,
                'hash_row_key_field' => SORT_ASC,
                'field_id' => SORT_ASC,
            ])->asArray()->all();
    }

    /**
     * Метод возвращает id офферов, выставленных на компанию клиента с учетом фильтра - без архивных или все
     *
     * @return array
     */
    private function getClientOfferIds(): array
    {
        $filter_values = FiltersHelper::getFilterValues($this->filters);
        if (!$filter_values) {
            return [];
        }
        $condition = [
            'client_company_id' => $this->client_company_id,
            'is_deleted' => Offer::NOT_DELETED,
            'is_archived' => $filter_values
        ];

        $models = Offer::find()
            ->where($condition)
            ->andWhere(['not', ['date_sent_to_client' => null]])
            ->all();

        $result = [];
        foreach($models as $model) {
            $result[] = $model->id;
        }

        return $result;
    }

    public function getResultRowsHeaders(array $field_ids = null): array
    {
        $ordered_title_fields = parent::getResultRowsHeaders($field_ids);
        $offer = Offer::findOne(['id' => $this->offer_id]);
        if ($offer && $offer->is_archived === Offer::ARCHIVED) {
            return $ordered_title_fields;
        }
        $ordered_title_fields[] = ['id'=> 0, 'title' => 'Подать заявку'];

        return $ordered_title_fields;
    }
}