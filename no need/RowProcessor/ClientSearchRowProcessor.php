<?php

namespace app\classes\helpers\RowProcessor;

use app\classes\helpers\StringFormatter;
use app\controllers\ClientSearchFilterApiController;
use app\controllers\PublicApiController;
use app\models\Currency;
use app\models\FieldConfig;
use app\models\KpTablesData;
use app\models\SearchFiltersKeys;
use app\models\store\OrderRate;
use app\models\store\OrderRateTypes;

/**
 * Класс содержит методы для работы с таблицей на странице поиска клиента
 *
 * Class ClientSearchRowProcessor
 * @package app\classes\helpers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class ClientSearchRowProcessor extends ManagerSearchRowProcessor {

    protected const ERROR_NO_MANAGER_SEARCH_FILTER = 'Возникла ошибка получения поисковой выдачи! Обратитесь к менеджеру!';
    public const ERROR_NO_CLIENT_SEARCH_FILTER = 'Возникла ошибка получения поисковой выдачи! Обратитесь к менеджеру!';

    const ORDER_FIELD_NAME = 'order_in_client_search';

    const YES_TEXT = 'Да';
    const NO_TEXT = 'Нет';

    protected $field_codes = [
        FieldConfig::SUBTOTAL_RW_IN,
        FieldConfig::VTT,
        FieldConfig::SECURITY,
        FieldConfig::HOME_BILL_CODE,
        FieldConfig::WEIGHT,
        FieldConfig::DANGER,
        FieldConfig::PLACE_TO,
        FieldConfig::RESULT_OUT,
        FieldConfig::SUBTOTAL_FRAHT_IN,
        FieldConfig::SUBTOTAL_FRAHT_OUT,
        FieldConfig::SUBTOTAL_RW_IN,
        FieldConfig::SUBTOTAL_RW_OUT,
        FieldConfig::SUBTOTAL_AUTO_IN,
        FieldConfig::SUBTOTAL_AUTO_OUT,
    ];

    /**
     * Массив fields_config_codes для доп полей из формы клиенсткого поиска(охрана, дом. кон-т, опасный груз)
     *
     * @var array
     */
    private $additional_field_codes = [
        FieldConfig::SECURITY,
        FieldConfig::HOME_BILL_CODE,
        FieldConfig::DANGER
    ];

    /**
     * Массива где ключ field_code ПИ без маржи, а значение field_code ПИ с маржой
     *
     * @var array
     */
    private $subtotals_in_to_subtotals_out = [
        FieldConfig::SUBTOTAL_FRAHT_IN => FieldConfig::SUBTOTAL_FRAHT_OUT,
        FieldConfig::SUBTOTAL_RW_IN => FieldConfig::SUBTOTAL_RW_OUT,
        FieldConfig::SUBTOTAL_AUTO_IN => FieldConfig::SUBTOTAL_AUTO_OUT
    ];

    /**
     * Массив настроенных фильтров менеджером для клиентского поиска
     * @var array
     */
    protected $filters_client_search = [];

    public function __construct($company_id, $user_id, $filters = [], $additional_params = [], $actual_date = null)
    {
        parent::__construct($company_id, $user_id, $filters, $additional_params, $actual_date);
        $this->filters_client_search = $this->getClientSearchFilter($filters);
    }

    /**
     * Если в конструктор передан клиентский фильтр, то возвращаем его,
     * если нет то получаем все менеджерские фильтры
     *
     * @param array $filters
     * @return array
     */
    protected function getClientSearchFilter(array $filters): array
    {
        if (isset($filters[ClientSearchFilterApiController::TITLE_FILTER])) {
            return $filters[ClientSearchFilterApiController::TITLE_FILTER];
        }

        return $this->getSearchFilter(SearchFiltersKeys::SOURCE_TYPE_CLIENT_SEARCH);
    }

    public function getResultRows(array $sql_data): array
    {
        if (!$this->filters_client_search || !$this->filters_client_search['data']) {
            $this->errors[] = self::ERROR_NO_CLIENT_SEARCH_FILTER;

            return [];
        }
        return parent::getResultRows($sql_data);
    }
    /**
     * Возвращает true, если строка удовлетворяет всем типам фильтров. Иначе - false.
     * @param $row
     * @return bool
     */
    protected function isGoodRow($row): bool
    {
        return parent::isGoodRow($row) && $this->isGoodRowByClientSearchFilter($row);
    }

    /**
     * Проверяет что строка соответствует "Настройкам клиентского поиска"
     * Настраивается менеджерами
     *
     * @param array $row
     * @return bool
     */
    protected function isGoodRowByClientSearchFilter(array $row): bool
    {
        return $this->isGoodRowBySearchFilter($row, $this->filters_client_search);
    }

    /**
     * Получает строки участвующие в рассчете для конкретной клиенсткой компании
     * @return array
     */
    protected function getCartData(): array
    {
        return [];
    }

    /**
     * Получает столбцы для выделения жирным в таблице
     * @return array
     */
    protected function getBoldFields(): array
    {
        return [];
    }

    /**
     * Заполняет массив параметров строк из поиска, которые уже есть в корзине
     * @param array $ordered_row
     * @param array $order_hash_row
     * @return array
     */
    protected function fillRowParamsFromCartData($ordered_row, $order_hash_row)
    {
        return $ordered_row;
    }

    /**
     * Преобразовывает массив результирующих строк менеджерского поиска в клиентский вариант,
     * изменяя значения полей и заменяя цены в строках, на которые у клиента уже есть кп
     * @param array $result_rows
     * @return array $result_rows
     */
    protected function transformManagerSearchRowsToClientSearchRows($result_rows): array
    {
        $currencies = Currency::getCurrencies($this->company_id);
        $rates = $this->getRates();

        $custom_type_title = $this->getCustomTypeTitleFromPostParams();

        if (!empty($rates['error'])) {
            $result_rows['error'] = $rates['error'];
            return $result_rows;
        }
        $intersect_key_field_ids_for_search_and_offers = KpTablesData::getIntersectKeyFieldIdsForSearchAndOffers($this->company_id);
        foreach ($result_rows as $row_key => $row) {
            $result_rows = $this->replaceWeightField($result_rows, $row_key);
            $result_rows = $this->replaceCustomTypeTitle($result_rows, $row_key, $custom_type_title);
            $result_rows = $this->replaceAdditionalFields($result_rows, $row_key);
            $result_rows = $this->replaceSubtotalFields($result_rows, $row_key, $rates);
            $result_rows = $this->addHashToSearchRow($result_rows, $row_key, $intersect_key_field_ids_for_search_and_offers);
            $result_rows = $this->replaceCalculatedResultOut($result_rows, $row_key, $currencies);
        }

        return $result_rows;
    }

    /**
     * Возвращает массив строк клиентского поиска с добавленным хэшом (используется для сравнения строк из кп и поиска)
     * @param array $result_rows
     * @param int $row_key
     * @param array $intersect_key_field_ids_for_search_and_offers
     * @return array
     */
    private function addHashToSearchRow(array $result_rows, int $row_key, array $intersect_key_field_ids_for_search_and_offers): array
    {
        $result_rows[$row_key]['hash_row_key_field'] = KpTablesData::getKeyFieldsRowHash($result_rows[$row_key], $intersect_key_field_ids_for_search_and_offers);
        return $result_rows;
    }

    /**
     * Возвращает массив строк клиентского поиска с измененным значением поля вес, вот так:
     * 1000 заменится на < 24т, а
     * 1000000 заменится на > 24т
     *
     * @param array $result_rows
     * @param int $row_key
     * @return array
     */
    private function replaceWeightField(array $result_rows, int $row_key): array
    {
        $weight_field_id = $this->fields_by_codes[FieldConfig::WEIGHT]->id;
        $result_rows[$row_key][$weight_field_id] = $this->getCorrectFieldValWeight($result_rows[$row_key][$weight_field_id]);

        return $result_rows;
    }

    /**
     * Возвращает массив строк клиентского поиска с измененными значениями полей подитоговых цен путем добавления базового тарифа.
     * @param array $result_rows
     * @param int $row_key
     * @param array $rates
     * @return array
     */
    private function replaceSubtotalFields(array $result_rows, int $row_key, array $rates): array
    {
        foreach ($this->subtotals_in_to_subtotals_out as $subtotal_in_field_code => $subtotal_out_field_code) {
            $field_id = $this->fields_by_codes[$subtotal_out_field_code]->id;
            $result_rows[$row_key][$field_id] = $this->getCalculatedSubtotalWithRate($subtotal_in_field_code, $result_rows[$row_key], $rates);
        }
        return $result_rows;
    }

    /**
     * Возвращает массив строк клиентского поиска с измененным значением поля Место ТО, вот так:
     * 0 меняется на 'Порт', 1 - на 'Станция'
     * @param array $result_rows
     * @param int $row_key
     * @return array
     */
    private function replaceCustomTypeTitle(array $result_rows, int $row_key, string $custom_type_title): array
    {
        $place_to_field_id = $this->fields_by_codes[FieldConfig::PLACE_TO]->id;
        $result_rows[$row_key][$place_to_field_id] = $custom_type_title;
        return $result_rows;
    }

    /**
     * Возвращает строку - название типа таможни, которую выбрал пользователь при поиске
     * @return string
     */
    private function getCustomTypeTitleFromPostParams(): string
    {
        $custom_type_title_flag = FieldConfig::getCodeOfSearchFormFields()[FieldConfig::PLACE_TO];
        $custom_type_id = $this->post_add_params[$custom_type_title_flag] ?? null;
        $custom_types = PublicApiController::$custom_types;
        foreach ($custom_types as $custom_type) {
            if ($custom_type['id'] == $custom_type_id) {
                return $custom_type['title'];
            }
        }
        return '';
    }

    /**
     * Возвращает массив строк клиентского поиска с измененными значениями полей подитога фрахат,жд,авто
     * из которых вычитаются цены всех модификаторов формы клиентского поиска:
     * охрана, опасный груз, домашний коносамент(additional_fields)
     * Таким образом цены после этого метода приводятся к "чистому" виду.
     * @param array $result_rows
     * @param int $row_key
     * @return array
     */
    private function replaceAdditionalFields(array $result_rows, int $row_key): array
    {
        foreach ($this->additional_field_codes as $field_code) {
            $field_id = $this->fields_by_codes[$field_code]->id;
            if (!$result_rows[$row_key][$field_id] || $result_rows[$row_key][$field_id] === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                $result_rows[$row_key][$field_id] = 0;
                continue;
            }
            $result_rows = $this->getCalculatedResultRowsWithoutAdditionalFieldValue($result_rows, $row_key, $field_code);
        }
        return $result_rows;
    }

    /**
     * Получаем значение поля вес вида '<24т.' или '>=24т.';
     *
     * @param string|null $field_val_weight
     * @return string
     */
    private function getCorrectFieldValWeight(string $field_val_weight = null): string
    {
        if (!$field_val_weight) {
            return $this->post_add_params['weight'] < FieldConfig::WEIGHT_TARIFF_CHANGE_BOUND ? '<24т.' : '>=24т.';
        }
        if ($field_val_weight === FieldConfig::FIELD_VAL_WEIGHT_MORE) {
            return FieldConfig::FIELD_VAL_WEIGHT_MORE_VIEW;
        }

        return FieldConfig::FIELD_VAL_WEIGHT_LESS_VIEW;
    }

    /**
     * Возвращает результирующие строки клиентского поиска из которых вычитается охрана, опасный груз и домашний коносамент(additional_fields)
     *
     * @param array $result_rows
     * @param int $row_key
     * @param string $field_code
     * @return array
     */
    private function getCalculatedResultRowsWithoutAdditionalFieldValue(array $result_rows, int $row_key, string $field_code):array
    {
        foreach ($this->subtotals_in_to_subtotals_out as $subtotal_in_field_code => $subtotal_out_field_code) {
            $field_id = $this->fields_by_codes[$subtotal_in_field_code]->id;
            $result_rows[$row_key][$field_id] = $this->getSubtotalInWithoutAdditionalFieldValue($field_code, $subtotal_in_field_code, $result_rows[$row_key]);
        }

        return $result_rows;
    }

    /**
     * Возвращает значение подитога фрахат,жд,авто
     * из которых вычитается охрана, опасный груз, домашний коносамент(additional_fields)
     *
     * @param string $additional_field_code
     * @param string $subtotal_in_field_code
     * @param array $row
     * @return float|string
     */
    private function getSubtotalInWithoutAdditionalFieldValue(string $additional_field_code, string  $subtotal_in_field_code, array $row)
    {
        $subtotal_in_field = $this->fields_by_codes[$subtotal_in_field_code];
        $additional_field = $this->fields_by_codes[$additional_field_code];
        if ($row[$subtotal_in_field->id] === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
            return $row[$subtotal_in_field->id];
        }
        if ($additional_field->type_of_delivery === $subtotal_in_field->type_of_delivery) {
            return (float)$row[$subtotal_in_field->id] - (float)$row[$additional_field->id];
        }

        return (float)$row[$subtotal_in_field->id];
    }

    /**
     * Возвращает подитог для фрахт, авто, жд с накинутой маржой
     *
     * @param string $subtotal_in_field_code
     * @param array $row
     * @param array|null $rates
     * @return float|string
     */
    private function getCalculatedSubtotalWithRate(string $subtotal_in_field_code, array $row, ?array $rates)
    {
        $cargo_type_field_id = FieldConfig::getFieldIdByCode(FieldConfig::TYPE_CONTAINER, $this->company_id);
        $cargo_type = $row[$cargo_type_field_id];

        $field_id = $this->fields_by_codes[$subtotal_in_field_code]->id;
        if ($row[$field_id] === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
            return $row[$field_id];
        }
        $field_type = $this->fields_by_codes[$subtotal_in_field_code]->type_of_delivery;
        if (!$rates) {
            return (float)$row[$field_id];
        }

        foreach ($rates as $rate) {
            if($rate['type'] !== $field_type || $rate['cargo_type'] !== $cargo_type) {
                continue;
            }

            return (float)$row[$field_id] + (float)$rate['value'];
        }

        return (float)$row[$field_id];
    }

    /**
     * Возвращает массив строк клиентского поиска заменяя ИТОГО
     * на вычисленную итоговую сумму (Фрахт + ЖД + Авто) в валюте,
     * указанной в настройках "виды доставок"
     *
     * @param array $result_rows
     * @param int $row_key
     * @param array $currencies
     * @return array
     */
    private function replaceCalculatedResultOut(array $result_rows, int $row_key, array $currencies): array
    {
        $subtotal_outs = [];
        foreach ($this->subtotals_in_to_subtotals_out as $subtotal_in_field_code => $subtotal_out_field_code) {
            $subtotal_out_currency_id = $this->fields_by_codes[$subtotal_out_field_code]->currency_id;
            $subtotal_out_id = $this->fields_by_codes[$subtotal_out_field_code]->id;
            foreach ($currencies as $currency) {
                if ($currency['id'] !== $subtotal_out_currency_id) {
                    continue;
                }
                if ($result_rows[$row_key][$subtotal_out_id] === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                    continue;
                }
                $currency_sign = Currency::getCurrencySignByIsoCode($currency['iso_code']);
                $subtotal_out_value = (float)$result_rows[$row_key][$subtotal_out_id];
                if (!isset($subtotal_outs[$currency_sign])) {
                    $subtotal_outs[$currency_sign] = 0;
                }
                $subtotal_outs[$currency_sign] += $subtotal_out_value;
            }
        }
        if (!$subtotal_outs) {
            return $result_rows;
        }

        $field_id = $this->fields_by_codes[FieldConfig::RESULT_OUT]->id;
        $result_rows[$row_key][$field_id] = StringFormatter::renderTotalFromSubtotals($subtotal_outs) ?: FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;

        return $result_rows;
    }

    /**
     * Возвращает маржу для фрахта, жд и авто
     *
     * @return array
     */
    private function getRates(): array
    {
        try {
            $order_rate_id = OrderRate::getBaseId($this->company_id);
        } catch (\Exception $e) {
            Yii:error('Ошибка получения объекта компании с id=' . $this->client_company_id);
            return ['error' => 'Ошибка получения маржи'];
        }

        if (!$order_rate_id) {
            return ['error' => 'Ваш менеджер не верно заполнил данные о вас, обратитесь к нему'];
        }

        return OrderRateTypes::getRatesByOrderRateId($order_rate_id);
    }

    public function getResultRowsHeaders(array $field_ids = null): array
    {
        $ordered_title_fields = parent::getResultRowsHeaders($field_ids);
        $ordered_title_fields[] = ['id'=> 0, 'title' => 'Подать заявку'];

        return $ordered_title_fields;
    }

    public function getSpecialTitle(): array
    {
      return [
            FieldConfig::SUBTOTAL_FRAHT_OUT => 'Фрахт',
            FieldConfig::SUBTOTAL_RW_OUT => 'Жд',
            FieldConfig::SECURITY => 'Охрана',
            FieldConfig::SUBTOTAL_AUTO_OUT => 'Авто',
            FieldConfig::RESULT_OUT => 'ИТОГО'
        ];
    }

    public function getBorderedBlocks(): array
    {
        return [];
    }

    /**
     * Возвращает массив фильтров вида:
     * [
     *      0 => [
     *          'title' => (string)field_title1,
     *          'values' => [
     *              (string)field_value1 => [
     *                  'value' => (string)field_value1,
     *                  'checked' => (bool)true
     *              ],
     *                  ...
     *              (string)field_valueN => [
     *                  'value' => (string)field_valueN,
     *                  'checked' => (bool)false
     *              ]
     *          ]
     *      ],
     *          ...
     *      N => [
     *          'title' => (string)field_titleN,
     *          'values' => [
     *              (string)field_value1 => [
     *                  'value' => (string)field_value1,
     *                  'checked' => (bool)false
     *              ],
     *                  ...
     *              (string)field_valueN => [
     *                  'value' => (string)field_valueN,
     *                  'checked' => (bool)true
     *              ]
     *          ]
     *      ]
     * ]
     * @param array $rows_result_table
     * @return array
     */
    public function getFiltersData(array $rows_result_table): array
    {
        $filters_data = [];
        $fields_for_filter_data = $this->getFieldsForFiltersData();
        if (!$fields_for_filter_data) {
            return $filters_data;
        }
        $original_rows = $this->getOriginalRows($rows_result_table);
        if (!$original_rows) {
            return $filters_data;
        }

        $filters_data_saved_state = $this->getFiltersState();
        foreach ($original_rows as $row) {
            foreach ($row as $key_field => $field) {
                if (!is_numeric($key_field)) {
                    continue;
                }
                $filters_data = $this->addFilter($filters_data, $fields_for_filter_data, $field, $key_field, $filters_data_saved_state);
            }
        }
        if (!$filters_data) {
            return $filters_data;
        }

        return $this->unsetFiltersIfFewValues($filters_data);
    }

    /**
     * Возвращает сохраненное состояние фильтров (скрыты/раскрыты) по умолчанию - раскрыты.
     * @param int $field_id
     * @param array $filters_data_saved_state
     * @return bool
     */
    protected function getFilterState($field_id, $filters_data_saved_state): bool
    {
        return isset($filters_data_saved_state[$field_id]['show']) ? (bool)$filters_data_saved_state[$field_id]['show'] : true;
    }


    /**
     * Возвращает поля для бокового фильтра (FieldConfig::IS_KEY || static::ORDER_FIELD_NAME IS NOT NULL)
     *
     * @return array
     */
    protected function getFieldsForFiltersData(): array
    {
        return FieldConfig::find()
            ->select('title')
            ->where([
                'company_id' => $this->company_id,
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'is_key' => FieldConfig::IS_KEY,
            ])
            ->andWhere('fields_config.' . static::ORDER_FIELD_NAME. ' IS NOT NULL')
            ->indexBy('id')
            ->column();
    }
}