<?php

namespace app\classes\helpers\RowProcessor;
use app\classes\exceptions\FileCacheProcessException;
use app\classes\helpers\DoubleRowsChecker;
use app\classes\helpers\FormulaHelper;
use app\classes\helpers\Rpn;
use app\classes\helpers\StringFormatter;
use app\commands\FileCacheController;
use app\models\Currency;
use app\models\data\Table;
use app\models\FieldConfig;
use app\models\File;
use app\models\FormulaList;
use app\models\LocationAlias;
use app\models\P2PDeliveryTime;
use app\models\PointTime;
use app\models\TableConfig;
use app\models\TablesData;
use app\models\TypesOfDeliveries;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * Класс содержит методы для работы с итоговой (результирующей) таблицей
 *
 * Class SimpleTableRowProcessor
 * @package app\classes\helpers
 */

class ResultTableRowProcessor extends AbstractRowProcessor {

    /** Название аттрибута поля fields отвечающего за order */
    const ORDER_FIELD_NAME = 'order_in_result_table';
    const URL_TO_EDIT_FIELD_CONFIG = '/table-variables-config/edit/?order_field=';

    /** @var array FieldConfigs */
    protected $all_fields_configs = [];

    /** @var array ids полей у которых FieldConfig::IS_KEY */
    protected $fields_transport_keys_ids = [];

    /** @var array ids полей у которых FieldConfig::IS_SIMPLE_FIELD */
    protected $fields_service_keys_ids = [];

    /** @var array ids полей у которых FieldConfig::IS_GEO_POINT */
    protected $fields_geo_points_ids = [];

    /** @var array выборка FormulaList где ключ result_field_id */
    private $formulas = [];

    /** @var array выборка FieldsConfig */
    private $fields_configs = [];

    public $errors = [];

    /** @var bool флаг говорит о запуске из консоли */
    protected $is_console = false;

    /**
     * Массив порядка столбцов вида: [order_number => ['id' => field_id, 'code' => field_code]]
     * @var array
     */
    protected $order_fields = [];

    /**
     * Массив уникальных значений по field_id для каждой таблицы
     * вида : [
     *          table_id1 => [
     *              field_id1 => [unique_value1, ..., unique_valueN],
     *                  ...
     *              field_idN => [unique_value1, ..., unique_valueN]
     *          ], ..., [
     *          table_idN => [
     *              field_id1 => [unique_value1, ..., unique_valueN],
     *                  ...
     *              field_idN => [unique_value1, ..., unique_valueN]
     *          ]
     *      ];
     *
     * @var array
     */
    protected $unique_values_tables = [];

    protected $formula_helper = null;
    /**
     * @var array Массив значений(количества дней) затраченных на каждом терминале(гео-точке)
     * Массив вида [location_id1 => time1, ..., location_idN => timeN]
     */
    protected $time_on_terminals = [];
    /**
     * @var array  Массив значений(количества дней) затраченных на перевозку между терминалами(фрахт, жд, авто)
     * Массив вида [from_location_id . '_' . to_location_id => time1, ..., from_location_id . '_' . to_location_id  => timeN]
     */
    protected $time_delivery_between_terminals = [];

    /**
     * @var DoubleRowsChecker
     */
    private $rows_checker;

    /**
     * ResultTableRowProcessor constructor.
     * @param $company_id
     * @param $user_id
     */
    public function __construct($company_id, $user_id)
    {
        parent::__construct($company_id, $user_id);
        $this->all_fields_configs = $this->getAllFieldsConfigs();
        $this->setFieldKeyIds();
        $this->order_fields = $this->getOrderFields();
        $this->formula_helper = $this->getFormulaHelper();
    }

    public function getFormulaHelper()
    {
        if (!$this->formula_helper) {
            $this->formula_helper = new FormulaHelper($this->company_id, $this->order_fields);
        }

        return $this->formula_helper;
    }

    public function getDoubleRowsChecker(): DoubleRowsChecker
    {
        if ($this->rows_checker === null) {
            $this->rows_checker = new DoubleRowsChecker($this->company_id);
        }
        return $this->rows_checker;
    }

    public function getSqlData(array $file_ids = null): array
    {
        if (!$file_ids) {
            $file_ids = $this->getCurrentFilesIds();
        }
        $condition = ['company_id' => $this->company_id];
        if ($file_ids) {
            $condition['file_id'] = $file_ids;
        }

        $sql_data = TablesData::find()
            ->select(['id','field_id','field_val','field_json', 'file_id'])
            ->where($condition)
            ->asArray()->all();

        return $sql_data;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     * @throws FileCacheProcessException
     */
    public function getResultRows(array $sql_data): array
    {
        if (!$sql_data) {
            return [];
        }

        $rows_result_table = $this->getRowsOriginal($sql_data);
        if (!$rows_result_table) {
            return [];
        }

        $rows_result_table = $this->getRowsWithLocationsTitlesInsteadIdsAndInWay($rows_result_table);
        return $this->getOrderedRows($rows_result_table);
    }

    public function getResultRowsHeaders(array $field_ids = null): array
    {
        if (!$field_ids) {
            $field_ids = ArrayHelper::getColumn($this->order_fields, 'id');
        }

        $special_titles = $this->getSpecialTitle();

        $title_fields = $this->getHeaderFieldsForIds($field_ids);
        $ordered_title_fields = [];
        foreach ($this->order_fields as $filed_order => $field_data) {
            if (!array_key_exists($field_data['id'], $title_fields)) {
                continue;
            }
            $ordered_title_fields[$filed_order] = $title_fields[$field_data['id']];
        }

        foreach ($ordered_title_fields as $filed_order => $field_data) {
            $ordered_title_fields[$filed_order] = $field_data;
            if(array_key_exists($field_data['code'], $special_titles)){
                $ordered_title_fields[$filed_order]['title'] =  $special_titles[$field_data['code']];
            }
        }

        $ordered_title_fields = $this->addCurrencyToTitles($ordered_title_fields);

        return $ordered_title_fields;
    }

    /**
     * Возвращает массив соотношений field_id к order_key:
     * [ field_id1 => $order_in_(search|kp|cart), ..., field_idN => $order_in_(search|kp|cart)]
     *
     * @return array
     */
    public function getFieldIdsToFieldOrders(): array
    {
        $field_ids_to_field_orders = [];
        foreach ($this->order_fields as $order_key => $field) {
            $field_ids_to_field_orders[(int)$field['id']] = $order_key;
        }

        return $field_ids_to_field_orders;
    }

    /**
     * Переопределяем метод из AbstractRowProcessor
     * Для того чтобы в полях локаций, оставался id, работать будем с ними.
     *
     * @param array $field
     * @return mixed
     */
    protected function getFieldValFromField(array $field)
    {
        return $field['field_val'];
    }

    /**
     * Метод возвращает строки в которых id локаций заменены на aliases title
     * И добавляет поле "В пути"(FieldConfig::IN_WAY)
     *
     * @param array $result_rows
     * @return array
     */
    protected function getRowsWithLocationsTitlesInsteadIdsAndInWay(array $result_rows): array
    {
        $in_way_field_id = FieldConfig::getFieldIdByCode(FieldConfig::IN_WAY, $this->company_id);
        $types_of_deliveries_field_ids = FieldConfig::getFieldIdsForTypesOfDeliveries($this->company_id);
        $canonical_location_aliases = $this->getCanonicalLocationAliases($this->getLanguageLocationAliases());
        foreach ($result_rows as $num_row => $row) {
            if ($in_way_field_id) {
                $this->addTimeInWay($row, $in_way_field_id, $types_of_deliveries_field_ids);
            }
            $this->replaceLocationIdsOnTitles($row, $canonical_location_aliases);
            $result_rows[$num_row] = $row;
        }

        return $result_rows;
    }
    
    /**
     * Рассчитываем и добавляем строке поле "В пути" и данные о незаполненном времени на терминалах и между терминалами
     *
     * @param array $row
     * @param int $in_way_field_id
     * @param array $types_of_deliveries_field_ids
     */
    private function addTimeInWay(array &$row, int $in_way_field_id, array $types_of_deliveries_field_ids): void
    {
        $row[$in_way_field_id] = 0;
        $this->addTimeOnTerminals($row, $in_way_field_id);
        $this->addTimeDeliveryBetweenTerminals($row, $in_way_field_id, $types_of_deliveries_field_ids);
    }

    /**
     * @param array $row
     * @param int $in_way_field_id
     */
    private function addTimeOnTerminals(array &$row, int $in_way_field_id): void
    {
        $this->time_on_terminals = $this->getTimeOnTerminals();
        if (!$this->time_on_terminals) {
            return;
        }

        foreach ($this->fields_geo_points_ids as $geo_point_field_id) {
            if (!isset($row[$geo_point_field_id])) {
                continue;
            }
            $location_id = $row[$geo_point_field_id];
            if (isset($this->time_on_terminals[$location_id]) && $row[$in_way_field_id] !== FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                $row[$in_way_field_id] += $this->time_on_terminals[$location_id];
            } else {
                $row['additional_params']['empty_time_on_terminals'][] = $geo_point_field_id;
                $row[$in_way_field_id] = FieldConfig::FIELD_VAL_NOT_EXIST_VALUE; // если какого-то времени не хватает, то вместо суммы выведем -, т.к сумма не корректна, если одно из слагаемых неизвестно
            }
        }
    }

    /**
     * @param array $row
     * @param int $in_way_field_id
     * @param array $types_of_deliveries_field_ids
     */
    private function addTimeDeliveryBetweenTerminals(array &$row, int $in_way_field_id, array $types_of_deliveries_field_ids): void
    {
        $this->time_delivery_between_terminals = $this->getTimeDeliveryBetweenTerminals();
        if (!$this->time_delivery_between_terminals) {
            return;
        }

        foreach ($types_of_deliveries_field_ids as $type_of_delivery => $loading_and_discharge_ids) {
            $key_p2p_delivery = $this->getKeyP2PDelivery((int)$row[$loading_and_discharge_ids['loading_id']], (int)$row[$loading_and_discharge_ids['discharge_id']]);
            if (
                isset($this->time_delivery_between_terminals[$key_p2p_delivery])
                && $this->time_delivery_between_terminals[$key_p2p_delivery]
                && $row[$in_way_field_id] !== FieldConfig::FIELD_VAL_NOT_EXIST_VALUE
            ) {
                $row[$in_way_field_id] += $this->time_delivery_between_terminals[$key_p2p_delivery];
            } else {
                $row['additional_params']['empty_time_delivery_between_terminals'][] = [
                    'loading' => $loading_and_discharge_ids['loading_id'],
                    'discharge' => $loading_and_discharge_ids['discharge_id'],
                ];
                $row[$in_way_field_id] = FieldConfig::FIELD_VAL_NOT_EXIST_VALUE; // если какого-то времени не хватает, то вместо суммы выведем -, т.к сумма не корректна, если одно из слагаемых неизвестно
            }
        }
    }

    /**
     * @param $row
     * @param array $canonical_location_aliases
     */
    private function replaceLocationIdsOnTitles(&$row, array $canonical_location_aliases): void
    {
        foreach ($row as $field_id => $field_val) {
            if (!$field_val || $field_val === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                continue;
            }
            if (in_array($field_id, $this->fields_geo_points_ids) 
                && isset($canonical_location_aliases[$field_val]) 
                && isset($canonical_location_aliases[$field_val]['title'])) 
            {
                $row[$field_id] = $canonical_location_aliases[$field_val]['title'];
            }
        }
        $this->replaceEmptyTimeTerminalsIdsOnTitles($row);
        $this->replaceEmptyTimeLeveragesIdsOnTitles($row);
    }

    /**
     * Заменяет location_id на title в строке о незаполненном времени на терминалах
     *
     * @param array $row
     */
    private function replaceEmptyTimeTerminalsIdsOnTitles(array &$row): void
    {
        if (!isset($row['additional_params']['empty_time_on_terminals'])) {
            return;
        }
        foreach ($row['additional_params']['empty_time_on_terminals'] as $index => $field_id) {
            $row['additional_params']['empty_time_on_terminals'][$index] = $row[$field_id] ?? '';
        }
    }

    /**
     * Заменяет location_id на title в строке о незаполненном времени между терминалами(плечами: Leverage - логистическое плечо)
     * Из location_id в title Например 3329 в Шанхай.
     *
     * @param array $row
     */
    private function replaceEmptyTimeLeveragesIdsOnTitles(array &$row): void
    {
        if (!isset($row['additional_params']['empty_time_delivery_between_terminals'])) {
            return;
        }
        $leverages = $row['additional_params']['empty_time_delivery_between_terminals'];
        foreach ($leverages as $index => $leverage) {
            $row['additional_params']['empty_time_delivery_between_terminals'][$index] = [
                'loading' => $row[$leverage['loading']],
                'discharge' => $row[$leverage['discharge']],
            ];
        }
    }

    /**
     * @param int $loading_id
     * @param int $discharge_id
     * @return string
     */
    private function getKeyP2PDelivery(int $loading_id, int $discharge_id): string
    {
        return $loading_id . '_' . $discharge_id;
    }

    /**
     * Возвращает язык алиасов, введенных в поисковую форму, если язык ввода совпадает для места отправления и места назначения,
     * иначе - пустую строку
     *
     * @return string
     */
    private function getLanguageLocationAliases(): string
    {
        $language_location_aliases = '';
        if ($this->getLoadingLocationLang() === $this->getDischargeLocationLang()) {
            $language_location_aliases = (string) $this->getLoadingLocationLang();
        }

        return $language_location_aliases;
    }

    /**
     * @return string
     */
    private function getLoadingLocationLang(): string
    {
        return $this->post_filters['loading_location_object']['lang'] ?? '';
    }

    /**
     * @return string
     */
    private function getDischargeLocationLang(): string
    {
        return $this->post_filters['discharge_location_object']['lang'] ?? '';
    }

    /**
     * Возвращает массив алиасов вида  ['location_id' => ['title' => val1, lang=> val2, location_id=> val3']]
     * Если есть алиас на языке ввода, берем его, если нет берем алиас на любом языке.
     * Приоритет типов алиасов: LocationAlias::TYPE_ALIIAS_BY_COMPANY, LocationAlias::TYPE_CANONICAL
     * Приоритет языков: LocationAlias::PRIORITY_LANGUAGES
     * @param string|null $language_location_aliases
     * @return array
     */
    private function getCanonicalLocationAliases(string $language_location_aliases = null): array
    {
        $canonical_location_aliases = LocationAlias::find()
            ->select('title, lang, location_id')
            ->where('IF(lang = "'. $language_location_aliases . '", lang = "' . $language_location_aliases . '", lang != "'.$language_location_aliases.'")')
            ->andWhere('IF(type = "'. LocationAlias::TYPE_ALIAS_BY_COMPANY . '", type = "' . LocationAlias::TYPE_ALIAS_BY_COMPANY . '", type = "' . LocationAlias::TYPE_CANONICAL . '")')
            ->indexBy('location_id')
            ->orderBy([new Expression('FIELD (lang, "' . implode('","', LocationAlias::PRIORITY_LANGUAGES) . '")')])
            ->asArray()
            ->all();

        return $canonical_location_aliases;
    }

    /**
     * Добаляет к названию поля знак валюты, которая указана в field_config, например: ($)
     * и отдельно добавляет к заголовку 'currency_sign'
     * Возвращаемый массив вида:
     * [
     *      order1 => [
     *          'id' => (string)12,
     *          'code' => (string)AUTO,
     *          'title' => (string)Авто(₽),
     *          'type' => (string)1, //FieldConfig::TYPE_CONST
     *          'currency_sign' => (string)₽ //это поле есть только у тех field_config у которых есть валюта
     *      ],
     *          ...
     *      orderN => [
     *          'id' => (string)24,
     *          'code' => (string)RWTF,
     *          'title' => (string)ЖД тариф($),
     *          'type' => (string)1, //FieldConfig::TYPE_CONST
     *          'currency_sign' => (string)$ //это поле есть только у тех field_config у которых есть валюта
     *      ]
     * ]
     *
     * @param array $ordered_title_fields
     * @return array
     */
    private function addCurrencyToTitles(array $ordered_title_fields): array
    {
        $field_codes_to_currency_iso_codes = $this->getFieldCodesToCurrencyIsoCodes(array_keys($ordered_title_fields));
        if (!$field_codes_to_currency_iso_codes) {
            return $ordered_title_fields;
        }

        foreach ($ordered_title_fields as $field_order => $field_data) {
            $code = $ordered_title_fields[$field_order]['code'];
            $currency_iso_code = (int)($field_codes_to_currency_iso_codes[$code] ?? null);
            if (!$currency_iso_code) {
                continue;
            }
            $currency_sign = Currency::getCurrencySignByIsoCode($currency_iso_code);
            if (!$currency_sign) {
                continue;
            }
            $field_data['title'] .= StringFormatter::wrapInParenthesesIfExists($currency_sign);
            $field_data['currency_sign'] = $currency_sign;
            $ordered_title_fields[$field_order] = $field_data;
        }

        return $ordered_title_fields;
    }

    /**
     * Возвращает массив сопостовления field_config->code к currency->iso_code(например FieldConfig::DROP => Currency::USD):
     * [
     *      (string)code1 => (string)iso_code1,
     *          ...
     *      (string)codeN => (string)iso_codeN
     * ]
     *
     * @param array $order_ids // массив field_config->static::ORDER_FIELD_NAME
     * @return array
     */
    private function getFieldCodesToCurrencyIsoCodes(array $order_ids): array
    {
        return FieldConfig::find()
            ->select(['currency.iso_code'])
            ->innerJoin('currency', 'fields_config.currency_id = currency.id')
            ->where([
                'fields_config.' . static::ORDER_FIELD_NAME => $order_ids,
                'fields_config.company_id' => $this->company_id,
                'fields_config.is_deleted' => FieldConfig::IS_NOT_DELETED
            ])
            ->indexBy('code')
            ->column();
    }

    /**
     * Возвращает массив неупорядоченных строк
     *
     * @param array $sql_data
     * @return array
     * @throws \Exception
     */
    protected function getRowsOriginal(array $sql_data): array
    {
        $rows_original = FileCacheController::get($this->getCacheFileName());
        if ($rows_original) {
            return $rows_original; // Строки нашлись в кэше. Все хорошо, просто вернем их.
        }

        $is_creating_cache_in_progress = \Yii::$app->cache->get(FileCacheController::getCacheKeyWriteRowsOriginalInProgress($this->company_id));
        if ($this->is_console) { // Запуск из консоли. Если процес не был запущен, получим строки с засетим их в кэш
            if (!$is_creating_cache_in_progress) {
                \Yii::$app->cache->set(FileCacheController::getCacheKeyWriteRowsOriginalInProgress($this->company_id), 1, FileCacheController::CREATING_CACHE_IN_PROGRESS_TTL); // Выставляем флаг того, что в данный момент происходит формирование кэша результирующей таблицы
                $rows_original = $this->getRowsOriginalFromDb($sql_data);
                FileCacheController::set($this->getCacheFileName(), $rows_original);
                \Yii::$app->cache->delete(FileCacheController::getCacheKeyWriteRowsOriginalInProgress($this->company_id)); // Формирование файлового кэша результирующей таблицы закончилась
            }
        } else { // Запуск из браузера. Запустим формированием кэша из консоли (если процес не был запущен) и бросим пользователю исключение, чтобы подождал, пока кэш сформируется
            if (!$is_creating_cache_in_progress) {
                FileCacheController::startScriptSetCacheRowsOriginal($this->company_id, $this->user_id);
            }
            throw new FileCacheProcessException(FileCacheProcessException::FILE_CACHE_IN_PROGRESS_ERROR_FOR_LOG);
        }

        return $rows_original;
    }

    /**
     * Тяжелый метод. Получает строки результирующей таблицы из БД со всеми преобразованиями.
     * Его результат затем сохраним в файловый кэш.
     * Если надо получить строки, используйте $this->getRowsOriginal().
     *
     * @param $sql_data
     * @return array
     * @throws \Exception
     */
    private function getRowsOriginalFromDb($sql_data): array
    {
        $rows = $this->getRows($sql_data);
        $rows_result_table = $this->getRowsWithCompositeKey($rows);
        if (!$rows_result_table) {
            $this->errors[] = 'Ошибка генерации составного ключа. Проверьте правильность заполнения таблицы <a href="/field-config/">Настройка данных</a>';
            return [];
        }

        $this->unique_values_tables = $this->getUniqueValuesTables($sql_data);
        $rows_all_variation = $this->getAllVariationRows($rows_result_table);
        if (!$rows_all_variation) {
            return [];
        }

        return $this->getComputeRowsTable($rows_all_variation);
    }

    /**
     * Генерирует составной ключ (FieldConfig::IS_KEY && FieldConfig::IS_SIMPLE_FIELD) строкам и возвращает их
     *
     * @param array $rows
     * @return array
     */
    public function getRowsWithCompositeKey(array $rows): array
    {
        $result_rows = [];
        $field_key_ids = array_merge($this->fields_transport_keys_ids, $this->fields_service_keys_ids);
        foreach ($rows as $row) {
            $composite_key = [];
            foreach ($field_key_ids as $field_key_id) {
                if(isset($row[$field_key_id])) {
                    $composite_key[] = $row[$field_key_id];
                }
            }
            $composite_key = implode(',', $composite_key);
            if(isset($result_rows[$composite_key])) {
                $result_rows[$composite_key] = (array)$result_rows[$composite_key] + $row;
            }
            else {
                $result_rows[$composite_key] = $row;
            }
        }

        return $result_rows;
    }

    /**
     * Возвращает все комбинации строк полученные путем соединения строк по общим гео точками
     * По сути это строки результирующей таблицы, БЕЗ подсчета подитогов
     *
     * @param array $rows
     * @return array
     */
    private function getAllVariationRows(array $rows): array
    {
        $rows_for_union = [];
        $rows_for_update = [];
        $all_variation_rows = [];
        $table_ids_type_services = [];
        foreach ($rows as $key => $row) {
            $count_geo_points_in_row = $this->getCountGeoPointsInRow($row);
            if ($count_geo_points_in_row > 1 && $row['type_table'] == Table::TYPE_TRANSPORT) {
                $rows_for_union[$key] = $row;
                continue;
            }
            if ($row['type_table'] == Table::TYPE_SERVICES){
                $rows_for_update[$row['table_id']][$key] = $row;
                if (!$table_ids_type_services || !in_array($row['table_id'], $table_ids_type_services)) {
                    $table_ids_type_services[] = $row['table_id'];
                }
                continue;
            }
        }
        if (!$rows_for_union) {
            $this->errors[] = 'Не хватает данных для построения маршрутов. Вы можете посмотреть все возможные строки в <a href="/result-table/">результирующей таблице</a>';
            return $all_variation_rows;
        }

        $all_variation_rows = $this->getFinalRowsWithCrossUnion($rows_for_union);
        $all_variation_rows = $this->getRowsMaxWeight($all_variation_rows);
        if (!$all_variation_rows) {
            $this->errors[] = 'Не хватает данных для построения маршрутов. Вы можете посмотреть все возможные строки в <a href="/result-table/">результирующей таблице</a>';
            return $all_variation_rows;
        }

        if ($rows_for_update && $table_ids_type_services) {
            $all_variation_rows = $this->getFinalRowsWithAdditionalData($all_variation_rows, $rows_for_update, $table_ids_type_services);
        }

        return $all_variation_rows;
    }

    /**
     * Возвращает рекурсивно полученные строки(маршруты) путем декартового произведения всех строк(маршрутов)
     * со всеми строками(плечами) из загруженной таблицы
     *
     * @param array $rows_for_union
     * @return array
     */
    private function getFinalRowsWithCrossUnion(array $rows_for_union): array
    {
        $united_rows = $rows_for_union;
        foreach ($rows_for_union as $key => $row) {
            $united_rows = $this->getRowsWithCrossUnion($united_rows, $row);
        }

        return $united_rows;
    }

    /**
     * Возвращает рекурсивно полученные строки(маршруты) с добавленными по общим полям-ключам, не ключевыми полями.
     * Не порождает новых строк(маршрутов), но может добавить строку(маршрут+услуга2),
     * если она подходит под несколько строк услуг
     *
     * @param array $all_variation_rows
     * @param array $rows_for_update
     * @param array $table_ids_type_services
     * @return array
     */
    private function getFinalRowsWithAdditionalData(array $all_variation_rows, array $rows_for_update, array $table_ids_type_services): array
    {
        $i = 0;
        $rows_with_additional_data = $all_variation_rows;
        while ($i < count($table_ids_type_services)) {
            $rows_with_additional_data = $this->getRowsWithAdditionalData($rows_with_additional_data, $rows_for_update[$table_ids_type_services[$i]]);
            $i++;
        }

        return $rows_with_additional_data;
    }

    /**
     * Возвращеет строки(маршрут+услуги), если строка(маршрут) подходит для строки(услуги),
     * то строка(маршрут) удаляется и заменяется на строки(маршрут+услуга)
     * (может быть несколько если подходит под несколько строк из таблиц услуг)
     *
     * @param array $rows_type_transport
     * @param array $rows_for_update
     * @return array
     */
    private function getRowsWithAdditionalData(array $rows_type_transport, array $rows_for_update): array
    {
        $rows_with_additional_data = $rows_type_transport;
        foreach ($rows_type_transport as $key_row => $row_type_transport) {
            $rows_for_replace_row_type_transport = $this->getRowsForReplaceRowTypeTransport($row_type_transport, $rows_for_update);
            if ($rows_for_replace_row_type_transport) {
                unset($rows_with_additional_data[$key_row]);
                $rows_with_additional_data += $rows_for_replace_row_type_transport;
            }
        }

        return $rows_with_additional_data;
    }

    /**
     * Возвращает строки(маршрут+услуги), если их несколько подходит
     *
     * @param array $row_type_transport
     * @param array $rows_for_update
     * @return array
     */
    private function getRowsForReplaceRowTypeTransport(array $row_type_transport, array $rows_for_update): array
    {
        $rows_for_replace_row_type_transport = [];
        foreach ($rows_for_update as $key_row => $row_for_update) {
            $row_with_additional_data = $this->getRowForReplaceRowTypeTransport($row_type_transport, $row_for_update);
            if (!$row_with_additional_data) {
               continue;
            }

            $rows_for_replace_row_type_transport += $row_with_additional_data;
        }

        return $rows_for_replace_row_type_transport;
    }

    /**
     * Возвращает одну строку(маршрут+услуги)
     *
     * @param array $row_type_transport
     * @param $row_for_update
     * @return array
     */
    private function getRowForReplaceRowTypeTransport(array $row_type_transport, $row_for_update): array
    {
        $row_with_additional_data = [];
        foreach ($row_for_update as $field_id => $field_val) {
            if ($this->isTechnicalField($field_id)) {
                continue;
            }
            $row_for_update[$field_id] = json_decode($field_val, true)['raw_value'] ?? $field_val;
        }

        $transport_key_ids_for_row = $this->getTransportKeyIdsForRow($row_for_update);
        if (!$transport_key_ids_for_row) {
            return $row_with_additional_data;
        }
        $common_keys = $this->getCommonKeys($row_type_transport, $row_for_update, $transport_key_ids_for_row);
        if (!$common_keys) {
            return $row_with_additional_data;
        }

        $field_ids_for_add = $this->getFieldIdsForAdd($transport_key_ids_for_row, $row_for_update);
        if (!$field_ids_for_add) {
            return $row_with_additional_data;
        }

        $row_with_additional_data = $this->getRowWithAdditionalData($field_ids_for_add, $row_type_transport, $row_for_update);

        return $row_with_additional_data;
    }

    /**
     * Возвращает количество гео точек в строке
     *
     * @param array $row
     * @return int
     */
    private function getCountGeoPointsInRow(array $row): int
    {
        $count_geo_points_in_row = 0;
        foreach ($this->fields_geo_points_ids as $field_geo_point_id) {
            if (array_key_exists($field_geo_point_id, $row)) {
                $count_geo_points_in_row++;
            }
        }

        return $count_geo_points_in_row;
    }

    /**
     * Возвращает строки(маршруты) путем получения декардового произведения всех текущих строк
     * со строкой(плечом) из загруженной таблицы
     * @see getFinalRowsWithCrossUnion
     *
     * @param array $rows
     * @param array $join_row
     * @return array
     */
    private function getRowsWithCrossUnion(array $rows, array $join_row): array
    {
        $transport_key_ids_for_row = $this->getTransportKeyIdsForRow($join_row);

        $next_row = false;
        foreach ($rows as $key_row => $row) {
            if ($this->isSameTable($row['table_id'], $join_row['table_id'])) {
                continue;
            }
            if ($this->isSameRow($key_row, $join_row)) {
                continue;
            }
            if(!$this->hasCommonGeoPoint($row, $join_row)) {
                continue;
            }
            foreach ($transport_key_ids_for_row as $transport_key_id) {
                if ($this->NotMatchingFieldValues($row[$transport_key_id], $join_row[$transport_key_id])) {
                    $next_row = true;
                    break;
                }
            }
            if ($next_row) {
                $next_row = false;
                continue;
            }

            $result_row_for_union = $this->getResultRowForUnion($row, $join_row);
            if (!$result_row_for_union) {
                continue;
            }

            $result_row[] = $result_row_for_union;
            $row_with_composite_key = $this->getRowsWithCompositeKey($result_row);
            $composite_key = key($row_with_composite_key);
            $rows[$composite_key] = $row_with_composite_key[$composite_key];
            $result_row = [];
        }

        return $rows;
    }

    /**
     * @param $row_table_id
     * @param $join_row_table_id
     * @return bool
     */
    private function isSameTable($row_table_id, $join_row_table_id): bool
    {
        return $row_table_id === $join_row_table_id;
    }

    /**
     * @param string $key_row
     * @param array $join_row
     * @return bool
     */
    private function isSameRow(string $key_row, array $join_row): bool
    {
        return array_key_exists($key_row, $join_row);
    }

    /**
     * Возвращает true если значения не являются спец значениями(любые, остальные)
     * и не совпадают по уникальным значениям
     *
     * @param string|null $row_value
     * @param string|null $join_row_value
     * @return bool
     */
    private function NotMatchingFieldValues(string $row_value = null, string $join_row_value = null): bool
    {
        return (isset($row_value)
            && isset($join_row_value)
            && $join_row_value != $row_value
            && !FieldConfig::isSpecialValue($join_row_value)
            && !FieldConfig::isSpecialValue($row_value))
            || ($row_value === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE
            || $join_row_value === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE);
    }

    /**
     * Замена array_merge. Так как у стандартного метода, если есть ключ то значение не перезаписывается.
     * А у нас есть случай что если ключ существует но значение null, нужно перезаписать.
     * Так как пустое загруженное поле пользователем считается любым.
     *
     * @param array $row
     * @param array $join_row
     * @return array
     */
    private function getResultRowForUnion(array $row, array $join_row): array
    {
        $result_row = $row;
        foreach ($join_row as $field_id => $field_value) {
            /** Если это не числовой id то это служебное поле его нужно пропустить */
            if ($this->isTechnicalField($field_id)) {
                continue;
            }

            $join_row = $this->getRowWithExceptFields($join_row, $field_id);
            $result_row = $this->getRowWithExceptFields($result_row, $field_id);

            /** Если у добавляемой строки есть поля json то добавить к результирующей */
            if (isset($join_row['fields_json'])) {
                $result_row['fields_json'] += $join_row['fields_json'];
            }

            /** Если добавляемое значение любые, то пропускаем */
            if ($field_value === FieldConfig::FIELD_VAL_ANY) {
                continue;
            }
            /**
             *  Если к конкретному(неспециальному) полю добавляем "остальные",
             *  то делаем проверку на то что конкретное значение относится к "остальным"
             *  если нет возвращаем пустую строку так как она не подходит для склейки
             */
            if ($field_value === FieldConfig::FIELD_VAL_OTHER
                && (isset($result_row[$field_id])
                    && !FieldConfig::isSpecialValue($result_row[$field_id]))) {

                        if (!$this->checkExceptFieldVal($join_row, $field_id, $result_row[$field_id])) {
                            return [];
                        }
                        continue;
            }
            /**
             *  Если к полю "остальные" добавляем конкретное(неспециальное) поле,
             *  то делаем проверку на то что конкретное значение относится к "остальным"
             *  если нет возвращаем пустую строку так как она не подходит для склейки,
             *  иначе заменяеям значение "остальные" на конкретное
             */
            if ($result_row[$field_id] === FieldConfig::FIELD_VAL_OTHER
                && !FieldConfig::isSpecialValue($field_value)
                && !$this->checkExceptFieldVal($result_row, $field_id, $field_value)) {

                    return [];
            }
            /**
             * Если к полю "любое" или "остальные" или не существующему, добавляем "остальные",
             * то оно заменится на "остальные" и возьмет(добавит) правила из $join_row
             */
            if ($field_value === FieldConfig::FIELD_VAL_OTHER
                && $this->isSpecialOrEmptyFieldValue($result_row[$field_id])
                && $this->isUniqueFieldIdForExceptFields($result_row, $join_row, $field_id)) {

                    $result_row['except_fields'][$join_row['table_id']][] = $field_id;
            }

            $result_row_old_field_value = $result_row[$field_id];
            $result_row[$field_id] = $field_value;

            /** Если поле было "остальные", и было заменено на другое значение */
            if ($result_row_old_field_value === FieldConfig::FIELD_VAL_OTHER
                && $result_row[$field_id] !== FieldConfig::FIELD_VAL_OTHER
                && isset($result_row['except_fields'][$result_row['table_id']])) {

                    $key = array_search($field_id, $result_row['except_fields'][$result_row['table_id']]);
                    if (isset($key)) {
                        unset($result_row['except_fields'][$result_row['table_id']][$key]);
                    }
            }
        }

        return $result_row;
    }

    /**
     * @param array $result_row
     * @param array $join_row
     * @param int $field_id
     * @return bool
     */
    private function isUniqueFieldIdForExceptFields(array $result_row, array $join_row, int $field_id): bool
    {
        return (!isset($result_row['except_fields'][$join_row['table_id']])
            || !in_array($field_id, $result_row['except_fields'][$join_row['table_id']]));
    }

    /**
     * Проверка что поле пустое, или является спец значением(остальные,любые)
     *
     * @param string|null $field_value
     * @return bool
     */
    private function isSpecialOrEmptyFieldValue(string $field_value = null): bool
    {
        return (!isset($field_value)
            || (isset($field_value)
                && FieldConfig::isSpecialValue($field_value)));
    }

    /**
     * Возвращает строку с массивом except_fields(все уникальные значения колонки),
     * иначе возвращает строку без изменений
     *
     * @param array $row
     * @param int $field_id
     * @return array
     */
    private function getRowWithExceptFields(array $row, int $field_id): array
    {
        $except_fields = $row['except_fields'][$row['table_id']] ?? null;
        if (
            $row[$field_id] === FieldConfig::FIELD_VAL_OTHER
            && (!$except_fields || !in_array($field_id, $except_fields))
        ) {
            $row['except_fields'][$row['table_id']][] = $field_id;
        }

        return $row;
    }

    /**
     * Проверяет наличие $field_val в области значений "остальные"
     * Если совпало то вернет false, проверка не пройдена
     *
     * @param array $row
     * @param int $field_id
     * @param string $field_val
     * @return bool
     */
    private function checkExceptFieldVal(array $row, int $field_id, string $field_val): bool {
        if (!$row['except_fields']) {
            return false;
        }
        foreach ($row['except_fields'] as $table_id => $except_field_ids) {
            if (!$except_field_ids || !$this->unique_values_tables[$table_id][$field_id]) {
                continue;
            }
            if (in_array($field_id, $except_field_ids)
                && in_array($field_val, $this->unique_values_tables[$table_id][$field_id])) {
                    return false;
            }
        }

        return true;
    }

    /**
     * Массив ids полей у которых FieldConfig::IS_KEY
     *
     * @param array $row
     * @return array
     */
    private function getTransportKeyIdsForRow(array $row): array
    {
        $transport_key_ids_for_row = [];
        foreach ($this->fields_transport_keys_ids as $field_transport_key_id) {
            if (array_key_exists($field_transport_key_id, $row)) {
                $transport_key_ids_for_row[] = $field_transport_key_id;
            }
        }

        return $transport_key_ids_for_row;
    }

    /**
     * Массив ids полей у которых FieldConfig::IS_SIMPLE_FIELD
     *
     * @param array $row
     * @return array
     */
    private function getServiceKeyIdsForRow(array $row): array
    {
        $service_key_ids_for_row = [];
        foreach ($this->fields_service_keys_ids as $field_service_key_id) {
            if (array_key_exists($field_service_key_id, $row)) {
                $service_key_ids_for_row[] = $field_service_key_id;
            }
        }

        return $service_key_ids_for_row;
    }

    /**
     * Метод проверяет имеют ли две строки общие гео точки (точки перевала)
     *
     * @param array $row
     * @param array $join_row
     * @return bool
     */
    private function hasCommonGeoPoint(array $row, array $join_row): bool
    {
        foreach ($this->fields_geo_points_ids as $field_geo_point_id) {
            if (isset($row[$field_geo_point_id]) && isset($join_row[$field_geo_point_id])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает массив ids полей которые нужно протянуть в строке.
     * Первое слагаемое получает ids полей, которые являются ключами результирующей таблицы, но не гео точками
     * и которых нет в строке к которой применится протягивание
     * Второе слагаемое получает ids полей, не являющимися ключами результирующей таблицы
     *
     * @param array $keys
     * @param array $row_for_update
     * @return array
     */
    private function getFieldIdsForAdd(array $keys, array $row_for_update): array
    {
        return array_diff(array_keys($row_for_update), $keys);
    }

    /**
     * @param array $row_transport
     * @param array $row_for_update
     * @param array $keys_row_for_update
     * @return array
     */
    private function getCommonKeys(array $row_transport, array $row_for_update, array $keys_row_for_update): array
    {
        $transport_key_ids_for_row_transport = $this->getTransportKeyIdsForRow($row_transport);
        $intersect_transport_keys_ids = array_intersect($transport_key_ids_for_row_transport, $keys_row_for_update);
        $service_key_ids_for_row = $this->getServiceKeyIdsForRow($row_for_update);
        $common_keys = array_merge($intersect_transport_keys_ids, $service_key_ids_for_row);

        if (!$common_keys) {
            return [];
        }

        foreach ($common_keys as $key_id) {
            if ($this->isServiceKeyNotInRowTransport($row_transport, $key_id)) {
                continue;
            }
            $row_for_update = $this->getRowWithExceptFields($row_for_update, $key_id);
            $row_transport = $this->getRowWithExceptFields($row_transport, $key_id);
            if ($this->NotMatchingFieldValues($row_transport[$key_id],$row_for_update[$key_id])) {
                return [];
            }
            if ($row_transport[$key_id] === FieldConfig::FIELD_VAL_OTHER
                && $row_for_update[$key_id] !== FieldConfig::FIELD_VAL_OTHER
                && !$this->checkExceptFieldVal($row_transport, $key_id, $row_for_update[$key_id])) {
                    return [];
            }
            if ($row_for_update[$key_id] === FieldConfig::FIELD_VAL_OTHER
                && $row_transport[$key_id] !== FieldConfig::FIELD_VAL_OTHER
                && !$this->checkExceptFieldVal($row_for_update, $key_id, $row_transport[$key_id])) {
                    return [];
            }
        }
        return $common_keys;
    }

    /**
     * Проверят, что у строки-маршрута нет ключа-услуги
     *
     * @param array $row_transport
     * @param $key_id
     * @return bool
     */
    private function isServiceKeyNotInRowTransport(array $row_transport, $key_id): bool
    {
        return !$row_transport[$key_id] && in_array($key_id, $this->fields_service_keys_ids);
    }

    /**
     * @param array $field_ids_for_add
     * @param array $row_transport_type
     * @param array $row_for_update
     * @return array
     */
    private function getRowWithAdditionalData(array $field_ids_for_add, array $row_transport_type, array $row_for_update): array
    {
        foreach ($field_ids_for_add as $field_id){
            if ($this->isAddOrReplaceServiceNeeded($row_transport_type, $row_for_update, $field_id)) {
                $row_transport_type[$field_id] = $row_for_update[$field_id];
            }
        }

        return $this->getRowWithKeyAllFields($row_transport_type);
    }

    /**
     * Проверяет нужно ли протянуть(добавить) поле-услугу в строке-маршруте.
     * Или заменить ключ-услугу в строке-маршруте,
     * если значение ключа-услуги FieldConfig::FIELD_VAL_ANY или FieldConfig::FIELD_VAL_OTHER,
     * а в строке-услуге значение ключа-услуги конкретное значение
     *
     * @param $row_transport_type
     * @param $row_for_update
     * @param $field_id
     * @return bool
     */
    private function isAddOrReplaceServiceNeeded(array $row_transport_type, array $row_for_update, $field_id): bool
    {
        if ($this->isTechnicalField($field_id)) {
            return false;
        }
        if (!$row_transport_type[$field_id]) {
            return true;
        }
        if (FieldConfig::isSpecialValue($row_transport_type[$field_id]) && !FieldConfig::isSpecialValue($row_for_update[$field_id])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $row
     * @return array
     */
    private function getRowWithKeyAllFields(array $row): array
    {
        $row_with_key_all_fields = [];
        $composite_key = [];
        foreach ($row as $field_id => $field_val) {
            $composite_key[] = $field_val;
        }
        if (!$composite_key) {
            return $row_with_key_all_fields;
        }
        $composite_key = implode(',', $composite_key);
        $row_with_key_all_fields[$composite_key] = $row;

        return $row_with_key_all_fields;
    }

    /**
     * Возвращает строки с максимальным количеством заполненных гео точек
     *
     * @param array $rows
     * @return array
     */
    private function getRowsMaxWeight(array $rows): array
    {
        $count_geo_point_prev = 0;
        $count_geo_point_max = 0;
        $rows_with_weight = [];

        foreach ($rows as $key => $row) {
            $count_geo_points_in_row = $this->getCountGeoPointsInRow($row);
            $rows_with_weight[$count_geo_points_in_row][$key] = $row;
            if($count_geo_points_in_row > $count_geo_point_prev) {
                $count_geo_point_max = $count_geo_points_in_row;
            }
            $count_geo_point_prev = $count_geo_points_in_row;
        }

        return $rows_with_weight[$count_geo_point_max];
    }

    /**
     * Возвращает строки с посчитаными полями по формулам
     *
     * @param array $rows
     * @return array
     * @throws \Exception
     */
    protected function getComputeRowsTable(array $rows): array
    {
        $this->formulas = FormulaList::find()
            ->where(['company_id' => $this->company_id])
            ->indexBy('result_field_id')
            ->asArray()->all();

        $this->fields_configs = $this->getAllFields();
        $rows = $this->getRowsWithAdditionAllFields($rows);
        $this->formulas = $this->getFormulasWithRpnFormulaIds();

        foreach ($rows as $key_row => $row) {
            foreach ($row as $key_field => $field_value) {
                if (array_key_exists($key_field, $this->formulas)) {
                    $row[$key_field] = $this->getCalculateField($row, $key_field);
                    $rows[$key_row] = $row;
                }
            }
        }

        return $rows;
    }

    /**
     * Получаем все поля компании
     *
     * @return array
     */
    private function getAllFields(): array
    {
        return FieldConfig::find()
            ->select(['id', 'code'])
            ->where([
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'company_id' => $this->company_id,
            ])
            ->asArray()->all();
    }

    /**
     * Дополняет строки отсутствующими полями, заполняя их 0,
     * нужно для подсчета формул по rpn
     *
     * @param array $rows
     * @return array
     */
    private function getRowsWithAdditionAllFields(array $rows): array
    {
        $field_ids = [];
        foreach ($this->fields_configs as $field) {
            $field_ids[$field['id']] = FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
        }

        $rows_with_all_fields = [];
        foreach ($rows as $key_row => $row) {
            $row_with_all_fields = $row + $field_ids;
            $rows_with_all_fields[$key_row] = $row_with_all_fields;
        }

        return $rows_with_all_fields;
    }

    /**
     * Метод возвращает набор формул с rpn_formula замененной на id
     *
     * @return array
     */
    private function getFormulasWithRpnFormulaIds(): array
    {
        if (!$this->formulas) {
            return [];
        }

        if (!$this->fields_configs) {
            return [];
        }

        $field_ids = [];
        foreach ($this->fields_configs as $field) {
            array_push($field_ids, ' ' . $field['id']);
        }
        $field_codes = ArrayHelper::getColumn($this->fields_configs, 'code');

        foreach ($this->formulas as $key_formula => $formula) {
            $this->formulas[$key_formula]['rpn_formula_ids'] = str_replace($field_codes, $field_ids, $formula['rpn_formula']);
            if (!$this->formulas[$key_formula]['rpn_formula_ids']) {
                continue;
            }
            preg_match_all('/\s(\d+)/m', $this->formulas[$key_formula]['rpn_formula_ids'], $matches);
            $this->formulas[$key_formula]['field_ids'] = $matches[0];
        }

        return $this->formulas;
    }

    /**
     * Метод вычисляет поле по заданной формуле
     *
     * @param array $row
     * @param int $result_field_id
     * @return string
     * @throws \Exception
     */
    private function getCalculateField(array $row, int $result_field_id): string
    {
        $formula = $this->formulas[$result_field_id];

        if (!$formula) {
            return FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
        }

        if ($formula['count_condition'] !== null) {
            preg_match('/^[A-Z]{4}/', $formula['count_condition'], $matches);
            $code = $matches[0] ?? null;

            if (!$code) {
                return $this->getFieldValueByFormula($formula, $row);
            }

            $condition_field_id = $this->getFieldIdByCode($code);
            if (!$condition_field_id) {
                return $this->getFieldValueByFormula($formula, $row);
            }

            $formula['count_condition'] = str_replace($code, $row[$condition_field_id], $formula['count_condition']);
        }

        return $this->getFieldValueByFormula($formula, $row);
    }

    /**
     * @param string $field_code
     * @return int
     */
    private function getFieldIdByCode(string $field_code): int
    {
        foreach ($this->fields_configs as $field_data) {
            if ($field_data['code'] === $field_code) {
                return $field_data['id'];
            }
        }
        return 0;
    }

    /**
     * Метод заменяет id полей в формуле
     * обратной польской нотации(rpn) на значения из строки и
     * считает по формуле с помощью rpn возвращает значение результирующего поля
     *
     * @param array $formula
     * @param array $row
     * @return string
     * @throws \Exception
     */
    private function getFieldValueByFormula(array $formula, array $row):string
    {
        $field_ids_replace = [];
        $field_values_replace = [];
        foreach ($row as $key => $value) {
            if(!in_array((string) $key, $formula['field_ids'])){
                continue;
            }

            if ($value === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE) {
                return FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
            }
            array_push($field_ids_replace, ' ' . $key . ' ');
            array_push($field_values_replace, ' '. $value . ' ');
        }

        $rpn_formula_values = str_replace($field_ids_replace, $field_values_replace, $formula['rpn_formula_ids']);

        return (string)Rpn::calculate($rpn_formula_values);
    }

    /**
     * Упорядочивает строки и проставляет доп поля
     *
     * @param array $rows
     * @return array
     */
    public function getOrderedRows(array $rows): array
    {
        $ordered_rows = [];
        foreach ($rows as $row_number => $row) {
            $ordered_rows[] = $this->getOrderedRowWithAddParams($row);
        }

        return $ordered_rows;
    }

    /**
     * Получаем все настройки переменных, для конкретной компании
     *
     * @return array
     */
    protected function getAllFieldsConfigs(): array
    {
        if (!$this->all_fields_configs) {
            $this->all_fields_configs = FieldConfig::find()
                ->where([
                    'company_id' => $this->company_id,
                    'is_deleted' => FieldConfig::IS_NOT_DELETED
                ])
                ->all();
        }

        return $this->all_fields_configs;
    }

    /**
     * Заполняет ids полей-ключей для $this->fields_transport_keys_ids, $this->fields_service_keys_ids,
     * $this->fields_geo_points_ids
     * из $this->all_fields_configs
     *
     * @return ResultTableRowProcessor
     */
    protected function setFieldKeyIds(): ResultTableRowProcessor
    {
        if (!$this->all_fields_configs) {
            return $this;
        }
        foreach ($this->all_fields_configs as $field_configs) {
            if ($field_configs->is_key === FieldConfig::IS_KEY) {
                $this->fields_transport_keys_ids[] = $field_configs->id;
            }
            if ($field_configs->is_simple_field === FieldConfig::IS_SIMPLE_FIELD) {
                $this->fields_service_keys_ids[] = $field_configs->id;
            }
            if ($field_configs->is_geo_point === FieldConfig::IS_GEO_POINT) {
                $this->fields_geo_points_ids[] = $field_configs->id;
            }
        }

        return $this;
    }

    /**
     * возвращает массив id актуальных файлов
     *
     * @return array
     */
    private function getCurrentFilesIds():array
    {
        $date = date('Y-m-d');
        $condition = "'{$date}' BETWEEN FROM_UNIXTIME(`date_upload_since`, '%Y-%m-%d') AND FROM_UNIXTIME(`date_upload_by`, '%Y-%m-%d')";
        $files_ids = File::find()
            ->select('id')
            ->where(['company_id' => $this->company_id])
            ->andWhere($condition)
            ->asArray()
            ->column();

        return $files_ids;
    }

    /**
     * Получаем fields в заданном порядке
     *
     * @return array
     */
    protected function getOrderFields():array
    {
        $order_fields =
            FieldConfig::find()
                ->select([
                    'fields_config.id as id',
                    'fields_config.code as code',
                    'fields_config.' . static::ORDER_FIELD_NAME . ' as ' . static::ORDER_FIELD_NAME,
                    'fields_config.type_of_delivery as type_of_delivery',
                    'currency.iso_code as currency_iso_code'
                ])
                ->leftJoin('currency', 'fields_config.currency_id = currency.id')
                ->where([
                    'fields_config.company_id' => $this->company_id,
                    'fields_config.is_shown' => FieldConfig::IS_SHOWN,
                    'fields_config.is_deleted' => FieldConfig::IS_NOT_DELETED
                ])
                ->andWhere('fields_config.' . static::ORDER_FIELD_NAME. ' IS NOT NULL')
                ->indexBy(static::ORDER_FIELD_NAME)
                ->asArray()
                ->all();

        return $order_fields;
    }

    /**
     * Возвращает упорядоченную строку с добавлением параметров
     *
     * @param array $row - массив вида [field_id1 => field_val1, field_id2 => field_val2...]
     * @return array
     */
    protected function getOrderedRowWithAddParams(array $row): array
    {
        return $this->getOrderedRow($row);
    }

    /**
     * Обратное преобразование относительно self::getOriginalRow
     * Возвращает строку - массив вида [field_order1 => field_val1, field_order2 => field_val2...]
     * @param array $row - массив вида [field_id1 => field_val1, field_id2 => field_val2...]
     * @return array
     */
    protected function getOrderedRow(array $row): array
    {
        $ordered_row = [];
        foreach ($this->order_fields as $field_order => $field_data) {
            if (!array_key_exists($field_data['id'], $row)) {
                continue;
            }
            $ordered_row[$field_order] = $row[$field_data['id']];
        }

        if (array_key_exists('additional_params', $row)) {
            $ordered_row['additional_params'] = $row['additional_params'];
        }

        $ordered_row['shown'] = true;

        return $ordered_row;
    }

    /**
     * Преобразует строки, заменяя поля строки order_field на field_id
     * @see getOriginalRow
     * @param array $rows
     * @return array
     */
    public function getOriginalRows(array $rows): array
    {
        $original_rows = [];
        foreach ($rows as $key_row => $row) {
            $original_rows[$key_row] = $this->getOriginalRow($row);
        }

        return $original_rows;
    }

    /**
     * Обратное преобразование относительно self::getOrderedRow
     * Возвращает строку - массив вида [field_id1 => field_val1, field_id2 => field_val2...]
     * @param array $row - массив вида [field_order1 => field_val1, field_order2 => field_val2...]
     * @return array
     */
    public function getOriginalRow(array $row): array
    {
        $original_row = [];
        foreach($row as $field_order => $field_val) {
            if(!isset($this->order_fields[$field_order]['id'])) {
                continue;
            }
            $original_row[$this->order_fields[$field_order]['id']] = $field_val;
        }
        $original_row['additional_params'] = $row['additional_params'];

        return $original_row;
    }

    /**
     * Возвращает массив вида
     * [
     *      'code_field_config' => 'название заголовка поля таблицы(не из БД)',
     * ]
     * Служит для того, чтобы заменить названия полей, заданные в field_config на кастомные.
     * @return array
     */
    public function getSpecialTitle(): array
    {
        return [];
    }

    /**
     * Метод возвращает массив для визуального выделения блоков типа доставки (Фрахт, ЖД, Авто)
     * посредством вертикальной отрисовки границ таблицы
     * возвращает массив вида:
     * [
     *      left => [order1 => title1, ... , orderN => titleN],
     *      right => [order1 => title1, ... , orderN => titleN],
     *      error => 'текст ошибки'
     * ]
     *
     * @return array
     */
    public function getBorderedBlocks(): array
    {
        $types_of_delivery = TypesOfDeliveries::getTypesOfDeliveriesList();
        $order_ids_group_by_type_of_delivery = $this->getOrderIdsGroupByTypeOfDelivery();
        $bordered_blocks = [];
        foreach ($order_ids_group_by_type_of_delivery as $type_of_delivery => $order_ids) {
            if ($type_of_delivery === 0) {
                continue;
            }
            $order_ids_for_type_of_delivery = $order_ids_group_by_type_of_delivery[$type_of_delivery];
            sort($order_ids_for_type_of_delivery);

            $title_type_of_delivery = $types_of_delivery[$type_of_delivery];
            $bordered_blocks['left'][reset($order_ids_for_type_of_delivery)] = $title_type_of_delivery;
            $bordered_blocks['right'][end($order_ids_for_type_of_delivery)] = $title_type_of_delivery;
            $error_out_of_orders = $this->getErrorOutOfOrders($order_ids_for_type_of_delivery, $title_type_of_delivery);
            if ($error_out_of_orders !== '') {
                $this->errors[] = $error_out_of_orders;
            }
        }

        return $bordered_blocks;
    }

    /**
     * Возвращает массив порядковых номеров полей для каждого типа доставки(Фрахт, ЖД, Авто)
     * вида [type_of_delivery1 => [order_id1, order_id2 ... , order_idN] ...]
     *
     * @return array
     */
    protected function getOrderIdsGroupByTypeOfDelivery(): array
    {
        $order_ids_key_type_of_delivery = [];
        if (!$this->order_fields) {
            return $order_ids_key_type_of_delivery;
        }

        foreach ($this->order_fields as $order_id => $field) {
            if (isset($field['type_of_delivery'])) {
                $order_ids_key_type_of_delivery[$field['type_of_delivery']] = $order_ids_key_type_of_delivery[$field['type_of_delivery']] ?? [];
                array_push($order_ids_key_type_of_delivery[$field['type_of_delivery']], $order_id);
            }
        }

        return $order_ids_key_type_of_delivery;
    }

    /**
     * Возвращает массив, где порядковые номера столбцов разеделны по 5 блокам:
     * поля-ключи, фрахт, жд, авто, итого.
     * @return array
     */
    public function getOrdersGroupByBlocks(): array
    {
        $orders = array_keys($this->order_fields);
        sort($orders);
        $first_col_num = array_shift($orders);
        $last_col_num = array_pop($orders);

        $result = $this->getOrderIdsGroupByTypeOfDelivery();
        $min_fraht_col_num = min($result[TypesOfDeliveries::CODE_FRAHT]);
        for ($i = $first_col_num; $i < $min_fraht_col_num; $i++) {
            $result[TableConfig::FIELD_KEYS_BLOCK_NAME][] = $i;
        }

        $max_auto_col_num = max($result[TypesOfDeliveries::CODE_AUTO]);
        for ($i = $max_auto_col_num + 1; $i <= $last_col_num; $i++) {
            $result[TableConfig::RESULT_BLOCK_NAME][] = $i;
        }

        foreach ($result as $key => $block) {
            sort($block);
            $result[$key] = $block;
        }

        return $result;
    }

    /**
     * Возвращает массив, orders блока, которые идут не по порядку
     *
     * @param array $orders
     * @param string $title
     * @return string
     */
    private function getErrorOutOfOrders(array $orders, string $title): string
    {
        if (!$orders) {
            return '';
        }

        $first_order = reset($orders);
        $prev_order = $first_order;
        foreach ($orders as $current_order) {
            if ($first_order === $current_order) {
                continue;
            }
            if (!$prev_order) {
                return $this->getErrorBorderForBlock($orders, $title);
            }
            if ($this->order_fields[$prev_order]['code'] === FieldConfig::RAILWAY_AFTER) {
                $prev_order++;
            }
            if ($current_order !== $prev_order + 1) {
                return $this->getErrorBorderForBlock($orders, $title);
            }

            $prev_order = $current_order;
        }

        return '';
    }

    /**
     * @param array $orders
     * @param string $title
     * @return string
     */
    private function getErrorBorderForBlock(array $orders, string $title): string
    {
        if (!$orders) {
            return '';
        }

        $url = static::URL_TO_EDIT_FIELD_CONFIG . static::ORDER_FIELD_NAME;
        return 'Колонки блока "' . $title . '" идут не по порядку: ' . implode(',', $orders) . '. Исправьте порядок столбцов в настройках <a href="' . $url . '">отображения переменных</a>';
    }

    /**
     * Возвращает массив уникальных значений по field_id для каждой таблицы
     * Пример применения для реализации логики поля "остальные", где нужно исключать уникальные значения
     *
     * @param array $sql_data
     * @return array
     */
    private function getUniqueValuesTables(array $sql_data): array
    {
        $unique_values_tables = [];
        $table_ids_by_file_ids = $this->getTableIdsByFileIds();
        foreach ($sql_data as $table_data) {
            if (!in_array($table_data['field_id'], $this->fields_transport_keys_ids) && !in_array($table_data['field_id'], $this->fields_service_keys_ids)) {
                continue;
            }

            $field_val = parent::getFieldValFromField($table_data);
            if ($this->isSpecialFieldVal($field_val)) {
                continue;
            }

            $table_id = $table_ids_by_file_ids[$table_data['file_id']];
            $field_id = $table_data['field_id'];
            if (isset($unique_values_tables[$table_id][$field_id])
                && in_array($field_val, $unique_values_tables[$table_id][$field_id])) {
                    continue;
            }

            $unique_values_tables[$table_id][$field_id][] = $field_val;
        }

        return $unique_values_tables;
    }

    private function getTableIdsByFileIds(): array
    {
        return File::find()
            ->select('table_id')
            ->where(['company_id' => $this->company_id])
            ->indexBy('id')
            ->column();
    }

    /**
     * Проверяет яыляется ли field_value спец значением например ("-", "любые", "остальные")
     *
     * @param string $field_val
     * @return bool
     */
    private function isSpecialFieldVal(string $field_val = null): bool
    {
        return $field_val === FieldConfig::FIELD_VAL_ANY
            || $field_val === FieldConfig::FIELD_VAL_OTHER
            || $field_val === FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
    }

    /**
     * @return string
     */
    public function getCacheFileName(): string
    {
        return FileCacheController::getFileNameRowsOriginal($this->company_id);
    }

    /**
     * @param bool $is_console
     * @return ResultTableRowProcessor
     */
    public function setIsConsole(bool $is_console): ResultTableRowProcessor
    {
        $this->is_console = $is_console;
        return $this;
    }

    /**
     * Возвращает массив вида [order_field => [required => bool, methods => [method_name1 => [message => 'some_msg',  rule => 'some_value'], ... method_nameN]]].
     * required == false - валидация будет применена только в случае, если поле заполнено.
     * required == true - валидаци будет применена всегда.
     * methods - массив с названиями методов валидации из validatorMixins, которые будут применены к полю на стороне js.
     *
     * @return array
     */
    public function getValidationRules() : array
    {
        return [];
    }

    protected function getOrderByCode(string $code, $order_filed_name): string
    {
        $field_order = '';
        foreach ($this->order_fields as $field) {
            if ($field['code'] === $code) {
                $field_order = $field[$order_filed_name];
                break;
            }
        }

        return $field_order;
    }

    /**
     * @return array
     */
    private function getTimeOnTerminals(): array
    {
        if (!$this->time_on_terminals) {
            $this->time_on_terminals = PointTime::find()
                ->select(['time'])
                ->indexBy('terminal_location_id')
                ->column();
        }

        return $this->time_on_terminals;
    }

    /**
     * @return array
     */
    private function getTimeDeliveryBetweenTerminals(): array
    {
        if (!$this->time_delivery_between_terminals) {
            $time_delivery_between_terminals = P2PDeliveryTime::find()
                ->select(['from_location_id', 'to_location_id', 'time'])
                ->all();
            foreach ($time_delivery_between_terminals as $p2p_delivery) {
                $key_p2p_delivery = $this->getKeyP2PDelivery($p2p_delivery['from_location_id'], $p2p_delivery['to_location_id']);
                $this->time_delivery_between_terminals[$key_p2p_delivery] = $p2p_delivery['time'];
            }
        }

        return $this->time_delivery_between_terminals;
    }
}