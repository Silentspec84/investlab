<?php

namespace app\classes\helpers\RowProcessor;
use app\controllers\PublicApiController;
use app\models\CartTablesData;
use app\models\CartVersion;
use app\models\Currency;
use app\models\FieldConfig;
use app\models\KpTablesData;
use app\models\store\Offer;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Класс содержит методы позволяющие собирать строки из данных таблицы cart_tables_data
 *
 * Class CartRowProcessor
 * @package app\classes\helpers
 */

class CartRowProcessor extends ResultTableRowProcessor
{

    /** @var int */
    private $client_company_id;

    /** @var array */
    private $rates = null;

    /** @var array */
    private $currencies = null;

    /** @var array */
    private $hash_checked_rows = null;

    /** @var array */
    private $kp_hash_codes = null;

    /** @var bool */
    private $is_update_row = null;

    /** @var array */
    private $fields_keys_ids = null;

    /** @var array */
    private $fields_subtotal = null;

    /**
     * Массив вида:
     * [
     *      hash1 => [field_id1 => field_val1, ... field_idn => field_valn]
     *         ...
     *      hashn => [field_idn => field_valn]
     * ]
     * @var array
     */
    private $indexed_cart_tables_data = null;

    /** @var CartVersion */
    private $cart_version_model = null;

    /** @var array */
    private $field_codes_list = [];

    /** Название аттрибута поля fields отвечающего за order
     */
    const ORDER_FIELD_NAME = 'order_in_cart';

    public function __construct($company_id, $user_id, $client_company_id = null, $rates = null, $currencies = null, $hash_checked_rows = null, $is_update_row = null)
    {
        parent::__construct($company_id, $user_id);
        $this->client_company_id = $client_company_id;
        $this->rates = $rates;
        $this->currencies = $currencies;
        $this->hash_checked_rows = $hash_checked_rows;
        $this->is_update_row = $is_update_row;
        $this->order_fields = $this->getOrderFields();
        $this->fields_keys_ids = $this->getFieldsKeysIds();
        $this->setFieldCodesList();
    }

    public function getSqlData(array $file_ids = null): array
    {
        $cart_version = $this->getCartVersionModel();
        if (!$cart_version) {
            return [];
        }
        /** @var CartVersion $cart_version */
        CartTablesData::$date_version = $cart_version->version_date;

        $tables_data =
            CartTablesData::find()
            ->where([
                'company_id' => $this->company_id,
                'client_company_id' => $this->client_company_id,
                'is_deleted' => CartTablesData::IS_NOT_DELETED
            ])
            ->asArray()
            ->all();

        return $tables_data;
    }

    /**
     * Возвращает итоговые строки с учтетом тарифа. Если $is_replace_subtotal_values
     * равна true, значения тарифа и подитоги не будут заменяться значениями из cart_tables_data
     * @param array $sql_data
     * @param bool $is_replace_with_exist_values_needed по умолчанию заменяем подытоги данными из БД.
     * Но если на фронте изменился тариф (сюда придет false),
     * то устанавливаем все значения подытогов в соответсятвии с тарифом, не смотря на то,
     * что данные могли быть изменены вручную
     * @return array
     */
    public function getResultRows(array $sql_data, bool $is_replace_with_exist_values_needed = true): array
    {
        if (!$sql_data) {
            return [];
        }

        $rows_cart = $this->getCartRows($sql_data);
        $this->addRates($rows_cart);

        if ($is_replace_with_exist_values_needed) {
            $this->replaceAllRowsSubtotalWithExistValues($rows_cart, $sql_data);
        }

        $this->addDefaultFields($rows_cart);

        return $this->getOrderedRows($this->getRowsCartData($rows_cart));
    }

    /**
     * @param array $rows_cart
     * @param array $sql_data
     */
    private function replaceAllRowsSubtotalWithExistValues(array &$rows_cart, array $sql_data)
    {
        foreach ($rows_cart as $key => $row) {
            $rows_cart[$key] = $this->replaceSubtotalWithExistValues($row, $sql_data);
        }
    }

    /**
     * @param array $rows_cart
     */
    private function addDefaultFields(array &$rows_cart)
    {
        $fields_default = [];
        foreach ($this->order_fields as $key_field => $field) {
            if (isset($field['type']) && (int)$field['type'] ===  FieldConfig::TYPE_CONST) {
                $fields_default[$key_field] = '';
                continue;
            }
            $fields_default[$key_field] = 0;
        }

        foreach ($rows_cart as $key_row => $row_cart) {
            $rows_cart[$key_row]['data'] = $row_cart['data'] + $fields_default;
        }
    }

    /**
     * @param array $rows_cart
     * @return array
     */
    private function getRowsCartData(array $rows_cart): array
    {
        $rows_cart_data = [];
        foreach ($rows_cart as $row_cart) {
            $rows_cart_data[] = $row_cart['data'];
        }

        return $rows_cart_data;
    }

    /**
     * Добавляет маржу к строкам
     * @param array $rows_cart
     */
    private function addRates(array &$rows_cart): void
    {
        foreach ($rows_cart as $key => $row) {
            $cargo_field_id = $this->field_codes_list[FieldConfig::TYPE_CONTAINER]['id'];
            $cargo_type = $row['data'][$cargo_field_id];

            foreach ($row['data'] as $field_id => $field_value) {
                if (!is_numeric($field_id)) {
                    continue;
                }
                if ($this->isFieldSubtotal($field_id)) {
                    $rows_cart[$key] = $this->getCartRowWithRate($rows_cart[$key], $field_id, $cargo_type);
                }
            }
        }
    }

    public function getResultRowsHeaders(array $field_ids = null): array
    {
        if (!$field_ids) {
            $field_ids = ArrayHelper::getColumn($this->order_fields, 'id');
        }

        return parent::getResultRowsHeaders($field_ids);
    }

    private function setFieldCodesList()
    {
        foreach ($this->order_fields as $field) {
            $this->field_codes_list[$field['code']] = $field;
        }
    }

    /**
     * @return array
     */
    private function getFieldsKeysIds(): array
    {
        if ($this->fields_keys_ids) {
            return $this->fields_keys_ids;
        }

        $this->fields_keys_ids = [];
        foreach ($this->order_fields as $field) {
            $this->fields_keys_ids[$field['id']] = $field;
        }

        return $this->fields_keys_ids;
    }

    /**
     * @param array $cart_tables_data
     * @return array
     */
    private function getCartRows(array $cart_tables_data): array
    {
        $field_ids_for_kp = $this->getFieldIdsForKp(FieldConfig::getConstForTunnelCart());
        $this->fields_subtotal = $this->getFieldsSubtotal();

        $row = [];
        $rows = [];
        $first_element_row = reset($cart_tables_data);
        $last_element_row = end($cart_tables_data);

        foreach ($cart_tables_data as $sql_row) {
            if ($this->isEndOfRow( $first_element_row, $sql_row)) {
                $rows[] = $row;
                $row = [];
            }

            $row['data']['additional_params']['cart_tables_data_ids'][] = $sql_row['id'];
            $row['data']['additional_params']['hash_row'] = $sql_row['hash_row'];

            if ($this->isFieldForKp($sql_row['field_id'], $field_ids_for_kp)) {
                $row['data']['additional_params']['fields_for_kp'][$sql_row['field_id']] = $sql_row['field_val'];
            }

            if (!$this->isForbiddenFields($sql_row['field_id'])) {
                $row['data'][$sql_row['field_id']] = $sql_row['field_val'];
            }

            if ($this->isLastElement($last_element_row, $sql_row)) {
                $rows[] = $row;
            }
        }

        return $rows;
    }

    /**
     * @param array $first_element_row
     * @param array $current_element_row
     * @return bool
     */
    protected function isEndOfRow(array $first_element_row, array $current_element_row): bool
    {
        return $first_element_row['id'] !== $current_element_row['id'] && $first_element_row['field_id'] === $current_element_row['field_id'];
    }

    /**
     * @param $last_element_row
     * @param $current_element_row
     * @return bool
     */
    protected function isLastElement(array $last_element_row, array $current_element_row) : bool
    {
        return $last_element_row['id'] === $current_element_row['id'];
    }

    /**
     * @param string $field_id
     * @return bool
     */
    private function isForbiddenFields($field_id) : bool
    {
        $forbidden_fields_ids =  [
            $this->field_codes_list[FieldConfig::RATE_FRAHT]['id'],
            $this->field_codes_list[FieldConfig::SUBTOTAL_FRAHT_OUT]['id'],
            $this->field_codes_list[FieldConfig::RATE_RW]['id'],
            $this->field_codes_list[FieldConfig::SUBTOTAL_RW_OUT]['id'],
            $this->field_codes_list[FieldConfig::RATE_AUTO]['id'],
            $this->field_codes_list[FieldConfig::SUBTOTAL_AUTO_OUT]['id']
        ];
        return in_array($field_id, $forbidden_fields_ids);
    }

    /**
     * @param string $field_id
     * @return bool
     */
    private function isFieldSubtotal($field_id) : bool
    {
        return in_array($field_id, array_keys($this->fields_subtotal));
    }

    /**
     * @param string $field_id
     * @param array $field_ids_for_kp
     * @return bool
     */
    private function isFieldForKp($field_id, array $field_ids_for_kp) : bool
    {
        return in_array($field_id, $field_ids_for_kp);
    }

    /**
     * @param string $field_val
     * @return bool
     */
    private function isExistValue($field_val): bool
    {
        return isset($field_val) && $field_val !== FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
    }

    /**
     * Получает из sql-данных ($cart_tables_data), новый массив c хэшами строк в качестве ключей вида:
     * [
     *      hash1 => [field_id1 => field_val1, ... field_idn => field_valn]
     *         ...
     *      hashn => [field_idn => field_valn]
     * ]
     * @param array $cart_tables_data
     * @return array
     */
    private function getIndexedCartTablesData(array $cart_tables_data): array
    {
        if (!$this->indexed_cart_tables_data) {
            foreach ($cart_tables_data as $sql_row) {
                $hash_row = $sql_row['hash_row'];
                $field_is = $sql_row['field_id'];
                $field_val = $sql_row['field_val'];
                $this->indexed_cart_tables_data[$hash_row][$field_is] = $field_val;
            }
        }

        return $this->indexed_cart_tables_data;
    }

    /**
     * Возвращает массив полей-подитогов вида: [field_code1 => field_id1, ... field_coden => field_idn]
     * @return array
     */
    private function getSubtotalAndRateFieldIds(): array
    {
        return [
            FieldConfig::SUBTOTAL_FRAHT_IN => $this->field_codes_list[FieldConfig::SUBTOTAL_FRAHT_IN]['id'],
            FieldConfig::RATE_FRAHT => $this->field_codes_list[FieldConfig::RATE_FRAHT]['id'],
            FieldConfig::SUBTOTAL_FRAHT_OUT => $this->field_codes_list[FieldConfig::SUBTOTAL_FRAHT_OUT]['id'],
            FieldConfig::SUBTOTAL_RW_IN =>  $this->field_codes_list[FieldConfig::SUBTOTAL_RW_IN]['id'],
            FieldConfig::RATE_RW => $this->field_codes_list[FieldConfig::RATE_RW]['id'],
            FieldConfig::SUBTOTAL_RW_OUT=> $this->field_codes_list[FieldConfig::SUBTOTAL_RW_OUT]['id'],
            FieldConfig::SUBTOTAL_AUTO_IN => $this->field_codes_list[FieldConfig::SUBTOTAL_AUTO_IN]['id'],
            FieldConfig::RATE_AUTO=> $this->field_codes_list[FieldConfig::RATE_AUTO]['id'],
            FieldConfig::SUBTOTAL_AUTO_OUT => $this->field_codes_list[FieldConfig::SUBTOTAL_AUTO_OUT]['id'],
        ];
    }

    /**
     * Заменяет вычесленные на основе дефолтной маржи подитоги на реальные значения из БД, если таковые существуют.
     * @param array $row
     * @param array $cart_tables_data
     * @return array
     */
    private function replaceSubtotalWithExistValues(array $row, array $cart_tables_data): array
    {
        $field_ids = $this->getSubtotalAndRateFieldIds();
        $indexed_cart_tables_data = $this->getIndexedCartTablesData($cart_tables_data);
        $hash_row = ($row['data']['additional_params']['hash_row']);
        foreach ($field_ids as $field_id) {
            $field_val = $indexed_cart_tables_data[$hash_row][$field_id];
            if ($this->isExistValue($field_val)) {
                $row['data'][$field_id] = $field_val;
            }
        }

        return $row;
    }

    /**
     * @param array $row
     * @param int $subtotal_field_id
      * @param string $cargo_type
     * @return array
     */
    private function getCartRowWithRate(array $row, int $subtotal_field_id, string $cargo_type): array
    {
        $rate = $this->rates ? $this->getRateValue($this->fields_keys_ids, $subtotal_field_id, $cargo_type) : 0;
        $subtotal_with_rate =  $this->isExistValue($row['data'][$subtotal_field_id]) ? (int)$row['data'][$subtotal_field_id] + $rate : FieldConfig::FIELD_VAL_NOT_EXIST_VALUE;
        $field_ids = $this->getSubtotalAndRateFieldIds();
        switch ($subtotal_field_id) {
            case (int)$field_ids[FieldConfig::SUBTOTAL_FRAHT_IN]:
                $row['data'][$field_ids[FieldConfig::RATE_FRAHT]] =$rate;
                $row['data'][$field_ids[FieldConfig::SUBTOTAL_FRAHT_OUT]] = $subtotal_with_rate;
                break;
            case (int)$field_ids[FieldConfig::SUBTOTAL_RW_IN]:
                $row['data'][$field_ids[FieldConfig::RATE_RW]] = $rate;
                $row['data'][$field_ids[FieldConfig::SUBTOTAL_RW_OUT]] = $subtotal_with_rate;
                break;
            case (int)$field_ids[FieldConfig::SUBTOTAL_AUTO_IN]:
                $row['data'][$field_ids[FieldConfig::RATE_AUTO]] = $rate;
                $row['data'][$field_ids[FieldConfig::SUBTOTAL_AUTO_OUT]] = $subtotal_with_rate;
                break;
        }

        return $row;
    }


    /**
     * @param array $fields_key_ids
     * @param int $field_id
     * @param string $cargo_type
     * @return float
     */
    private function getRateValue(array $fields_key_ids, int $field_id, string $cargo_type): float
    {
        foreach ($this->rates as $rate) {
            if ($rate['type'] == $fields_key_ids[$field_id]['type_of_delivery'] && $rate['cargo_type'] === $cargo_type) {
                return (float)$rate['value'];
            }
        }

        return 0.0;
    }

    /**
     * @return FieldConfig[]
     */
    private function getFieldsSubtotal(): array
    {
        $codes = FieldConfig::getSubtotalCodes();
        $fields_subtotal = FieldConfig::find()
            ->select(['id', 'currency_id', 'type_of_delivery'])
            ->where([
                'company_id' => $this->company_id,
                'code' => $codes,
                'is_deleted' => FieldConfig::IS_NOT_DELETED
            ])
            ->indexBy('id')
            ->all();

        return $fields_subtotal;
    }


    /**
     * Получает id полей по кодам, которые не отображаются в корзине, но перейдут в КП
     *
     * @param array $codes_for_kp
     * @return array
     */
    private function getFieldIdsForKp(array $codes_for_kp): array
    {
        $fields_subtotal = FieldConfig::find()
            ->select(['id', 'currency_id', 'type_of_delivery'])
            ->where([
                'company_id' => $this->company_id,
                'code' => $codes_for_kp,
                'is_deleted' => FieldConfig::IS_NOT_DELETED
            ])
            ->indexBy('id')
            ->column();

        return $fields_subtotal;
    }

    protected function getOrderedRowWithAddParams(array $row): array
    {
        $ordered_row = $this->getOrderedRow($row);

        if ($row['additional_params']) {
            $ordered_row['additional_params'] = $row['additional_params'];
        }

        if (!empty($this->hash_checked_rows) && in_array($ordered_row['additional_params']['hash_row'], $this->hash_checked_rows)) {
            $ordered_row['checked'] = true;
        }

        if (!empty($this->is_update_row)) {

            return $ordered_row;
        }

        if (in_array($ordered_row['additional_params']['hash_row'], $this->getKpHashCodes())) {
            $ordered_row['checked'] = true;
        }

        return $ordered_row;
    }

    /**
     * Метод возвращает массив полей которые можно редактировать
     *
     * @param array $ordered_title_fields
     * @return array
     */
    public function getEditableFields(array $ordered_title_fields): array
    {
        $editable_rows = [];
        $editable_fields_for_cart = self::getEditableFieldNames();
        foreach ($ordered_title_fields as $key_order_table => $field) {
            if(in_array($field['code'],$editable_fields_for_cart)) {
                $editable_rows[] = $key_order_table;
            }
        }

        return $editable_rows;
    }

    /**
     * @return array
     */
    public static function getEditableFieldNames()
    {
        return [
            FieldConfig::SUBTOTAL_FRAHT_OUT,
            FieldConfig::RATE_FRAHT,
            FieldConfig::SUBTOTAL_RW_OUT,
            FieldConfig::RATE_RW,
            FieldConfig::SUBTOTAL_AUTO_OUT,
            FieldConfig::RATE_AUTO,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * Получаем формулы для подсчета подитога(вхб исх),
     * маржи при редактирование полей в корзине
     *
     * @return array
     */
    public function getFormulasForCart(): array
    {
        $formulas_calculate = CartTablesData::getFormulasCalculateSubtotal();
        $fields_codes = ArrayHelper::getColumn($this->order_fields, 'code');
        $fields_order_ids = array_keys($this->order_fields);

        $fields_codes_on_ids = [];
        foreach ($this->order_fields as $order_field_id => $field) {
            $fields_codes_on_ids[$field['code']] = $order_field_id;
        }

        foreach ($formulas_calculate as $key_formula => $formula_calculate){
            if (isset($fields_codes_on_ids[$key_formula]) && $fields_codes_on_ids[$key_formula]) {
                $new_key_formula = $fields_codes_on_ids[$key_formula];
                $formulas_calculate[$new_key_formula] = $formulas_calculate[$key_formula];
                unset($formulas_calculate[$key_formula]);
                $key_formula = $new_key_formula;
            }

            switch ($formula_calculate['type_field']) {
                case FieldConfig::CODE_FOR_CART_CALC_SUBTOTAL:
                    $formulas_calculate[$key_formula]['formula'] = str_replace($fields_codes, $fields_order_ids, $formula_calculate['formula']);
                    $formulas_calculate[$key_formula]['formula_for_recalculate'] = str_replace($fields_codes, $fields_order_ids, $formula_calculate['formula_for_recalculate']);
                    break;
                case FieldConfig::CODE_FOR_CART_CALC_RATE:
                    $formulas_calculate[$key_formula]['formula'] = str_replace($fields_codes, $fields_order_ids, $formula_calculate['formula']);
                    break;
                case FieldConfig::CODE_FOR_CART_CALC_RESULT:
                    $formulas_calculate[$key_formula]['calculated_field'] = str_replace($fields_codes, $fields_order_ids, $formula_calculate['calculated_field']);
                    $formulas_calculate[$key_formula]['formula_for_recalculate'] = str_replace($fields_codes, $fields_order_ids, $formula_calculate['formula_for_recalculate']);
                    break;
            }
            $formulas_calculate[$key_formula]['related_fields'] = str_replace($fields_codes, $fields_order_ids, $formula_calculate['related_fields']);
        }

        return $formulas_calculate;
    }

    /**
     * Получаем hash коды строк участвующих в рассчете для конкретной клиенсткой компании
     *
     * @return array
     */
    private function getKpHashCodes(): array
    {
        if (!$this->kp_hash_codes) {
            $this->kp_hash_codes = KpTablesData::find()
                ->distinct(true)
                ->select(['kp_tables_data.hash_row as hash_row'])
                ->leftJoin('offers', 'offers.id = kp_tables_data.offer_id')
                ->where([
                    'kp_tables_data.client_company_id' => $this->client_company_id,
                    'offers.status' => Offer::STATUS_NEW,
                    'offers.is_archived' => Offer::NOT_ARCHIVED,
                    'offers.is_deleted' => Offer::NOT_DELETED,
                ])
                ->column();
        }

        return $this->kp_hash_codes;
    }

    /**
     * Возвращает массив field_config->order_in_cart => currency_sign(например $) вида:
     * [ (int)order_in_cart1 => (string)currency_sign1, ..., (int)order_in_cartN => (string)currency_signN ]
     *
     * @return array
     */
    public function getFieldOrdersToCurrencySigns(): array
    {
        $field_orders_to_currency_signs = [];
        if (!$this->order_fields) {
            return $field_orders_to_currency_signs;
        }

        foreach ($this->order_fields as $field_order => $field) {
            $currency_sign = Currency::getCurrencySignByIsoCode($field['currency_iso_code']);
            $field_orders_to_currency_signs[$field_order] = $currency_sign;
        }
        ksort($field_orders_to_currency_signs);

        return $field_orders_to_currency_signs;
    }

    public function setCartVersionModel(): CartRowProcessor
    {
        $this->cart_version_model = CartVersion::getCurrentVersionModel($this->company_id, $this->client_company_id, $this->user_id);

        return $this;
    }

    public function getCartVersionModel(): ?CartVersion
    {
        return $this->cart_version_model;
    }

    /**
     * Возвращает массив id первых полей каждой строки в cart_tables_data_ids
     * @param $rows
     * @return array
     */
    private function getCartTableFieldIds($rows): array
    {
        $cart_tables_data_ids = [];
        foreach ($rows as $index => $field) {
            if (isset($field['additional_params']['cart_tables_data_ids'][0])) {
                $cart_tables_data_ids[$index] = $field['additional_params']['cart_tables_data_ids'][0];
            }
        }
        return $cart_tables_data_ids;
    }

    /**
     * Возвращает массив id полей для additional_params
     * @param $code_additional_fields
     * @return array
     */
    private function getCartTableAdditionalFieldIds($code_additional_fields)
    {
        $field_codes = array_keys($code_additional_fields);
        return FieldConfig::find()
            ->select(['id'])
            ->where([
                'company_id' => $this->company_id,
                'code' => $field_codes,
                'is_deleted' => FieldConfig::IS_NOT_DELETED
            ])
            ->indexBy('code')
            ->column();
    }

    /**
     * Добавляет additional_params к строкам
     * @param $rows
     * @param $key_row
     * @param $cart_table_model
     * @return array
     */
    private function addAdditionalParamsToRow($rows, $key_row, $cart_table_model): array
    {
        $additional_fields_codes = FieldConfig::getCodeOfSearchFormFields();
        $additional_field_ids = $this->getCartTableAdditionalFieldIds($additional_fields_codes);

        foreach ($additional_fields_codes as $code => $field_name) {
            $field_value = null;
            switch ($code) {
                case FieldConfig::WEIGHT:
                    $field_value = !empty($cart_table_model[$field_name]) ? '>=24т.' : '<24т.';
                    break;
                case FieldConfig::PLACE_TO:
                    $custom_types = PublicApiController::$custom_types;
                    foreach ($custom_types as $custom_type) {
                        if ($cart_table_model[$field_name] == $custom_type['id']) {
                            $field_value = $custom_type['title'];
                            break;
                        }
                    }
                    break;
                default:
                    $field_value = !empty($cart_table_model[$field_name]) ? 'Да' : 'Нет';
                    break;
            }
            $additional_field_id = $additional_field_ids[$code];
            $rows[$key_row]['additional_params']['only_kp_fields'][$additional_field_id] = $field_value;
            $rows[$key_row][$additional_field_id] = $field_value;
        }
        return $rows;
    }

    /**
     * @param array $rows
     * @return array
     */
    public function getRowsForKp($rows): array
    {
        $cart_tables_data_ids = $this->getCartTableFieldIds($rows);
        $cart_tables_data = CartTablesData::getCartTableFieldsForKp($cart_tables_data_ids);

        foreach ($rows as $key_row => $row) {
            $hash_row = $row['additional_params']['hash_row'];
            $cart_table_model = $cart_tables_data[$hash_row];

            $rows[$key_row] = $this->getOriginalRow($row);
            $rows[$key_row]['additional_params']['offer_id'] = $cart_table_model['offer_id'];
            $rows[$key_row]['additional_params']['client_company_id'] = $cart_table_model['client_company_id'];

            $rows = $this->addAdditionalParamsToRow($rows, $key_row, $cart_table_model);
        }

        return $this->getFieldIdsForRowKeys($rows);
    }

    /**
     * Устанавливает id полей для ключей в массиве строк
     * @param $rows
     * @return array
     */
    public function getFieldIdsForRowKeys($rows) :array
    {
        $intersect_key_field_ids_for_search_and_offers = KpTablesData::getIntersectKeyFieldIdsForSearchAndOffers($this->company_id);
        $result_rows = [];

        foreach ($rows as $key_row => $row) {
            if (!is_array($row)) {
                continue;
            }
            $hash_row_key_field = KpTablesData::getKeyFieldsRowHash($row, $intersect_key_field_ids_for_search_and_offers);
            $row['hash_row_key_field'] = $hash_row_key_field;
            $result_rows[] = $row;
        }
        return $result_rows;
    }

    /** @inheritdoc */
    public function getValidationRules() : array
    {
        $subtotal_fraht_in_field_order = $this->getOrderByCode(FieldConfig::SUBTOTAL_FRAHT_IN, 'order_in_cart');
        $rate_fraht_field_order = $this->getOrderByCode(FieldConfig::RATE_FRAHT, 'order_in_cart');
        $subtotal_fraht_out_field_order = $this->getOrderByCode(FieldConfig::SUBTOTAL_FRAHT_OUT, 'order_in_cart');

        $subtotal_rw_in_field_order = $this->getOrderByCode(FieldConfig::SUBTOTAL_RW_IN, 'order_in_cart');
        $subtotal_rw_out_field_order = $this->getOrderByCode(FieldConfig::SUBTOTAL_RW_OUT, 'order_in_cart');
        $subtotal_rw_rate_field_order = $this->getOrderByCode(FieldConfig::RATE_RW, 'order_in_cart');

        $subtotal_auto_in_field_order = $this->getOrderByCode(FieldConfig::SUBTOTAL_AUTO_IN, 'order_in_cart');
        $subtotal_auto_out_field_order = $this->getOrderByCode(FieldConfig::SUBTOTAL_AUTO_OUT, 'order_in_cart');
        $subtotal_auto_rate_field_order = $this->getOrderByCode(FieldConfig::RATE_AUTO, 'order_in_cart');

        $date_actual_field_order = $this->getOrderByCode(FieldConfig::DATE_ACTUAL_CODE, 'order_in_cart');
        $in_way_field_order = $this->getOrderByCode(FieldConfig::IN_WAY, 'order_in_cart');

        $validation_rules = [];
        if ($in_way_field_order) {
            $validation_rules[$in_way_field_order] = [
                'required' => false,
                'methods' => [
                    'isNumericInt' => [
                        'message' => 'Значение должно быть целым числом'
                    ],
                    'maxInt' => [
                        'message' => 'Значние не должно превышать 365',
                        'rule' => ['max' => 365]
                    ],
                    'minInt' => [
                        'message' => 'Значние не должно быть меньше 0',
                        'rule' => ['min' => 0]
                    ]
                ]
            ];
        }

        if ($subtotal_fraht_in_field_order) {
            $validation_rules[$subtotal_fraht_in_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($rate_fraht_field_order) {
            $validation_rules[$rate_fraht_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_fraht_out_field_order) {
            $validation_rules[$subtotal_fraht_out_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_rw_in_field_order) {
            $validation_rules[$subtotal_rw_in_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_rw_out_field_order) {
            $validation_rules[$subtotal_rw_out_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_rw_rate_field_order) {
            $validation_rules[$subtotal_rw_rate_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_auto_in_field_order) {
            $validation_rules[$subtotal_auto_in_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_auto_out_field_order) {
            $validation_rules[$subtotal_auto_out_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($subtotal_auto_rate_field_order) {
            $validation_rules[$subtotal_auto_rate_field_order] = [
                'required' => true,
                'methods' => [
                    'isNumericFloat' => [
                        'message' => 'Значение должно быть числом'
                    ],
                ]
            ];
        }

        if ($date_actual_field_order) {
            $validation_rules[$date_actual_field_order] = [
                'required' => true,
                'methods' => [
                    'isDate' => [
                        'message' => 'Значение должно датой в формате DD.MM.YYYY',
                        'rule' => ['format' => 'DD.MM.YYYY']
                    ],
                ]
            ];
        }

        return $validation_rules;
    }
}