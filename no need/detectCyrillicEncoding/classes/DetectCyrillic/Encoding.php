<?php

namespace app\classes\detectCyrillicEncoding\classes\DetectCyrillic;

/**
 * Определяет кодировку русского текста.
 */
class Encoding
{
	private $possible_encodings = array("windows-1251", "koi8-r", "iso8859-5");
	private $specters = array();

	function __construct()
	{
		$this->setSpecterPath(__DIR__ . "/../../specters/");
	}

	/**
	 * Определяет кодировку переданного текста.
	 * Принцип работы:
	 * для каждой проверяемой кодировки находим частоту текущего символа
	 * и прибавляем к «рейтингу» этой кодировки.
	 * Кодировка с бОльшим рейтингом и есть, скорее всего, кодировка текста.
	 * @param string $content
	 * @return false|int|string
	 */
	public function detectMaxRelevant(string $content)
	{
		if(!$content or preg_match('#.#u', $content))
		{
			return "utf-8";
		}
		else
		{
			$weights = $this->getWeights($content);
			return array_search(max($weights), $weights);
		}
	}

	private function setSpecterPath($path)
	{
		foreach ($this->possible_encodings as $encoding)
		{
			$this->specters[$encoding] = require rtrim($path, "/")."/".$encoding.".php";
		}
	}

	private function getWeights($content)
	{
		$weights = array_fill_keys($this->possible_encodings, 0);

		if(preg_match_all("#(?<let>.{2})#", $content, $matches))
		{
			foreach($matches['let'] as $key)
			{
				foreach ($this->possible_encodings as $encoding)
				{
					if(isset($this->specters[$encoding][$key]))
					{
						$weights[$encoding] += $this->specters[$encoding][$key];
					}
				}
			}
		}

		$weightSum = array_sum($weights);
		foreach ($weights as $encoding => $weight)
		{
			$weights[$encoding] = $weightSum?($weight / $weightSum):0;
		}

		return $weights;
	}
}

?>
