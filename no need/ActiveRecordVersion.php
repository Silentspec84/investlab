<?php

namespace app\classes\db\mysql;

use app\classes\helpers\DateHelper;
use Throwable;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 *  Надстройка для ActiveRecord, для реализации версионности моделей
 */
class ActiveRecordVersion extends ActiveRecord
{
    const IS_DELETED = 1;
    const IS_NOT_DELETED = 0;

    const ERROR_EXIST_RECORD = 'Такая запись уже существует, отредактируйте или удалите её.';
    const ERROR_MISSING_FIELD_IS_DELETED = 'У версионной модели отстутствует поле is_deleted, добавьте его!';

    /**
     * @var int Дата в timestamp,
     * задает верхнюю границу date_created для выборки данных из версионных таблиц,
     * т.е. определяет, какую версию набора данных забрать из таблиц
     */
    public static $date_version = null;


    /**
     * Валидация уникальности поля, в таблице, для набора конкретной версии
     *
     * @param $attribute
     */
    public function validateUniqueInVersion($attribute)
    {
        if ($this->getIsNewRecord() && $this->isExistRecord($attribute)) {
            $this->addError($attribute, self::ERROR_EXIST_RECORD);
        }
    }

    /**
     * Переопределяем find, делаем join,
     * для получения нужной версии данных(записей с уникальными id) с максимальной date_created < static::date_version,
     * при отсутствии static::date_version получаем текущую версию набора данных, при этом
     * условие date_created < static::date_version опускается
     *
     * @return ActiveQuery|object
     */
    public static function find()
    {
        return parent::find()->innerJoin(static::getJoinSubQueryTable(), static::getJoinCondition());
    }

    /**
     * Переопределенный save всегда делает insert, валидация происходит до вызова insert()
     *
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws Throwable
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        /** Валидация вынесена выше. т.к. дальше будут затерты  _OldAttributes */
        if ($runValidation && !$this->validate($attributeNames)) {
            return false;
        }

        if ($this->getIsNewRecord()) {
            $this->id = static::getIncrementId();
            $is_deleted =  self::IS_NOT_DELETED;
        } else {
            $is_deleted =  $this->is_deleted;
        }

        $this->setModelDates(self::getCurrentDateTimeInMillisecond())
             ->setModelIsDeleted($is_deleted)
             ->setOldAttributes(null); //Очищаем старые аттрибуты, т.к. у обновляемой модели происходит insert только измененных полей, а нужны все поля.

        return $this->insert(false, $attributeNames);
    }

    /**
     * Переопределяем update, т.к. запись не может обновляться, "обновление" происходит через updateAll()
     *
     * @see updateAll()
     * @param bool $runValidation
     * @param null $attributeNames
     * @throws Throwable
     */
    public function update($runValidation = true, $attributeNames = null)
    {
        throw new InvalidConfigException(static::class . ': нельзя вызывать update() у версионных моделей, воспользуйтесь updateAll().');
    }

    /**
     * Переобпределенный updateAll, вместо привычного update делает batchInsert обновляемых данных
     * Возвращает количество "обновленных" строк
     *
     * @param array $attributes
     * @param string|array $condition
     * @param array $params
     * @return int
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     * @throws Throwable
     */
    public static function updateAll($attributes, $condition = '', $params = []): int
    {
        if (!$attributes) {
            throw new InvalidConfigException('Не передан параметр $attributes');
        }
        $models = static::getModelsByCondition($condition);
        if (!$models) {
            return 0;
        }
        $columns = static::getTableColumns();
        if (!$columns) {
            throw new Exception('Не удалось получить столбцы таблицы ' . static::tableName());
        }
        $rows_for_update = static::getRowsForUpdate($attributes, $models);

        try {
            \Yii::$app->db->createCommand()->batchInsert(static::tableName(), $columns, $rows_for_update)->execute();
        } catch (Exception $e) {
            throw new Exception('Не удалось сделать batchInsert для таблицы ' . static::tableName() . ': ' . $e->getMessage());
        }

        return count($rows_for_update);
    }

    /**
     * Переопределяем delete, т.к. запись не может удаляться, "удаление" происходит через deleteAll()
     *
     * @see deleteAll()
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function delete()
    {
        throw new InvalidConfigException(static::class . ': нельзя вызывать delete() у версионных моделей, воспользуйтесь deleteAll().');
    }

    /**
     * Переобпределенный deleteAll, вместо привычного delete делает batchInsert удаляемых данных
     * Возвращает количество "удаленных" строк
     *
     * @param null $condition
     * @param array $params
     * @return int
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     * @throws Throwable
     */
    public static function deleteAll($condition = null, $params = [])
    {
        $models = static::getModelsByCondition($condition);
        if (!$models) {
            return 0;
        }
        $columns = static::getTableColumns();
        if (!$columns) {
            throw new Exception('Не удалось получить колонки таблицы ' . static::tableName());
        }
        $rows_for_delete = static::getRowsForDelete($models);

        try {
            \Yii::$app->db->createCommand()->batchInsert(static::tableName(), $columns, $rows_for_delete)->execute();
        } catch (\yii\db\Exception $e) {
            throw new Exception('Не удалось сделать batchInsert для таблицы ' . static::tableName() . ': ' . $e->getMessage());
        }

        return count($rows_for_delete);
    }

    /**
     * Действительно удаляет строки.
     * Иногда это нужно даже в версионной модели, когда хотим ее например очистить для определенной компании.
     *
     * @param null $condition
     * @param array $params
     * @return int
     */
    public static function forceDeleteAll($condition = null, array $params = []): int
    {
        return parent::deleteAll($condition, $params);
    }

    /**
     * Возвращает текущую дату в миллисекундах
     * @return int
     */
    public static function getCurrentDateTimeInMillisecond(): int
    {
       return (int)round(microtime(true) * 1000);
    }

    /**
     * Проверка существования записи в таблице по аттрибуту, для набора записей под конкретную версию
     *
     * @param $attribute
     * @return bool
     */
    private function isExistRecord($attribute): bool
    {
        return static::find()
            ->where([
                $attribute => $this->$attribute,
                'is_deleted' => self::IS_NOT_DELETED
            ])
            ->andWhere(static::getCompanyIdCondition($this->company_id))
            ->exists();
    }

    /**
     * @param int|null $company_id
     * @return string
     */
    private static function getCompanyIdCondition(int $company_id = null): string
    {
        return !empty($company_id) ? 'company_id = '. $company_id : '1';
    }

    /**
     * Возвращает массив аттрибутов моделей($model->getAttributes()) с обновленными данными.
     *
     * @param $attributes
     * @param ActiveRecordVersion[] $models
     * @return array
     * @throws \Exception
     */
    public static function getRowsForUpdate($attributes, $models)
    {
        $now = self::getCurrentDateTimeInMillisecond();
        $rows_for_update = [];
        foreach ($models as $model) {
            /** @var ActiveRecordVersion $model */
            foreach ($attributes as $column_name => $column_value) {
                if (!$model->hasAttribute($column_name)) {
                    throw new \Exception($column_name . ' не является аттрибутом');
                }
                $model->$column_name = $column_value;
            }
            $model->setModelDates($now);
            $rows_for_update[] = $model->getAttributes();
        }

        return $rows_for_update;
    }

    /**
     * Возвращает массив аттрибутов моделей($model->getAttributes()) с обновленными данными
     * и проставленным полем is_deleted со значением self::IS_DELETED.
     *
     * @param ActiveRecordVersion[] $models
     * @return array
     * @throws InvalidConfigException
     */
    private static function getRowsForDelete($models)
    {
        $now = self::getCurrentDateTimeInMillisecond();
        $rows_for_delete = [];
        foreach ($models as $model) {
            /** @var ActiveRecordVersion $model */
            if (!$model->hasAttribute('is_deleted')) {
                throw new InvalidConfigException(self::ERROR_MISSING_FIELD_IS_DELETED);
            }
            $model->setModelDates($now);
            $model->setModelIsDeleted(self::IS_DELETED);
            $rows_for_delete[] = $model->getAttributes();
        }

        return $rows_for_delete;
    }

    /**
     * Уставнавливаем модели date_created и date_modified переданную $date,
     * если $date не передана устанавливаем текущую дату
     *
     * @param int $date
     * @return $this
     */
    private function setModelDates(int $date)
    {
        $this->date_created = $date;
        $this->date_modified = $date;

        return $this;
    }

    /**
     * Устанавливаем значение поля is_deleted
     *
     * @param int $is_deleted
     * @return ActiveRecordVersion
     */
    private function setModelIsDeleted(int $is_deleted)
    {
        if (!$this->hasAttribute('is_deleted')) {
            $this->addError('is_deleted', self::ERROR_MISSING_FIELD_IS_DELETED);
            return $this;
        }
        $this->is_deleted = $is_deleted;

        return $this;
    }

    /**
     * Получение моделей по $condition
     *
     * @param string|array|null $condition
     * @return ActiveRecordVersion[]
     */
    private static function getModelsByCondition($condition = null)
    {
        $query = static::find();
        if ($condition) {
            $query->andWhere($condition);
        }

        return $query->all();
    }

    /**
     * Возвращает массив из наименований колонок таблицы
     *
     * @return array
     * @throws InvalidConfigException
     */
    public static function getTableColumns(): array
    {
        return array_keys(static::getTableSchema()->columns);
    }

    /**
     * Возвращает подзапрос, генерирующий таблицу с нужной версией данных
     *
     * @return string
     */
    private static function getJoinSubQueryTable(): string
    {
        return '(SELECT id as select_id, max(date_created) as max_date_created
            FROM ' . static::tableName() .
            static::getDateVersionCondition() .
            ' GROUP BY select_id) version_table';
    }

    /**
     * Возвращает условие выбора максимальной date_created <= static::date_version
     *
     * @return string
     */
    private static function getDateVersionCondition(): string
    {
        static::setDateVersionInTimestampMS();
        return static::$date_version > 0 ? ' WHERE date_created <= ' . static::$date_version  : '';
    }

    /**
     * Конвертирует date_version в timestamp в микросекундах(13значный int), иначе 0
     */
    private static function setDateVersionInTimestampMS(): bool
    {
        if (DateHelper::isDateInTimestamp(static::$date_version)) {
            static::$date_version = (int)static::$date_version * 1000;
            return true;
        }
        if (!DateHelper::isDateInTimestampMS(static::$date_version)) {
            static::$date_version = 0;
            return false;
        }

        return true;
    }

    /**
     * Возвращает условие склейки таблиц: ON для inner Join version_table
     *
     * @return string
     */
    private static function getJoinCondition(): string
    {
        return static::tableName() . '.id = select_id AND ' .static::tableName() . '.date_created = max_date_created';
    }

    /**
     * Возвращает max id + 1 из таблицы
     *
     * @return int
     */
    public static function getIncrementId(): int
    {
        $last_model = static::find()
            ->select('MAX(id) as id')
            ->one();
        $new_id = $last_model->id ?? 0;

        return ++$new_id;
    }

    /**
     * Для миграции, чтобы сделать версионный primary key
     *
     * @param string $table_name
     * @return string
     */
    public static function getModifyPrimaryKeySql(string $table_name): string
    {
        $sql = <<<SQL
ALTER TABLE {$table_name} MODIFY id INT NOT NULL;
ALTER TABLE {$table_name} DROP PRIMARY KEY;
ALTER TABLE {$table_name} ADD PRIMARY KEY (id, date_created);
SQL;
        return $sql;
    }
}