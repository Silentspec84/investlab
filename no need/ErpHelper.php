<?php
namespace app\classes\helpers;

use app\commands\FileCacheController;
use app\components\erp\AbstractErpConnector;
use app\components\erp\ErpConnectorsFactory;
use app\components\erp\exceptions\ErpConfigException;
use app\components\erp\exceptions\ErpConnectionException;
use app\components\Logger;
use app\components\rabbitmq\RabbitSenderErp;
use app\models\erp\entities\ErpOrderFile;
use app\models\erp\ErpConfig;
use app\models\erp\ErpOrder;
use app\models\erp\ErpImportStatus;
use yii\mongodb\Exception;

/**
 * Содержит методы для работы с erp
 *
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * @package app\classes\helpers
 */
class ErpHelper
{
	/** @var Logger */
	protected $error_logs;
	/** @var Logger */
	protected $info_logs;
	/** @const int количество раз повторной постановки задачи в очередь */
	public const ATTEMPTS_COUNT = 3;

	public function __construct()
	{
		$this->error_logs = Logger::i(Logger::CAT_RABBIT, true)
			->setType(Logger::TYPE_ERROR)
			->setTtl(Logger::TTL_WEEK)
			->setFlush(true);

		$this->info_logs = Logger::i(Logger::CAT_RABBIT, true)
			->setType(Logger::TYPE_INFO)
			->setTtl(Logger::TTL_WEEK)
			->setFlush(true);
	}

	/**
	 * @param array $erp_orders
	 * @return int
	 */
	public function deleteOrdersFiles(array $erp_orders): void
	{
		foreach ($erp_orders as $order) {
			$order->save();// файлы будут удалены т.к. в методе beforeSave будет вызван deleteOrderFiles
		}
	}

	/**
	 * Удаляет загруженные файлы у всех заказов для компании
	 * @param int $company_id
	 */
	public function deleteOrderFilesByCompanyId(int $company_id): void
	{
		$erp_orders = ErpOrder::find()
			->where([
				'company_id' => $company_id,
				'files_download_status' => ErpOrder::STATUS_FILES_DOWNLOADED,
			])
			->all();

		$this->deleteOrdersFiles($erp_orders);
	}

	/**
	 * Удаляет заказы у компании
	 * @param int $company_id
	 * @return int
	 */
	public function deleteOrdersByCompanyId(int $company_id): int
	{
		return ErpOrder::deleteAll(['company_id' => $company_id]);
	}

	/**
	 * Ставит в очередь задания на импорт заказов.
	 * @param int | NULL $company_id - если не передан, то поставит в очередь задания на импорт заказов для всех компаний
	 * @return int Общее количество заказов в erp клиента
	 */
	public function queueGettingDataFromErp(int $company_id = null): int
	{
		$erp_config_list = ErpConfig::find();
		if ($company_id) {
			$erp_config_list = $erp_config_list->where(['company_id' => $company_id]);
		}
		$erp_config_list = $erp_config_list->all();

		$logger = Logger::i(Logger::CAT_RABBIT)
			->setType(Logger::TYPE_INFO)
			->setTtl(Logger::TTL_DAY)
			->setFlush(true);
		$total_remote_orders_count = 0;
		/** @var ErpConfig $erp_config */
		foreach ($erp_config_list as $erp_config) {
			/** @var  AbstractErpConnector $connector Получить соединение с ЕРП */
			$connector = $this->getConnector($erp_config, false);
			if (!$connector) {
				$logger->save('Пустой коннектор для erp_config: '.serialize($erp_config));
				continue;
			}
			$this->saveFilesToCache($connector, $erp_config);
			$total_remote_orders_count = $erp_config->getOrderCount();
			for ($offset = 0; $offset <= $total_remote_orders_count; $offset += $erp_config->getChunkSize()) {
				$order_ids = $this->getOrderIds($connector, $erp_config, ['offset' => $offset, 'limit' => $erp_config->getChunkSize()]);
				$this->queueGettingDataFromErpByIds($order_ids, $erp_config->company_id);
				$logger->save("Queued $offset orders in rabbit for company " . $erp_config->company_id);
			}
		}
		unset($erp_config_list);

		return $total_remote_orders_count;
	}

	/**
	 * @param array $order_ids
	 * @param int $company_id
	 */
	public function queueGettingDataFromErpByIds(array $order_ids, int $company_id): void
	{
		if (!$order_ids) {
			$this->error_logs->save(['Не удалось получить id заказов'], false);
			return;
		}

		$message['id'] = $company_id;
		$message['params']['where_in'] = $order_ids;
		$message['attempts_count'] = 1;
		(new RabbitSenderErp())->setMessage(serialize($message))->send();
	}

	/**
	 * Сохраняет массив ErpOrderFile[], полученный на основе массива с данными о файлах, пришедшем из ERP-системы компании,
	 * в файловый кэш.
	 * @param AbstractErpConnector $connector
	 * @param ErpConfig $erp_config
	 */
	private function saveFilesToCache($connector, $erp_config): void
	{
		if (!$erp_config->getFilesQuery()) {
			$this->error_logs->save(['Нет запроса для erp конфига: ', serialize($erp_config)], true);
			return;
		}

		$files_from_erp = [];
		try {
			$files_from_erp = $connector->getResponse($erp_config->getFilesQuery());
		} catch (ErpConnectionException $e) {
			$this->error_logs->save(['Request error: ', serialize($e->getMessage())], true);
		} catch (ErpConfigException $e) {
			$this->error_logs->save(['Connection error: ', serialize($e->getMessage())], true);
		}

		FileCacheController::set($erp_config->getErpFilesCacheFileName(), ErpOrderFile::generate($erp_config->company_id, $files_from_erp));
	}


	/**
	 * @param ErpConfig $erp_config
	 * @param bool $renew
	 * @return AbstractErpConnector|null
	 */
	public function getConnector(ErpConfig $erp_config, bool $renew): ?AbstractErpConnector
	{
		/** Получение класса-коннектора для коннекта с ERP */
		try {
			$connector = ErpConnectorsFactory::createConnector($erp_config->company_id, $erp_config->type, $renew);
			$connector->init($erp_config->getConnectionConfig());

		} catch (ErpConfigException $e) {
			$this->error_logs->save(['Error trying to get connector: ', $e->getMessage()], true);
			return null;
		} catch (ErpConnectionException $e) {
			$this->error_logs->save(['Connection error: ', $e->getMessage()], true);
			return null;
		}
		$this->info_logs->save(['Connector created success', 'Trying to connect and receive rows from erp...'], true);

		return $connector;
	}

	/**
	 * Возвращает массив заказов из erp клиента
	 * @param AbstractErpConnector $connector
	 * @param ErpConfig $erp_config
	 * @param array $params
	 * @return array
	 * @throws ErpConfigException
	 * @throws ErpConnectionException
	 */
	public function getOrders(AbstractErpConnector $connector, ErpConfig $erp_config, array $params): ?array
	{
		/** Подключение и получени ответа от ERP */
		$total_orders_count = $connector->getRemoteOrdersCount($erp_config->getOrdersQuery());
		$orders = $connector->setParams($params)->getResponse($erp_config->getOrdersQuery());

		if (!$erp_config->updateOrderCount($total_orders_count)){
			$this->error_logs->save(['Erp order counter save error:', serialize($erp_config->getErrors())], true);
		}

		$this->info_logs->save(['Erp response success', 'Erp rows received', 'Trying to save rows in ErpOrder...'] , true);
		return $orders;
	}

	/**
	 * Возвращает массив id заказов из erp клиента
	 * @param AbstractErpConnector $connector
	 * @param ErpConfig $erp_config
	 * @param $params
	 * @return array
	 */
	private function getOrderIds(AbstractErpConnector $connector, ErpConfig $erp_config, array $params): array
	{
		$erp_order_ids = [];

		/** Подключение и получени ответа от ERP */
		try {
			$total_orders_count = $connector->getRemoteOrdersCount($erp_config->getOrderIdsQuery());
			$order_ids = $connector->setParams($params)->getResponse($erp_config->getOrderIdsQuery());
			foreach ($order_ids as $order_id) {
				$erp_order_ids[] = (int)$order_id['id'];
			}
		} catch (ErpConfigException $e) {
			$this->error_logs->save(['Error trying to get response from erp:', serialize($connector->getErrors())], true);
			return [];
		} catch (ErpConnectionException $e) {
			$this->error_logs->save(['Error trying to get response from erp:', serialize($connector->getErrors())], true);
			return [];
		}

		if (!$erp_config->updateOrderCount($total_orders_count)){
			$this->error_logs->save(['Erp order counter save error:', serialize($erp_config->getErrors())], true);
		}

		$this->info_logs->save(['Erp response success', 'Erp rows received', 'Trying to save rows in ErpOrder...'] , true);
		return $erp_order_ids;
	}

	/**
	 * @param AbstractErpConnector $connector
	 * @param ErpConfig $erp_config
	 * @return int|mixed|null
	 */
	public function getTotalOrdersCount(AbstractErpConnector $connector, ErpConfig $erp_config) {
		/** Подключение и получени ответа от ERP */
		try {
			$total_orders_count = $connector->getRemoteOrdersCount($erp_config->getOrderIdsQuery());
		} catch (ErpConnectionException $e) {
			$this->error_logs->save(['Error trying to get response from erp:', serialize($connector->getErrors())], true);
			return null;
		}

		return $total_orders_count;
	}

	/**
	 * @param int $company_id
	 */
	public function resetImportStatus(int $company_id)
	{
		$erp_import_status = ErpImportStatus::find()->where(['company_id' => $company_id])->one();

		if (!$erp_import_status) {
			$erp_import_status = new ErpImportStatus();
			$erp_import_status->company_id = $company_id;
		}

		$begin_time = time();
		$begin_params = [
			'status' => ErpImportStatus::STATUS_IN_PROCESS,
			'date_end_process' => null,
			'date_last_update' => $begin_time,
			'date_start_process' => $begin_time,
			'total_count' => 0,
			'current_position' => 0,
		];

		$erp_import_status->setAttributes($begin_params);
		$erp_import_status->save();
	}

	/**
	 * Ожидает окончание процесса импорта erp заказов
	 * обновляя статус в таблице erp_import_status
	 * @param ErpImportStatus $erp_import_status
	 * @param int $total_remote_orders_count
	 * @param int $company_id
	 * @throws \yii\mongodb\Exception
	 */
	public function checkErpImportStatus (ErpImportStatus $erp_import_status, int $total_remote_orders_count, int $company_id): void
	{
		$end_import = false;
		$current_local_orders_count = 0;
		$no_progress_ticks = 0;//счетчик тиков цикла подряд когда количество импортированных записей не менялось
		while (!$end_import) {
			$last_local_orders_count = $current_local_orders_count;
			$current_local_orders_count = (int)ErpOrder::find()->where(['company_id' => $company_id])->count();

			// заказы импортированы полностью
			if ($current_local_orders_count >= $total_remote_orders_count) {
				$this->saveImportStatus(ErpImportStatus::STATUS_SUCCESS, $current_local_orders_count, $erp_import_status);
				$end_import = true;
			}

			$no_progress_ticks = $last_local_orders_count === $current_local_orders_count ? $no_progress_ticks + 1 : 0;

			//когда число тиков без прогресса превышает максимальное значение
			//устанавливаем статус ошибки
			if ($no_progress_ticks === $this->getMaxTicsNoProgressCount()) {
				$this->saveImportStatus(ErpImportStatus::STATUS_ERROR, $current_local_orders_count, $erp_import_status);
				$end_import = true;
			}

			if (!$end_import) {
				$this->saveImportStatus(ErpImportStatus::STATUS_IN_PROCESS, $current_local_orders_count, $erp_import_status);
			}

			sleep(ErpImportStatus::TIME_PAUSE_IN_SECOND);
		}
	}

	/**
	 * Возвращает максимальное количество итераций цикла в рамкох которых не было изменений,
	 * после которых импорт завершается с ошибкой
	 * @return int
	 */
	private function getMaxTicsNoProgressCount(): int
	{
		return (int)(ErpImportStatus::MAX_TIME_WAIT_BEFORE_SET_ERROR / ErpImportStatus::TIME_PAUSE_IN_SECOND);
	}

	/**
	 * Сохраняет статус импорта erp в ErpImportStatus
	 * @param string $status
	 * @param int $current_local_orders_count
	 * @param ErpImportStatus $erp_import_status
	 */
	private function saveImportStatus(string $status, int $current_local_orders_count, ErpImportStatus $erp_import_status): void
	{
		$current_time = time();

		$update_params = [
			'date_last_update' => $current_time,
			'current_position' => $current_local_orders_count,
			'status' => $status,
			'date_end_process' => $status === ErpImportStatus::STATUS_SUCCESS ? $current_time : null,
		];
		$erp_import_status->setAttributes($update_params);
		$erp_import_status->save();
	}

	/**
	 * @param int $company_id
	 * @param int $total_remote_orders_count
	 * @return ErpImportStatus
	 * @throws \yii\base\Exception
	 */
	public function getErpImportStatusModel(int $company_id, int $total_remote_orders_count)
	{
		/** @var $erp_import_status ErpImportStatus */
		$erp_import_status = ErpImportStatus::find()->where(['company_id' => $company_id])->one();
		if (!$erp_import_status) {
			$this->error_logs->save(['Ошибка получения из БД строки статуса'], false);
			throw new \yii\base\Exception('Не создана модель проверки статуса импорта ERP');
		}

		$begin_params = [
			'total_count' => $total_remote_orders_count,
			'current_position' => 1,
		];

		$erp_import_status->setAttributes($begin_params);
		$erp_import_status->save();

		return $erp_import_status;
	}
}