<?php

namespace app\commands;

use app\components\rabbitmq\RabbitDispatcherErp;
use app\components\rabbitmq\RabbitDispatcherFileDownloader;
use app\components\rabbitmq\RabbitDispatcherMail;
use yii\console\Controller;

/**
 *
 * Консольный контроллер для получения сообщения из RabbitMQ.
 * Становится рабочим через 20 секунд после запуска.
 *
 * @package app\commands
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class RabbitDispatcherController extends Controller
{
    /**
     *  Экшн для запуска демона разбирающего очередь загрузки файлов
     *  bash: php yii rabbit-dispatcher/file-downloader-daemon-start
     */
    public function actionFileDownloaderDaemonStart()
    {
        RabbitDispatcherFileDownloader::i()->setUpServer();
    }
    /**
     *  Экшн для запуска демона разбирающего очередь из RabbitMQ для загрузки данных из ERP
     *  bash: php yii rabbit-dispatcher/erp-daemon-start
     */
    public function actionErpDaemonStart()
    {
        RabbitDispatcherErp::i()->setUpServer();
    }

    /**
     *  Экшн для запуска демона разбирающего очередь отправки писем из RabbitMQ
     *  bash: php yii rabbit-dispatcher/mail-daemon-start
     */
    public function actionMailDaemonStart()
    {
        RabbitDispatcherMail::i()->setUpServer();
    }

}
