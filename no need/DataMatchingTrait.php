<?php

namespace app\classes\helpers;

use app\classes\api\Response;
use app\models\FieldConfig;
use app\models\TableConfig;
use app\models\RawTablesData;
use app\models\File;
use \app\classes\excel\Excel;

/**
 * В трейт вынесены местоды общие для контроллеров controllers/DataMatchingApiController.php
 * и controllers/ResetDataApiController.php
 * Trait DataMatchingTrait
 *
 * @package app\classes\helpers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
trait DataMatchingTrait
{
    /**
     * Получение данных для заполнения RAW таблицы (1 шаг загрузки данных)
     * @return \app\classes\api\Response
     * @throws \yii\db\Exception
     */
    protected function loadAndPrepareDataFromExcelFile(): Response
    {
        $user = \Yii::$app->user->identity;
        if (!$user) {
            return $this->response->addError('Пользователь не найден');
        }
        $file = File::getLastFileDataByUserId((int)$user->id);
        $this->getExelDataFromFile($file);
        if ($this->response->hasErrors()) {
            return $this->response;
        }
        $excel_data = $this->response->getContent();
        $table_id = (int)$file['table_id'];
        $select_options = $this->getSelectOptions($table_id);

        $column_keys = [];
        $default_select = [];
        foreach ($excel_data[0] as $column_key => $val) {
            $column_keys[] = $column_key;
            $default_select[$column_key] = $this->getDefaultSelectedOptionValue($select_options, $column_key, $table_id);
        }

        return $this->response->setContent([
            'excel_data' => $excel_data,
            'column_keys' => $column_keys,
            'select_options' => $select_options,
            'select' => $default_select,
            'table_id' => $table_id
        ]);
    }

    /**
     * @param array $fields
     * @return int
     * @throws \yii\db\Exception
     */
    protected function saveInRawTable(array $fields): int
    {
        $file = File::getLastFileDataByUserId($this->user->id);
        $insert_count = RawTablesData::insertMultipleRows($fields, $this->user->id, $this->company->id, $file['id']);
        if ($file) {
            File::saveAsMatched($file['id']);
        }
        return $insert_count;
    }

    /**
     * Читает excel файл и формирет массив строк
     * @param File $file
     * @return Response
     */
    protected  function getExelDataFromFile(array $file):Response {
        $excel_data = [];
        if (!$file || !$file['path']) {
            return $this->response->addError('Не удалось распознать файл');
        }
        try {
            $raw_excel_data = Excel::import($file['path'], [
                'setFirstRecordAsKeys' => false,
                'setIndexSheetByName' => true,
            ]);
        } catch (\Exception $e) {
            return $this->response->addError('Не удалось распознать файл. Попробуйте загрузить в другом формате');
        }

        if (!is_array($raw_excel_data)) {
            return $this->response->addError('Не удалось загрузить данные');
        }

        $this->fillExcelData($excel_data, $raw_excel_data);

        return $this->response->setContent($excel_data);
    }

    /**
     * Разбивает массив полученный при чтении Excel файла на строки
     * @param array $excel_data
     * @param array $raw_excel_data
     */
    protected function fillExcelData(array &$excel_data, array &$raw_excel_data)
    {
        $row_number = 0;
        if (Excel::$sheetCount > 1) {
            foreach ($raw_excel_data as $sheet) {
                $row_number = count($excel_data);
                $this->explodeSheetOnRows($sheet, $row_number, $excel_data);
            }
        } else {
            $this->explodeSheetOnRows($raw_excel_data, $row_number, $excel_data);
        }
    }

    /**
     * Разбивает поля листа Excel на строки
     * @param array $sheet
     * @param int $row_number
     * @param array $excel_data
     */
    protected function explodeSheetOnRows(array $sheet, int $row_number, array &$excel_data): void
    {
        foreach ($sheet as $row) {
            foreach ($row as $column) {
                $this->setCellToExcelData($excel_data, $row_number, $column);
            }
           $row_number++;
        }
    }

    /**
     * получает набор значений для сопоставления столбцов таблицы
     * @param int $table_id
     * @return array
     */
    protected function getSelectOptions(int $table_id): array {
        if (!$table_id) {
            return [];
        }

        $fields_models = TableConfig::find()->where(['table_id' => $table_id])->all();
        $field_ids = [];
        foreach ($fields_models as $model) {
            $field_ids[] = $model->field_id;
        }

        $options = FieldConfig::find()->select(['id', 'code', 'title'])
            ->where([
                'company_id' => $this->company->id,
                'is_deleted' => FieldConfig::IS_NOT_DELETED,
                'type' => FieldConfig::TYPE_CONST,
                'id' => $field_ids
            ])
            ->asArray()
            ->all();

        $select_options[] = ['не учитывать', 0];
        foreach ($options as $option) {
            if (!$option['code']) {
                continue;
            }
            $select_options[] = [$option['code'] . ' (' . $option['title'] . ')', $option['id']];
        }

        return $select_options;
    }

    /**
     * Получает значение опции выбранной по умолчанию либо из кэша, либо (если в кэшэ пусто) по следующей логике:
     * Для первого селекта будет выбрана первая опция, для второго - вторая и т.д.
     * @param $select_options
     * @param $column_key
     * @param $table_id
     * @return int
     */
    private function getDefaultSelectedOptionValue($select_options, $column_key, $table_id): int
    {
        $result = \Yii::$app->cache->get($this->getSelectCacheKey($column_key, $table_id));
        if (empty($result)) {
            $result = $select_options[$column_key + 1][1] ?? 0; // +1, т.к. опции начинаются с 0
        }
        return (int)$result;
    }

    /**
     * @param int $key - номер колонки с селектом по порядку
     * @param int $table_id
     * @return string
     */
    private function getSelectCacheKey($key, $table_id): string
    {
        return 'match_select_' . $this->user->id . '_' . $table_id . '_' . $key;
    }

    /**
     * сохраняет значение ячейки в $excel_data, предварительно приведя к
     * типу float с округлением если соотвествует формату числа с точкой или запятой
     * @param $excel_data
     * @param $row_number
     * @param $column
     */
    private function setCellToExcelData(&$excel_data, $row_number, $column): void
    {
        $column = trim($column);
        /* При получении данных в phpExcel:
         * Если в эксель разряды чисел отбиваются пробелом: например 500 000, то пробелы почему-то заменяются на запятую.
         * Запятая, отделяющая дробную заменяется точкой
         */
        if (preg_match('/^[0-9]+,{1}[0-9]+.*$/', $column)) { // определяем, что это число.
            $column = str_replace(',' , '', $column);
            $column = (float)$column;
            $column = round($column, 2);
        }

        if (is_numeric($column)) {
            $column = (float)$column;
            $column = round($column, 2);
        }


        $excel_data[$row_number][] = $column;
    }

}