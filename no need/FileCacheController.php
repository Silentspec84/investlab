<?php

namespace app\commands;

use app\classes\helpers\RowProcessor\ResultTableRowProcessor;
use yii\console\Controller;

/**
 * Контроллер отвечающий за файловый кеш
 *
 * Class FileCacheController
 * @package app\commands
 * @see FileCacheProcessException
 */
class FileCacheController extends Controller
{
    public const CREATING_CACHE_IN_PROGRESS_TTL = 60 * 5; // 5 минут - время жизни метки, что создание файловго кэша в процессе
    public const CACHE_DIRECTORY = 'resources';

    // Определяет режим работы - true, если запущены Unit тесты
    public static $is_test = false;

    public function actionSetCacheRowsOriginal($company_id, $user_id)
    {
        if (!$company_id || !$user_id) {
            \Yii::error('В actionSetCacheRowsOriginal не пришли параметры company_id или user_id');
        }
        $result_table_row_processor = new ResultTableRowProcessor($company_id, $user_id);
        $result_table_row_processor = $result_table_row_processor->setIsConsole(true);
        $delete_cache = self::delete($result_table_row_processor->getCacheFileName());
        if (!$delete_cache) {
            \Yii::error('Не удалось очистить файловый кеш результирующих строк');
        }

        $sql_data = $result_table_row_processor->getSqlData();
        $ordered_rows_result_table = $result_table_row_processor->getResultRows($sql_data);
        if (!$ordered_rows_result_table) {
            \Yii::warning('actionSetCacheRowsOriginal: Нет результирующих строк!');
        }
    }

    /**
     * @param $company_id
     * Проверка существует ли файл с кэшом из консоли: php yii file-cache/is-file-cache-exist
     */
    public function actionIsFileCacheExist($company_id): void
    {
        $filename = self::getFileNameRowsOriginal($company_id);
        echo file_exists($filename) ? 'true '.PHP_EOL : 'false '.PHP_EOL;
    }

    /**
     * @param $company_id
     * Проверка существует ли метка того, что создание файловго кэша в процессе: php yii file-cache/is-creating-cache-in-progress
     */
    public function actionIsCreatingCacheInProgress($company_id): void
    {
        $is_creating_cache_in_progress = \Yii::$app->cache->get(self::getCacheKeyWriteRowsOriginalInProgress($company_id));
        echo $is_creating_cache_in_progress ? 'true '.PHP_EOL : 'false '.PHP_EOL;
    }

    /**
     * @param $company_id
     * @param $user_id
     * Удаление файлового кэша и метки: php yii file-cache/invalidate-cache-rows-original
     */
    public function actionInvalidateCacheRowsOriginal($company_id, $user_id): void
    {
        self::invalidateCacheRowsOriginal($company_id, $user_id);
    }

    public static function getFileNameRowsOriginal($company_id): string
    {
        return \Yii::getAlias('@app') . '/'.FileCacheController::CACHE_DIRECTORY.'/result_table_' . $company_id . '.txt';
    }

    /**
     * Ключ кэша, в котором хранится флаг того, что в данный момент происходит формирование кэша результирующей таблицы
     * @param $company_id
     * @return string
     */
    public static function getCacheKeyWriteRowsOriginalInProgress($company_id):string
    {
        return 'write_rows_original_'.$company_id;
    }

    /**
     * Запускаем скрипт устанавливающий файловый кеш на строки для результирующей таблицы
     * @param int $company_id
     * @param int $user_id
     */
    public static function startScriptSetCacheRowsOriginal(int $company_id, int $user_id): void
    {
        exec('cd /app && /usr/local/bin/php yii file-cache/set-cache-rows-original ' . $company_id . ' ' . $user_id. ' >/dev/null &');
    }

    /**
     * Удаляет файловый кэш и метку того, что создание кэша в процессе.
     * @param $company_id
     * @param $user_id
     */
    public static function invalidateCacheRowsOriginal($company_id, $user_id): void
    {
        $result_table_row_processor = new ResultTableRowProcessor($company_id, $user_id);
        self::delete($result_table_row_processor->getCacheFileName());
        \Yii::$app->cache->delete(self::getCacheKeyWriteRowsOriginalInProgress($company_id));
    }

    /**
     * Кладет данные в файловый кэш
     * @param $filename
     * @param $data
     * @return bool
     */
    public static function set($filename, $data) {
        $data = json_encode($data);
        $filename = self::modifyFileName($filename);
        if (!$data) {
            \Yii::error('Не удалось выполнить json_encode при создании файлого кэша '.$filename);
            return false;
        }
        if (!file_put_contents($filename, $data)) {
            \Yii::error('Не удалось создать файловый кеш для файла '.$filename);
            return false;
        }
        return true;
    }

    /**
     * Достает данные из файлового кэша
     * @param $filename
     * @return array|mixed
     */
    public static function get($filename) {
        $filename = self::modifyFileName($filename);
        if (!file_exists($filename)) {
            return [];
        }
        $data = file_get_contents($filename);
        if (!$data) {
            return [];
        }

        return json_decode($data, true);
    }

    /**
     * Удаляет файл, который хранит кэш
     * @param $filename
     * @return bool
     */
    public static function delete($filename): bool
    {
        $filename = self::modifyFileName($filename);
        if (!file_exists($filename)) {
            return true;
        }

        return unlink($filename);
    }

    /**
     * Возвращает пусть к файлу с измененными именем файла в случае c перфиксом test_, если запущены unit tests.
     * Иначе вернет изначальное имя файла.
     * Например, на вход пришел путь:  $filename = '/app/uploads/my_super_file.txt';
     * Если если запущены unit tests, метод вернет '/app/uploads/test_my_super_file.txt', иначе '/app/uploads/my_super_file.txt'.
     *
     * @param $filename
     * @return string
     */
    private static function modifyFileName(string $filename): string
    {
        if (self::$is_test) {
            $start = strrpos($filename,'/') + 1;
            $replacement = 'test_' . substr($filename, $start);
            $filename = substr_replace($filename, $replacement, $start);
        }
        return $filename;
    }

}