<?php


namespace app\classes\excel;

/**
 * Переопределили класс \moonland\phpexcel\Excel для того,
 * чтобы можно было получить число листов в документе Excel
 * при импорте в массив
 * Класс Excel
 * @package app\classes\excel
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
class Excel extends \moonland\phpexcel\Excel
{
    public static $sheetCount;

    /**
     * Читает Xls файл
     * @param $fileName
     * @return array|mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function readFile($fileName)
    {
        if (!isset($this->format))
            $this->format = \PhpOffice\PhpSpreadsheet\IOFactory::identify($fileName);
        $objectreader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($this->format);
        $objectPhpExcel = $objectreader->load($fileName);

        $sheetCount = $objectPhpExcel->getSheetCount();
        self::$sheetCount = $sheetCount;

        $sheetDatas = [];

        if ($sheetCount > 1) {
            foreach ($objectPhpExcel->getSheetNames() as $sheetIndex => $sheetName) {
                if (isset($this->getOnlySheet) && $this->getOnlySheet != null) {
                    if (!$objectPhpExcel->getSheetByName($this->getOnlySheet)) {
                        return $sheetDatas;
                    }
                    $objectPhpExcel->setActiveSheetIndexByName($this->getOnlySheet);
                    $indexed = $this->getOnlySheet;
                    $sheetDatas[$indexed] = $objectPhpExcel->getActiveSheet()->toArray(null, true, true, true);
                    if ($this->setFirstRecordAsKeys) {
                        $sheetDatas[$indexed] = $this->executeArrayLabel($sheetDatas[$indexed]);
                    }
                    if (!empty($this->getOnlyRecordByIndex)) {
                        $sheetDatas[$indexed] = $this->executeGetOnlyRecords($sheetDatas[$indexed], $this->getOnlyRecordByIndex);
                    }
                    if (!empty($this->leaveRecordByIndex)) {
                        $sheetDatas[$indexed] = $this->executeLeaveRecords($sheetDatas[$indexed], $this->leaveRecordByIndex);
                    }
                    return $sheetDatas[$indexed];
                } else {
                    $objectPhpExcel->setActiveSheetIndexByName($sheetName);
                    $indexed = $this->setIndexSheetByName == true ? $sheetName : $sheetIndex;
                    $sheetDatas[$indexed] = $objectPhpExcel->getActiveSheet()->toArray(null, true, true, true);
                    if ($this->setFirstRecordAsKeys) {
                        $sheetDatas[$indexed] = $this->executeArrayLabel($sheetDatas[$indexed]);
                    }
                    if (!empty($this->getOnlyRecordByIndex) && isset($this->getOnlyRecordByIndex[$indexed]) && is_array($this->getOnlyRecordByIndex[$indexed])) {
                        $sheetDatas = $this->executeGetOnlyRecords($sheetDatas, $this->getOnlyRecordByIndex[$indexed]);
                    }
                    if (!empty($this->leaveRecordByIndex) && isset($this->leaveRecordByIndex[$indexed]) && is_array($this->leaveRecordByIndex[$indexed])) {
                        $sheetDatas[$indexed] = $this->executeLeaveRecords($sheetDatas[$indexed], $this->leaveRecordByIndex[$indexed]);
                    }
                }
            }
        } else {
            $sheetDatas = $objectPhpExcel->getActiveSheet()->toArray(null, true, true, true);
            if ($this->setFirstRecordAsKeys) {
                $sheetDatas = $this->executeArrayLabel($sheetDatas);
            }
            if (!empty($this->getOnlyRecordByIndex)) {
                $sheetDatas = $this->executeGetOnlyRecords($sheetDatas, $this->getOnlyRecordByIndex);
            }
            if (!empty($this->leaveRecordByIndex)) {
                $sheetDatas = $this->executeLeaveRecords($sheetDatas, $this->leaveRecordByIndex);
            }
        }

        return $sheetDatas;
    }
}