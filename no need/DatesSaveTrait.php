<?php

namespace app\classes\helpers;

/**
 * Trait DatesSaveTrait
 * Автоматически сохраняет date_created и date_modified если подмешать его в AR модели, в которой есть соотвествующие поля
 * и не переопределен родительский метод beforeSave
 *
 * @package app\classes\helpers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
trait DatesSaveTrait {

    public function beforeSave($insert) {
        $time = time();
        $this->date_modified = $time;
        if ($insert) {
            $this->date_created = $time;
        }
        return parent::beforeSave($insert);
    }
}

