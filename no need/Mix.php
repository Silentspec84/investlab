<?php

namespace app\classes\helpers;

/**
 * Класс для подключения скомпилированных скриптов и стилей с функцией антикеша
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 *
 */
class Mix
{
    private static $manifest;

    /**
     * Получает путь к файлу
     * @param  string $path
     * @return string
     * @throws \Exception
     */
    public static function mix($path): string
    {
        if (!self::$manifest) {
            if (!file_exists($manifestPath = \Yii::getAlias('@app/web').'/mix-manifest.json')) {
                throw new \Exception('The Mix manifest does not exist.');
            }

            self::$manifest = json_decode(file_get_contents($manifestPath), true);
        }

        if (!array_key_exists($path, self::$manifest)) {
            throw new \Exception(
                "Unable to locate Mix file: {$path}. Please check your " .
                'webpack.mix.js output paths and try again.'
            );
        }

        return self::$manifest[$path];
    }
}