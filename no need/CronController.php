<?php

namespace app\commands;

use app\classes\helpers\ErpHelper;
use app\components\Logger;
use app\models\erp\ErpConfig;
use app\models\erp\ErpOrder;
use app\models\SendOffers;
use app\models\store\Offer;
use yii\console\Controller;

/**
 * Class CronController
 * @package app\commands
 */
class CronController extends Controller
{
    /**
     * Выставляет Offer::ARCHIVED КП, дата (date_till) которых истекла
     */
    public function actionAutoArchiveKp()
    {
        $archived_kp_count = Offer::archiveOld();
        Logger::i(Logger::CAT_KP)
            ->setType(Logger::TYPE_INFO)
            ->setTtl(Logger::TTL_MONTH)
            ->save('archived success '.$archived_kp_count.' kp');
    }

    /**
     * Удаляет КП, которые находятся в архиве и которые не были отправлены
     */
    public function actionAutoDeleteKp()
    {
        $deleted_kp_count = Offer::deleteArchivedInWrongStatus();
        Logger::i(Logger::CAT_KP)
            ->setType(Logger::TYPE_INFO)
            ->setTtl(Logger::TTL_MONTH)
            ->save('deleted success '.$deleted_kp_count.' kp');
    }

    /**
     * Удаляет КП, которые находятся в архиве и которые не были отправлены
     */
    public function actionAutoDeleteTokenSendOffers()
    {
        $send_offers_ids = SendOffers::find()
            ->select('send_offers.id')
            ->innerJoin('offers', 'offers.id = send_offers.offer_id')
            ->where(['offers.is_archived' => Offer::ARCHIVED])
            ->orWhere(['offers.is_deleted' => Offer::DELETED])
            ->column();

        if ($send_offers_ids) {
            $deleted_send_offers_count = SendOffers::deleteAll(['id' => $send_offers_ids]);
            \Yii::info('deleted success '. $deleted_send_offers_count.' send_offers', 'send_offers_delete');
        }
    }

	/**
	 *  Постановка задачи в очередь сообщений RabbitMQ на получение данных из ERP
	 * @param int|null $company_id
	 */
	public function actionQueueGettingDataFromErp(int $company_id = null): void
	{
		$erp_helper = $this->getErpHelper();
		$erp_helper->queueGettingDataFromErp($company_id);
	}

	/**
	 * Удаляет загруженные файлы у всех заказов в статусе "Архивный"
	 * @return int
	 */
	public function actionDeleteFilesForArchiveOrders()
	{
		$erp_helper = $this->getErpHelper();
		$erp_orders = ErpOrder::find()
			->where([
				'status' => ErpOrder::STATUS_ARCHIVE,
				'files_download_status' => ErpOrder::STATUS_FILES_DOWNLOADED,
			])
			->all();

		return $erp_helper->deleteOrdersFiles($erp_orders);
	}

	/**
	 * Удаляет и снова загружает заказы для erp компании
	 * @param int $company_id
	 * @return void
	 */
	public function actionRefreshErpForCompany(int $company_id)
	{
		/** var $erp ErpHelper */
		$erp_helper = $this->getErpHelper();
		$erp_helper->resetImportStatus($company_id);
		$erp_helper->deleteOrderFilesByCompanyId($company_id);
		$erp_helper->deleteOrdersByCompanyId($company_id);
		$total_remote_orders_count = $erp_helper->queueGettingDataFromErp($company_id);
		exec('cd /app && /usr/local/bin/php yii cron/erp-import-status ' . $company_id . ' ' . $total_remote_orders_count . ' >/dev/null &');// отслеживаем статус импорта
	}

    /**
     * Архивирует и удаляет старые логи
     * @param int $ttl
     */
    public function actionRotateLogs(int $ttl) {
        if ($ttl === Logger::TTL_DAY) {
            $dir = 'day';
        } elseif ($ttl === Logger::TTL_WEEK) {
            $dir = 'week';
        } elseif ($ttl === Logger::TTL_MONTH) {
            $dir = 'month';
        } else {
            trigger_error('wrong ttl passed');
        }

        $dir_parts = explode('/', $dir);
        $log_dir = \Yii::getAlias('@logs').'/'.$dir;
        $archive_dir = \Yii::getAlias('@logs/archive/').$dir;
        $archive_file = date('Y-m-d_H-i').'_'.array_pop($dir_parts).'.tar.gz';
        $command_archive = 'cd '.$archive_dir. ' && tar czf '.$archive_file.' '.$log_dir. '&& echo archive success';
        $output = [];
        exec($command_archive, $output);
        if ($this->isArchiveSuccess($output)) {
            $command_delete = 'rm '.$log_dir.'/*';
            exec($command_delete);
        }

        Logger::i(Logger::CAT_ARCHIVE_LOGS)
            ->setTtl(Logger::TTL_MONTH)
            ->setType(Logger::TYPE_INFO)
            ->save($archive_file . ' created');
    }

	/**
	 * Отслеживает процесс сброса erp закзов обновляя данные в таблице erp_refresh_status
	 * @param int $company_id
	 * @param int $total_remote_orders_count
	 * @return void
	 */
	public function actionErpImportStatus(int $company_id, int $total_remote_orders_count)
	{
		$logger = Logger::i(Logger::CAT_RABBIT, true)
			->setType(Logger::TYPE_ERROR)
			->setTtl(Logger::TTL_WEEK)
			->setFlush(true);

		$erp_config = ErpConfig::findOne(['company_id' => $company_id]);

		if (!$erp_config) {
			print 'Не найдена конфигурация erp';
			$logger->save(['Не найдена конфигурация erp'], false);
			return;
		}
		/** var $erp ErpHelper */
		$erp_helper = $this->getErpHelper();

		try {
			$erp_import_status = $erp_helper->getErpImportStatusModel($company_id, $total_remote_orders_count);
			$erp_helper->checkErpImportStatus($erp_import_status, $total_remote_orders_count, $company_id);
		} catch ( \yii\base\Exception $e) {
			$logger->save(['Ошибка БД:', $e->getMessage()], false);
		}
	}

	/**
	 * @param $output
	 * @return bool
	 */
	private function isArchiveSuccess($output): bool
    {
        return isset($output[0]) && $output[0] === 'archive success';
    }

	/**
	 * @return ErpHelper
	 */
	private function getErpHelper()
	{
		return new ErpHelper();
	}
}
