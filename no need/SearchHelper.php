<?php
/**
 * Хелпер по работе с компонентом "Поиск"
 * @see components/views/search-component.php
 */

namespace app\classes\helpers;


class SearchHelper
{
    /**
     * Возвращает строки, подпопадающие под поисковый запрос $query
     *
     * @param string $query
     * @param array $rows_result_table
     * @return array
     */
    public static function getRowsResultTableBySearchQuery(string $query, array $rows_result_table)
    {
        $rows_result_table_by_search_query = [];
        foreach ($rows_result_table as $index_row => $row) {
            if (self::isMatchingRowBySearchQuery($row, $query)) {
                $rows_result_table_by_search_query[] = $row;
            }
        }

        return $rows_result_table_by_search_query;
    }

    /**
     * Строка подходит под поисковый запрос?
     *
     * @param array $row
     * @param string $query
     * @return bool
     */
    private static function isMatchingRowBySearchQuery(array $row, string $query): bool
    {
        foreach ($row as $field_id => $field_val) {
            if (is_string($field_val) && mb_stristr($field_val, $query)) {
                return true;
            }
        }

        return false;
    }
}