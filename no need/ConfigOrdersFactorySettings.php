<?php

namespace app\classes\helpers;

use app\models\FieldConfig;

/**
 * Class ConfigOrdersFactorySettings
 * Определяет сортировку переменных для заводстких настроек.
 *
 * @package app\classes\helpers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
class ConfigOrdersFactorySettings
{
    const ORDER_IN_CLIENT_SEARCH = 'OrderInClientSearch';
    const ORDER_IN_SEARCH = 'OrderInSearch';
    const ORDER_IN_CART = 'OrderInCart';
    const ORDER_IN_KP = 'OrderInKp';
    const ORDER_IN_CLIENT_KP = 'OrderInClientKp';
    const ORDER_IN_CLIENT_BID = 'OrderInClientBid';
    const ORDER_IN_MANAGER_BID = 'OrderInManagerBid';
    const ORDER_IN_RESULT_TABLE = 'OrderInResultTable';

    /**
     * По типу сортировки и коду определяет значение сортировки
     *
     * @param $name
     * @param $code
     * @return int|null
     * @throws \Exception
     */
    public static function getOrder($name, $code): ?int
    {
        $getter = 'get' . $name;
        if (method_exists(self::class, $getter)) {

            $order = self::$getter();
            $key = array_search($code, $order, true);
            if ($key !== false) {
                return (int)$key + 1; // сдвиг, т.к. в массиве нумерация с нуля
            }
            return null;
        }
        throw new \Exception('Getting unknown property: ' . $name);
    }

    /**
     * @return array
     */
    private static function getOrderInClientSearch(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::WEIGHT,
            FieldConfig::PLACE_TO,
            FieldConfig::SUBTOTAL_FRAHT_OUT,
            FieldConfig::SUBTOTAL_RW_OUT,
            FieldConfig::SECURITY,
            FieldConfig::SUBTOTAL_AUTO_OUT,
            FieldConfig::RESULT_OUT,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInSearch(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::LINE,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::CODE_AGENCY_NAME,
            FieldConfig::FRAHT,
            FieldConfig::DROP,
            FieldConfig::AGENCY,
            FieldConfig::SUBTOTAL_FRAHT_IN,
            FieldConfig::RAILWAY_TARIFF,
            FieldConfig::SECURITY,
            FieldConfig::VTT,
            FieldConfig::SPECIAL_TARIFF,
            FieldConfig::DANGER,
            FieldConfig::CODE_AGENCY_IN_PORT,
            FieldConfig::SUBTOTAL_RW_IN,
            FieldConfig::DISCREDITING,
            FieldConfig::AUTO,
            FieldConfig::SUBTOTAL_AUTO_IN,
            FieldConfig::RESULT_SEARCH,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInCart(): array
    {
        return [
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::LINE,
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::PAYER_FRAHT,
            FieldConfig::OWNERSHIP_CONTAINER,
            FieldConfig::OWNER_CONTAINER,
            FieldConfig::TERMS_OF_USE,
            FieldConfig::SUBTOTAL_FRAHT_IN,
            FieldConfig::SUBTOTAL_FRAHT_OUT,
            FieldConfig::RATE_FRAHT,
            FieldConfig::SUBTOTAL_RW_IN,
            FieldConfig::SUBTOTAL_RW_OUT,
            FieldConfig::RATE_RW,
            FieldConfig::SUBTOTAL_AUTO_IN,
            FieldConfig::SUBTOTAL_AUTO_OUT,
            FieldConfig::RATE_AUTO,
            FieldConfig::RESULT_OUT,
            FieldConfig::RESULT_RATE,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInKp(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::WEIGHT,
            FieldConfig::PLACE_TO,
            FieldConfig::SUBTOTAL_FRAHT_OUT,
            FieldConfig::DANGER,
            FieldConfig::SUBTOTAL_RW_OUT,
            FieldConfig::SECURITY,
            FieldConfig::SUBTOTAL_AUTO_OUT,
            FieldConfig::HOME_BILL_CODE,
            FieldConfig::RESULT_OUT,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInClientKp(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::WEIGHT,
            FieldConfig::PLACE_TO,
            FieldConfig::SUBTOTAL_FRAHT_OUT,
            FieldConfig::DANGER,
            FieldConfig::SUBTOTAL_RW_OUT,
            FieldConfig::SECURITY,
            FieldConfig::SUBTOTAL_AUTO_OUT,
            FieldConfig::HOME_BILL_CODE,
            FieldConfig::RESULT_OUT,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInClientBid(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::WEIGHT,
            FieldConfig::PLACE_TO,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInManagerBid(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::WEIGHT,
            FieldConfig::PLACE_TO,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

    /**
     * @return array
     */
    private static function getOrderInResultTable(): array
    {
        return [
            FieldConfig::CODE_GEO_POINT_LOADING_PORT,
            FieldConfig::CODE_GEO_POINT_DISCHARGE_PORT,
            FieldConfig::CODE_GEO_POINT_RAIL_STATION,
            FieldConfig::CODE_GEO_POINT_DELIVERY,
            FieldConfig::TYPE_CONTAINER,
            FieldConfig::LINE,
            FieldConfig::PAYER_FRAHT,
            FieldConfig::OWNERSHIP_CONTAINER,
            FieldConfig::OWNER_CONTAINER,
            FieldConfig::CODE_AGENCY_NAME,
            FieldConfig::TERMS_OF_USE,
            FieldConfig::WEIGHT,
            FieldConfig::FRAHT,
            FieldConfig::DROP,
            FieldConfig::AGENCY,
            FieldConfig::SUBTOTAL_FRAHT_IN,
            FieldConfig::SPECIAL_TARIFF,
            FieldConfig::RAILWAY_TARIFF,
            FieldConfig::SECURITY,
            FieldConfig::VTT,
            FieldConfig::CODE_AGENCY_IN_PORT,
            FieldConfig::SUBTOTAL_RW_IN,
            FieldConfig::DANGER,
            FieldConfig::DISCREDITING,
            FieldConfig::AUTO,
            FieldConfig::SUBTOTAL_AUTO_IN,
            FieldConfig::RESULT_SEARCH,
            FieldConfig::IN_WAY,
            FieldConfig::DATE_ACTUAL_CODE,
        ];
    }

}
