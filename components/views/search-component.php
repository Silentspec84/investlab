<!-- Поисковый компонент, используется, например, в итоговой таблице, гео-точках и настройках поисковой выдачи(фильтрах) -->
<div class="search-component mb-4">
    <label class="search-component__label" for="search">Поиск</label>
    <div class="row">
        <div class="col-10">
            <input id="search" type="text" :placeholder="search_placeholder"
                   data-toggle="tooltip"
                   data-placement="right"
                   data-original-title=""
                   title=""
                   v-bind:data-original-title="getError('search')"
                   v-bind:class="getError('search') ? 'search-component__input form-control form-control-sm is-invalid' : 'search-component__input form-control form-control-sm'"
                   v-model="query"
                   v-on:keyup.enter="onSearch">
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-outline-primary btn-sm btn-block" v-on:click="onSearch">Искать</button>
        </div>
    </div>
</div>