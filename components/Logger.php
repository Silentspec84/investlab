<?php

namespace app\components;

/**
 * Class Logger
 * Глобальный логер проекта.
 *
 * @package app\components
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
class Logger {

    public const TYPE_ERROR = 1;
    public const TYPE_WARNING = 2;
    public const TYPE_INFO = 3;

    public const TTL_MONTH = 1;
    public const TTL_WEEK = 2;
    public const TTL_DAY= 3;

    /**
     * Чтобы сохранять лог с перфиксом по категории,
     * добавьте сюда константу.
     */
    public const CAT_RABBIT = 'rabbit';
    public const CAT_KP = 'kp';
    public const CAT_MANAGER_SEARCH = 'manager_search';
    public const CAT_RESULT_TABLE = 'result_table';
    public const CAT_CLIENT_SEARCH = 'client_search';
    public const CAT_ARCHIVE_LOGS = 'archive_logs';
    public const CAT_VESSEL_API = 'vessel_api_log';
    public const CAT_CSV = 'csv';

    /**
     * @var [] Хранит объекты Logger в различных конфигурациях
     */
    private static $instances;

    private $category;
    private $type = self::TYPE_ERROR;
    private $ttl = self::TTL_DAY;
    private $flush = false;

    private function __construct($category)
    {
        $this->category = $category;
    }

    /**
     * @param string $category
     * @return Logger
     */
    public function setCategory(string $category): Logger
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @param int $type
     * @return Logger
     */
    public function setType(int $type): Logger
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param int $ttl
     * @return Logger
     */
    public function setTtl(int $ttl): Logger
    {
        $this->ttl = $ttl;
        return $this;
    }

    /**
     * @param bool $flush
     * @return Logger
     */
    public function setFlush(bool $flush): Logger
    {
        $this->flush = $flush;
        return $this;
    }

    public static function i($category, $return_new = false): Logger
    {
        if ($return_new) {
            return new Logger($category);
        }
        if (!in_array($category, self::getCategories(), true)) {
            trigger_error('wrong category');
        }
        if ($return_new) {
            return new Logger($category);
        }
        if (!isset(self::$instances[$category])) {
            self::$instances[$category] = new Logger($category);
        }

        return self::$instances[$category];
    }

    private static function getCategories(): array
    {
        return [
            self::CAT_RABBIT,
            self::CAT_ARCHIVE_LOGS,
            self::CAT_KP,
            self::CAT_RESULT_TABLE,
            self::CAT_MANAGER_SEARCH,
            self::CAT_CLIENT_SEARCH,
            self::CAT_VESSEL_API,
			self::CAT_CSV
        ];
    }

    /**
     * Сохраняет сообщение в лог в категорию self::CATEGORY
     * @param array|string $messages - массив или строка с сообщением, которое будет записано в лог
     * @param bool $is_print_needed - выводит ли сообщение лога на экран?
     * @return void
     */
    public function save($messages, $is_print_needed = false): void
    {
        list($message, $method, $yii_category) = $this->getYiiLoggerParams($messages);
        \Yii::$method($message, $yii_category);
        if ($this->flush) {
            \Yii::getLogger()->flush(true);
        }
        if ($is_print_needed) {
            echo $message;
        }
    }

    public function getYiiLoggerParams($messages) {
        return [
            $this->formatMessage($messages),
            $this->getMethod(),
            $this->getYiiCategory(),
        ];
    }

    private function getMethod(): ?string
    {
        if ($this->type === self::TYPE_ERROR) {
           return 'error';
        }
        if ($this->type === self::TYPE_WARNING) {
            return 'warning';
        }
        if ($this->type === self::TYPE_INFO) {
            return 'info';
        }

        trigger_error('wrong log type');
    }

    private function getYiiCategory(): string
    {
        $yii_category = '';
        if ($this->ttl === self::TTL_DAY) {
            $yii_category = 'day';
        } elseif ($this->ttl === self::TTL_WEEK) {
            $yii_category = 'week';
        } elseif ($this->ttl === self::TTL_MONTH) {
            $yii_category = 'month';
        } else {
            trigger_error('wrong log ttl');
        }

        if ($this->type === self::TYPE_ERROR || $this->type === self::TYPE_WARNING) {
            $yii_category .= '_error';
        } elseif ($this->type === self::TYPE_INFO) {
            $yii_category .= '_success';
        } else {
            trigger_error('wrong log type');
        }

       return $yii_category;
    }

    private function getCategoryPrefix(): string
    {
        return '['.strtoupper($this->category).']';
    }

    /**
     * Форматирует сообщение, перед сохранением в файл или выводом на экран (например в консоли).
     * Пример формата:
     * [info][month_success][RABBIT]. Задание не было поставлено в очередь ...
     * @param array|string $messages
     * @return string
     */
    private function formatMessage($messages): string
    {
        $prefix = $this->getCategoryPrefix();
        if (!is_array($messages)) {
            $messages = (array)$messages;
        }

        return "\n    {$prefix} ".implode("\n    {$prefix} ", $messages);
    }
}