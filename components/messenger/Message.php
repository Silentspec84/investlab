<?php
namespace app\components\messenger;

/**
 * Class Message
 * @package app\components
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * }
 */
class Message
{
    /** @var  string */
    public $template;
    /** @var  []|string */
    public $body;
    /** @var  string */
    public $from;
    /** @var  str */
    public $to;
    /** @var  string */
    public $subaccount_key;
}
