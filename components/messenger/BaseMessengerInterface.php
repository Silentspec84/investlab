<?php
namespace app\components\messenger;

use app\models\User;

/**
 * Interface BaseMessengerInterface
 * @package app\components
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
interface BaseMessengerInterface
{

    /**
     * @param string $template
     * @param User $user
     * @param array $data
     * @return $this
     */
    public function createMessage($template, $user, $data);

    public function getMailer();

    /**
     * @param $options []
     * @return bool
     */
    public function send();
}