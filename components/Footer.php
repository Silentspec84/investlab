<?php

namespace app\components;

use yii\base\ErrorException;
use yii\base\Widget;

/**
 * Виджет отвечает за вывод кастомного футера
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */
class Footer extends Widget
{
    /**
     * @var array $config_company
     */
    private $config_company;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('footer', []);
    }
}