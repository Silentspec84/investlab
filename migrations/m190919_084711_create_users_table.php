<?php

use yii\db\Migration;

/**
 * m190919_084711_create_users_table
 */
class m190919_084711_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->unique(),
            'login_name' => $this->string(255)->unique(),
            'password' => $this->string(255),
            'unsafe_password' => $this->string(255),
            'status' => $this->integer()->notNull(),
            'role' => $this->integer()->notNull(),
            'hash' => $this->string(255),
            'recovery_key' => $this->string(255),
            'avatar' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('email_index', 'users', 'email');
        $this->createIndex('login_name_index', 'users', 'login_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}