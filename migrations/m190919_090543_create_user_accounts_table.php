<?php

use yii\db\Migration;

/**
 * m190919_090543_create_user_accounts_table
 */
class m190919_090543_create_user_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('user_accounts', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'account' => $this->integer()->notNull(),
            'acc_name' => $this->string(255),
            'acc_descr' => $this->text(),
            'acc_type' => $this->integer(),
            'terminal_bild' => $this->integer(),
            'terminal_company_name' => $this->string(255),
            'account_trade_mode' => $this->integer(),
            'account_leverage' => $this->integer(),
            'account_limit_orders' => $this->integer(),
            'account_margin_so_mode' => $this->integer(),
            'account_trade_allowed' => $this->integer(),
            'account_trade_expert' => $this->integer(),
            'account_balance' => $this->float(2),
            'account_credit' => $this->float(2),
            'account_profit' => $this->float(2),
            'account_equity' => $this->float(2),
            'account_margin' => $this->float(2),
            'account_margin_free' => $this->float(2),
            'account_margin_level' => $this->float(2),
            'account_margin_so_call' => $this->float(2),
            'account_margin_so_so' => $this->float(2),
            'client_name' => $this->string(255),
            'account_company' => $this->string(255),
            'account_server' => $this->string(255),
            'account_currency' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
            'date_first_connected' => $this->integer(),
            'date_last_connected' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('user_id_index', 'user_accounts', 'user_id');
        $this->addForeignKey('user_id_fk', 'user_accounts', 'user_id', 'users', 'id', 'CASCADE');
        $this->createIndex('account_index', 'user_accounts', 'account');
        $this->createIndex('acc_name_index', 'user_accounts', 'acc_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_accounts');
    }
}