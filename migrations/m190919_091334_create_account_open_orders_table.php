<?php

use yii\db\Migration;

/**
 * m190919_091334_create_account_open_orders_table
 */
class m190919_091334_create_account_open_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('account_open_orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'ticket' => $this->integer(),
            'order_open_time' => $this->integer(),
            'order_type' => $this->integer(),
            'order_lots' => $this->float(2),
            'order_symbol' => $this->string(255),
            'order_open_price' => $this->float(2),
            'order_sl' => $this->float(2),
            'order_tp' => $this->float(2),
            'commission' => $this->float(2),
            'order_swap' => $this->float(2),
            'order_profit' => $this->float(2),
            'order_comment' => $this->string(255),
            'order_user_comment' => $this->string(255),
            'order_screenshot' => $this->string(255),
            'order_magic' => $this->integer(),
            'order_hash' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('user_id_index', 'account_open_orders', 'user_id');
        $this->addForeignKey('user_id_ao_table_fk', 'account_open_orders', 'user_id', 'users', 'id', 'CASCADE');
        $this->createIndex('account_id_index', 'account_open_orders', 'account_id');
        $this->addForeignKey('account_id_ao_table_fk', 'account_open_orders', 'account_id', 'user_accounts', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('account_open_orders');
    }
}