<?php

use app\models\User;
use yii\db\Migration;

/**
 * m190919_084756_add_admin_to_users_table
 */
class m190919_084756_add_admin_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $admin = new User();
        $admin->email = 'silentspec84@mail.ru';
        $admin->login_name = 'silentspec';
        $admin->password = 'e8290fb9413a0735312e15ab6d65e932';
        $admin->status = User::STATUS_ACTIVE;
        $admin->role = User::ROLE_ADMIN;
        $admin->hash = 'eb9b159f650f8a4b8ba7528438eb2435';
        $admin->avatar = '../web/images/avatar/ava.jpg';

        $admin->save();
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}